package com.livennew.latestdietqueen.network_support;


public class Api {
 //public static final String LIVE_URL = "http://dietqueen.in/liven/";
  // public static final String LIVE_URL = "https://dietqueen.infinitylabs.in:8082/liven/";
 //public static final String LIVE_URL = "http://dev.dietqueen.in/liven/";
 //public static final String LIVE_URL = "http://test.dietqueen.in/liven_mcc2/";
// public static final String LIVE_URL = "http://dev.dietqueen.in/liven/";

//public static final String LIVE_URL = "http://test.dietqueen.in/liven_mcc3/";

public static final String LIVE_URL = "https://dietqueen.in/liven/public/";


 //public static final String LIVE_URL = "http://192.168.1.113:8000/";
//  public static final String LIVE_URL = "http://test.dietqueen.in/liven_mcc3/";
//  public static final String LIVE_URL = "http://kxdevelopers.com/dietqueen_server/public/";


    public static final String MAIN_URL = LIVE_URL;
    public static final String GoogleAppId = "658233118169-59jnjeobo5131jc95qbmokrorkf76ccf.apps.googleusercontent.com";

    public static final String ClientSecret = "fPmts8qSjKwZQoVNvp2RUrxu";
    //"306228625577-3sncelmehoig5rt784a06k7539cdb6n4.apps.googleusercontent.com";
    //"897877296189-cbkbqrlbhirb95f1uu4d1cmkd7hhid94.apps.googleusercontent.com";
    // 725115945722-gfjhcumm14mbe8mc2ttjom25c5c0krhc.apps.googleusercontent.com
    public static final String LOGIN_SOCIAL = "mobileApi/login/social";
    public static final String USER_UPDATE_MOBILE = "mobileApi/user/mobile";
    public static final String USER_SOCIALMEDIA_OTP = "mobileApi/user/otp";
    public static final String LANGUAGE_LIST = "commonApi/language";
    public static final String EXCLUSIVE = "mobileApi/mobile/blog-exclusive?";
    public static final String DIETPLAN = "mobileApi/mobile/diet-plan?";
    public static final String DIETDETAILS = "mobileApi/mobile/diet-plan/feature-benefit/";
    public static final String SUBSCRIBE = "mobileApi/user/subscribe";
    public static final String FAQ = "mobileApi/mobile/faq";
    public static final String BLOGLIST = "mobileApi/mobile/blog-exclusive?";
    public static final String BLOGDETAILS = "mobileApi/mobile/blog-exclusive";
    public static final String UPLOADIMAGE = "mobileApi/user/profileImage";
    public static final String GETSTATE = "commonApi/state";
    public static final String GETCITY = "commonApi/city";
    public static final String INSERTBODYWEIGHT = "mobileApi/mobile/user/body-weight";
    public static final String GETPROFILE = "mobileApi/user/profile/detail";
    public static final String INSERTUSERPROFILE = "mobileApi/user/profile";
    public static final String PARTYPLANNERLIST = "mobileApi/mobile/user/party-planner/category";
    public static final String INSERTPARTYPLANNER = "mobileApi/user/party-planner";
    public static final String INSERTPHYSICALDATA = "mobileApi/user/physical-activity";
    public static final String SHOWBODYWEIGHT = "mobileApi/mobile/user/body-weight";
    public static final String SHOWREGIONALLIST = "mobileApi/mobile/user/regional-preference";
    public static final String GETPROFESSION = "mobileApi/mobile/user/profession";
    public static final String GETPHYSICALACTIVITY = "mobileApi/mobile/physical-activity";
    public static final String GETACHIVEMENT = "mobileApi/mobile/user/achivement";
    public static final String GETINSTRUCTION = "mobileApi/mobile/instruction?";
    public static final String GETFITNESSADVICE = "mobileApi/mobile/fitness-advice";
    public static final String NOTIFICATIONLIST = "mobileApi/mobile/notification?";
    public static final String UPDATELANGUAGE = "mobileApi/user/langugae";
    public static final String UPDATEIMAGE = "mobileApi/user/profileImage/base64";
    public static final String GETBANNER = "mobileApi/mobile/video-banner";
}
