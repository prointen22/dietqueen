package com.livennew.latestdietqueen.network_support;

import android.content.Context;
import android.util.Log;

import com.chuckerteam.chucker.api.ChuckerInterceptor;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.retrofit.NetworkConnectionInterceptor;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import dagger.hilt.android.qualifiers.ApplicationContext;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;
    static String lang = "";

    public static Retrofit getClient(Context context) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Api.LIVE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getOkHttpClient(context))
                .build();

        return retrofit;
    }

    static HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    static OkHttpClient getOkHttpClient(Context context) {
        return new OkHttpClient.Builder()
                .addInterceptor(getHeaderInterceptor(context))
                .addInterceptor(getHttpLoggingInterceptor())
                .addInterceptor(new NetworkConnectionInterceptor(context))
                .addInterceptor(new ChuckerInterceptor(context))
                .build();
    }

    static Interceptor getHeaderInterceptor(Context context) {
        SessionManager sessionManager = new SessionManager(context);
        String token = " ";

        if (sessionManager.getUser() != null) {
            if (sessionManager.getUser().getToken() != null)
                token = sessionManager.getUser().getToken();
        }

        String finalToken = token;
        return chain -> {

            if (sessionManager.getUser() != null) {
                if (sessionManager.getUser().getUserDetail() != null) {
                    lang = String.valueOf(sessionManager.getUser().getUserDetail().getLanguage().getId());
                } else {
                    lang = "1";
                }

                Log.d("jjjjjjj", "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk   lang" + lang);

            }
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Accept", "application/json")
                    .header("Authorization", finalToken)
                    .header("userId", sessionManager.getUser() == null ? " " : String.valueOf(sessionManager.getUser().getId()))
                    .header("languageId", sessionManager.getUser() == null ? " " : lang)
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        };
    }
}