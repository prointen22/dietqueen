package com.livennew.latestdietqueen.faq.model;

public class FAQDataModel {
    private int FAQQuestionid;
    private String FAQQuestion;
    private String FAQAnswer;
    //private boolean expanded;

    /*public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }*/

    public int getFAQQuestionid() {
        return FAQQuestionid;
    }

    public void setFAQQuestionid(int FAQQuestionid) {
        this.FAQQuestionid = FAQQuestionid;
    }

    public String getFAQQuestion() {
        return FAQQuestion;
    }

    public void setFAQQuestion(String FAQQuestion) {
        this.FAQQuestion = FAQQuestion;
    }

    public String getFAQAnswer() {
        return FAQAnswer;
    }

    public void setFAQAnswer(String FAQAnswer) {
        this.FAQAnswer = FAQAnswer;
    }
}
