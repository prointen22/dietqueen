package com.livennew.latestdietqueen.faq;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.faq.adapter.FAQAdapter;
import com.livennew.latestdietqueen.faq.model.FAQDataModel;
import com.livennew.latestdietqueen.faq.model.FAQModel;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class FAQActivity extends FabActivity {
    private Toolbar toolbarFAQ;
    private ArrayList<FAQModel> faqParentModelList;
    //    private ArrayList<FAQDataModel> faqChildModelList;
    private RecyclerView rvFAQ;
    private ProgressBar progressBar;
    private FAQAdapter faqAdapter;
    //private CustomExpandableListAdapter faqAdapter;
    private static final String TAG = "Erros";
    private LinearLayoutManager recyclerLayoutManager;
    @Inject
    SessionManager sessionManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f_a_q);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorAccent));
        user = sessionManager.getUser();
        init();

        /**
         *Inflate tab_layout and setup Views.
         */
        setSupportActionBar(toolbarFAQ);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.faqs);
        }
        toolbarFAQ.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);

        if (SupportClass.checkConnection(this)) {
            getFAQ();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }

    }

    private void init() {
        // sample code snippet to set the text content on the ExpandableTextView
        toolbarFAQ = findViewById(R.id.toolbar_faq);
        rvFAQ = findViewById(R.id.expandableListView);
        progressBar = findViewById(R.id.progress_bar);
        faqParentModelList = new ArrayList<>();
        //  faqChildModelList = new ArrayList<>();
        //    expandableListDetail = new HashMap<>();
        //  listDataHeader = new ArrayList<>();
    }

    private void getFAQ() {
        progressBar.setVisibility(View.VISIBLE);
        rvFAQ.setVisibility(View.GONE);
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.GET, Api.MAIN_URL + Api.FAQ, null, response -> {
            Log.d(TAG, "onResponse Response: " + response);
            progressBar.setVisibility(View.GONE);
            rvFAQ.setVisibility(View.VISIBLE);
            try {
                JSONObject jObj = new JSONObject(response.toString());
                boolean error = jObj.getBoolean("error");
                if (!error) {
                    JSONArray jsonArray = jObj.getJSONArray("data");
                    faqParentModelList.clear();
                    //  faqChildModelList.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObjectData = jsonArray.getJSONObject(i);
                        FAQModel faqModel = new FAQModel();
                        //String dietPlanOffer, dietPlanPrice;
                        faqModel.setFAQTitleid(jsonObjectData.getInt("id"));
                        faqModel.setFAQTitle(jsonObjectData.getString("name"));
                        JSONArray jsonArray1 = jsonObjectData.getJSONArray("faq");
                        ArrayList<FAQDataModel> faqChildModelList = new ArrayList<FAQDataModel>();
                        for (int j = 0; j < jsonArray1.length(); j++) {
                            JSONObject jsonObjectData1 = jsonArray1.getJSONObject(j);
                            FAQDataModel faqDataModel = new FAQDataModel();
                            faqDataModel.setFAQQuestionid(jsonObjectData1.getInt("id"));
                            faqDataModel.setFAQQuestion(jsonObjectData1.getString("question"));
                            faqDataModel.setFAQAnswer(jsonObjectData1.getString("answer"));
                            faqChildModelList.add(faqDataModel);
                        }//Set layout manager to position the items
                        faqModel.setFaqDataModelsList(faqChildModelList);
                        faqParentModelList.add(faqModel);
                    }
                    faqAdapter = new FAQAdapter(faqParentModelList, FAQActivity.this);
                    recyclerLayoutManager = new LinearLayoutManager(FAQActivity.this);
                    rvFAQ.setLayoutManager(recyclerLayoutManager);
                    rvFAQ.setAdapter(faqAdapter);
                    for (int i = 0; i < faqAdapter.getItemCount(); i++) {
                        faqAdapter.callOnItemClick(i);
                    }

                } else {
                    Toast.makeText(FAQActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                headers.put("Authorization", user.getToken());
              //  headers.put("languageId",user.getUserDetail().getLanguage().getId());
                headers.put("languageId",String.valueOf(user.getUserDetail().getLanguage().getId()));

                headers.put("userId", String.valueOf(user.getId()));
                return headers;
            }
        };
        strReq.setShouldCache(false);
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(FAQActivity.this);
        strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(strReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
