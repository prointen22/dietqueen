package com.livennew.latestdietqueen.faq;

import android.os.Build;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.faq.model.FAQModel;
import com.livennew.latestdietqueen.libmoduleExpandable.ViewHolder.ParentViewHolder;

public class FAQParentViewHolder extends ParentViewHolder {
    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;
    private TextView tvName;

    public FAQParentViewHolder(View itemView) {
        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.tv_faq_title);
    }

    public void bind(FAQModel biodataHead) {
        tvName.setText(biodataHead.getFAQTitle());
        int position = getAdapterPosition();
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            RotateAnimation rotateAnimation;
            if (expanded) { // rotate clockwise
                rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            } else { // rotate counterclockwise
                rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            }

            rotateAnimation.setDuration(200);
            rotateAnimation.setFillAfter(true);
            //ivCollapse.startAnimation(rotateAnimation);
        }
    }
}
