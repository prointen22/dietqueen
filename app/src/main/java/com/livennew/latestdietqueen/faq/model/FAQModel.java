package com.livennew.latestdietqueen.faq.model;

import com.livennew.latestdietqueen.libmoduleExpandable.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

public class FAQModel implements ParentListItem {
    private int FAQTitleid;
    private String FAQTitle;
    private ArrayList<FAQDataModel> faqDataModelsList;

    /*private String title;
    private String genre;
    private int year;
    // State of the item
    private boolean expanded;*/

    public int getFAQTitleid() {
        return FAQTitleid;
    }

    public void setFAQTitleid(int FAQTitleid) {
        this.FAQTitleid = FAQTitleid;
    }

    public String getFAQTitle() {
        return FAQTitle;
    }

    public void setFAQTitle(String FAQTitle) {
        this.FAQTitle = FAQTitle;
    }

    public ArrayList<FAQDataModel> getFaqDataModelsList() {
        return faqDataModelsList;
    }

    public void setFaqDataModelsList(ArrayList<FAQDataModel> faqDataModelsList) {
        this.faqDataModelsList = faqDataModelsList;
    }

    @Override
    public List<?> getChildItemList() {
        return faqDataModelsList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
    /*public void addFaqDataModelsList(FAQDataModel p) {
        faqDataModelsList.add(p);
    }*/
}
