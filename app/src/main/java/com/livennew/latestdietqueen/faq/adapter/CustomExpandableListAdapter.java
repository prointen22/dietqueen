package com.livennew.latestdietqueen.faq.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.faq.model.FAQDataModel;
import com.livennew.latestdietqueen.faq.model.FAQModel;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<FAQModel> expandableListTitle;
    private HashMap<FAQModel, List<FAQDataModel>> expandableListDetail;
    ExpandableListView expandList;

    public CustomExpandableListAdapter(Context context, List<FAQModel> expandableListTitle, HashMap<FAQModel, List<FAQDataModel>> expandableListDetail, ExpandableListView mView) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
        this.expandList = mView;

    }

    @Override
    public int getGroupCount() {
        return Objects.requireNonNull(this.expandableListTitle.size());
    }

    @Override
    public int getChildrenCount(int listPosition) {
        // return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
        int childCount = 0;
        if (listPosition != 0) {
            childCount = this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
        }
        return childCount;
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {
          FAQDataModel expandedListText = (FAQDataModel) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.faq_content, null);
        }
//       FAQDataModel expandedListText = expandableListDetail.get(listPosition).get(expandedListPosition);
        TextView expandedListTextViewQuestion = (TextView) convertView.findViewById(R.id.item_question);
        expandedListTextViewQuestion.setText(expandedListText.getFAQQuestion());
        TextView expandedListTextViewAnswer = (TextView) convertView.findViewById(R.id.item_answer);
        expandedListTextViewAnswer.setText(expandedListText.getFAQAnswer());

        return convertView;
    }


    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        FAQModel listTitle = (FAQModel) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.faq_header, null);
        }
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.tv_faq_title);
        //listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle.getFAQTitle());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}

