package com.livennew.latestdietqueen.faq.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.livennew.latestdietqueen.Adapter.ExpandableRecyclerAdapter;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.faq.FAQChildViewHolder;
import com.livennew.latestdietqueen.faq.FAQParentViewHolder;
import com.livennew.latestdietqueen.faq.model.FAQDataModel;
import com.livennew.latestdietqueen.faq.model.FAQModel;
import com.livennew.latestdietqueen.libmoduleExpandable.Model.ParentListItem;

import java.util.ArrayList;

public class FAQAdapter extends ExpandableRecyclerAdapter<FAQParentViewHolder, FAQChildViewHolder> {

    private LayoutInflater mLayoutInflater;
    private Context mContext;

    public FAQAdapter(@NonNull ArrayList<? extends ParentListItem> parentItemList, Context mContext) {
        super(parentItemList);
        this.mLayoutInflater = LayoutInflater.from(mContext);
        this.mContext = mContext;
    }

    @Override
    public FAQParentViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View view = mLayoutInflater.inflate(R.layout.faq_header, parentViewGroup, false);
        return new FAQParentViewHolder(view);
    }

    @Override
    public FAQChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View view = mLayoutInflater.inflate(R.layout.faq_content, childViewGroup, false);
        return new FAQChildViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(FAQParentViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        FAQModel head = (FAQModel) parentListItem;
        parentViewHolder.bind(head);

    }

    @Override
    public void onBindChildViewHolder(FAQChildViewHolder childViewHolder, int position, Object childListItem) {
        FAQDataModel child = (FAQDataModel) childListItem;
        childViewHolder.bind(child);
    }

    public void callOnItemClick(int position) {
        expandParent(position);
    }
}
