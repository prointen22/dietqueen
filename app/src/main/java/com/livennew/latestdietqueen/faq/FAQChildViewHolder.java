package com.livennew.latestdietqueen.faq;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.faq.model.FAQDataModel;
import com.livennew.latestdietqueen.libmoduleExpandable.ViewHolder.ChildViewHolder;

public class FAQChildViewHolder extends ChildViewHolder {
    private TextView tvQuestion;
    private TextView tvAnswer;

    public FAQChildViewHolder(View itemView) {
        super(itemView);
        tvQuestion = (TextView) itemView.findViewById(R.id.item_question);
        tvAnswer = (TextView) itemView.findViewById(R.id.item_answer);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvAnswer.getVisibility() == View.VISIBLE) {
                    tvAnswer.setVisibility(View.GONE);
                } else {
                    tvAnswer.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void bind(FAQDataModel biodataChildModel) {
        Log.d("TAG", "appears " + getAdapterPosition());
        tvQuestion.setText(biodataChildModel.getFAQQuestion());
        tvAnswer.setText(biodataChildModel.getFAQAnswer());
    }

}
