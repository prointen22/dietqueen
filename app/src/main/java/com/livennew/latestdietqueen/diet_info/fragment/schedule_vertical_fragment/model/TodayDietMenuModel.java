package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import java.util.ArrayList;

public class TodayDietMenuModel {
    int todayDietId;
    String todayDietTime;
    boolean isEditable, isDone;
    ArrayList<TodayDietMenu> todayDietMenuArrayList;

    public TodayDietMenuModel(int todayDietId, String todayDietTime, boolean isEditable, boolean isDone) {
        this.todayDietId = todayDietId;
        this.todayDietTime = todayDietTime;
        this.isEditable = isEditable;
        this.isDone = isDone;
        this.todayDietMenuArrayList = new ArrayList<TodayDietMenu>();
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public int getTodayDietId() {
        return todayDietId;
    }

    public void setTodayDietId(int todayDietId) {
        this.todayDietId = todayDietId;
    }

    public String getTodayDietTime() {
        return todayDietTime;
    }

    public void setTodayDietTime(String todayDietTime) {
        this.todayDietTime = todayDietTime;
    }

    public ArrayList<TodayDietMenu> getTodayDietMenuArrayList() {
        return todayDietMenuArrayList;
    }

    public void addTodayDietMenuArrayList(TodayDietMenu p) {
        todayDietMenuArrayList.add(p);
    }

}
