package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

public class Plan extends MyResponse {
    @SerializedName("data")
    @Expose
    private Diet data;

    @SerializedName("others")
    @Expose
    private Others others;


    public Diet getData() {
        return data;
    }

    public void setData(Diet data) {
        this.data = data;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }

    @Override
    public String toString() {
        return "Plan{" +
                "data=" + data +
                ", others=" + others +
                '}';
    }
}
