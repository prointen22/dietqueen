package com.livennew.latestdietqueen.diet_info;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.ActivityMainDietContainerBinding;
import com.livennew.latestdietqueen.diet_info.fragment.ScheduleFragmentTabSelection;
import com.livennew.latestdietqueen.diet_info.fragment.nutrition.IngredientFragment;
import com.livennew.latestdietqueen.exclusive_youtube.YouTubePlayerActivity;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Id;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.VideoResp;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeBlogAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeVideosAdapter;
import com.livennew.latestdietqueen.fragments.home.model.Blog;
import com.livennew.latestdietqueen.fragments.home.model.BlogModel;
import com.livennew.latestdietqueen.fragments.home.model.BlogListResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.newsblog.BlogDetailActivity;
import com.livennew.latestdietqueen.newsblog.NewsBlogListActivity;
import com.livennew.latestdietqueen.newsvideo.VideoList;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.RecyclerItemClickListener;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;

import static com.facebook.FacebookSdk.getApplicationContext;

@AndroidEntryPoint
public class MainDietContainerFragment extends Fragment {
    private static final String TAG = "MainDietContainerFragme";
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;

    @Inject
    SessionManager sessionManager;
    User user;
    private ActivityMainDietContainerBinding binding;
    private TextView tvTermsCon;
    private Button btnOk;
    private FragmentActivity activity;
    private VideoResp videos;
    private HomeVideosAdapter homeVideosAdapter;
    private HomeBlogAdapter homeBlogAdapter;
    private ArrayList<BlogModel> alHomeBlog;
    @Inject
    APIInterface apiInterface;


    public static Fragment getInstance() {
        return new MainDietContainerFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = sessionManager.getUser();
        activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = ActivityMainDietContainerBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        homeBlogAdapter = new HomeBlogAdapter(activity);
        binding.inc.rvHomeBlog.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        binding.inc.rvHomeBlog.setAdapter(homeBlogAdapter);
        homeBlogAdapter.setListener(new HomeBlogAdapter.Listener() {
            @Override
            public void onItemClick(Blog blog) {
                Intent intent = new Intent(activity, BlogDetailActivity.class);
                intent.putExtra("blog_id", blog.getId());
                intent.putExtra("type", "blog");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        binding.btnSchedule.setTextColor(getResources().getColor(R.color.white));
        binding.btnNutrition.setTextColor(getResources().getColor(R.color.gray_300));
        openScheduleScreen();
        if (SupportClass.checkConnection(activity)) {
            VideoList();
            blogList();
            binding.inc.rvHomeVideo.setVisibility(View.VISIBLE);
        } else {
            SupportClass.noInternetConnectionToast(activity);
            binding.inc.rvHomeVideo.setVisibility(View.GONE);
        }
        binding.inc.tvShowBlogList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NewsBlogListActivity.class);
                startActivity(intent);
                activity.overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        });

        binding.inc.tvShowVideoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), VideoList.class);
                startActivity(intent);
                activity.overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        });
        binding.btnEnquiry.setOnClickListener(View -> {
            startActivity(new Intent(activity, InstructionActivity.class));
        });
        binding.rvSchedule.setOnRippleCompleteListener(rippleView -> {
            binding.btnSchedule.setTextColor(getResources().getColor(R.color.white));
            binding.btnNutrition.setTextColor(getResources().getColor(R.color.gray_300));
            if (SupportClass.checkConnection(getApplicationContext())) {
//                if (!isFirstTime) {
                openScheduleScreen();
//                }
            } else {
                Toast.makeText(activity, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }
        });
        if (SupportClass.checkConnection(getApplicationContext())) {
            GetBanner();
        } else {
            Toast.makeText(activity, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
        binding.rvNutrition.setOnRippleCompleteListener(rippleView -> {
            binding.btnNutrition.setTextColor(getResources().getColor(R.color.white));
            binding.btnSchedule.setTextColor(getResources().getColor(R.color.gray_300));
            if (SupportClass.checkConnection(getApplicationContext())) {
                openNutritionScreen();
            } else {
                Toast.makeText(activity, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void VideoList() {
        binding.inc.llDashboardVideo.setVisibility(View.GONE);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId=" + getString(R.string.channel_id) + "&maxResults=5&key=" + getString(R.string.youtube_key), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                binding.inc.llDashboardVideo.setVisibility(View.VISIBLE);
                videos = new Gson().fromJson(response.toString(), VideoResp.class);
                homeVideosAdapter = new HomeVideosAdapter(activity, videos.getItems());
                binding.inc.rvHomeVideo.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                binding.inc.rvHomeVideo.setAdapter(homeVideosAdapter);
                binding.inc.rvHomeVideo.addOnItemTouchListener(new RecyclerItemClickListener(activity, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        getVideoDetails(videos.getItems().get(position).getId(), position);
                    }
                }));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                binding.inc.llDashboardVideo.setVisibility(View.GONE);
                Log.d("Youtube Error", error.toString());
            }
        });
        // AppController.getInstance().addToRequestQueue(jsonObjectRequest, tagGetVideos);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void getVideoDetails(Id id, int position) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "https://www.googleapis.com/youtube/v3/videos?id=" + id.getVideoId() + "&key=" + getString(R.string.youtube_key) + "&part=snippet,contentDetails,statistics,status", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String desc = response.getJSONArray("items").getJSONObject(0).getJSONObject("snippet").getString("description");
                    videos.getItems().get(position).getSnippet().setDescription(desc);
                    callVideoPlayer(position);
                } catch (JSONException e) {
                    e.printStackTrace();
                    callVideoPlayer(position);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        //  AppController.getInstance().addToRequestQueue(jsonObjectRequest, tagGetVideoDetails);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void callVideoPlayer(int position) {
        Intent intent = new Intent(activity, YouTubePlayerActivity.class);
        intent.putExtra("video", videos.getItems().get(position));
        startActivity(intent);
    }

    private void blogList() {
        binding.inc.llDashboardBlog.setVisibility(View.GONE);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", "blog");
        params.put("perPage", 5);
        apiInterface.getBlogs(params).enqueue(new Callback<BlogListResp>() {
            @Override
            public void onResponse(@NotNull Call<BlogListResp> call, @NotNull retrofit2.Response<BlogListResp> response) {
                BlogListResp blogResp = response.body();
                if (!blogResp.getError()) {
                    homeBlogAdapter.setData(blogResp.getData().getBlogExclusive(), blogResp.getOthers());
                    if (blogResp.getData().getBlogExclusive().size() > 0)
                        binding.inc.llDashboardBlog.setVisibility(View.VISIBLE);
                } else {
                    //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                    Toast.makeText(activity, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<BlogListResp> call, @NotNull Throwable t) {
                binding.inc.llDashboardBlog.setVisibility(View.GONE);
            }
        });

    }

    private void GetBanner() {
        try {
            JSONObject params = new JSONObject();
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.GETBANNER, params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject jsonObject = jObj.getJSONObject("data");
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
                        Glide
                                .with(this)
                                .load(jsonObjectOther.getString("banner_image_path") + jsonObject.getString("banner"))
                                .centerCrop()
                                .placeholder(R.drawable.image_background_dark)
                                .into(binding.ivBanner);
//                        Picasso.get().load(jsonObjectOther.getString("banner_image_path") + jsonObject.getString("banner")).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).error(R.drawable.ic_award).into(binding.ivBanner);
                    } else {
                        //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                        JSONArray jsonArray = jObj.getJSONArray("message");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Toast.makeText(activity, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openScheduleScreen() {
        ScheduleFragmentTabSelection frgment = new ScheduleFragmentTabSelection();
        mFragmentManager = activity.getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        mFragmentTransaction.replace(R.id.containerViewDiet, frgment).commitAllowingStateLoss();
    }

    private void openNutritionScreen() {
        mFragmentManager = activity.getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        mFragmentTransaction.replace(R.id.containerViewDiet, IngredientFragment.getInstance()).commitAllowingStateLoss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            activity.finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
