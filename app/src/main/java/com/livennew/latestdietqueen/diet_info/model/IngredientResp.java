package com.livennew.latestdietqueen.diet_info.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

public class IngredientResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private IngredientData data;

    public IngredientData getData() {
        return data;
    }

    public void setData(IngredientData data) {
        this.data = data;
    }
}
