package com.livennew.latestdietqueen.diet_info.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.ReplaceItemAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.MealsDataItem;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.ReplaceItemModel;
import com.livennew.latestdietqueen.utils.DateUtilNew;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ScheduleItemMenuAdapter extends RecyclerView.Adapter<ScheduleItemMenuAdapter.ViewHolder> {

    private List<MealsDataItem> alTodayDietMenu;
    private int mainItemPosition, mMealTypeItemPosition;
    private Others others;
    private Listener listener;
    private Context mContext;

    public ScheduleItemMenuAdapter(Context mContext, List<MealsDataItem> alTodayDietMenu, int mainItemPosition, int mealTypeItemPosition, Others others, Listener dietItemMenuAdapterListener) {
        this.mContext = mContext;
        this.alTodayDietMenu = alTodayDietMenu;
        this.mainItemPosition = mainItemPosition;
        this.mMealTypeItemPosition = mealTypeItemPosition;
        this.others = others;
        listener = dietItemMenuAdapterListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_row_child, parent, false);
        return new ScheduleItemMenuAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        MealsDataItem todayDietMenu = alTodayDietMenu.get(position);
        Glide.with(mContext).load(others.getDietPlanImagePath() + todayDietMenu.getFood().getImage()).into(holder.mIvDietMenu);
        holder.mTvDietMenuNm.setText(todayDietMenu.getFood().getFoodMasterName());
        holder.mTvDietMenuQty.setText("Qty: "+getDietQty(Util.calculateQty(todayDietMenu, others), todayDietMenu.getFood().getUnit().getName()));
            holder.mIvChoice.setOnClickListener(View -> {
                if (listener != null)
                    listener.onReplaceClick(mainItemPosition, mMealTypeItemPosition, position, todayDietMenu, others);
            });

        holder.mCvMain.setOnClickListener(View -> {
            if (listener != null)
                if(others.getDate().equals(DateUtilNew.getFullDateFromDateObject(new Date()))) {
                    listener.onReplaceClick(mainItemPosition, mMealTypeItemPosition, position, todayDietMenu, others);
                }
        });
        Log.i("TAG",""+others.getDate());

        Log.i("TAG",""+DateUtilNew.getFullDateFromDateObject(new Date()));
        if(others.getDate().equals(DateUtilNew.getFullDateFromDateObject(new Date()))) {
            holder.mIvChoice.setVisibility(View.VISIBLE);
        } else {
            holder.mIvChoice.setVisibility(View.INVISIBLE);
        }
    }

    private String getDietQty(long calculateQty, String name) {
        if (calculateQty == 0)
            return "NA";
        return calculateQty + " " + name;
    }


    @Override
    public int getItemCount() {
        if (alTodayDietMenu != null && !alTodayDietMenu.isEmpty()) {
            return alTodayDietMenu.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mIvDietMenu, mIvChoice;
        TextView mTvDietMenuNm, mTvDietMenuQty;
        CardView mCvMain;

        public ViewHolder(View view) {
            super(view);
            mIvDietMenu = view.findViewById(R.id.iv_diet_menu);
            mTvDietMenuNm = view.findViewById(R.id.tv_diet_item);
            mTvDietMenuQty = view.findViewById(R.id.tv_diet_qty);
            mCvMain = view.findViewById(R.id.cv_main);
            mIvChoice = view.findViewById(R.id.iv_choice);
        }
    }

    public interface Listener {
        void onReplaceClick(int dietItemPosition, int mealTypeItemPosition, int mealItemPosition, MealsDataItem todayDietMenu, Others others);
    }
}