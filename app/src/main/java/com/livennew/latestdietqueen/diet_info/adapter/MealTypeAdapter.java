package com.livennew.latestdietqueen.diet_info.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Diet;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFoodMealByTag;

import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class MealTypeAdapter extends RecyclerView.Adapter<MealTypeAdapter.ViewHolder> {
    private List<DietEPlanFoodMealByTag> mMealTypeList;
    private final Context mContext;
    OnItemClickListener mClickListener;

    public MealTypeAdapter(Context mContext, List<DietEPlanFoodMealByTag> mealTypeList, OnItemClickListener onItemClickListener) {
        this.mContext = mContext;
        this.mMealTypeList = mealTypeList;
        this.mClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.meal_type_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        DietEPlanFoodMealByTag mealTypeObj = mMealTypeList.get(position);

        holder.mRbMealType.setText(mealTypeObj.getName());

        if (mealTypeObj.getIsUsersPreferredMeal() == 1) {
            holder.mRbMealType.setChecked(true);
            holder.mLLMealType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_orange_background));
            holder.mRbMealType.setTextColor(mContext.getColor(R.color.orange_theme));

            mClickListener.onItemClick(mealTypeObj, position, false);
        } else {
            holder.mRbMealType.setChecked(false);
            holder.mLLMealType.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_default_background));
            holder.mRbMealType.setTextColor(mContext.getColor(R.color.color_dark_gray));
        }

        holder.mRbMealType.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mClickListener.onItemClick(mealTypeObj, position,true);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mMealTypeList != null && !mMealTypeList.isEmpty()) {
            return mMealTypeList.size();
        } else {
            return 0;
        }
    }

    public List<DietEPlanFoodMealByTag> getList() {
        return mMealTypeList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout mLLMealType;
        public RadioButton mRbMealType;

        public ViewHolder(View view) {
            super(view);
            mLLMealType = view.findViewById(R.id.ll_meal_type);
            mRbMealType = view.findViewById(R.id.rb_meal_type);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DietEPlanFoodMealByTag mealTypeObj, int position, boolean callAPI);
    }
}
