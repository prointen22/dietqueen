package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.databinding.ReplaceMennuLayoutBinding;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReplaceItemAdapter extends RecyclerView.Adapter<ReplaceItemAdapter.ViewHolder> {

    private List<Food> alReplaceItemModel;
    private Foods.FoodsOthers others;
    private Context mContext;

    private int lastSelectedPosition = -1;
    private final OnItemClickListener mClickListener;

    public ReplaceItemAdapter(Context mContext, List<Food> alReplaceItemModel, OnItemClickListener listener) {
        this.mContext = mContext;
        this.alReplaceItemModel = alReplaceItemModel;
        this.mClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReplaceMennuLayoutBinding view = ReplaceMennuLayoutBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Food replaceItemModel = alReplaceItemModel.get(position);
        Glide.with(mContext).load(others.getDietPlanImagePath() + replaceItemModel.getImage()).into(holder.ivReplaceMenu);
        holder.tvReplaceMenuNm.setText(replaceItemModel.getFoodMasterName());
        if (lastSelectedPosition == position) {
//            holder.llReplaceMenuContent.setBackground(mContext.getResources().
//            getDrawable(R.drawable.button_background));
            holder.ivReplaceMenuCheck.setImageResource(R.drawable.ic_replace_round_selector);
//            replaceItem = replaceItemModel.getTvReplaceItem();
        } else {
//            holder.llReplaceMenuContent.setBackgroundResource(0);
            holder.ivReplaceMenuCheck.setImageResource(R.drawable.ic_replace_round);
//            holder.ivReplaceMenuCheck.setVisibility(View.GONE);
        }
        // rest of the code here

        holder.llReplaceMenuContent.setOnClickListener(view -> {
            /*if (lastSelectedPosition == position) {
                lastSelectedPosition = -1;
                notifyDataSetChanged();
                return;
            }
            lastSelectedPosition = position;
            notifyDataSetChanged();*/
            holder.ivReplaceMenuCheck.setImageResource(R.drawable.ic_replace_round_selector);
            mClickListener.onItemClick(replaceItemModel);
        });
    }

    @Override
    public int getItemCount() {
        if (alReplaceItemModel != null && !alReplaceItemModel.isEmpty()) {
            return alReplaceItemModel.size();
        } else {
            return 0;
        }
    }

    public void addAll(List<Food> data, Foods.FoodsOthers others) {
        this.alReplaceItemModel = data;
        this.others = others;
        notifyDataSetChanged();
    }

    public Food getLastSelectedFood() {
        if (lastSelectedPosition != -1) {
            return alReplaceItemModel.get(lastSelectedPosition);
        }
        return null;
    }

    public void updateItem(Food lastSelectedFood, int position) {
        alReplaceItemModel.set(position, lastSelectedFood);
        lastSelectedPosition = -1;
        notifyItemChanged(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final public TextView tvReplaceMenuNm;
        final public LinearLayoutCompat llReplaceMenuContent;
        final public ImageView ivReplaceMenuCheck;
        final public CircleImageView ivReplaceMenu;

        public ViewHolder(ReplaceMennuLayoutBinding view) {
            super(view.getRoot());
            llReplaceMenuContent = view.llReplaceItem;
            ivReplaceMenu = view.ivReplaceMenu;
            tvReplaceMenuNm = view.tvReplaceMenuNm;
            ivReplaceMenuCheck = view.ivCheckReplaceMenu;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Food food);
    }
}