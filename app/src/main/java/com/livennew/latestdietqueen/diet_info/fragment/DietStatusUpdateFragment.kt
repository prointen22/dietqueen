package com.livennew.latestdietqueen.diet_info.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.livennew.latestdietqueen.R
import com.livennew.latestdietqueen.SessionManager
import com.livennew.latestdietqueen.databinding.DietStatusUpdateBinding
import com.livennew.latestdietqueen.diet_info.ScheduleFragmentNew
import com.livennew.latestdietqueen.diet_info.adapter.ScheduleItemMenuAdapterNew
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.*
import com.livennew.latestdietqueen.diet_info.model.Day
import com.livennew.latestdietqueen.model.MyResponse
import com.livennew.latestdietqueen.model.User
import com.livennew.latestdietqueen.network_support.APIClient
import com.livennew.latestdietqueen.retrofit.APIInterface
import com.livennew.latestdietqueen.utils.DateUtil
import com.livennew.latestdietqueen.utils.DateUtilNew
import com.livennew.latestdietqueen.utils.DateUtilNew.Companion.compareDietTimeWithCurrentTime
import com.livennew.latestdietqueen.utils.DateUtilNew.Companion.getDateFromDateObject
import com.livennew.latestdietqueen.utils.DateUtilNew.Companion.getDayFromDateObject
import com.livennew.latestdietqueen.utils.DateUtilNew.Companion.getFullDateFromDateObject
import com.livennew.latestdietqueen.utils.DateUtilNew.Companion.getMonthFromDateObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class DietStatusUpdateFragment : Fragment() {
    private var mBinding: DietStatusUpdateBinding? = null
    private var mDietList: ArrayList<Diet> = ArrayList()
    private var mPosition = 0
    private lateinit var mOthers: Others
    private lateinit var mDayObj: Day
    private lateinit var mDietObj: Diet
    private lateinit var mScheduleItemMenuAdapter: ScheduleItemMenuAdapterNew
    var apiInterface: APIInterface? = null
    var sessionManager: SessionManager? = null
    var user: User? = null
    var mPrefsobj: SharedPreferences? = null

    private var mOperation = ""
    private var mReason = ""
    val currentDate = ""

    companion object {
        @JvmStatic
        fun newInstance(): Fragment {
            return DietStatusUpdateFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(requireActivity().applicationContext)
        apiInterface = APIClient.getClient(requireActivity().applicationContext).create(APIInterface::class.java)
        user = sessionManager!!.user
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        mBinding = DietStatusUpdateBinding.inflate(inflater, container, false)
        mPrefsobj = requireActivity().getPreferences(Context.MODE_PRIVATE)

        init()

        return mBinding!!.root
    }

    private fun init() {
        try {
            mBinding?.progressBar?.visibility = View.VISIBLE
            if (arguments != null) {
                mOperation = requireArguments().getString("operation")!!
                mReason = requireArguments().getString("reason")!!
                mPosition = requireArguments().getInt("position")
                mDietList = requireArguments().getParcelableArrayList("dietList")!!
                mOthers = requireArguments().getParcelable("others")!!
                mDayObj = requireArguments().getParcelable("day")!!
                mDietObj = mDietList[mPosition]

                if (mOperation.equals("No", ignoreCase = true)) {
                    setDietDone()
                    return
                }





            Log.v("TAG", "Diet list size: ${mDietList.size}")

            val mDay = "${mDayObj.date} ${mDayObj.month} (${mDayObj.day})"
            mBinding?.tvDay?.text = mDay

            val mDietTaken = "Taken ${mDietObj.planSubcategory}?"
            mBinding?.tvTakenDiet?.text = mDietTaken

            mBinding?.tvDiet?.text = mDietObj.planSubcategory
            mBinding?.tvTime?.text = DateUtil.convertTime24To12(mDietObj.time)

            mBinding?.btnNo?.setOnClickListener { loadReasonFragment() }

            mBinding?.btnYes?.setOnClickListener { setDietDone() }

            var mealsDataItem:List<MealsDataItem>?=null
            for (i in mDietObj.dietEPlanFoodByMealTag.indices){
                if (mDietObj.dietEPlanFoodByMealTag[i].isUsersPreferredMeal == 1){
                    mealsDataItem = mDietObj.dietEPlanFoodByMealTag[i].mealsData
                }
            }

            mBinding?.rvDiet?.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
            mScheduleItemMenuAdapter = ScheduleItemMenuAdapterNew(requireActivity(), mealsDataItem, mOthers)
            mBinding?.rvDiet?.adapter = mScheduleItemMenuAdapter

            mBinding?.progressBar?.visibility = View.GONE
            }else{
//               currentDate =
//                val c = Calendar.getInstance().time
//                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
//                val formattedDate: String = simpleDateFormat.format(c)
                mDayObj = Day()
                val currentDate = Date()
//                val dayObjPrev = Day()
                mDayObj.date = getDateFromDateObject(currentDate)
                mDayObj.day = getDayFromDateObject(currentDate)
                mDayObj.month = getMonthFromDateObject(currentDate)
                mDayObj.fullDate = getFullDateFromDateObject(currentDate)
//                mDayObj.fullDate = formattedDate
                callDietPlanAPI()
            }
        } catch (ex: Exception) {
            Log.v("TAG", "Exception: ${ex.message}")
        }
    }

    private fun setDietDone() {
        mBinding?.progressBar?.visibility = View.VISIBLE

        var foodDetails:String = ""
//        for (i in mDietObj.dietEPlanFoodByMealTag.indices){
////            foodDetails = foodDetails+","+mDietObj.dietEPlanFoodByMealTag.get(i).mealsData.get(i).
//        }

        var mealsDataItem:List<MealsDataItem>?=null
        for (i in mDietObj.dietEPlanFoodByMealTag.indices){
            if (mDietObj.dietEPlanFoodByMealTag[i].isUsersPreferredMeal == 1){
                mealsDataItem = mDietObj.dietEPlanFoodByMealTag[i].mealsData
            }
        }
        for (i in mealsDataItem!!.indices){
            if (foodDetails.isEmpty()){
                foodDetails = mealsDataItem.get(i).food.foodMasterName
            }else {
                foodDetails = foodDetails + "," + mealsDataItem.get(i).food.foodMasterName
            }
        }
        var doneParam = DoneParam(mDietObj.id, mOthers.date, "Y",mDietObj.time,foodDetails)
        if (mOperation.equals("No", ignoreCase = true)) {
            doneParam = DoneParam(mDietObj.id, mOthers.date, "N", mReason,mDietObj.time,foodDetails)
        }
        apiInterface!!.setDietDone(doneParam).enqueue(object : Callback<MyResponse?> {
            override fun onResponse(call: Call<MyResponse?>, response: Response<MyResponse?>) {
                mBinding?.progressBar?.visibility = View.GONE
                if (response.body() != null) {
                    if (response.body()!!.error) {
                        for (i in response.body()!!.message.indices) Toast.makeText(activity, response.body()!!.message[i], Toast.LENGTH_SHORT).show()
                    } else {
//                        Toast.makeText(activity, "Success", Toast.LENGTH_SHORT).show()
//                        loadFragment(ScheduleFragmentNew.newInstance())
//                        Log.i("TAG","position = "+mPosition)
                        mDietList[mPosition].isDone = true
                        mOperation = ""
                        mReason =""
                        var allDataDone = true
                        if (DateUtilNew.isPastDate(mDayObj.fullDate)) {
                            for (i in mDietList.indices){
                                if (!mDietList.get(i).isDone){
                                    mPosition = i
                                    mDietObj = mDietList[mPosition]
                                    setData()
                                    allDataDone = false
                                    break
                                }
                            }

                        }else{
                            for (i in mDietList.indices){
//                                val diffInHours = compareDietTimeWithCurrentTime(mDietObj.time)
                                val diffInHours = compareDietTimeWithCurrentTime(mDietList.get(i).time)
                                if (diffInHours < 0) {
//                                    catalog_outdated = 1
                                    if (!mDietList.get(i).isDone){
                                        mPosition = i
                                        mDietObj = mDietList[mPosition]
                                        setData()
                                        allDataDone = false
                                        break
                                    }
                                }
                            }

                        }
                        if (allDataDone){
                            loadFragment(ScheduleFragmentNew.newInstance())
                        }
//                        if (mPosition ==0){
//                            loadFragment(ScheduleFragmentNew.newInstance())
//                        }else{
//                            mDietList[mPosition].isDone = true
//                            mPosition = mPosition - 1
//                            mDietObj = mDietList[mPosition]
//                            setData()
//                        }
                    }
                }
            }

            override fun onFailure(call: Call<MyResponse?>, t: Throwable) {
                mBinding?.progressBar?.visibility = View.GONE
                Toast.makeText(activity, "Error in setting diet as Done: ${t.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun setData() {
        Log.v("TAG", "Diet list size: ${mDietList.size}")

        val mDay = "${mDayObj.date} ${mDayObj.month} (${mDayObj.day})"
        mBinding?.tvDay?.text = mDay

        val mDietTaken = "Taken ${mDietObj.planSubcategory}?"
        mBinding?.tvTakenDiet?.text = mDietTaken

        mBinding?.tvDiet?.text = mDietObj.planSubcategory
        mBinding?.tvTime?.text = DateUtil.convertTime24To12(mDietObj.time)

        mBinding?.btnNo?.setOnClickListener { loadReasonFragment() }

        mBinding?.btnYes?.setOnClickListener { setDietDone() }

        var mealsDataItem:List<MealsDataItem>?=null
        for (i in mDietObj.dietEPlanFoodByMealTag.indices){
            if (mDietObj.dietEPlanFoodByMealTag[i].isUsersPreferredMeal == 1){
                mealsDataItem = mDietObj.dietEPlanFoodByMealTag[i].mealsData
            }
        }

        mBinding?.rvDiet?.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        mScheduleItemMenuAdapter = ScheduleItemMenuAdapterNew(requireActivity(), mealsDataItem, mOthers)
        mBinding?.rvDiet?.adapter = mScheduleItemMenuAdapter

        mBinding?.progressBar?.visibility = View.GONE
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction: FragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun loadReasonFragment() {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        val fragment = ReasonFragment.newInstance()
        val bundle = Bundle()
        bundle.putInt("position", mPosition)
        bundle.putParcelableArrayList("dietList", mDietList)
        bundle.putParcelable("others", mOthers)
        bundle.putParcelable("day", mDayObj)
        fragment.arguments = bundle
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun callDietPlanAPI() {
        Log.v(
            "TAG", """Diet plan id:${sessionManager!!.dietEPlanId} Date: ${mDayObj.fullDate}"""
        )
        val planParam = PlanParam(sessionManager!!.dietEPlanId, mDayObj.fullDate)
        mBinding?.progressBar?.visibility = View.VISIBLE
        apiInterface!!.getDietPlansNew(planParam).enqueue(object : Callback<Plans> {
            override fun onResponse(call: Call<Plans>, response: Response<Plans>) {
                Log.v("TAG", "Diet plan response size: " + response.body()!!.data.size)
                response.body()!!.others.date = planParam.date
                mBinding?.progressBar?.visibility = View.GONE
                mDietList = response.body()!!.data as ArrayList<Diet>
                mOthers = response.body()!!.others
                var allDataDone = true
                for (i in mDietList.indices){
//                                val diffInHours = compareDietTimeWithCurrentTime(mDietObj.time)
                    val diffInHours = compareDietTimeWithCurrentTime(mDietList.get(i).time)
                    if (diffInHours < 0) {
//                                    catalog_outdated = 1
                        if (!mDietList.get(i).isDone){
                            mPosition = i
                            mDietObj = mDietList[mPosition]
                            setData()
                            allDataDone = false
                            break
                        }
                    }
                }
                if (allDataDone){
                    Toast.makeText(activity,"Today's all meals status is updated successfully.",Toast.LENGTH_SHORT).show()
                    activity!!.finish()
                }
//                mDietObj = mDietList[0]
//                setData()
//                setDietAdapter()
            }

            override fun onFailure(call: Call<Plans>, t: Throwable) {
                Log.e("TAG", "onFailure: " + t.message)
                mBinding?.progressBar?.visibility = View.GONE
                Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
