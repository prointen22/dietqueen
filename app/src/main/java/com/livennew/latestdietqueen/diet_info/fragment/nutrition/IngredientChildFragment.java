package com.livennew.latestdietqueen.diet_info.fragment.nutrition;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.databinding.IngredientChildFragmentBinding;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.MyTodayScheduleRecyclerViewAdapter;
import com.livennew.latestdietqueen.diet_info.model.IngredientResp;
import com.livennew.latestdietqueen.retrofit.APIInterface;


import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class IngredientChildFragment extends Fragment {

    private NutritionChildViewModel mViewModel;
    private IngredientChildFragmentBinding binding;
    private View view;
    @Inject
    APIInterface apiInterface;
    private FragmentActivity activity;
    private IngredientAdapter adapter;

    public static IngredientChildFragment newInstance() {
        return new IngredientChildFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = IngredientChildFragmentBinding.inflate(inflater, container, false);
        view = binding.getRoot();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(NutritionChildViewModel.class);
        // TODO: Use the ViewModel
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        adapter = new IngredientAdapter(activity, new ArrayList<>());
        binding.recyclerView.setAdapter(adapter);
        getIngredient();
    }

    private void getIngredient() {
        binding.progress.setVisibility(View.VISIBLE);
        apiInterface.getIngredient().enqueue(new Callback<IngredientResp>() {
            @Override
            public void onResponse(@NotNull Call<IngredientResp> call, @NotNull Response<IngredientResp> response) {
                binding.progress.setVisibility(View.GONE);
                if (!response.body().getError()) {
                    adapter.updateData(response.body().getData().getIngredient());
                } else {
                    Toast.makeText(activity, response.body().getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(@NotNull Call<IngredientResp> call, @NotNull Throwable t) {
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                binding.progress.setVisibility(View.GONE);
            }
        });
    }

}