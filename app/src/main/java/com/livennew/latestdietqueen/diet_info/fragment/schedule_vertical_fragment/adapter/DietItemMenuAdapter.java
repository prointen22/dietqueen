package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.ReplaceItemModel;

import java.util.ArrayList;
import java.util.List;

public class DietItemMenuAdapter extends RecyclerView.Adapter<DietItemMenuAdapter.ViewHolder> {

    private List<DietEPlanFood> alTodayDietMenu;
    private int mainItemPosition;
    private Others others;
    private Listener listener;
    private boolean isToday;
    private Context mContext;

    private ArrayList<ReplaceItemModel> alReplaceItemModel;
    private ReplaceItemAdapter replaceItemAdapter;
    private String replaceItem = "";

    public DietItemMenuAdapter(Context mContext, List<DietEPlanFood> alTodayDietMenu, int mainItemPosition, Others others, Listener dietItemMenuAdapterListener, boolean isToday) {
        this.mContext = mContext;
        this.alTodayDietMenu = alTodayDietMenu;
        this.mainItemPosition = mainItemPosition;
        this.others = others;
        listener = dietItemMenuAdapterListener;
        this.isToday = isToday;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_today_menu_schedule, parent, false);
        return new DietItemMenuAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        DietEPlanFood todayDietMenu = alTodayDietMenu.get(position);
        Glide.with(mContext).load(others.getDietPlanImagePath() + todayDietMenu.getFood().getImage()).into(holder.ivDietMenu);
        holder.tvDietMenuNm.setText(todayDietMenu.getFood().getFoodMasterName());
        holder.tvDietMenuQty.setText(getDietQty(Util.calculateQty(todayDietMenu, others), todayDietMenu.getFood().getUnit().getName()));
        if (todayDietMenu.isItemClickable() && isToday) {
            holder.llScheduleSell.setOnClickListener(View -> {
                if (listener != null)
                    listener.onReplaceClick(mainItemPosition, position, todayDietMenu, others);
            });
        }
    }

    private String getDietQty(long calculateQty, String name) {
        if (calculateQty == 0)
            return "NA";
        return calculateQty + " " + name;
    }


    @Override
    public int getItemCount() {
        if (alTodayDietMenu != null && !alTodayDietMenu.isEmpty()) {
            return alTodayDietMenu.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivDietMenu;
        TextView tvDietMenuNm, tvDietMenuQty;
        LinearLayoutCompat llScheduleSell;

        public ViewHolder(View view) {
            super(view);
            ivDietMenu = view.findViewById(R.id.iv_diet_menu);
            tvDietMenuNm = view.findViewById(R.id.tv_diet_menu_nm);
            tvDietMenuQty = view.findViewById(R.id.tv_diet_menu_qty);
            llScheduleSell = view.findViewById(R.id.ll_schedule_cell);
        }
    }

    public interface Listener {
        void onReplaceClick(int mainItemPosition, int subItemPosition, DietEPlanFood todayDietMenu, Others others);
    }
}