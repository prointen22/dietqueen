package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.SerializedName;

public class EnergyCalorie{

	@SerializedName("calories")
	private float calories;

	public float getCalories(){
		return calories;
	}
}