package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlanParam {
    @SerializedName("diet_e_plan_id")
    @Expose
    private int id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("is_terms_accepted")
    @Expose
    private int is_terms_accepted;

    public int getIs_terms_accepted(){
        return is_terms_accepted;
    }

    public void setIs_terms_accepted(){this.is_terms_accepted =is_terms_accepted;}

    public PlanParam(int id, String date) {
        this.date = date;
        this.id = id;
    }

    public PlanParam(int id) {

        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "PlanParam{" +
                "id=" + id +
                ", date='" + date + '\'' +
                '}';
    }
}
