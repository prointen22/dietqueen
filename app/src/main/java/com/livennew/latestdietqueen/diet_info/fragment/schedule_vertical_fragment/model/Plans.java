package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

import java.util.List;

public class Plans extends MyResponse {

    @SerializedName("data")
    @Expose
    private List<Diet> data = null;

    @SerializedName("others")
    @Expose
    private Others others;

    public List<Diet> getData() {
        return data;
    }

    public void setData(List<Diet> data) {
        this.data = data;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }

}