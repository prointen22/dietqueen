package com.livennew.latestdietqueen.diet_info.fragment.nutrition;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.databinding.IngredientListItemBinding;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Diet;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Ingredient;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;

import java.util.ArrayList;
import java.util.List;

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.ViewHolder> {

    private Context context;
    private List<Ingredient> list;

    public IngredientAdapter(Context context, ArrayList<Ingredient> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(IngredientListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Ingredient item = list.get(position);
        holder.binding.name.setText(item.getIngredientName());
        holder.binding.image.setLetter(item.getIngredientName());

    }

    @Override
    public int getItemCount() {
        if (list != null && !list.isEmpty()) {
            return list.size();
        } else {
            return 0;
        }
    }

    public void updateData(List<Ingredient> list) {
        this.list = list;

        notifyDataSetChanged();

    }


    public List<Ingredient> getList() {
        return list;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private IngredientListItemBinding binding;

        public ViewHolder(@NonNull IngredientListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }

}
