package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

public class TodayDietMenu {
    private String dietMenuName, dietMenuQty;
    private int mImageid, dietMenuItemId;
    private boolean isItemClickable;
    public TodayDietMenu(int dietMenuItemId, int Imageid, String dietMenuName, String dietMenuQty, boolean isItemClickable) {
        this.dietMenuItemId = dietMenuItemId;
        this.mImageid = Imageid;
        this.dietMenuName = dietMenuName;
        this.dietMenuQty = dietMenuQty;
        this.isItemClickable = isItemClickable;
    }

    public boolean isItemClickable() {
        return isItemClickable;
    }

    public void setItemClickable(boolean itemClickable) {
        isItemClickable = itemClickable;
    }

    public String getDietMenuName() {
        return dietMenuName;
    }

    public void setDietMenuName(String dietMenuName) {
        this.dietMenuName = dietMenuName;
    }

    public String getDietMenuQty() {
        return dietMenuQty;
    }

    public void setDietMenuQty(String dietMenuQty) {
        this.dietMenuQty = dietMenuQty;
    }

    public int getmImageid() {
        return mImageid;
    }

    public void setmImageid(int mImageid) {
        this.mImageid = mImageid;
    }

    public int getDietMenuItemId() {
        return dietMenuItemId;
    }

    public void setDietMenuItemId(int dietMenuItemId) {
        this.dietMenuItemId = dietMenuItemId;
    }
}

