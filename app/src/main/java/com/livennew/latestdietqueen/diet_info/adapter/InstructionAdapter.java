package com.livennew.latestdietqueen.diet_info.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.diet_info.model.Instruction;
import com.livennew.latestdietqueen.diet_info.model.InstructionData;

import java.util.ArrayList;
import java.util.List;

public class InstructionAdapter extends RecyclerView.Adapter<InstructionAdapter.RecViewHolder> {

    private List<Instruction> alInstruction;
    private Context mContext;

    public InstructionAdapter(Context mContext) {
        this.mContext = mContext;
        this.alInstruction = new ArrayList<Instruction>();
    }

    @Override
    public RecViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.instruction_content, parent, false);
        return new RecViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecViewHolder holder, int position) {
        Instruction instructionModel = alInstruction.get(position);
        holder.tvInstructionContent.setText(instructionModel.getName());
    }

    @Override
    public int getItemCount() {
        return alInstruction.size();
    }

    public void setData(List<Instruction> list) {
        this.alInstruction = list;
        notifyDataSetChanged();
    }

    public void addItems(List<Instruction> instruction) {
        this.alInstruction.addAll(instruction);
        notifyDataSetChanged();
    }

    public class RecViewHolder extends RecyclerView.ViewHolder {

        private TextView tvInstructionContent;

        public RecViewHolder(View itemView) {
            super(itemView);
            tvInstructionContent = itemView.findViewById(R.id.tv_instrcution_content);
        }
    }
}
