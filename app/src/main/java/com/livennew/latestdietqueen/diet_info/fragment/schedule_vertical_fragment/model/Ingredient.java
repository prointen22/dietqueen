package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Unit;

public class Ingredient {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ingredient_name")
    @Expose
    private String ingredientName;
    @SerializedName("unit")
    @Expose
    private Unit unit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

}