package com.livennew.latestdietqueen.diet_info.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Reason implements Parcelable {
    public int id;
    public String reason = "";
    public Boolean isSelected = false;

    public Reason() {

    }

    public Reason(Parcel in) {
        reason = in.readString();
        id = in.readInt();
        byte tmpIsSelected = in.readByte();
        isSelected = tmpIsSelected == 0 ? null : tmpIsSelected == 1;
    }

    public static final Creator<Reason> CREATOR = new Creator<Reason>() {
        @Override
        public Reason createFromParcel(Parcel in) {
            return new Reason(in);
        }

        @Override
        public Reason[] newArray(int size) {
            return new Reason[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(reason);
        dest.writeByte((byte) (isSelected == null ? 0 : isSelected ? 1 : 2));
    }
}
