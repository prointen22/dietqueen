package com.livennew.latestdietqueen.diet_info.fragment.nutrition;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.google.android.material.tabs.TabLayout;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.databinding.IngredientFragmentBinding;


public class IngredientFragment extends Fragment {

    private IngredientFragmentBinding binding;

    public static IngredientFragment getInstance() {
        return new IngredientFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = IngredientFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        binding.viewPager.setCurrentItem(0);
                        break;
                    case 1:
                        binding.viewPager.setCurrentItem(1);
                        break;
                    case 2:
                        binding.viewPager.setCurrentItem(2);
                        break;

                    default:
                        binding.viewPager.setCurrentItem(tab.getPosition());
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        /**
         *Set an Apater for the View Pager
         */
        binding.viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        binding.viewPager.setCurrentItem(1);
        binding.tabLayout.post(new Runnable() {
            @Override
            public void run() {
                binding.tabLayout.setupWithViewPager(binding.viewPager);
            }
        });


        return view;
    }

    class MyAdapter extends FragmentStatePagerAdapter {

        MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return IngredientChildFragment.newInstance();
                case 1:
                    return IngredientChildFragment.newInstance();
                case 2:
                    return IngredientChildFragment.newInstance();

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;

        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return getString(R.string.daily);
                case 1:
                    return getString(R.string.weekly);
                case 2:
                    return getString(R.string.monthly);

            }
            return null;
        }

    }


}