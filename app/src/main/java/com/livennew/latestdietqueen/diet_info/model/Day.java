package com.livennew.latestdietqueen.diet_info.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Day implements Parcelable {
    public String date = "";
    public String day = "";
    public String month = "";
    public String fullDate = "";
    public Boolean isSelected;

    public Day() {

    }

    public Day(Parcel in) {
        date = in.readString();
        day = in.readString();
        month = in.readString();
        fullDate = in.readString();
        byte tmpIsSelected = in.readByte();
        isSelected = tmpIsSelected == 0 ? null : tmpIsSelected == 1;
    }

    public static final Creator<Day> CREATOR = new Creator<Day>() {
        @Override
        public Day createFromParcel(Parcel in) {
            return new Day(in);
        }

        @Override
        public Day[] newArray(int size) {
            return new Day[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(day);
        dest.writeString(month);
        dest.writeString(fullDate);
        dest.writeByte((byte) (isSelected == null ? 0 : isSelected ? 1 : 2));
    }
}
