package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.SerializedName;

public class MealsDataItem{

	@SerializedName("diet_e_data_id")
	private int dietEDataId;

	@SerializedName("percentage")
	private int percentage;

	@SerializedName("id")
	private int id;

	@SerializedName("meal_tag_id")
	private int mealTagId;

	@SerializedName("food")
	private Food food;

	@SerializedName("old_food_id")
	private int oldFoodId;

	@SerializedName("new_food_id")
	private int newFoodId;

	public int getDietEDataId(){
		return dietEDataId;
	}

	public int getPercentage(){
		return percentage;
	}

	public int getId(){
		return id;
	}

	public int getMealTagId(){
		return mealTagId;
	}

	public int getOldFoodId(){
		return oldFoodId;
	}

	public int getNewFoodId(){
		return newFoodId;
	}

	public void setOldFoodId(int oldFoodId) {
		this.oldFoodId = oldFoodId;
	}

	public void setNewFoodId(int newFoodId) {
		this.newFoodId = newFoodId;
	}

	public Food getFood(){
		return food;
	}

	public void setFood(Food food) {
		this.food = food;
	}
}