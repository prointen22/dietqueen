package com.livennew.latestdietqueen.diet_info.fragment

import android.content.*
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.livennew.latestdietqueen.R
import com.livennew.latestdietqueen.SessionManager
import com.livennew.latestdietqueen.databinding.ReasonFragmentBinding
import com.livennew.latestdietqueen.diet_info.adapter.ReasonAdapter
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Diet
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others
import com.livennew.latestdietqueen.diet_info.model.Day
import com.livennew.latestdietqueen.diet_info.model.Reason
import com.livennew.latestdietqueen.model.User
import com.livennew.latestdietqueen.network_support.APIClient
import com.livennew.latestdietqueen.retrofit.APIInterface

class ReasonFragment : Fragment() {
    private var mBinding: ReasonFragmentBinding? = null
    private var mReasonList: ArrayList<Reason> = ArrayList()

    var apiInterface: APIInterface? = null
    var sessionManager: SessionManager? = null
    var user: User? = null
    var mPrefsobj: SharedPreferences? = null
    private lateinit var reasonAdapter:ReasonAdapter

    private var mDietList: ArrayList<Diet> = ArrayList()
    private var mPosition = 0
    private lateinit var mOthers: Others
    private lateinit var mDayObj: Day
    private lateinit var mDietObj: Diet

    companion object {
        @JvmStatic
        fun newInstance(): Fragment {
            return ReasonFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(requireActivity().applicationContext)
        apiInterface = APIClient.getClient(requireActivity().applicationContext).create(APIInterface::class.java)
        user = sessionManager!!.user
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        mBinding = ReasonFragmentBinding.inflate(inflater, container, false)
        mPrefsobj = requireActivity().getPreferences(Context.MODE_PRIVATE)

        init()

        return mBinding!!.root
    }

    private fun init() {
        try {
            if (arguments != null) {
                mPosition = requireArguments().getInt("position")
                mDietList = requireArguments().getParcelableArrayList("dietList")!!
                mOthers = requireArguments().getParcelable("others")!!
                mDayObj = requireArguments().getParcelable("day")!!

                mDietObj = mDietList[mPosition]
            }

            mBinding?.rvReason?.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            prepareReasonData()
        } catch (ex: Exception) {
            Log.v("TAG", "Exception: ${ex.message}")
        }
    }

    private fun prepareReasonData() {
        mReasonList.clear()

        val reason1 = Reason()
        reason1.id = 1
        reason1.reason = "I ate something else"
        mReasonList.add(reason1)

        val reason2 = Reason()
        reason2.id = 2
        reason2.reason = "Was too busy"
        mReasonList.add(reason2)

        val reason3 = Reason()
        reason3.id = 3
        reason3.reason = "Was at Party/Event"
        mReasonList.add(reason3)

        val reason4 = Reason()
        reason4.id = 4
        reason4.reason = "Fasting"
        mReasonList.add(reason4)

        val reason5 = Reason()
        reason5.id = 5
        reason5.reason = "Travelling"
        mReasonList.add(reason5)

        val reason6 = Reason()
        reason6.id = 1
        reason6.reason = "Guests at home"
        mReasonList.add(reason6)

        val reason7 = Reason()
        reason7.id = 7
        reason7.reason = "Other"
        mReasonList.add(reason7)

        setAdapter()
    }

    private fun setAdapter() {
        reasonAdapter = activity?.let { ReasonAdapter(it, mReasonList) { reason: Reason ->
            adapterItemClicked(reason)
        } }!!
        mBinding?.rvReason?.adapter = reasonAdapter
    }

    private fun adapterItemClicked(reason: Reason) {
        Log.v("TAG", "Selected reason: ${reason.reason}")
        for (item in mReasonList) {
            item.isSelected = false
        }
        reason.isSelected = true
        reasonAdapter.notifyDataSetChanged()

        loadFragment(reason.reason)
    }

    private fun loadFragment(reason: String) {
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        val fragment = DietStatusUpdateFragment.newInstance()
        val bundle = Bundle()
        bundle.putString("operation", "No")
        bundle.putString("reason", reason)
        bundle.putInt("position", mPosition)
        bundle.putParcelableArrayList("dietList", mDietList)
        bundle.putParcelable("others", mOthers)
        bundle.putParcelable("day", mDayObj)
        fragment.arguments = bundle
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
