package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

import java.util.List;

public class Foods extends MyResponse {
    @SerializedName("data")
    @Expose
    private List<Food> data = null;

    @SerializedName("others")
    @Expose
    private FoodsOthers others;

    public List<Food> getData() {
        return data;
    }

    public void setData(List<Food> data) {
        this.data = data;
    }

    public FoodsOthers getOthers() {
        return others;
    }

    public void setOthers(FoodsOthers others) {
        this.others = others;
    }

    public class FoodsOthers {
        @SerializedName("foodImagePath")
        @Expose
        private String dietPlanImagePath;

        public String getDietPlanImagePath() {
            return dietPlanImagePath;
        }

        public void setDietPlanImagePath(String dietPlanImagePath) {
            this.dietPlanImagePath = dietPlanImagePath;
        }
    }
}