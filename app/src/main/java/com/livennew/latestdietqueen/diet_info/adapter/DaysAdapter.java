package com.livennew.latestdietqueen.diet_info.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.diet_info.model.Day;
import com.livennew.latestdietqueen.utils.OnDayItemClickListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.DaysViewHolder> {

    private List<Day> mDaysList;
    private Context mContext;
    private OnDayItemClickListener mListener;

    public DaysAdapter(Context mContext, ArrayList<Day> alDays, OnDayItemClickListener listener) {
        this.mContext = mContext;
        this.mDaysList = alDays;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public DaysViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_day_row, parent, false);
        return new DaysViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DaysViewHolder holder, int position) {
        Day dayObj = mDaysList.get(position);
        holder.mTvDate.setText(dayObj.date);
        holder.mTvMonth.setText(dayObj.month);
        holder.mTvDay.setText(dayObj.day);

        if (dayObj.isSelected) {
            if (position ==0) {
                holder.mLLScheduleDay.setBackgroundResource(R.drawable.round_dark_grey);
                holder.mTvDate.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                holder.mTvMonth.setTextColor(ContextCompat.getColor(mContext,R.color.white));
            }else{
                holder.mLLScheduleDay.setBackgroundResource(R.drawable.round_white);
                holder.mTvDate.setTextColor(ContextCompat.getColor(mContext, R.color.orange_theme));
                holder.mTvMonth.setTextColor(ContextCompat.getColor(mContext,R.color.black));
            }
            holder.mTvMonth.setVisibility(View.VISIBLE);
            holder.mLLDay.setVisibility(View.VISIBLE);
        } else {
            holder.mLLScheduleDay.setBackgroundResource(R.drawable.round_gray);
            holder.mTvMonth.setVisibility(View.GONE);
            holder.mTvDate.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            holder.mLLDay.setVisibility(View.GONE);
        }

        holder.mLLScheduleDay.setOnClickListener(v -> mListener.onItemClick(dayObj, position));
    }

    @Override
    public int getItemCount() {
        return mDaysList.size();
    }

    public static class DaysViewHolder extends RecyclerView.ViewHolder {
        public TextView mTvDate, mTvMonth, mTvDay;
        public LinearLayout mLLScheduleDay, mLLDay;

        public DaysViewHolder(View view) {
            super(view);
            mLLScheduleDay = view.findViewById(R.id.ll_schedule_day);
            mTvDate = view.findViewById(R.id.tv_date);
            mTvMonth = view.findViewById(R.id.tv_month);
            mTvDay = view.findViewById(R.id.tv_day);
            mLLDay = view.findViewById(R.id.ll_day);
        }
    }

}
