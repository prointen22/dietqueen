package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

public class ReplaceItemModel {
    int replaceItemId, ivReplaceItem;
    String tvReplaceItem;
    boolean isSelected;
    public ReplaceItemModel(int replaceItemId, int ivReplaceItem, String tvReplaceItem) {
        this.replaceItemId = replaceItemId;
        this.ivReplaceItem = ivReplaceItem;
        this.tvReplaceItem = tvReplaceItem;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getReplaceItemId() {
        return replaceItemId;
    }

    public void setReplaceItemId(int replaceItemId) {
        this.replaceItemId = replaceItemId;
    }

    public int getIvReplaceItem() {
        return ivReplaceItem;
    }

    public void setIvReplaceItem(int ivReplaceItem) {
        this.ivReplaceItem = ivReplaceItem;
    }

    public String getTvReplaceItem() {
        return tvReplaceItem;
    }

    public void setTvReplaceItem(String tvReplaceItem) {
        this.tvReplaceItem = tvReplaceItem;
    }
}
