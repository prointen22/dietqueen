package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FoodGroupParam {
    @SerializedName("food_group")
    @Expose
    private Food food;

    public FoodGroupParam(Food food) {

        this.food = food;
    }




    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    @Override
    public String toString() {
        return "FoodGroupParam{" +
                "food=" + food +
                '}';
    }
}
