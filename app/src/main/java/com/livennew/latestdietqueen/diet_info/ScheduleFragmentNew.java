package com.livennew.latestdietqueen.diet_info;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.AddDietMenuReplaceBinding;
import com.livennew.latestdietqueen.databinding.DietContainerBinding;
import com.livennew.latestdietqueen.diet_info.adapter.DaysAdapter;
import com.livennew.latestdietqueen.diet_info.adapter.ScheduleAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.DietStatusUpdateFragment;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.ReplaceItemAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Diet;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFoodMealByTag;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DoneParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.MealsDataItem;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.PlanParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Plans;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.UpdateMealPrefParam;
import com.livennew.latestdietqueen.diet_info.model.Day;
import com.livennew.latestdietqueen.info_screen.weight_screen.WeightActivity;
import com.livennew.latestdietqueen.model.FoodReplaceResp;
import com.livennew.latestdietqueen.model.MyResponse;
import com.livennew.latestdietqueen.model.ReplaceFoodParam;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.DateUtil;
import com.livennew.latestdietqueen.utils.DateUtilNew;
import com.livennew.latestdietqueen.utils.SupportClass;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.inject.Inject;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ScheduleFragmentNew#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class ScheduleFragmentNew extends Fragment implements ISelectedItemListner{
    private FragmentActivity activity;

    @Inject
    SessionManager sessionManager;
    User user;
    @Inject
    APIInterface apiInterface;


    private DietContainerBinding binding;

    private ArrayList<Day> mDaysList = new ArrayList<>();
    private DaysAdapter mDaysAdapter;
    int mSelectedPosition = 1;
    Day mSelectedDay;

    private ArrayList<Diet> mDietList = new ArrayList<>();
    private Others mOthers;
    private ScheduleAdapter mScheduleAdapter;

    private ArrayList<Food> alReplaceItemModel;
    private ProgressDialog pDialog;

    public ScheduleFragmentNew() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        return new ScheduleFragmentNew();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = sessionManager.getUser();
        activity = getActivity();
        pDialog = new ProgressDialog(activity);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DietContainerBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle!=null) {
            String type = bundle.getString("type");
            if (type.equals(getString(R.string.diet_status))){
                loadFragment();
            }
        }

        LinearLayoutManager llmHorizontal = new LinearLayoutManager(getApplicationContext());
        llmHorizontal.setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.rvDays.setLayoutManager(llmHorizontal);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rvSchedule.setLayoutManager(llm);

        binding.ivPrevious.setOnClickListener(v -> {
            navigateToPreviousDay();
        });

        binding.ivNext.setOnClickListener(v -> {
            navigateToNextDay();
        });

        prepareDaysList();
    }

    private void prepareDaysList() {
        ArrayList<Date> dateList = new ArrayList<>();
        mDaysList = new ArrayList<>();

        Date currentDate = new Date();
        Date previousDate = DateUtil.findPrevDay(currentDate);

        Day dayObjPrev = new Day();
        dayObjPrev.date = DateUtilNew.getDateFromDateObject(previousDate);
        dayObjPrev.day = DateUtilNew.getDayFromDateObject(previousDate);
        dayObjPrev.month = DateUtilNew.getMonthFromDateObject(previousDate);
        dayObjPrev.fullDate = DateUtilNew.getFullDateFromDateObject(previousDate);
        dayObjPrev.isSelected = false;
        mDaysList.add(dayObjPrev);

        Day dayObjCurrent = new Day();
        dayObjCurrent.date = DateUtilNew.getDateFromDateObject(currentDate);
        dayObjCurrent.day = DateUtilNew.getDayFromDateObject(currentDate);
        dayObjCurrent.month = DateUtilNew.getMonthFromDateObject(currentDate);
        dayObjCurrent.fullDate = DateUtilNew.getFullDateFromDateObject(currentDate);
        dayObjCurrent.isSelected = true;
        mSelectedDay = dayObjCurrent;
        mDaysList.add(dayObjCurrent);

        for (int i = 0; i < 6; i++) {
            Date date = DateUtil.findNextDay(currentDate);
            currentDate = date;
            dateList.add(date);
        }

        Log.v("TAG", "Date list size:" + dateList.size());

        for (Date dateObj : dateList) {
            Day dayObj = new Day();
            dayObj.date = DateUtilNew.getDateFromDateObject(dateObj);
            dayObj.day = DateUtilNew.getDayFromDateObject(dateObj);
            dayObj.month = DateUtilNew.getMonthFromDateObject(dateObj);
            dayObj.fullDate = DateUtilNew.getFullDateFromDateObject(dateObj);
            dayObj.isSelected = false;
            mDaysList.add(dayObj);
        }

        setDaysAdapter();
    }

    private void setDaysAdapter() {
        mDaysAdapter = new DaysAdapter(activity, mDaysList, (dayObj, position) -> {
            mSelectedPosition = position;
            mSelectedDay = dayObj;
            selectDayObj();
        });
        binding.rvDays.setAdapter(mDaysAdapter);
        callDietPlanAPI();
    }

    private void selectDayObj() {
        mSelectedDay = mDaysList.get(mSelectedPosition);
        for (Day dayObject : mDaysList) {
            dayObject.isSelected = dayObject.date.equals(mSelectedDay.date);
        }
        mDaysAdapter.notifyDataSetChanged();
        binding.rvDays.scrollToPosition(mSelectedPosition);

        callDietPlanAPI();
    }

    private void navigateToPreviousDay() {
        if (mSelectedPosition > 0) {
            mSelectedPosition -= 1;
            selectDayObj();
        }
    }

    private void navigateToNextDay() {
        if (mSelectedPosition < mDaysList.size() - 1) {
            mSelectedPosition += 1;
            selectDayObj();
        }
    }

    private void callDietPlanAPI() {
        Log.v("TAG", "Diet plan id:" + sessionManager.getDietEPlanId() + "\n Date: " + mSelectedDay.fullDate);
        PlanParam planParam = new PlanParam(sessionManager.getDietEPlanId(), mSelectedDay.fullDate);
        binding.llProgress.setVisibility(View.VISIBLE);
        apiInterface.getDietPlansNew(planParam).enqueue(new Callback<Plans>() {
            @Override
            public void onResponse(@NonNull Call<Plans> call, @NonNull Response<Plans> response) {
                Log.v("TAG", "Diet plan response size: " + response.body().getData().size());
                response.body().getOthers().setDate(planParam.getDate());
                binding.llProgress.setVisibility(View.GONE);
                mDietList = (ArrayList<Diet>) response.body().getData();
                mOthers = response.body().getOthers();
                setDietAdapter();
            }

            @Override
            public void onFailure(@NonNull Call<Plans> call, @NonNull Throwable t) {
                Log.e("TAG", "onFailure: " + t.getMessage());
                binding.llProgress.setVisibility(View.GONE);
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDietAdapter() {
        String currentTime = DateUtilNew.getCurrentTimeFromDateObject(new Date());
        Log.v("TAG", "Current time: " + currentTime);

        if (mSelectedDay.fullDate.equals(DateUtilNew.getFullDateFromDateObject(new Date()))) {
            for (Diet diet : mDietList) {
                int diffInHours = DateUtilNew.compareDietTimeWithCurrentTime(diet.getTime());
                Log.v("TAG", "Diet category: " + diet.getPlanSubcategory() + " Time diff in hours: " + diffInHours);

                if (diffInHours >= 0) {
                    diet.setMenuItemsVisible(true);
                    diet.setCanExpanded(true);
                    diet.setExpanded(true);
                    break;
                }
            }

            for (Diet diet : mDietList) {
                int diffInHours = DateUtilNew.compareDietTimeWithCurrentTime(diet.getTime());
                Log.v("TAG", "Diet category: " + diet.getPlanSubcategory() + " Time diff in hours: " + diffInHours);

                if (diffInHours >= 0) {
                    diet.setCanExpanded(true);
                    diet.setDoneButtonVisible(false);
                } else {
                    diet.setDoneButtonVisible(true);
                }
            }
        }

        if (DateUtilNew.isFutureDate(mSelectedDay.fullDate)) {
            for (Diet diet : mDietList) {
                diet.setDoneButtonVisible(false);
                diet.setCanExpanded(true);
            }
        }

        if (DateUtilNew.isPastDate(mSelectedDay.fullDate)) {
            for (Diet diet : mDietList) {
                diet.setDoneButtonVisible(true);
                diet.setCanExpanded(false);
            }
        }

        mScheduleAdapter = new ScheduleAdapter(activity, mDietList, mOthers,this, (dietObj, mealTypeObj) -> {
            Log.v("TAG", "Diet obj id: "+dietObj.getId()+" Meal type Obj name: "+mealTypeObj.getName()+"& id: "+mealTypeObj.getId());
            //ToDo: Open dialog box here before calling below method
            updateMealPreferenceAPICall(mealTypeObj.getId(),sessionManager.getDietEPlanId(), mSelectedDay.fullDate,mSelectedDay.fullDate,dietObj.getTime());
        });
        binding.rvSchedule.setAdapter(mScheduleAdapter);

        setListeners();
    }

    private void setListeners() {
        Log.i("TAG","diet done");
        mScheduleAdapter.setListener((position, todayDietMenuModel, others) -> loadFragment(position)
                , (mainItemPosition, subItemPosition, mealItemPosition, todayDietMenu, others) -> {
            if (SupportClass.checkConnection(activity))
                onFoodReplace(mainItemPosition, subItemPosition, mealItemPosition, todayDietMenu, others);
            else
                SupportClass.noInternetConnectionToast(activity);
        });
    }

    private void onFoodReplace(int mainItemPosition, int subItemPosition, int mealItemPosition, MealsDataItem todayDietMenu, Others others) {
        Dialog dialog = new Dialog(activity,R.style.theme_sms_receive_dialog);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        AddDietMenuReplaceBinding binding = AddDietMenuReplaceBinding.inflate(getLayoutInflater());
        dialog.setContentView(binding.getRoot());//add_diet_menu_replace
        dialog.setCanceledOnTouchOutside(false);
        alReplaceItemModel = new ArrayList<>();
        LinearLayoutManager blogLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.rvReplaceItemList.setLayoutManager(blogLayoutManager);
        ReplaceItemAdapter replaceItemAdapter = new ReplaceItemAdapter(activity, alReplaceItemModel, food -> {
            if (food!=null) {
                int oldFoodId = 0;
                if (todayDietMenu.getOldFoodId() == 0) {
                     oldFoodId = todayDietMenu.getFood().getId();
                }else{
                    oldFoodId =  todayDietMenu.getOldFoodId();
                }
                apiInterface.replaceFood(new ReplaceFoodParam(oldFoodId, food.getId(), todayDietMenu.getDietEDataId(), DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
                    @Override
                    public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
                        if (response.body() != null) {
                            if (response.body().getError()) {
                                for (int i = 0; i < response.body().getMessage().size(); i++)
                                    Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                            } else {
                                todayDietMenu.setOldFoodId(response.body().getData().getOldFoodId());
                                todayDietMenu.setFood(response.body().getData().getFood());

                                mScheduleAdapter.updateSubListItem(mainItemPosition, subItemPosition, mealItemPosition, response.body().getData().getFood());
                            }
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                        dialog.dismiss();
                        Log.v("TAG", "Error in replaceFood API response"+t.getMessage());
                    }
                });
            } else {
                dialog.dismiss();
            }
        });
        binding.rvReplaceItemList.setAdapter(replaceItemAdapter);
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.ivClose.setOnClickListener(view -> dialog.dismiss());
        apiInterface.getFoodsByGroup(new FoodGroupParam(todayDietMenu.getFood())).enqueue(new Callback<Foods>() {
            @Override
            public void onResponse(@NonNull Call<Foods> call, @NonNull Response<Foods> response) {
                if (response.body() != null) {
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        Iterator<Food> i = response.body().getData().iterator();
                        while (i.hasNext()) {
                            Food e = i.next();
                            if (e.getId() == todayDietMenu.getFood().getId()) {
                                i.remove();
                            }
                        }
                        if (response.body().getData().size() == 0) {
                            dialog.dismiss();
                            Toast.makeText(activity, getString(R.string.there_is_no_food_to_replace), Toast.LENGTH_SHORT).show();
                        }
                        replaceItemAdapter.addAll(response.body().getData(),response.body().getOthers());
                        binding.progressBar.setVisibility(View.GONE);
                    } else {
                        dialog.dismiss();
                        Toast.makeText(activity, getString(R.string.there_is_no_food_to_replace), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Foods> call, @NonNull Throwable t) {
                Log.e("TAG", "onFailure: " + t.getMessage());
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void loadFragment(int position) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment fragment = DietStatusUpdateFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("operation", "");
        bundle.putString("reason", "");
        bundle.putInt("position", position);
        bundle.putParcelableArrayList("dietList", mDietList);
        bundle.putParcelable("others", mOthers);
        bundle.putParcelable("day", mSelectedDay);
        fragment.setArguments(bundle);
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void loadFragment() {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        Fragment fragment = DietStatusUpdateFragment.newInstance();
//        Bundle bundle = new Bundle();
//        bundle.putString("operation", "");
//        bundle.putString("reason", "");
//        bundle.putInt("position", 0);
//        bundle.putParcelableArrayList("dietList", mDietList);
//        bundle.putParcelable("others", mOthers);
//        bundle.putParcelable("day", mSelectedDay);
//        fragment.setArguments(bundle);
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void updateMealPreferenceAPICall(int id, int dietEPlanId, String startDate, String endDate, String time) {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        apiInterface.updateMealPreference(new UpdateMealPrefParam(id,dietEPlanId,startDate,endDate,time)).enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(@NonNull Call<MyResponse> call, @NonNull Response<MyResponse> response) {
                pDialog.dismiss();
                if (response.body() != null) {
                    MyResponse myResponse = response.body();
                    assert myResponse != null;

                    if (!myResponse.getError()) {
                        Toast.makeText(activity, Util.getMessageInSingleLine(myResponse.getMessage()), Toast.LENGTH_SHORT).show();
                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                        dialog.setMessage(Util.getMessageInSingleLine(myResponse.getMessage()));
                        dialog.setCancelable(false);
                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                    }
                } else {
                    Toast.makeText(activity, "Something went wrong..", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyResponse> call, @NonNull Throwable t) {
                Log.e("TAG", "onFailure: " + t.getMessage());
                pDialog.dismiss();
            }
        });
    }

    @Override
    public void showSelectedItem(int position) {
        binding.rvSchedule.post(() -> binding.rvSchedule.smoothScrollToPosition(position));
    }
}