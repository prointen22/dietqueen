package com.livennew.latestdietqueen.diet_info.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

public class InstructionResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private InstructionData data;

    public InstructionData getData() {
        return data;
    }

    public void setData(InstructionData data) {
        this.data = data;
    }
}
