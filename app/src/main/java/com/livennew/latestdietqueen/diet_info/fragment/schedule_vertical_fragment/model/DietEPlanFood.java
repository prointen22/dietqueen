package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DietEPlanFood {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("diet_e_data_id")
    @Expose
    private int dietEDataId;

    @SerializedName("percentage")
    @Expose
    private double percentage;
    @SerializedName("food")
    @Expose
    private Food food;
    @SerializedName("item")
    @Expose
    private String item;

    @SerializedName("old_food_id")
    private int oldFoodId;

    @SerializedName("new_food_id")
    private int newFoodId;

    public int getOldFoodId(){
        return oldFoodId;
    }

    public int getNewFoodId(){
        return newFoodId;
    }

    public void setOldFoodId(int oldFoodId) {
        this.oldFoodId = oldFoodId;
    }

    public void setNewFoodId(int newFoodId) {
        this.newFoodId = newFoodId;
    }


    public String getImgpath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    @SerializedName("imgpath")
    @Expose
    private String imgpath;
    private boolean itemClickable = true;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getDietEDataId() {
        return dietEDataId;
    }

    public void setDietEDataId(int dietEDataId) {
        this.dietEDataId = dietEDataId;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public boolean isItemClickable() {
        return itemClickable;
    }

    public void setItemClickable(boolean itemClickable) {
        this.itemClickable = itemClickable;
    }
}