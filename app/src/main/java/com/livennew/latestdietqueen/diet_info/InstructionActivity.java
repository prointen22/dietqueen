package com.livennew.latestdietqueen.diet_info;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Toast;

import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.ActivityInstructionBinding;
import com.livennew.latestdietqueen.diet_info.adapter.InstructionAdapter;
import com.livennew.latestdietqueen.diet_info.model.InstructionResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.retrofit.APIInterface;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class InstructionActivity extends FabActivity {
    private static final String TAG = "InstructionActivity";
    private ActivityInstructionBinding binding;
    private InstructionAdapter adapter;
    private boolean isScrolling;
    private Handler handler;
    private int currentPageNo = 1;
    private int lastPage = 1;
    private int currentItem;
    private int totalItems;
    private int scrollOutItems;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorAccent));
        binding = ActivityInstructionBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        user = sessionManager.getUser();
        Log.d("hhhhh",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> instruction"+user.getToken()+"lang"+user.getUserDetail().getLanguage().getId());

        binding.toolbar.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
        binding.toolbar.setNavigationOnClickListener(v -> finish());
        adapter = new InstructionAdapter(this);
        LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(recyclerLayoutManager);
        binding.recyclerView.setAdapter(adapter);
        handler = new Handler();
        binding.btnOk.setOnClickListener(view1 -> finish());
        getInstruction(currentPageNo);
        getInstruction(++currentPageNo);


      binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            currentPageNo++;
                            if (currentPageNo <= lastPage) {
                                getInstruction(currentPageNo);
                            }

                        }
                    }, 200);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = recyclerLayoutManager.getChildCount();
                totalItems = recyclerLayoutManager.getItemCount();
                scrollOutItems = recyclerLayoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItem + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            currentPageNo++;
                            if (currentPageNo <= lastPage) {
                                getInstruction(currentPageNo);
                            }
                        }
                    }, 200);
                }
            }
        });
    }

    private void getInstruction(int page) {
        if (page == 1) {
            binding.progressBar.setVisibility(View.VISIBLE);
            binding.recyclerView.setVisibility(View.GONE);
            binding.btnOk.setVisibility(View.GONE);
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("perPage", 20);
        params.put("languageId",user.getUserDetail().getLanguage().getId());
        Integer languageId=user.getUserDetail().getLanguage().getId();
        apiInterface.getInstructions(page,languageId).enqueue(new Callback<InstructionResp>() {
            @Override
            public void onResponse(@NotNull Call<InstructionResp> call, @NotNull Response<InstructionResp> response) {
                if (page == 1) {
                    binding.progressBar.setVisibility(View.GONE);
                    binding.recyclerView.setVisibility(View.VISIBLE);
                    binding.btnOk.setVisibility(View.VISIBLE);
                }
                if (!response.body().getError()) {
                    if (page == 1) {
                        adapter.setData(response.body().getData().getInstruction());
                        lastPage = response.body().getData().getLastPage();
                    } else {
                        adapter.addItems(response.body().getData().getInstruction());
                    }
                } else
                    Toast.makeText(InstructionActivity.this, response.body().getMessage().get(0), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(@NotNull Call<InstructionResp> call, @NotNull Throwable t) {
                Toast.makeText(InstructionActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}