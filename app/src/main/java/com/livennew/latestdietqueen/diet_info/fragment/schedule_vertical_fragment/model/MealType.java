package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MealType {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("is_users_preferred_meal")
    @Expose
    private int isPreferredMeal = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsPreferredMeal() {
        return isPreferredMeal;
    }

    public void setIsPreferredMeal(int isPreferredMeal) {
        this.isPreferredMeal = isPreferredMeal;
    }
}