package com.livennew.latestdietqueen.diet_info.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import com.livennew.latestdietqueen.R
import androidx.core.content.ContextCompat
import android.widget.TextView
import android.widget.RadioButton
import androidx.appcompat.widget.LinearLayoutCompat
import android.text.TextUtils
import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.Filter
import com.livennew.latestdietqueen.diet_info.model.Reason
import java.util.ArrayList

class ReasonAdapter internal constructor(private val context: Context, private var mReasonList: List<Reason>, private val clickListener: (Reason) -> Unit) : RecyclerView.Adapter<ReasonAdapter.MyViewHolder>() {
    private val mFilter: ItemFilter = ItemFilter()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.rv_language_content, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val reasonObj = mReasonList[position]
        holder.tvReason.text = reasonObj.reason
        holder.rbSelectReason.isChecked = reasonObj.isSelected
        holder.itemView.setOnClickListener { view: View? -> clickListener(reasonObj) }
        holder.rbSelectReason.setOnClickListener { view: View? -> clickListener(reasonObj) }
        if (reasonObj.isSelected) {
            holder.mLLC.setBackgroundColor(ContextCompat.getColor(context, R.color.selector_color))
            holder.tvReason.setTextColor(ContextCompat.getColor(context, R.color.orange_theme))
        } else {
            holder.mLLC.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            holder.tvReason.setTextColor(ContextCompat.getColor(context, R.color.black))
        }
    }

    override fun getItemCount(): Int {
        return mReasonList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvReason: TextView
        var rbSelectReason: RadioButton
        val mLLC: LinearLayoutCompat

        init {
            tvReason = itemView.findViewById(R.id.tv_language)
            rbSelectReason = itemView.findViewById(R.id.rb_check_lang)
            mLLC = itemView.findViewById(R.id.llc_row)
        }
    }

    val filter: Filter
        get() = mFilter

    private inner class ItemFilter : Filter() {
        override fun performFiltering(constraint: CharSequence): FilterResults {
            val results = FilterResults()
            val filterString = constraint.toString().toLowerCase()
            if (TextUtils.isEmpty(filterString)) {
                results.values = mReasonList
                results.count = mReasonList.size
                return results
            }
            val list = mReasonList
            val count = list.size
            val nlist = ArrayList<Reason>(count)
            var filterableString: String
            for (i in 0 until count) {
                filterableString = list[i].reason
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list[i])
                }
            }
            results.values = nlist
            results.count = nlist.size
            return results
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            mReasonList = results.values as ArrayList<Reason>
            notifyDataSetChanged()
        }
    }
}