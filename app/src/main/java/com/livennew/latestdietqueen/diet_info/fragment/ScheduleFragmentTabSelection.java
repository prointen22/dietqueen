package com.livennew.latestdietqueen.diet_info.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.FragmentScheduleBinding;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.DayScheduleFragment;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.utils.DateUtil;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ScheduleFragmentTabSelection extends Fragment {
    private static final String TAG = "ScheduleFragmentTabSele";
    private FragmentActivity activity;
    private int item_count = 3;
    private FragmentScheduleBinding binding;
    public MutableLiveData<String> requiredEnergyLiveDate = new MutableLiveData();
    @Inject
    SessionManager sessionManager;
    User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        user = sessionManager.getUser();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentScheduleBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        binding.tlSchedule.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        binding.vpSchedule.setCurrentItem(0);
                        break;
                    case 1:
                        binding.vpSchedule.setCurrentItem(1);
                        break;
                    case 2:
                        binding.vpSchedule.setCurrentItem(2);
                        break;

                    default:
                        binding.vpSchedule.setCurrentItem(tab.getPosition());
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        /**
         *Set an Apater for the View Pager
         */
        binding.vpSchedule.setAdapter(new MyAdapter(getChildFragmentManager()));
        binding.vpSchedule.setCurrentItem(1);
        binding.tlSchedule.post(new Runnable() {
            @Override
            public void run() {
                binding.tlSchedule.setupWithViewPager(binding.vpSchedule);
            }
        });
        Glide
                .with(this)
                .load(sessionManager.getUser().getProfileImage())
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(binding.ivProfile);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        requiredEnergyLiveDate.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String integer) {
                if (integer != null) {
                    binding.tvBrnCalCnt.setText(integer + "");
               //     binding.tvtRemaning.setText("hjjjjjjjjjjj");
                } else {
                    binding.tvBrnCalCnt.setText("0");
                }
            }
        });
    }

    class MyAdapter extends FragmentStatePagerAdapter {

        MyAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return DayScheduleFragment.getInstance(DateUtil.getYesterdayDate(), false,"yesterday");

                case 1:
                    return DayScheduleFragment.getInstance(DateUtil.getTodayDate(), true,"today");
                case 2:
                    return DayScheduleFragment.getInstance(DateUtil.getTomorrowDate(), false,"tommrow");


            }
            return null;
        }

        @Override
        public int getCount() {
            return item_count;

        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return activity.getString(R.string.yesterday);
                case 1:
                    return activity.getString(R.string.today);
                case 2:
                    return activity.getString(R.string.tomorrow);

            }
            return null;
        }

    }

}
