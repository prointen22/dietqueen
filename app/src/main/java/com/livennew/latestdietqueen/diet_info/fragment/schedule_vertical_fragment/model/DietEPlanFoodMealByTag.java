package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DietEPlanFoodMealByTag{

	@SerializedName("is_users_preferred_meal")
	private int isUsersPreferredMeal;

	@SerializedName("meals_data")
	private List<MealsDataItem> mealsData;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	private boolean itemClickable = true;

	public int getIsUsersPreferredMeal(){
		return isUsersPreferredMeal;
	}

	public List<MealsDataItem> getMealsData(){
		return mealsData;
	}

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}

	public boolean isItemClickable() {
		return itemClickable;
	}

	public void setItemClickable(boolean itemClickable) {
		this.itemClickable = itemClickable;
	}

	public void setIsUsersPreferredMeal(int isUsersPreferredMeal) {
		this.isUsersPreferredMeal = isUsersPreferredMeal;
	}
}