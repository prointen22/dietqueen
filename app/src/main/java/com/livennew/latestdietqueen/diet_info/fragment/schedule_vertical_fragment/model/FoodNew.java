package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FoodNew implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;


    @SerializedName("is_recipe")
    @Expose
    private String is_recipe;

    @SerializedName("food_group_id")
    @Expose
    private String foodGroupId;
    @SerializedName("food_master_name")
    @Expose
    private String foodMasterName;
    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("alt_quantity")
    @Expose
    private String altQuantity;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("unit")
    @Expose
    private Unit unit;
    @SerializedName("alt_unit")
    @Expose
    private AltUnit altUnit;
    @SerializedName("energy_calorie")
    @Expose
    private EnergyCalorie energyCalorie;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFoodGroupId() {
        return foodGroupId;
    }

    public void setFoodGroupId(String foodGroupId) {
        this.foodGroupId = foodGroupId;
    }

    public String getFoodMasterName() {
        return foodMasterName;
    }

    public void setFoodMasterName(String foodMasterName) {
        this.foodMasterName = foodMasterName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAltQuantity() {
        return altQuantity;
    }

    public void setAltQuantity(String altQuantity) {
        this.altQuantity = altQuantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public AltUnit getAltUnit() {
        return altUnit;
    }

    public void setAltUnit(AltUnit altUnit) {
        this.altUnit = altUnit;
    }

    public EnergyCalorie getEnergyCalorie() {
        return energyCalorie;
    }

    public void setEnergyCalorie(EnergyCalorie energyCalorie) {
        this.energyCalorie = energyCalorie;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @SerializedName("search")
    @Expose
    private String search;


    public String getIs_recipe() {
        return is_recipe;
    }

    public void setIs_recipe(String is_recipe) {
        this.is_recipe = is_recipe;
    }


}