package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.AddDietMenuReplaceBinding;
import com.livennew.latestdietqueen.databinding.FragmentTodayScheduleListBinding;
import com.livennew.latestdietqueen.diet_info.fragment.ScheduleFragmentTabSelection;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.MyTodayScheduleRecyclerViewAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.ReplaceItemAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Diet;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DoneParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.PlanParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Plans;
import com.livennew.latestdietqueen.model.FoodReplaceResp;
import com.livennew.latestdietqueen.model.MyResponse;
import com.livennew.latestdietqueen.model.ReplaceFoodParam;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.DateUtil;
import com.livennew.latestdietqueen.utils.SupportClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class DayScheduleFragment extends Fragment {
    private static final String TAG = "YesterdaySchedule";
    private ArrayList<Diet> todayDietModelArrayList = new ArrayList<>();
    private FragmentActivity activity;
    public MyTodayScheduleRecyclerViewAdapter myTodayScheduleRecyclerViewAdapter;
    @Inject
    APIInterface apiInterface;
    private String replaceItem = "";
    private ArrayList<Food> alReplaceItemModel;
    private FragmentTodayScheduleListBinding binding;
    @Inject
    SessionManager sessionManager;
    User user;

    public static Fragment getInstance(String date, boolean isEnable,String tab) {
        DayScheduleFragment fr = new DayScheduleFragment();
        Bundle bundle = new Bundle();
        bundle.putString("date", date);
        bundle.putBoolean("isEnable", isEnable);
        bundle.putString("tab", tab);
        fr.setArguments(bundle);
        return fr;


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        user = sessionManager.getUser();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentTodayScheduleListBinding.inflate(inflater, container, false);
        init();
        onSetYesterdayDietMenuRecyclerView();


        Log.d("hhhhh","((((((((((((((((()))))))))))))))))) "+ getArguments().getString("date"));

//if( getArguments().getString("tab").equals("today")) {
    energycall();
//}

        return binding.getRoot();
    }

    private void init() {
        myTodayScheduleRecyclerViewAdapter = new MyTodayScheduleRecyclerViewAdapter(activity, todayDietModelArrayList);
        binding.recyclerView.setAdapter(myTodayScheduleRecyclerViewAdapter);


/*
        myTodayScheduleRecyclerViewAdapter.setListener(new MyTodayScheduleRecyclerViewAdapter.Listener() {
            @Override
            public void onDoneClick(int position, Diet todayDietMenuModel, Others others) {



                Log.d("jjjjj"," >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  testing    ***** c>>>>>>>>>   "+position+todayDietMenuModel.getIsDone());

            }



    });*/



     //   Log.d("kkkk"," tttttttttttttttttttttttttttttttttttttttttttttttt  todayDietModelArrayList   "+todayDietModelArrayList);

//        myTodayScheduleRecyclerViewAdapter.setListener((position,todayDietMenuModel,others) -> apiInterface.setDietDone(new DoneParam(todayDietMenuModel.getId(), others.getDate(),"Y")).enqueue(new Callback<MyResponse>() {
//            @Override
//            public void onResponse(@NonNull Call<MyResponse> call, @NonNull Response<MyResponse> response) {
//                energycall();
//                if (response.body() != null && !response.body().getError()) {
//                    myTodayScheduleRecyclerViewAdapter.getList().get(position).setIsDone(true);
//                    myTodayScheduleRecyclerViewAdapter.notifyItemChanged(position);
//
//                } else {
//                    if (response.body() != null) {
//                        for (int i = 0; i < response.body().getMessage().size(); i++)
//                            Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
//                Log.e(TAG, "onFailure: " + t.getMessage());
//                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        }), (mainItemPosition, subItemPosition, todayDietMenu, others) -> {
//            if (SupportClass.checkConnection(activity))
//                onFoodReplace(mainItemPosition, subItemPosition, todayDietMenu, others);
//            else
//                SupportClass.noInternetConnectionToast(activity);
//
//        });


//        myTodayScheduleRecyclerViewAdapter.setListener2((position,todayDietMenuModel,others) -> apiInterface.setDietDone(new DoneParam(todayDietMenuModel.getId(), others.getDate(),"U")).enqueue(new Callback<MyResponse>() {
//            @Override
//            public void onResponse(@NonNull Call<MyResponse> call, @NonNull Response<MyResponse> response) {
//                energycall();
//                if (response.body() != null && !response.body().getError()) {
//                    myTodayScheduleRecyclerViewAdapter.getList().get(position).setIsDone(true);
//                    myTodayScheduleRecyclerViewAdapter.notifyItemChanged(position);
//                } else {
//                    if (response.body() != null) {
//                        for (int i = 0; i < response.body().getMessage().size(); i++)
//                            Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
//                Log.e(TAG, "onFailure: " + t.getMessage());
//                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        }), (mainItemPosition, subItemPosition, todayDietMenu, others) -> {
//            if (SupportClass.checkConnection(activity))
//                onFoodReplace(mainItemPosition, subItemPosition, todayDietMenu, others);
//            else
//                SupportClass.noInternetConnectionToast(activity);
//
//        });



        PlanParam planParam = new PlanParam(sessionManager.getDietEPlanId(), getArguments().getString("date"));
        binding.progress.setVisibility(View.VISIBLE);
        apiInterface.getDietPlans(planParam).enqueue(new Callback<Plans>() {
            @Override
            public void onResponse(@NonNull Call<Plans> call, @NonNull Response<Plans> response) {
                boolean isEnable = false;

                if (getArguments() != null) {
                    isEnable = getArguments().getBoolean("isEnable", false);
                }
                if (DayScheduleFragment.this.getParentFragment() != null) {

                    Log.d("hhhhh","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj getUtilized_energy"+response.body().getOthers().getUtilized_energy().toString());
                    ((ScheduleFragmentTabSelection) DayScheduleFragment.this.getParentFragment()).requiredEnergyLiveDate.postValue(response.body().getOthers().getRequiredEnergy()+" / "+response.body().getOthers().getUtilized_energy().toString());
                }
                response.body().getOthers().setDate(planParam.getDate());
                myTodayScheduleRecyclerViewAdapter.updateData(response.body().getData(), response.body().getOthers(), isEnable);
                binding.progress.setVisibility(View.GONE);

                energycall();
            }

            @Override
            public void onFailure(@NonNull Call<Plans> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                binding.progress.setVisibility(View.GONE);
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    /**
     * Today's diet recycler view with CenterZoomLayoutManager class implemented
     */
    private void onSetYesterdayDietMenuRecyclerView() {
        todayDietModelArrayList = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        binding.recyclerView.setLayoutManager(layoutManager);
    }

    private void onFoodReplace(int mainItemPosition, int subItemPosition, DietEPlanFood todayDietMenu, Others others) {
        Dialog dialog = new Dialog(activity);
        AddDietMenuReplaceBinding binding = AddDietMenuReplaceBinding.inflate(getLayoutInflater());
        dialog.setContentView(binding.getRoot());//add_diet_menu_replace
        dialog.setCanceledOnTouchOutside(false);
        binding.tvReplcItmHeader.setText("Replace " + todayDietMenu.getFood().getFoodMasterName() + " \nwith below item");
        alReplaceItemModel = new ArrayList<>();
        LinearLayoutManager blogLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.rvReplaceItemList.setLayoutManager(blogLayoutManager);
        ReplaceItemAdapter replaceItemAdapter = new ReplaceItemAdapter(activity, alReplaceItemModel, food -> {
            if (food!=null) {
                apiInterface.replaceFood(new ReplaceFoodParam(todayDietMenu.getFood().getId(), food.getId(), todayDietMenu.getDietEDataId(), DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
                    @Override
                    public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
                        if (response.body() != null) {
                            if (response.body().getError()) {
                                for (int i = 0; i < response.body().getMessage().size(); i++)
                                    Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                            } else {
                                todayDietMenu.setFood(response.body().getData().getFood());
                                myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                            }
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                        dialog.dismiss();
                    }
                });
            } else {
                dialog.dismiss();
            }
        });
        binding.rvReplaceItemList.setAdapter(replaceItemAdapter);
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.ivClose.setOnClickListener(view -> dialog.dismiss());
        /*binding.btnReplaceItemOk.setOnClickListener(v -> {
            if (replaceItemAdapter.getLastSelectedFood() != null) {
                apiInterface.replaceFood(new ReplaceFoodParam(todayDietMenu.getFood().getId(), replaceItemAdapter.getLastSelectedFood().getId(), todayDietMenu.getDietEDataId(), DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
                    @Override
                    public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
                        if (response.body() != null) {
                            if (response.body().getError()) {
                                for (int i = 0; i < response.body().getMessage().size(); i++)
                                    Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                            } else {
                                todayDietMenu.setFood(response.body().getData().getFood());
                                myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                            }
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                        dialog.dismiss();
                    }
                });
            } else {
                dialog.dismiss();
            }
        });*/
        apiInterface.getFoodsByGroup(new FoodGroupParam(todayDietMenu.getFood())).enqueue(new Callback<Foods>() {
            @Override
            public void onResponse(@NonNull Call<Foods> call, @NonNull Response<Foods> response) {
                if (response.body() != null) {
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        Iterator<Food> i = response.body().getData().iterator();
                        while (i.hasNext()) {
                            Food e = i.next();
                            if (e.getId().equals(todayDietMenu.getFood().getId())) {
                                i.remove();
                            }
                        }
                        if (response.body().getData().size() == 0) {
                            dialog.dismiss();
                            Toast.makeText(activity, getString(R.string.there_is_no_food_to_replace), Toast.LENGTH_SHORT).show();
                        }
                        replaceItemAdapter.addAll(response.body().getData(),response.body().getOthers());
                        binding.progressBar.setVisibility(View.GONE);
                    } else {
                        dialog.dismiss();
                        Toast.makeText(activity, getString(R.string.there_is_no_food_to_replace), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Foods> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }


    public  void energycall(){

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String formattedDate = df.format(c);

        PlanParam planParam = new PlanParam(sessionManager.getDietEPlanId(),formattedDate);

        apiInterface.getDietPlans(planParam).enqueue(new Callback<Plans>() {
            @Override
            public void onResponse(@NonNull Call<Plans> call, @NonNull Response<Plans> response) {
                boolean isEnable = false;
                if (getArguments() != null) {
                    isEnable = getArguments().getBoolean("isEnable", false);
                }
                if (DayScheduleFragment.this.getParentFragment() != null) {

                    Log.d("hhhhh","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj getUtilized_energy"+response.body().getOthers().getUtilized_energy().toString()+getArguments().getString("tab"));
                    //if(getArguments().getString("tab").equals("today")) {
                        ((ScheduleFragmentTabSelection) DayScheduleFragment.this.getParentFragment()).requiredEnergyLiveDate.postValue(response.body().getOthers().getRequiredEnergy() + " / " + response.body().getOthers().getUtilized_energy().toString());
                    //}else{
                        //((ScheduleFragmentTabSelection) DayScheduleFragment.this.getParentFragment()).requiredEnergyLiveDate.postValue(response.body().getOthers().getRequiredEnergy() + " / " + "0");

                //    }
                }
                response.body().getOthers().setDate(planParam.getDate());
                myTodayScheduleRecyclerViewAdapter.updateData(response.body().getData(), response.body().getOthers(), isEnable);
                binding.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<Plans> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                binding.progress.setVisibility(View.GONE);
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
