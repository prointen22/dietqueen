package com.livennew.latestdietqueen.diet_info.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.diet_info.ISelectedItemListner;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Diet;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFoodMealByTag;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.MealsDataItem;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.utils.DateUtil;

import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {
    private List<Diet> mDietList;
    private Others mOthers;
    private boolean isToday;
    private final Context mContext;
    private Listener listener;
    private Listener2 listener2;
    private ScheduleItemMenuAdapter.Listener dietItemMenuAdapterListener;
    private final OnItemClickListener mClickListener;
    private ISelectedItemListner iSelectedItemListner;

    public ScheduleAdapter(Context mContext, List<Diet> mDietList, Others others, ISelectedItemListner iSelectedItemListner,OnItemClickListener listener) {
        this.mContext = mContext;
        this.mDietList = mDietList;
        this.mOthers = others;
        this.mClickListener = listener;
        this.iSelectedItemListner=iSelectedItemListner;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_row_parent, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Diet dietObj = mDietList.get(position);

        holder.mTvDiet.setText(dietObj.getPlanSubcategory());
        holder.mTvTime.setText(DateUtil.convertTime24To12(dietObj.getTime()));

        if (dietObj.getIsDone()) {
            holder.mIvAction.setClickable(false);
            if (dietObj.getIs_diet_done().equals("Y")) {
                holder.mIvAction.setBackgroundResource(R.drawable.ic_right_green);
            }else{
                holder.mIvAction.setBackgroundResource(R.drawable.ic_no_meal);
            }
        } else {
            holder.mIvAction.setClickable(true);
            holder.mIvAction.setBackgroundResource(R.drawable.ic_question_red);
        }

        if (dietObj.isMenuItemsVisible()) {
            holder.rvDietHorizantal.setVisibility(View.VISIBLE);
            holder.mCVMain.setBackgroundResource(R.drawable.bg_pink_rounded_corners);
            if (dietObj.getDietEPlanFoodByMealTag().size() > 0) {
                holder.mRvMealType.setVisibility(View.VISIBLE);
            } else {
                holder.mRvMealType.setVisibility(View.GONE);
            }
        } else {
            holder.rvDietHorizantal.setVisibility(View.GONE);
            holder.mRvMealType.setVisibility(View.GONE);
            holder.mCVMain.setBackgroundResource(R.drawable.bg_blue_rounded_corners);
        }

        if (dietObj.isDoneButtonVisible()) {
            holder.mIvAction.setVisibility(View.VISIBLE);
            holder.mIvExpand.setVisibility(View.GONE);
        } else {
            holder.mIvAction.setVisibility(View.GONE);
            holder.mIvExpand.setVisibility(View.VISIBLE);
        }

        if (dietObj.isMenuItemsVisible()) {
            holder.mIvExpand.setVisibility(View.GONE);
        }

        holder.mIvExpand.setOnClickListener(v ->
                expandButtonClick(dietObj, holder,position)
        );

        holder.mCVMain.setOnClickListener(v -> {
            if (!dietObj.isDoneButtonVisible()) {
                expandButtonClick(dietObj, holder,position);
                Log.i("TAG","position = "+position);
            } else {
                doneClick(dietObj, position);
            }
       });

        holder.mIvAction.setOnClickListener(View -> {
            doneClick(dietObj, position);
        });

        holder.mMealTypeAdapter = new MealTypeAdapter(mContext, dietObj.getDietEPlanFoodByMealTag(), (mealTypeObj, mealTypeItemPosition, isAPICall) -> {
            Log.v("TAG", "ScheduleAdapter -> Meal Type Obj name: "+ mealTypeObj.getName()+ "and id: "+mealTypeObj.getId());

           for (int i =0; i< dietObj.getDietEPlanFoodByMealTag().size();i++) {
               DietEPlanFoodMealByTag obj = dietObj.getDietEPlanFoodByMealTag().get(i);
               if (mealTypeObj.getId() == obj.getId()) {
                   obj.setIsUsersPreferredMeal(1);
               } else {
                   obj.setIsUsersPreferredMeal(0);
               }
           }

            setMealsAdapter(holder, mealTypeObj.getMealsData(), position, mealTypeItemPosition);
            if (isAPICall) {
                holder.mMealTypeAdapter.notifyDataSetChanged();
                mClickListener.onItemClick(dietObj, mealTypeObj);
            }
        });
        holder.mLayoutManagerMealType = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.mRvMealType.setLayoutManager(holder.mLayoutManagerMealType);
        holder.mRvMealType.setAdapter(holder.mMealTypeAdapter);
    }

    @Override
    public int getItemCount() {
        if (mDietList != null && !mDietList.isEmpty()) {
            return mDietList.size();
        } else {
            return 0;
        }
    }

    public void setListener(Listener listener, ScheduleItemMenuAdapter.Listener dietItemMenuAdapterListener) {
        this.listener = listener;
        this.dietItemMenuAdapterListener = dietItemMenuAdapterListener;
    }

    public void setListener2(Listener2 listener2, ScheduleItemMenuAdapter.Listener dietItemMenuAdapterListener) {
        this.listener2 = listener2;
        this.dietItemMenuAdapterListener = dietItemMenuAdapterListener;
    }

    public List<Diet> getList() {
        return mDietList;
    }

    public void updateSubListItem(int dietItemPosition, int mealTypeItemPosition, int mealItemPosition, Food foodItem) {
        mDietList.get(dietItemPosition).getDietEPlanFoodByMealTag().get(mealTypeItemPosition).getMealsData().get(mealItemPosition).setFood(foodItem);
//        notifyItemChanged(dietItemPosition);
//        mDietList.get(dietItemPosition).isExpanded();
        notifyDataSetChanged();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mCVMain;
        public TextView mTvDiet, mTvTime;
        ImageView mIvExpand, mIvAction;
        public RecyclerView rvDietHorizantal;
        public ScheduleItemMenuAdapter adapter;
        public RecyclerView.LayoutManager layoutManager;

        public RecyclerView mRvMealType;
        public MealTypeAdapter mMealTypeAdapter;
        public RecyclerView.LayoutManager mLayoutManagerMealType;

        public ViewHolder(View view) {
            super(view);
            mCVMain = view.findViewById(R.id.cv_main);
            mTvDiet = view.findViewById(R.id.tv_diet);
            mTvTime = view.findViewById(R.id.tv_time);
            mIvExpand = view.findViewById(R.id.iv_expand);
            mIvAction = view.findViewById(R.id.iv_action);
            rvDietHorizantal = view.findViewById(R.id.rv_hori_diet_item);
            mRvMealType = view.findViewById(R.id.rv_meal_type);
        }

    }

    public interface Listener {
        void onDoneClick(int position, Diet todayDietMenuModel, Others others);
    }

    public interface Listener2 {
        void showSelectedItem(int position);
    }

    public interface Listener3 {
        void showSelectedItem(int position);
    }

    public interface OnItemClickListener {
        void onItemClick(Diet dietObj, DietEPlanFoodMealByTag mealTypeObj);
    }

    public void expandButtonClick(Diet dietObj, ViewHolder holder, int position) {
        if (dietObj.isCanExpanded()) {
            if (dietObj.isExpanded()) {
                holder.rvDietHorizantal.setVisibility(View.GONE);
                holder.mRvMealType.setVisibility(View.GONE);
                dietObj.setExpanded(false);
                holder.mIvExpand.setVisibility(View.VISIBLE);
            } else {
                holder.rvDietHorizantal.setVisibility(View.VISIBLE);
                holder.mRvMealType.setVisibility(View.VISIBLE);
                dietObj.setExpanded(true);
                holder.mIvExpand.setVisibility(View.GONE);
                if (iSelectedItemListner != null) {
                    iSelectedItemListner.showSelectedItem(position);
                }

            }
        }
    }

    private void setMealsAdapter(ViewHolder holder, List<MealsDataItem> mealsList, int position, int mealTypeItemPosition) {
        holder.adapter = new ScheduleItemMenuAdapter(mContext, mealsList, position, mealTypeItemPosition, mOthers, dietItemMenuAdapterListener);
        holder.layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        holder.rvDietHorizantal.setLayoutManager(holder.layoutManager);
        holder.rvDietHorizantal.setAdapter(holder.adapter);
    }

    private void doneClick(Diet dietObj, int position) {
        Log.v("TAG", "Done icon clicked");
        if (dietObj.getIsDone()) {
            Log.v("TAG", "Already Done");
            return;
        }
        if (listener != null) {
            Log.v("TAG", "Done click:" + position + dietObj.getIs_diet_done() + mOthers.getDate());
            listener.onDoneClick(position, dietObj, mOthers);
        }
    }
}
