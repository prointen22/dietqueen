package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Diet;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.utils.DateUtil;

import java.util.ArrayList;
import java.util.List;

public class MyTodayScheduleRecyclerViewAdapter extends RecyclerView.Adapter<MyTodayScheduleRecyclerViewAdapter.ViewHolder> {

    private List<Diet> alTodayDietMenuModel;
    private Others others;
    private boolean isToday;
    private Context mContext;
    private Listener listener;

    private   Listener2 listener2;
    private DietItemMenuAdapter.Listener dietItemMenuAdapterListener;

    public MyTodayScheduleRecyclerViewAdapter(Context mContext, ArrayList<Diet> alTodayDietMenuModel) {
        this.mContext = mContext;
        this.alTodayDietMenuModel = alTodayDietMenuModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_today_schedule, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Diet
                todayDietMenuModel = alTodayDietMenuModel.get(position);
        holder.tvDietTime.setText(DateUtil.convertTime24To12(todayDietMenuModel.getTime()) + " - " + todayDietMenuModel.getPlanSubcategory());
        if (todayDietMenuModel.getIsDone() && !isToday) {
            holder.tvDietDone.setTextColor(mContext.getResources().getColor(R.color.gray_400));
            holder.tvDietDone.setClickable(false);
            holder.tvDietDone.setVisibility(View.VISIBLE);
//*********************************************************************


        } else if (todayDietMenuModel.getIsDone() && isToday) {
            holder.tvDietDone.setTextColor(mContext.getResources().getColor(R.color.gray_400));
            holder.tvDietDone.setClickable(false);
            holder.tvDietDone.setVisibility(View.VISIBLE);

            holder.tvDietDone.setVisibility(View.INVISIBLE);
            holder.tv_diet_udone.setVisibility(View.VISIBLE);
        } else if (!todayDietMenuModel.getIsDone() && !isToday) {
            holder.tvDietDone.setVisibility(View.INVISIBLE);
            holder.tv_diet_udone.setVisibility(View.INVISIBLE);
        } else {
            holder.tvDietDone.setTextColor(mContext.getResources().getColor(R.color.checktoggle));
            holder.tvDietDone.setClickable(true);
            holder.tvDietDone.setVisibility(View.VISIBLE);
            holder.tv_diet_udone.setVisibility(View.INVISIBLE);
        }

        if (holder.tvDietDone.isClickable()) {
            holder.tvDietDone.setOnClickListener(View -> {
                if (listener != null) {
                    Log.d("kkkkkk",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   fff"+position+todayDietMenuModel.getIs_diet_done()+others.getDate());
                    listener.onDoneClick(position, todayDietMenuModel, others);
                    holder.tv_diet_udone.setVisibility(View.VISIBLE);
                    holder.tvDietDone.setVisibility(View.GONE);

                    //    loadFragment(MainDietContainerFragment.getInstance());
                }
            });
        }


        holder.tv_diet_udone.setOnClickListener(View -> {

                Log.d("kkkkkk",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> tv_diet_udone  fff"+position+todayDietMenuModel.getIsDone()+others.getDate());

            listener2.onunDoneClick(position, todayDietMenuModel, others);
            holder.tvDietDone.setVisibility(View.VISIBLE);
            holder.tv_diet_udone.setVisibility(View.GONE);
            //  loadFragment(MainDietContainerFragment.getInstance());
            //holder.tv_diet_udone.setVisibility(View.GONE);
            //holder.tvDietDone.setVisibility(View.VISIBLE);
        });

        holder.adapter = new DietItemMenuAdapter(mContext, todayDietMenuModel.getDietEPlanFood(), position, others, dietItemMenuAdapterListener,isToday);
        holder.layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.rvDietHorizantal.setLayoutManager(holder.layoutManager);
        holder.rvDietHorizantal.setAdapter(holder.adapter);
    }

    @Override
    public int getItemCount() {
        if (alTodayDietMenuModel != null && !alTodayDietMenuModel.isEmpty()) {
            return alTodayDietMenuModel.size();
        } else {
            return 0;
        }
    }

    public void updateData(List<Diet> data, Others others, boolean isEnable) {
        this.alTodayDietMenuModel = data;
        this.others = others;
        this.isToday = isEnable;
        notifyDataSetChanged();

    }

    public void setListener(Listener listener, DietItemMenuAdapter.Listener dietItemMenuAdapterListener) {
        this.listener = listener;
        this.dietItemMenuAdapterListener = dietItemMenuAdapterListener;
    }

    public void setListener2(Listener2 listener2, DietItemMenuAdapter.Listener dietItemMenuAdapterListener) {
        this.listener2 = listener2;
        this.dietItemMenuAdapterListener = dietItemMenuAdapterListener;
    }

    public List<Diet> getList() {
        return alTodayDietMenuModel;
    }

    public void updateSubListItem(int mainItemPosition, int subItemPosition, DietEPlanFood dietEPlanFood) {
        ((Diet) alTodayDietMenuModel.get(mainItemPosition)).getDietEPlanFood().set(subItemPosition, dietEPlanFood);
        notifyItemChanged(mainItemPosition);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvDietDone, tvDietEdit, tvDietTime,tv_diet_udone;
        public RecyclerView rvDietHorizantal;
        public DietItemMenuAdapter adapter;
        public RecyclerView.LayoutManager layoutManager;

        public ViewHolder(View view) {
            super(view);
            tvDietTime = view.findViewById(R.id.tvDieTime);
            tvDietEdit = view.findViewById(R.id.tv_diet_edit);
            tvDietDone = view.findViewById(R.id.tv_diet_done);
            tv_diet_udone= view.findViewById(R.id.tv_diet_udone);
            rvDietHorizantal = view.findViewById(R.id.rv_hori_diet_item);
        }

    }

    public interface Listener {
        void onDoneClick(int position, Diet todayDietMenuModel, Others others);

    }


    public interface Listener2 {
         void onunDoneClick(int position, Diet todayDietMenuModel, Others others) ;



    }


    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = ((AppCompatActivity)
                mContext).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
