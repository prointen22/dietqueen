package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateMealPrefParam {
    @SerializedName("meal_tag_id")
    @Expose
    private int id;
    @SerializedName("diet_e_plan_id")
    @Expose
    private int planId;
    @SerializedName("start_date")
    @Expose
    private String start_date;
    @SerializedName("end_date")
    @Expose
    private String end_date;
    @SerializedName("time")
    @Expose
    private String time;



    public UpdateMealPrefParam(int id,int planId, String start_date, String end_date, String time) {
        this.id = id;
        this.planId = planId;
        this.start_date = start_date;
        this.end_date = end_date;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int id) {
        this.planId = planId;
    }


    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(int id) {
        this.start_date = start_date;
    }


    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(int id) {
        this.end_date = end_date;
    }


    public String getTime() {
        return time;
    }

    public void setTime(int id) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "UpdateMealPrefParam{" +
                "id=" + id +
                '}';
    }
}
