package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.xml.sax.helpers.AttributesImpl;

public class DoneParam {
    @SerializedName("diet_e_data_id")
    @Expose
    private int id;
    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("is_done")
    @Expose
    private String is_done;

    @SerializedName("reason")
    @Expose
    private String reason;


    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("food_details")
    @Expose
    private String food_details;

    public DoneParam(int id, String date) {
        this.date = date;
        this.id = id;
    }

    public DoneParam(int id, String date,String is_done,String time, String food_details) {
        this.date = date;
        this.id = id;
        this.is_done = is_done;
        this.time = time;
        this.food_details = food_details;
    }

    public DoneParam(int id, String date,String is_done, String reason,String time, String food_details) {
        this.date = date;
        this.id = id;
        this.is_done = is_done;
        this.reason = reason;
        this.time = time;
        this.food_details = food_details;
    }

    public DoneParam() {

        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time){this.time = time;}

    public String getTime(){return time;}

    public void setFood_details(String food_details){this.food_details = food_details;}

    public String getFood_details(){return food_details;}

    public String getIs_done() {
        return is_done;
    }

    public void setIs_done(String is_done) {
        this.is_done = is_done;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "PlanParam{" +
                "id=" + id +
                ", date='" + date + '\'' +
                '}';
    }
}
