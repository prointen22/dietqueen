package com.livennew.latestdietqueen.diet_info.adapter

import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import com.livennew.latestdietqueen.R
import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import android.widget.TextView
import com.livennew.latestdietqueen.Util
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.MealsDataItem
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others
import de.hdodenhof.circleimageview.CircleImageView

class ScheduleItemMenuAdapterNew(private val mContext: Context, private val alTodayDietMenu: List<MealsDataItem>?, private val others: Others) : RecyclerView.Adapter<ScheduleItemMenuAdapterNew.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.diet_update_row, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val todayDietMenu = alTodayDietMenu!![position]
        Glide.with(mContext).load(others.dietPlanImagePath + todayDietMenu.food.image).into(holder.mIvDietMenu)
        holder.mTvDietMenuNm.text = todayDietMenu.food.foodMasterName
        holder.mTvDietMenuQty.text = getDietQty(Util.calculateQty(todayDietMenu, others), todayDietMenu.food.unit.name)
    }

    private fun getDietQty(calculateQty: Long, name: String): String {
        return if (calculateQty == 0L) "NA" else "$calculateQty $name"
    }

    override fun getItemCount(): Int {
        return if (alTodayDietMenu != null && !alTodayDietMenu.isEmpty()) {
            alTodayDietMenu.size
        } else {
            0
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var mIvDietMenu: CircleImageView
        var mTvDietMenuNm: TextView
        var mTvDietMenuQty: TextView

        init {
            mIvDietMenu = view.findViewById(R.id.civ_diet_menu)
            mTvDietMenuNm = view.findViewById(R.id.tv_diet_item)
            mTvDietMenuQty = view.findViewById(R.id.tv_diet_qty)
        }
    }
}