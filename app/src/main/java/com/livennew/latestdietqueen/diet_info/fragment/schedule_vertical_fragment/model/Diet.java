package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class  Diet implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("energy")
    @Expose
    private String energy;

    @SerializedName("is_diet_done")
    @Expose
    private String is_diet_done;

    @SerializedName("aos_version")
    @Expose
    private String aos_version;
    @SerializedName("is_terms_accepted")
    @Expose
    private Integer is_terms_accepted;

    protected Diet(Parcel in) {
        id = in.readInt();
        time = in.readString();
        energy = in.readString();
        is_diet_done = in.readString();
        aos_version = in.readString();
        if (in.readByte() == 0) {
            is_terms_accepted = null;
        } else {
            is_terms_accepted = in.readInt();
        }
        time_difference = in.readString();
        planSubcategory = in.readString();
        byte tmpIsDone = in.readByte();
        isDone = tmpIsDone == 0 ? null : tmpIsDone == 1;
        editable = in.readByte() != 0;
        isMenuItemsVisible = in.readByte() != 0;
        canExpanded = in.readByte() != 0;
        isExpanded = in.readByte() != 0;
        isDoneButtonVisible = in.readByte() != 0;
    }

    public static final Creator<Diet> CREATOR = new Creator<Diet>() {
        @Override
        public Diet createFromParcel(Parcel in) {
            return new Diet(in);
        }

        @Override
        public Diet[] newArray(int size) {
            return new Diet[size];
        }
    };

    public Integer getIs_terms_accepted(){
        return is_terms_accepted;
    }

    public void setIs_terms_accepted(){this.is_terms_accepted =is_terms_accepted;}

    public  String getAos_version(){
        return aos_version;
    }
    public void setAos_version(){
        this.aos_version =aos_version;
    }

    public String getIs_diet_done() {
        return is_diet_done;
    }

    public void setIs_diet_done(String is_diet_done) {
        this.is_diet_done = is_diet_done;
    }

    public Boolean getDone() {
        return isDone;
    }

    public void setDone(Boolean done) {
        isDone = done;
    }

    @SerializedName("time_difference")
    @Expose
    private String time_difference;

    public String getTime_difference() {
        return time_difference;
    }

    public void setTime_difference(String time_difference) {
        this.time_difference = time_difference;
    }

    @SerializedName("plan_subcategory")
    @Expose
    private String planSubcategory;

    @SerializedName("is_done")
    @Expose
    private Boolean isDone;

    @SerializedName("diet_e_plan_food")
    @Expose
    private List<DietEPlanFood> dietEPlanFood = null;

    @SerializedName("diet_e_plan_food_by_meal_tag")
    @Expose
    private List<DietEPlanFoodMealByTag> dietEPlanFoodByMealTag = null;

    private boolean editable;

    private boolean isMenuItemsVisible, canExpanded, isExpanded, isDoneButtonVisible = true;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public String getPlanSubcategory() {
        return planSubcategory;
    }

    public void setPlanSubcategory(String planSubcategory) {
        this.planSubcategory = planSubcategory;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

    public List<DietEPlanFood> getDietEPlanFood() {
        return dietEPlanFood;
    }

    public void setDietEPlanFood(List<DietEPlanFood> dietEPlanFood) {
        this.dietEPlanFood = dietEPlanFood;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isMenuItemsVisible() {
        return isMenuItemsVisible;
    }

    public void setMenuItemsVisible(boolean menuItemsVisible) {
        isMenuItemsVisible = menuItemsVisible;
    }

    public boolean isCanExpanded() {
        return canExpanded;
    }

    public void setCanExpanded(boolean canExpanded) {
        this.canExpanded = canExpanded;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public boolean isDoneButtonVisible() {
        return isDoneButtonVisible;
    }

    public void setDoneButtonVisible(boolean doneButtonVisible) {
        isDoneButtonVisible = doneButtonVisible;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<DietEPlanFoodMealByTag> getDietEPlanFoodByMealTag() {
        return dietEPlanFoodByMealTag;
    }

    public void setDietEPlanFoodByMealTag(List<DietEPlanFoodMealByTag> dietEPlanFoodByMealTag) {
        this.dietEPlanFoodByMealTag = dietEPlanFoodByMealTag;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(time);
        dest.writeString(energy);
        dest.writeString(is_diet_done);
        dest.writeString(aos_version);
        if (is_terms_accepted == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(is_terms_accepted);
        }
        dest.writeString(time_difference);
        dest.writeString(planSubcategory);
        dest.writeByte((byte) (isDone == null ? 0 : isDone ? 1 : 2));
        dest.writeByte((byte) (editable ? 1 : 0));
        dest.writeByte((byte) (isMenuItemsVisible ? 1 : 0));
        dest.writeByte((byte) (canExpanded ? 1 : 0));
        dest.writeByte((byte) (isExpanded ? 1 : 0));
        dest.writeByte((byte) (isDoneButtonVisible ? 1 : 0));
    }
}