package com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Others implements Parcelable {

    @SerializedName("dietPlanImagePath")
    @Expose
    private String dietPlanImagePath;
    @SerializedName("required_energy")
    @Expose
    private Integer requiredEnergy;

    @SerializedName("utilized_energy")
    @Expose
    private Integer utilized_energy;

    @SerializedName("remaining_energy")
    @Expose
    private Integer remaining_energy;

    protected Others(Parcel in) {
        dietPlanImagePath = in.readString();
        if (in.readByte() == 0) {
            requiredEnergy = null;
        } else {
            requiredEnergy = in.readInt();
        }
        if (in.readByte() == 0) {
            utilized_energy = null;
        } else {
            utilized_energy = in.readInt();
        }
        if (in.readByte() == 0) {
            remaining_energy = null;
        } else {
            remaining_energy = in.readInt();
        }
        date = in.readString();
    }

    public static final Creator<Others> CREATOR = new Creator<Others>() {
        @Override
        public Others createFromParcel(Parcel in) {
            return new Others(in);
        }

        @Override
        public Others[] newArray(int size) {
            return new Others[size];
        }
    };

    public Integer getRemaining_energy() {
        return remaining_energy;
    }

    public void setRemaining_energy(Integer remaining_energy) {
        this.remaining_energy = remaining_energy;
    }

    private String date;

    public Integer getUtilized_energy() {
        return utilized_energy;
    }

    public void setUtilized_energy(Integer utilized_energy) {
        this.utilized_energy = utilized_energy;
    }

    public String getDietPlanImagePath() {
        return dietPlanImagePath;
    }

    public void setDietPlanImagePath(String dietPlanImagePath) {
        this.dietPlanImagePath = dietPlanImagePath;
    }

    public Integer getRequiredEnergy() {
        return requiredEnergy;
    }

    public void setRequiredEnergy(Integer requiredEnergy) {
        this.requiredEnergy = requiredEnergy;
    }

    @Override
    public String toString() {
        return "Others{" +
                "dietPlanImagePath='" + dietPlanImagePath + '\'' +
                ", requiredEnergy=" + requiredEnergy +
                '}';
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dietPlanImagePath);
        if (requiredEnergy == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(requiredEnergy);
        }
        if (utilized_energy == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(utilized_energy);
        }
        if (remaining_energy == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(remaining_energy);
        }
        dest.writeString(date);
    }
}