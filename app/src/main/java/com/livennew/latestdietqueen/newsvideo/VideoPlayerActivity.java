package com.livennew.latestdietqueen.newsvideo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.livennew.latestdietqueen.R;

public class VideoPlayerActivity extends AppCompatActivity {
    private VideoView videoView;
    ProgressBar progressBar = null;
    Toolbar toolbarVideoPlayer;
    String videoLink = "";
    private MediaController mediaControls;
    String TAG = "VideoPlayer";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        init();
        setSupportActionBar(toolbarVideoPlayer);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            videoLink = extras.getString("video_thumbnail");
        } else {
            Toast.makeText(this, "Data not available", Toast.LENGTH_SHORT).show();
        }
        toolbarVideoPlayer.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
        configureVideoView();


    }

    public void init() {
        videoView = findViewById(R.id.diet_video);
        toolbarVideoPlayer = findViewById(R.id.toolbar_video_player);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
    }

    private void configureVideoView() {

        if (mediaControls == null) {
            // create an object of media controller class
            mediaControls = new MediaController(VideoPlayerActivity.this);
            mediaControls.setAnchorView(videoView);
        }
        // set the media controller for video view
        videoView.setMediaController(mediaControls);
        // set the uri for the video view
        // videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.fishvideo));
        videoView.setVideoPath(videoLink);
        // start a video
        videoView.start();
        progressBar.setVisibility(View.VISIBLE);
        // implement on completion listener on video view
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.start();
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int arg1,
                                                   int arg2) {
                        // TODO Auto-generated method stub
                        progressBar.setVisibility(View.GONE);
                        mp.start();
                    }
                });
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Toast.makeText(getApplicationContext(), "Thank You...!!!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(getApplicationContext(), "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
                return false;
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
