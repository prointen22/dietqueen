package com.livennew.latestdietqueen.newsvideo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.fragments.home.model.VideoModel;

import java.util.ArrayList;

public class NewsVideoListAdapter extends RecyclerView.Adapter<NewsVideoListAdapter.MyViewHolder> {

    private ArrayList<VideoModel> alNewsVideoList;
    private Context context;
    private Listener listener;


    public NewsVideoListAdapter(Context context, ArrayList<VideoModel> alNewsVideoList) {
        this.context = context;
        this.alNewsVideoList = alNewsVideoList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_video_content, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final VideoModel videoModel = alNewsVideoList.get(position);
        RequestOptions requestOptions = new RequestOptions();
        Glide.with(context)
                .load(videoModel.getVideoURL())
                .apply(requestOptions)
                .thumbnail(Glide.with(context).load(videoModel.getVideoURL()))
                .into(holder.ivListBlog);
        if (videoModel.getVideoTitle().isEmpty()) {
            holder.tvListBlogDesc.setText(videoModel.getVideoDesc());
        } else {
            holder.tvListBlogDesc.setText(videoModel.getVideoTitle());
        }
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null)
                    listener.onItemClick(videoModel,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return alNewsVideoList.size();
    }

    public void setListener(Listener listener) {

        this.listener = listener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivListBlog;
        TextView tvListBlogDesc;
        private View view;

        public MyViewHolder(@NonNull View view) {
            super(view);
            ivListBlog = view.findViewById(R.id.iv_thumbnail);
            tvListBlogDesc = view.findViewById(R.id.tv_video_desc);
            this.view = view;
        }
    }

    public interface Listener {
        void onItemClick(VideoModel videoModel, int position);
    }
}