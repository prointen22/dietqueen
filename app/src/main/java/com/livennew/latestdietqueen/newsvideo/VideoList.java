package com.livennew.latestdietqueen.newsvideo;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.databinding.ActivityVideoListBinding;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTestimonialAdapter;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class VideoList extends FabActivity {
    private ActivityVideoListBinding binding;
    private Button btnNewsBlogReload;
    private int lastPage = 0;
    private String curapgeNo = "1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityVideoListBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorAccent));
        setSupportActionBar(binding.toolbarVideoList);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        binding.toolbarVideoList.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }




}