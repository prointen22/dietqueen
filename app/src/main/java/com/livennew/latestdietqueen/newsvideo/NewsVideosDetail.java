package com.livennew.latestdietqueen.newsvideo;

import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class NewsVideosDetail extends FabActivity {
    private Toolbar toolbarVideoDesc;
    private TextView tvVideoDetailDesc, tvVideoTitle;
    private ImageView ivVideo;
    private String videoThumbnail = ""/*, txtVideoDesc = "", videoDetailLike = ""*/, type = "";
    private AppBarLayout mAppBarLayoutVideo;
    //private FloatingActionButton fabVideoDetailLike;
    private int video_id = 0;
    private static final String TAG = "Erros";
    @Inject
    SessionManager sessionManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos_news_detail);
        user=sessionManager.getUser();
        init();
        setSupportActionBar(toolbarVideoDesc);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbarVideoDesc.setNavigationIcon(R.drawable.ic_navigation_before_pink_24dp);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            // if (extras.getString("type") != null) {
            video_id = extras.getInt("video_id");
            type = extras.getString("type");
            if (SupportClass.checkConnection(this)) {
                ExclusiveDetails();
            } else {
                SupportClass.noInternetConnectionToast(this);
            }
            /*} else {
                video_id = extras.getInt("video_id");
                videoThumbnail = extras.getString("video_thumbnail");
                txtVideoDesc = extras.getString("video_desc");
                videoDetailLike = extras.getString("video_status");
            }*/
        } else {
            Toast.makeText(this, "Data not available", Toast.LENGTH_SHORT).show();
        }

        /*if (videoDetailLike.equalsIgnoreCase("L")) {
            fabVideoDetailLike.setImageResource(R.drawable.like);
        } else {
            fabVideoDetailLike.setImageResource(R.drawable.unlike);
        }
        fabVideoDetailLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videoDetailLike.equalsIgnoreCase("L")) {
                    fabVideoDetailLike.setImageResource(R.drawable.unlike);
                    videoDetailLike = "U";
                    Toast.makeText(NewsVideosDetail.this, "You select " + videoDetailLike, Toast.LENGTH_SHORT).show();
                } else {
                    fabVideoDetailLike.setImageResource(R.drawable.like);
                    videoDetailLike = "L";
                    Toast.makeText(NewsVideosDetail.this, "You select " + videoDetailLike, Toast.LENGTH_SHORT).show();
                }
            }
        });*/
        /*if (type.equalsIgnoreCase("")) {
            tvVideoTitle.setText("Dummy Title");
            tvVideoDetailDesc.setText(txtVideoDesc);
            Glide.with(this).load(videoThumbnail).into(ivVideo);
        }*/
        ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewsVideosDetail.this, VideoPlayerActivity.class);
                intent.putExtra("video_thumbnail", videoThumbnail);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        });
        mAppBarLayoutVideo.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                } else if (isShow) {
                    isShow = false;
                }
            }
        });
        mAppBarLayoutVideo.setBackgroundColor(getResources().getColor(R.color.white));
    }

    public void init() {
        toolbarVideoDesc = findViewById(R.id.toolbar_video_desc);
        tvVideoDetailDesc = findViewById(R.id.tv_video_detail_desc);
        ivVideo = findViewById(R.id.iv_video);
        mAppBarLayoutVideo = findViewById(R.id.app_bar_video_detail);
        tvVideoTitle = findViewById(R.id.tv_video_title);
        //fabVideoDetailLike = findViewById(R.id.fab_like_video);
    }

    private void ExclusiveDetails() {
        try {
            // llProgress.setVisibility(View.VISIBLE);
            // rvNewsVideoList.setVisibility(View.GONE);
            JSONObject params = new JSONObject();
            params.put("id", video_id);
            params.put("type", type);
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.EXCLUSIVE, params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                //       llProgress.setVisibility(View.GONE);
                //     rvNewsVideoList.setVisibility(View.VISIBLE);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    // NewTractors newTractors = new Gson().fromJson(response.toString(), NewTractors.class);
                    //if (!newTractors.isError()) {
                    if (!error) {
                        JSONObject jsonObject1 = jObj.getJSONObject("data");
                        //totPagesTrac = newTractors.getTotPages();
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
                        String exclusiveImagePath = jsonObjectOther.getString("exclusiveImagePath");

                        tvVideoTitle.setText(jsonObject1.getString("title"));
                        tvVideoDetailDesc.setText(jsonObject1.getString("description"));
                        videoThumbnail = exclusiveImagePath + jsonObject1.getString("upload_file");
                        Glide.with(this).load(videoThumbnail).into(ivVideo);
                    } else {
                        //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                        Toast.makeText(this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}