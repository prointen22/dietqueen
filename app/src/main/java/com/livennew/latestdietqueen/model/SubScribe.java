package com.livennew.latestdietqueen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.fragments.products.model.Product;

public class SubScribe {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("diet_plan_id")
    @Expose
    private String dietPlanId;
    @SerializedName("diet_plan")
    @Expose
    private Product dietPlan;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDietPlanId() {
        return dietPlanId;
    }

    public void setDietPlanId(String dietPlanId) {
        this.dietPlanId = dietPlanId;
    }

    public Product getDietPlan() {
        return dietPlan;
    }

    public void setDietPlan(Product dietPlan) {
        this.dietPlan = dietPlan;
    }
}
