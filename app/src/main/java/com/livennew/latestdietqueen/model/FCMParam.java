package com.livennew.latestdietqueen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FCMParam {
    @SerializedName("fcm_token")
    @Expose
    private String fcmToken;
    @SerializedName("device_type")
    @Expose
    private String deviceType = "android";
    @SerializedName("notification_type")
    @Expose
    private String notificationType = "dietqueen";

    public FCMParam(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    @Override
    public String toString() {
        return "FCMParam{" +
                "fcmToken='" + fcmToken + '\'' +
                ", deviceType='" + deviceType + '\'' +
                ", notificationType='" + notificationType + '\'' +
                '}';
    }
}
