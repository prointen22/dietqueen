package com.livennew.latestdietqueen.model;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({FoodType.VEG, FoodType.NON_VEG, FoodType.VEG_EGG})
@Retention(RetentionPolicy.SOURCE)
public @interface FoodType {
    String VEG = "v";
    String NON_VEG = "nv";
    String VEG_EGG = "ve";
}
