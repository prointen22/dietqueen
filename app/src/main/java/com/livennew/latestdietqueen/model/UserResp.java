package com.livennew.latestdietqueen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserResp extends MyResponse implements Parcelable {
    @SerializedName("data")
    @Expose
    private User data;

    @SerializedName("others")
    @Expose
    private Others others;

    public User getData() {
        return data;
    }

    public void setData(User data) {
        this.data = data;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }

    public class Others implements Parcelable {
        @SerializedName("user_profile_images")
        @Expose
        private String userProfileImages;
        @SerializedName("userImageBase64Path")
        @Expose
        private String userImageBase64Path;

        public String getUserProfileImages() {
            return userProfileImages;
        }

        public void setUserProfileImages(String userProfileImages) {
            this.userProfileImages = userProfileImages;
        }

        public String getUserImageBase64Path() {
            return userImageBase64Path;
        }

        public void setUserImageBase64Path(String userImageBase64Path) {
            this.userImageBase64Path = userImageBase64Path;
        }

        @Override
        public String toString() {
            return "Others{" +
                    "userProfileImages='" + userProfileImages + '\'' +
                    ", userImageBase64Path='" + userImageBase64Path + '\'' +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.userProfileImages);
            dest.writeString(this.userImageBase64Path);
        }

        public Others() {
        }

        protected Others(Parcel in) {
            this.userProfileImages = in.readString();
            this.userImageBase64Path = in.readString();
        }

        public final Creator<Others> CREATOR = new Creator<Others>() {
            @Override
            public Others createFromParcel(Parcel source) {
                return new Others(source);
            }

            @Override
            public Others[] newArray(int size) {
                return new Others[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.data, flags);
        dest.writeParcelable(this.others, flags);
    }

    public UserResp() {
    }

    protected UserResp(Parcel in) {
        this.data = in.readParcelable(User.class.getClassLoader());
        this.others = in.readParcelable(Others.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserResp> CREATOR = new Parcelable.Creator<UserResp>() {
        @Override
        public UserResp createFromParcel(Parcel source) {
            return new UserResp(source);
        }

        @Override
        public UserResp[] newArray(int size) {
            return new UserResp[size];
        }
    };
}
