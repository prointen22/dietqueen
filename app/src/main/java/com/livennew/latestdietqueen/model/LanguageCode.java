package com.livennew.latestdietqueen.model;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({LanguageCode.ENGLISH, LanguageCode.HINDI, LanguageCode.MARATHI, LanguageCode.GUJARATI, LanguageCode.TELUGU, LanguageCode.TAMIL, LanguageCode.KANNADA, LanguageCode.PUNJABI, LanguageCode.BENGALI})
@Retention(RetentionPolicy.SOURCE)
public @interface LanguageCode {
    String ENGLISH = "en";
    String HINDI = "hi";
    String MARATHI = "mr";
    String GUJARATI = "gu";
    String TELUGU = "te";
    String TAMIL = "ta";
    String KANNADA = "kn";
    String PUNJABI = "pa";
    String BENGALI = "bn";
}