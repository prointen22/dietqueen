package com.livennew.latestdietqueen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetail implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("waistline")
    @Expose
    private String waistline;
    @SerializedName("food")
    @Expose
    @FoodType
    private String food;
    @SerializedName("job_type")
    @Expose
    @JobType
    private String jobType;
    @SerializedName("wake_up")
    @Expose
    private String wakeUp;
    @SerializedName("sleep")
    @Expose
    private String sleep;
    @SerializedName("office_hrs_from")
    @Expose
    private String officeHrsFrom;
    @SerializedName("office_hrs_to")
    @Expose
    private String officeHrsTo;
    @SerializedName("breakfast")
    @Expose
    private String breakfast;
    @SerializedName("lunch")
    @Expose
    private String lunch;
    @SerializedName("dinner")
    @Expose
    private String dinner;
    @SerializedName("language")
    @Expose
    private Language language;
    @SerializedName("profession")
    @Expose
    private Profession profession;
    @SerializedName("required_energy")
    @Expose
    private String requiredEnergy;
    @SerializedName("complaint")
    @Expose
    private String complaint;
    @SerializedName("past_history")
    @Expose
    private String pastHistory;
    @SerializedName("personal_history")
    @Expose
    private String personalHistory;
    @SerializedName("family_history")
    @Expose
    private String familyHistory;
    @SerializedName("hours_of_sleep")
    @Expose
    private String hoursOfSleep;
    @SerializedName("region")
    @Expose
    private Region region;

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    @SerializedName("request_type")
    @Expose
    private String request_type;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWaistline() {
        return waistline;
    }

    public void setWaistline(String waistline) {
        this.waistline = waistline;
    }


    public String getFood() {
        return food;
    }

    public void setFood(@FoodType String food) {
        this.food = food;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(@JobType String jobType) {
        this.jobType = jobType;
    }

    public String getWakeUp() {
        return wakeUp;
    }

    public void setWakeUp(String wakeUp) {
        this.wakeUp = wakeUp;
    }

    public String getSleep() {
        return sleep;
    }

    public void setSleep(String sleep) {
        this.sleep = sleep;
    }

    public String getOfficeHrsFrom() {
        return officeHrsFrom;
    }

    public void setOfficeHrsFrom(String officeHrsFrom) {
        this.officeHrsFrom = officeHrsFrom;
    }

    public String getOfficeHrsTo() {
        return officeHrsTo;
    }

    public void setOfficeHrsTo(String officeHrsTo) {
        this.officeHrsTo = officeHrsTo;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public String getDinner() {
        return dinner;
    }

    public void setDinner(String dinner) {
        this.dinner = dinner;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public String getRequiredEnergy() {
        return requiredEnergy;
    }

    public void setRequiredEnergy(String requiredEnergy) {
        this.requiredEnergy = requiredEnergy;
    }

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getPastHistory() {
        return pastHistory;
    }

    public void setPastHistory(String pastHistory) {
        this.pastHistory = pastHistory;
    }

    public String getPersonalHistory() {
        return personalHistory;
    }

    public void setPersonalHistory(String personalHistory) {
        this.personalHistory = personalHistory;
    }

    public String getFamilyHistory() {
        return familyHistory;
    }

    public void setFamilyHistory(String familyHistory) {
        this.familyHistory = familyHistory;
    }

    public String getHoursOfSleep() {
        return hoursOfSleep;
    }

    public void setHoursOfSleep(String hoursOfSleep) {
        this.hoursOfSleep = hoursOfSleep;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.height);
        dest.writeString(this.weight);
        dest.writeString(this.waistline);
        dest.writeString(this.food);
        dest.writeString(this.jobType);
        dest.writeString(this.wakeUp);
        dest.writeString(this.sleep);
        dest.writeString(this.officeHrsFrom);
        dest.writeString(this.officeHrsTo);
        dest.writeString(this.breakfast);
        dest.writeString(this.lunch);
        dest.writeString(this.dinner);
        dest.writeParcelable(this.language, flags);
        dest.writeParcelable(this.profession, flags);
        dest.writeString(this.requiredEnergy);
        dest.writeString(this.complaint);
        dest.writeString(this.pastHistory);
        dest.writeString(this.personalHistory);
        dest.writeString(this.familyHistory);
        dest.writeString(this.hoursOfSleep);
        dest.writeParcelable(this.region, flags);
    }

    public UserDetail() {
    }

    protected UserDetail(Parcel in) {
        this.id = in.readInt();
        this.height = in.readString();
        this.weight = in.readString();
        this.waistline = in.readString();
        this.food = in.readString();
        this.jobType = in.readString();
        this.wakeUp = in.readString();
        this.sleep = in.readString();
        this.officeHrsFrom = in.readString();
        this.officeHrsTo = in.readString();
        this.breakfast = in.readString();
        this.lunch = in.readString();
        this.dinner = in.readString();
        this.language = in.readParcelable(Language.class.getClassLoader());
        this.profession = in.readParcelable(Profession.class.getClassLoader());
        this.requiredEnergy = in.readString();
        this.complaint = in.readString();
        this.pastHistory = in.readString();
        this.personalHistory = in.readString();
        this.familyHistory = in.readString();
        this.hoursOfSleep = in.readString();
        this.region = in.readParcelable(Region.class.getClassLoader());
    }

    public static final Creator<UserDetail> CREATOR = new Creator<UserDetail>() {
        @Override
        public UserDetail createFromParcel(Parcel source) {
            return new UserDetail(source);
        }

        @Override
        public UserDetail[] newArray(int size) {
            return new UserDetail[size];
        }
    };
}
