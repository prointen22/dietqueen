package com.livennew.latestdietqueen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;

public class SubScribeResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private SubScribe data;

    @SerializedName("others")
    @Expose
    private Others others;

    public SubScribe getData() {
        return data;
    }

    public void setData(SubScribe data) {
        this.data = data;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }
}
