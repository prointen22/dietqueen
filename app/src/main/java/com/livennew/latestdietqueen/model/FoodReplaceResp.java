package com.livennew.latestdietqueen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FoodReplaceResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private FoodReplace data;

    public FoodReplace getData() {
        return data;
    }

    public void setData(FoodReplace data) {
        this.data = data;
    }
}
