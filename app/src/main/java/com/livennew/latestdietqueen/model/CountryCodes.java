package com.livennew.latestdietqueen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountryCodes implements Parcelable {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("dial_code")
    @Expose
    private String dialCode;
    @SerializedName("code")
    @Expose
    private String code;

    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name=" + name +
                ", dialCode='" + dialCode + '\'' +
                ", code='" + code + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.dialCode);
        dest.writeString(this.code);
    }

    public CountryCodes() {
    }

    protected CountryCodes(Parcel in) {
        this.name = in.readString();
        this.dialCode = in.readString();
        this.code = in.readString();
    }

    public static final Creator<CountryCodes> CREATOR = new Creator<CountryCodes>() {
        @Override
        public CountryCodes createFromParcel(Parcel source) {
            return new CountryCodes(source);
        }

        @Override
        public CountryCodes[] newArray(int size) {
            return new CountryCodes[size];
        }
    };
}
