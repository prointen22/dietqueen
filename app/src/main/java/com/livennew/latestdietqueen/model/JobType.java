package com.livennew.latestdietqueen.model;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({JobType.WORKING, JobType.HOUSE_WIFE})
@Retention(RetentionPolicy.SOURCE)
public @interface JobType {
    String WORKING = "working";
    String HOUSE_WIFE = "house wife";
}
