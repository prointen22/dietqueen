package com.livennew.latestdietqueen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("user_name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("isd_code")
    @Expose
    private String isd_code;
    @SerializedName("mobile_no")
    @Expose
    private String mobile;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("height")
    @Expose
    private String height;

    @SerializedName("is_terms_accepted")
    @Expose
    private int isTermsAccepted;

    public int getIsTermsAccepted(){
        return isTermsAccepted;
    }
    public void setIsTermsAccepted(int isTermsAccepted){
        this.isTermsAccepted = isTermsAccepted;
    }

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    @SerializedName("request_type")
    @Expose
    private String request_type;
    @SerializedName("aos_version")
    @Expose
    private  String aos_version;


    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("user_detail")
    @Expose
    private UserDetail userDetail;
    @SerializedName("state")
    @Expose
    private State state;
    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("diet_plan")
    @Expose
    private DietPlan dietPlan;

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("diet_e_plan_id")
    @Expose
    private int dietEPlanId;

    @SerializedName("insert_from")
    @Expose
    String insertFrom="App";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public  String getAos_version(){
        return aos_version;
    }
    public  void setAos_version(String aos_version){
        this.aos_version = aos_version;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIsd_code() {
        return isd_code;
    }

    public void setIsd_code(String isd_code) {
        this.isd_code = isd_code;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getDietEPlanId() {
        return dietEPlanId;
    }

    public void setDietEPlanId(int dietEPlanId) {
        this.dietEPlanId = dietEPlanId;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public DietPlan getDietPlan() {
        return dietPlan;
    }

    public void setDietPlan(DietPlan dietPlan) {
        this.dietPlan = dietPlan;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.mobile);
        dest.writeString(this.aos_version);
        dest.writeString(this.isd_code);
        dest.writeString(this.profileImage);
        dest.writeString(this.age);
        dest.writeString(this.dob);
        dest.writeParcelable(this.userDetail, flags);
        dest.writeParcelable(this.state, flags);
        dest.writeParcelable(this.city, flags);
        dest.writeParcelable(this.dietPlan, flags);
        dest.writeString(this.otp);
        dest.writeString(this.token);
        dest.writeInt(this.dietEPlanId);
        dest.writeString(this.insertFrom);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.email = in.readString();
        this.mobile = in.readString();
        this.aos_version = in.readString();
        this.isd_code = in.readString();
        this.profileImage = in.readString();
        this.age = in.readString();
        this.dob = in.readString();
        this.userDetail = in.readParcelable(UserDetail.class.getClassLoader());
        this.state = in.readParcelable(State.class.getClassLoader());
        this.city = in.readParcelable(City.class.getClassLoader());
        this.dietPlan = in.readParcelable(DietPlan.class.getClassLoader());
        this.otp = in.readString();
        this.token = in.readString();
        this.dietEPlanId = in.readInt();
        this.insertFrom = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
