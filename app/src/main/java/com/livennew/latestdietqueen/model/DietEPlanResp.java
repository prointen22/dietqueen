package com.livennew.latestdietqueen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.PlanParam;

public class DietEPlanResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private PlanParam data;

    public PlanParam getData() {
        return data;
    }

    public void setData(PlanParam data) {
        this.data = data;
    }
}
