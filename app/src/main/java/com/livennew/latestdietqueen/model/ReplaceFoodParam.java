package com.livennew.latestdietqueen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReplaceFoodParam {

    @SerializedName("old_food_id")
    @Expose
    private int oldFoodId;
    @SerializedName("new_food_id")
    @Expose
    private int newFoodId;
    @SerializedName("diet_e_data_id")
    @Expose
    private int dietEDataId;
    @SerializedName("date")
    @Expose
    private String date;

    public ReplaceFoodParam(int oldFoodId, int newFoodId, int dietEDataId, String date) {
        this.oldFoodId = oldFoodId;
        this.newFoodId = newFoodId;
        this.dietEDataId = dietEDataId;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getOldFoodId() {
        return oldFoodId;
    }

    public void setOldFoodId(int oldFoodId) {
        this.oldFoodId = oldFoodId;
    }

    public int getNewFoodId() {
        return newFoodId;
    }

    public void setNewFoodId(int newFoodId) {
        this.newFoodId = newFoodId;
    }


    public int getDietEDataId() {
        return dietEDataId;
    }

    public void setDietEDataId(int dietEDataId) {
        this.dietEDataId = dietEDataId;
    }

    @Override
    public String toString() {
        return "ReplaceFoodParam{" +
                "oldFoodId=" + oldFoodId +
                ", newFoodId=" + newFoodId +
                ", dietEDataId=" + dietEDataId +
                ", date='" + date + '\'' +
                '}';
    }
}
