package com.livennew.latestdietqueen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DietPlan implements Parcelable {
    @SerializedName("diet_plan_id")
    @Expose
    private int dietPlanId;

    @SerializedName("diet_plan")
    @Expose
    private PlanDetails dietPlaDetails;

    public int getDietPlanId() {
        return dietPlanId;
    }

    public void setDietPlanId(int dietPlanId) {
        this.dietPlanId = dietPlanId;
    }

    public PlanDetails getDietPlaDetails() {
        return dietPlaDetails;
    }

    public void setDietPlaDetails(PlanDetails dietPlaDetails) {
        this.dietPlaDetails = dietPlaDetails;
    }

    @Override
    public String toString() {
        return "DietPlan{" +
                "dietPlanId=" + dietPlanId +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.dietPlanId);
    }

    public DietPlan() {
    }

    protected DietPlan(Parcel in) {
        this.dietPlanId = in.readInt();
    }

    public static final Parcelable.Creator<DietPlan> CREATOR = new Parcelable.Creator<DietPlan>() {
        @Override
        public DietPlan createFromParcel(Parcel source) {
            return new DietPlan(source);
        }

        @Override
        public DietPlan[] newArray(int size) {
            return new DietPlan[size];
        }
    };

    public class PlanDetails implements Parcelable {
        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("amount")
        @Expose
        private int amount;
        @SerializedName("free_month")
        @Expose
        private int free_month;
        @SerializedName("plan_name")
        @Expose
        private String plan_name;
        @SerializedName("plan_type")
        @Expose
        private String plan_type;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public int getFree_month() {
            return free_month;
        }

        public void setFree_month(int free_month) {
            this.free_month = free_month;
        }

        public String getPlan_name() {
            return plan_name;
        }

        public void setPlan_name(String plan_name) {
            this.plan_name = plan_name;
        }

        public String getPlan_type() {
            return plan_type;
        }

        public void setPlan_type(String plan_type) {
            this.plan_type = plan_type;
        }

        protected PlanDetails(Parcel in) {
        }

        public final Creator<PlanDetails> CREATOR = new Creator<PlanDetails>() {
            @Override
            public PlanDetails createFromParcel(Parcel in) {
                return new PlanDetails(in);
            }

            @Override
            public PlanDetails[] newArray(int size) {
                return new PlanDetails[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }
    }
}
