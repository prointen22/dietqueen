package com.livennew.latestdietqueen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;

public class FoodReplace  {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("diet_e_data_id")
    @Expose
    private Integer dietEDataId;
    @SerializedName("old_food_id")
    @Expose
    private Integer oldFoodId;
    @SerializedName("new_food_id")
    @Expose
    private Integer newFoodId;
    @SerializedName("percentage")
    @Expose
    private Integer percentage;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("food")
    @Expose
    private Food food;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDietEDataId() {
        return dietEDataId;
    }

    public void setDietEDataId(Integer dietEDataId) {
        this.dietEDataId = dietEDataId;
    }

    public Integer getOldFoodId() {
        return oldFoodId;
    }

    public void setOldFoodId(Integer oldFoodId) {
        this.oldFoodId = oldFoodId;
    }

    public Integer getNewFoodId() {
        return newFoodId;
    }

    public void setNewFoodId(Integer newFoodId) {
        this.newFoodId = newFoodId;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    @Override
    public String toString() {
        return "FoodReplace{" +
                "id=" + id +
                ", userId=" + userId +
                ", dietEDataId=" + dietEDataId +
                ", oldFoodId=" + oldFoodId +
                ", newFoodId=" + newFoodId +
                ", percentage=" + percentage +
                ", date='" + date + '\'' +
                ", food=" + food +
                '}';
    }
}
