package com.livennew.latestdietqueen.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyResponse {
    @SerializedName("message")
    @Expose
    private List<String> message = null;
    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }
}
