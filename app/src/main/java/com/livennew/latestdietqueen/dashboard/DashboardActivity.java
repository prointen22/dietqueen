package com.livennew.latestdietqueen.dashboard;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.fxn.BubbleTabBar;
import com.fxn.OnBubbleClickListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.appupdate.testing.FakeAppUpdateManager;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.BuildConfig;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.about_us.AboutUsActivity;
import com.livennew.latestdietqueen.dashboard.adapter.ExpandableListAdapter;
import com.livennew.latestdietqueen.dashboard.model.ExpandedMenuModel;
import com.livennew.latestdietqueen.diet_info.ScheduleFragmentNew;
import com.livennew.latestdietqueen.exclusive_youtube.ExclusiveYoutubeMainClass;
import com.livennew.latestdietqueen.faq.FAQActivity;
import com.livennew.latestdietqueen.fragments.graph.GraphFragment;
import com.livennew.latestdietqueen.fragments.home.HomeFragment;
import com.livennew.latestdietqueen.fragments.products.ProductDetailActivity;
import com.livennew.latestdietqueen.fragments.products.ProductListFragment;
import com.livennew.latestdietqueen.language.LanguageActivity;
import com.livennew.latestdietqueen.login_screen.LoginActivity;
import com.livennew.latestdietqueen.model.MyResponse;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.my_plans.AddMyWeightActivity;
import com.livennew.latestdietqueen.my_plans.MyPhysicalActivity;
import com.livennew.latestdietqueen.newsblog.NewsBlogListActivity;
import com.livennew.latestdietqueen.notification.MyFirebaseMessagingService;
import com.livennew.latestdietqueen.notification.NotificationListActivity;
import com.livennew.latestdietqueen.notification.model.Notification;
import com.livennew.latestdietqueen.party_planner.PartyPlannerActivity;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.user_detail.AchievementsActivity;
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class DashboardActivity extends FabActivity implements ExpandableListView.OnChildClickListener {
    private static final String TAG = "DashboardActivity";
    private static final int APP_UPDATE_TYPE_SUPPORTED = AppUpdateType.IMMEDIATE;
    private static final int REQUEST_UPDATE = 54;
    boolean doubleBackToExitPressedOnce = false;
    private FragmentManager mFragmentManager;
   // BottomNavigationView bottomNavigationView;
   private static FirebaseAnalytics firebaseAnalytics;
    ChipNavigationBar bottomNavigationView;
    DrawerLayout mDrawerLayout;
    ExpandableListView mDrawerLayoutList;
    private NavigationView navigationView;
    public ExpandableListAdapter expandableListAdapter;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    private List<ExpandedMenuModel> listDataHeader;
    private HashMap<ExpandedMenuModel, List<ExpandedMenuModel>> listDataChild;
    private Toolbar toolbar;
    private CircleImageView ivProfile;
    private TextView tvUserNm, tvUserLocation;
    private LinearLayout llHeader;
    private ImageView ivProfileIcon;
    BubbleTabBar tabBar;
    @Inject
    SessionManager sessionManager;
    private LinearLayoutCompat llFooter;
    @Inject
    APIInterface apiInterface;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        setContentView(R.layout.dashboard_main);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        init();
        initToolbar();
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
        initListener();
        initDrawer();
    /*    bottomNavigationView.setOnNavigationItemSelectegetSupportActionBardListener(item -> {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.btm_nav_home:
                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.btm_nav_product:
                    fragment = new ProductListFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navSchedule:
//                    fragment = new GraphFragment();
                    loadFragment(MainDietContainerFragment.getInstance());
                    return true;
            }
            return false;
        });
*/

        bottomNavigationView.setOnItemSelectedListener
                (new ChipNavigationBar.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(int i) {
                        Fragment fragment = null;
                        switch (i){
                            case R.id.btm_nav_home:
                                fragment = new HomeFragment();
                                loadFragment(fragment);
                                break;
                            case R.id.btm_nav_product:
                                FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.PRODUCTS,firebaseAnalytics);
                                fragment = new ProductListFragment();
                                loadFragment(fragment);
                                 break;
                            case R.id.navSchedule:
//                    fragment = new GraphFragment();
//                                loadFragment(MainDietContainerFragment.getInstance());
                                FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.SCHEDULE,firebaseAnalytics);
                                loadFragment(ScheduleFragmentNew.newInstance());
                                break;

                            case R.id.btm_nav_profile:
//                    fragment = new GraphFragment();
                             //   loadFragment(MainDietContainerFragment.getInstance());
                                FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.ACHIEVEMENT,firebaseAnalytics);
                                Intent intent = new Intent(DashboardActivity.this, AchievementsActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                break;
                        }

                    }
                });

        tabBar.addBubbleListener(new OnBubbleClickListener() {
            @Override
            public void onBubbleClick(int id) {
                Fragment fragment = null;


                Log.d("jjjjj","hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"+id);
                switch (id) {
                    case R.id.btm_nav_home:

                        fragment = new HomeFragment();
                        loadFragment(fragment);
                        break;
                    case R.id.btm_nav_product:
                        fragment = new ProductListFragment();
                        loadFragment(fragment);
                        break;
                    case R.id.navSchedule:
//                    fragment = new GraphFragment();
//                        loadFragment(MainDietContainerFragment.getInstance());
                        loadFragment(ScheduleFragmentNew.newInstance());
                        break;

                    case R.id.btm_nav_profile:
//                    fragment = new GraphFragment();
                        //   loadFragment(MainDietContainerFragment.getInstance());
                        Intent intent = new Intent(DashboardActivity.this, AchievementsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        break;
                }
            }
        });

        // load the store fragment by default
        loadFragment(new HomeFragment());
        llHeader.setOnClickListener(View -> {
            Intent intent = new Intent(DashboardActivity.this, AchievementsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });

        ivProfileIcon.setOnClickListener(View -> {
            Intent intent = new Intent(DashboardActivity.this, AchievementsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });
        llFooter = (LinearLayoutCompat) findViewById(R.id.ll_footer);
        llFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("market://details?id=" + getPackageName()));
                    startActivity(i);
                } catch (ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" +
                            getPackageName())));
                }
            }
        });
        navigate();
    }

    private void navigate() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().hasExtra("navigate"))
                Log.d("jjjj","jjjjjjjjjjjjjjjjjjjjjjjjjj");
             //   bottomNavigationView.setSelectedItemId(bottomNavigationView.getMenu().getItem(getIntent().getExtras().getInt("navigate", 0)).getItemId());
            else if (getIntent().hasExtra("notification")) {
                Intent intent = new Intent(this, NotificationListActivity.class);
                intent.putExtra("notification", (Notification) getIntent().getParcelableExtra("notification"));
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        navigate();
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = sessionManager.getUser();
        try {
            Glide
                    .with(this)
                    .load(user.getProfileImage())
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .into(ivProfile);
            Glide
                    .with(this)
                    .load(user.getProfileImage())
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(ivProfileIcon);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sessionManager.getUser().getName() != null) {
            tvUserNm.setText(sessionManager.getUser().getName());
        }
        if (sessionManager.getUser().getState() != null) {
            tvUserLocation.setText(sessionManager.getUser().getCity().getName() + ", " + sessionManager.getUser().getState().getName());
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        setFabMargin(85);
        checkForUpdates();
    }


    public void init() {
        bottomNavigationView = findViewById(R.id.navigation);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerLayoutList = findViewById(R.id.navigationmenu);
        navigationView = findViewById(R.id.nav_view);
        View view = navigationView.getHeaderView(0);
        llHeader = view.findViewById(R.id.header_profile);
        tvUserLocation = view.findViewById(R.id.tv_header_location);
        tvUserNm = view.findViewById(R.id.tv_header_name);
        ivProfile = view.findViewById(R.id.imageView);
        ivProfileIcon = findViewById(R.id.ivProfile);

        bottomNavigationView.setItemSelected(R.id.btm_nav_home,true);
        tabBar=findViewById(R.id.bubbleTabBar);

        Intent i=new Intent(this, MyFirebaseMessagingService.class);
        startService(i);
        FirebaseMessaging.getInstance().subscribeToTopic("breakfast");  // Topic Push Notification
        FirebaseMessaging.getInstance().subscribeToTopic("general");  // Topic Push Notification

    }

    @Override
    public void onBackPressed() {
        Fragment fragmentCurrent = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        Log.d("ddddd","hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh  Fragment"+fragmentCurrent);

        if (bottomNavigationView.getSelectedItemId() != R.id.btm_nav_home) {
            bottomNavigationView.setItemSelected(R.id.btm_nav_home,true);
        }else  if (fragmentCurrent instanceof HomeFragment){
            finish();
        } else {
            super.onBackPressed();
            //finish();
        }
    }

    /**
     * loading fragment into FrameLayout
     *
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    // preparing list data
    public void initDrawer() {
        prepareAfterLoginMenuData();
        expandableListAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild, mDrawerLayoutList);
        mDrawerLayoutList.setAdapter(expandableListAdapter);
        mDrawerLayoutList.setOnChildClickListener(this);
        mDrawerLayoutList.setOnGroupClickListener((parent, v, groupPosition, id) -> {
            mFragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            FrameLayout frameLayout = findViewById(R.id.frame_container);
            switch (listDataHeader.get(groupPosition).getName()) {
                case "Home": {
                    bottomNavigationView.setItemSelected(R.id.btm_nav_home,true);
                    fragmentTransaction.replace(frameLayout.getId(), new HomeFragment()).addToBackStack(TAG).commit();
                    mDrawerLayout.closeDrawers();
                    break;
                }
                case "Notification": {
                    //   Toast.makeText(this, "Notification", Toast.LENGTH_SHORT).show();
                    FireBaseAnalyticsUtil.screenViewEvent("",firebaseAnalytics);
                    Intent intent = new Intent(DashboardActivity.this, NotificationListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    mDrawerLayout.closeDrawers();
                    break;
                }
                case "Diet Party Planner": {
                    FireBaseAnalyticsUtil.screenViewEvent("",firebaseAnalytics);
                    Intent intent = new Intent(DashboardActivity.this, PartyPlannerActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    mDrawerLayout.closeDrawers();
                    break;
                }
                case "My Calendar": {
                    FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.MYCALENDER,firebaseAnalytics);
                    fragmentTransaction.replace(frameLayout.getId(), new GraphFragment()).addToBackStack(TAG).commit();
                    mDrawerLayout.closeDrawers();
                    break;
                }
                case "Our Weight Loss Plan": {
                    bottomNavigationView.setItemSelected(R.id.btm_nav_product,true);
                    fragmentTransaction.replace(frameLayout.getId(), new ProductListFragment()).addToBackStack(TAG).commit();
                    mDrawerLayout.closeDrawers();
                    break;
                }
                case "Blogs": {
                    FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.BLOGS,firebaseAnalytics);
                    Intent intent = new Intent(DashboardActivity.this, NewsBlogListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    mDrawerLayout.closeDrawers();
                    break;
                }
                case "Videos": {
                    FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.VIDEOS,firebaseAnalytics);
                    mDrawerLayout.closeDrawers();
                    startActivity(new Intent(DashboardActivity.this, ExclusiveYoutubeMainClass.class));
                    break;
                }
                case "About Us": {
                    FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.ABOUTUS,firebaseAnalytics);
                    startActivity(new Intent(DashboardActivity.this, AboutUsActivity.class));
                    mDrawerLayout.closeDrawers();
                    break;
                }
                case "FAQ": {
                    FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.LANGUAGE,firebaseAnalytics);
                    startActivity(new Intent(DashboardActivity.this, FAQActivity.class));
                    mDrawerLayout.closeDrawers();
                    break;
                }
                case "Launguage": {
                 //   startActivity(new Intent(DashboardActivity.this, LanguageActivity.class));

                    FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.LANGUAGE,firebaseAnalytics);
                    Intent intent = new Intent(DashboardActivity.this, LanguageActivity.class);
                    intent.putExtra("achievment", "dashboard");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    mDrawerLayout.closeDrawers();
                    break;
                }
                case "Logout": {
                    logout();
                    mDrawerLayout.closeDrawers();
                    break;
                }
            }
            return false;
        });

        mDrawerLayoutList.setIndicatorBounds(GetDipsFromPixel(205), GetDipsFromPixel(0));
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
       // actionBarDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_berger_icon);
        //actionBarDrawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.checktoggle));
        actionBarDrawerToggle.syncState();
    }

    private void prepareAfterLoginMenuData() {
        listDataHeader = new ArrayList<ExpandedMenuModel>();
        listDataChild = new HashMap<ExpandedMenuModel, List<ExpandedMenuModel>>();
        // Adding data header
        listDataHeader.add(new ExpandedMenuModel("Home", getResources().getString(R.string.home), R.drawable.newdietqueen_logo));
        listDataHeader.add(new ExpandedMenuModel("Notification", getResources().getString(R.string.notification), R.drawable.navigation_notification));
        listDataHeader.add(new ExpandedMenuModel("My Plans", getString(R.string.my_plan), R.drawable.navigation_dietplan));
        listDataHeader.add(new ExpandedMenuModel("Diet Party Planner", getString(R.string.party_planner), R.drawable.navigation_party));
        listDataHeader.add(new ExpandedMenuModel("My Calendar", getString(R.string.my_calendar), R.drawable.navigation_calender));
        listDataHeader.add(new ExpandedMenuModel("Our Weight Loss Plan", getString(R.string.weight_loss_plan), R.drawable.navigation_dietplan));
        listDataHeader.add(new ExpandedMenuModel("Blogs", getString(R.string.blog), R.drawable.navigation_blog));
        listDataHeader.add(new ExpandedMenuModel("Videos", getString(R.string.videos), R.drawable.navigation_videos));
        listDataHeader.add(new ExpandedMenuModel("About Us", getString(R.string.about_us), R.drawable.navigation_about));
        listDataHeader.add(new ExpandedMenuModel("FAQ", getString(R.string.faq), R.drawable.navigation_aboutus));
        listDataHeader.add(new ExpandedMenuModel("Launguage", getResources().getString(R.string.language), R.drawable.navigation_customer));
        listDataHeader.add(new ExpandedMenuModel("Logout", getString(R.string.logout), R.drawable.navigation_logout));
        for (int i = 0; i < listDataHeader.size(); i++) {
            List<ExpandedMenuModel> heading = new ArrayList<ExpandedMenuModel>();
            switch (listDataHeader.get(i).getName()) {
                case "My Plans": {
                    heading.add(new ExpandedMenuModel("My Weight", getString(R.string.my_weight)));
                    heading.add(new ExpandedMenuModel("My Physical Activity", getString(R.string.my_physical_activity)));
                    //  heading.add(new ExpandedMenuModel("My Problems", getString(R.string.my_problems)));
                    break;
                }
            }
            listDataChild.put(listDataHeader.get(i), heading);
        }

    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition, long id) {
        mDrawerLayout.closeDrawers();
        switch (listDataHeader.get(groupPosition).getName()) {
            case "My Plans": {
                switch (listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).getName()) {
                    case "My Weight": {
                        Intent intent = new Intent(DashboardActivity.this, AddMyWeightActivity.class);
                        startActivity(intent);
                        break;
                    }
                    case "My Physical Activity": {
                        Intent intent = new Intent(DashboardActivity.this, MyPhysicalActivity.class);
                        startActivity(intent);
                        break;
                    }
                }
                break;
            }
        }
        mDrawerLayoutList.collapseGroup(groupPosition);
        return false;
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    menuItem.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return true;
                });
    }

    private void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDatePicker);
        builder.setTitle(getString(R.string.logout));
        builder.setMessage(getString(R.string.are_you_sure_you_want));
        builder.setPositiveButton(getString(R.string.yes), (dialog, which) -> {
            // Do nothing but close the dialog
            deregisterFCMToken();
            dialog.dismiss();
        });
        builder.setNegativeButton(getString(R.string.no), (dialog, which) -> {
            // Do nothing
            dialog.dismiss();
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deregisterFCMToken() {
        apiInterface.deregisterPushToken().enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(@NotNull Call<MyResponse> call, @NotNull Response<MyResponse> response) {
                navigateToLoginScreen();
            }

            @Override
            public void onFailure(@NotNull Call<MyResponse> call, @NotNull Throwable t) {
                navigateToLoginScreen();
            }
        });
    }

    private void navigateToLoginScreen() {
        sessionManager.logout();
        startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }

    //    Convert pixel to dip
    public int GetDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    private void initListener() {
        mDrawerLayoutList.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (mLastFirstVisibleItem < firstVisibleItem) {
                    Log.i("SCROLLING DOWN", "TRUE");
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    Log.i("SCROLLING UP", "TRUE");
                }
                mLastFirstVisibleItem = firstVisibleItem;

            }
        });
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");

         //   getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_berger_icon);
        }

    }

    private void checkForUpdates() {
        AppUpdateManager appUpdateManager;
        if (BuildConfig.DEBUG) {
            appUpdateManager = new FakeAppUpdateManager(this);
            ((FakeAppUpdateManager) appUpdateManager).setUpdateAvailable(13);
            ((FakeAppUpdateManager) appUpdateManager).setUpdatePriority(5);
        } else {
            appUpdateManager = AppUpdateManagerFactory.create(this);
        }

        Task<AppUpdateInfo> appUpdateInfo = appUpdateManager.getAppUpdateInfo();
        appUpdateInfo.addOnSuccessListener(result -> handleUpdate(result, appUpdateManager, appUpdateInfo));
    }

    private void handleUpdate(AppUpdateInfo task, AppUpdateManager manager, Task<AppUpdateInfo> info) {
        if (task.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
            handleImmediateUpdate(manager, info);
        } else if (task.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
            handleFlexibleUpdate(manager, info);
        }
    }

    private void handleImmediateUpdate(AppUpdateManager manager, Task<AppUpdateInfo> info) {
        if ((info.getResult().updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE ||
                info.getResult().updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS)
                && info.getResult().isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {

            try {
                manager.startUpdateFlowForResult(info.getResult(), AppUpdateType.IMMEDIATE, this, REQUEST_UPDATE);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleFlexibleUpdate(AppUpdateManager manager, Task<AppUpdateInfo> info) {
        if ((info.getResult().updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE ||
                info.getResult().updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS)
                && info.getResult().isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
            try {
                manager.startUpdateFlowForResult(info.getResult(), AppUpdateType.FLEXIBLE, this, REQUEST_UPDATE);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
            if (BuildConfig.DEBUG) {
                FakeAppUpdateManager fakeAppUpdate = (FakeAppUpdateManager) manager;
                if (fakeAppUpdate.isConfirmationDialogVisible()) {
                    fakeAppUpdate.userAcceptsUpdate();
                    fakeAppUpdate.downloadStarts();
                    fakeAppUpdate.downloadCompletes();
                    fakeAppUpdate.completeUpdate();
                    fakeAppUpdate.installCompletes();
                }
            }
        }
    }

    public void showProductDetails(int id, boolean isCurrentPlan) {
        Intent intent = new Intent(DashboardActivity.this, ProductDetailActivity.class);
        intent.putExtra("product_id", id);
        intent.putExtra("isCurrentPlan", isCurrentPlan);
        startActivityForResult(intent, ProductDetailActivity.PRODUCT_DETAILS_REQ);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }
}

