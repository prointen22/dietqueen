package com.livennew.latestdietqueen.dashboard;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.about_us.AboutDietQueenActivity;
import com.livennew.latestdietqueen.diet_info.InstructionActivity;
import com.livennew.latestdietqueen.diet_info.ScheduleFragmentNew;
import com.livennew.latestdietqueen.fragments.home.HomeFragment;
import com.livennew.latestdietqueen.fragments.home.HomeFragmentNew;
import com.livennew.latestdietqueen.fragments.products.ProductListFragment;
import com.livennew.latestdietqueen.notification.NotificationListActivity;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil;
import com.livennew.latestdietqueen.utils.TopBarUtil;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class DashboardSubActivity extends AppCompatActivity implements View.OnClickListener, PaymentResultWithDataListener {
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;

    private static FirebaseAnalytics firebaseAnalytics;

    LinearLayout mLLHome, mLLMyDiet, mLLDashboard, mLLPricing, mLLProfile, mLLHome1, mLLMyDiet1, mLLDashboard1, mLLPricing1, mLLProfile1;
    private boolean mNavigateBackToDashboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        setContentView(R.layout.activity_dashboard2);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        init();
    }

    @Override
    public void onBackPressed() {
        if (mNavigateBackToDashboard) {
            mNavigateBackToDashboard = false;
            navigateToDashboard();
        } else {
            Fragment fragmentCurrent = getSupportFragmentManager().findFragmentById(R.id.frame_container);
            Log.d("TAG", "onBackPressed()  Fragment" + fragmentCurrent);

            if (fragmentCurrent instanceof HomeFragment) {
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    /**
     * loading fragment into FrameLayout
     *
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void loadFragment(Fragment fragment,String type) {
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }

        init();
    }

    private void init() {
        mLLHome = findViewById(R.id.ll_home);
        mLLMyDiet = findViewById(R.id.ll_my_diet);
        mLLDashboard = findViewById(R.id.ll_dashboard);
        mLLPricing = findViewById(R.id.ll_pricing);
        mLLProfile = findViewById(R.id.ll_my_profile);
        mLLHome1 = findViewById(R.id.ll_home1);
        mLLMyDiet1 = findViewById(R.id.ll_my_diet1);
        mLLDashboard1 = findViewById(R.id.ll_dashboard1);
        mLLPricing1 = findViewById(R.id.ll_pricing1);
        mLLProfile1 = findViewById(R.id.ll_my_profile1);

        mLLHome.setOnClickListener(this);
        mLLMyDiet.setOnClickListener(this);
        mLLDashboard.setOnClickListener(this);
        mLLPricing.setOnClickListener(this);
        mLLProfile.setOnClickListener(this);

        ImageView mIvNotification = findViewById(R.id.iv_notification);
        ImageView mIvDoctor = findViewById(R.id.iv_doctor);
        ImageView mIvSideMenu = findViewById(R.id.iv_menu);
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager);

        if (getIntent() != null && getIntent().getExtras() != null) {
            mNavigateBackToDashboard = true;
            if (getIntent().hasExtra("type")) {
                if (getIntent().getStringExtra("type").equals(getString(R.string.my_diet_schedule)) || getIntent().getStringExtra("type").equals(getString(R.string.diet_status))) {
                    setMyDietSelected();
//                    loadFragment(MainDietContainerFragment.getInstance());
                    loadFragment(ScheduleFragmentNew.newInstance(),getIntent().getStringExtra("type"));
                } else if (getIntent().getStringExtra("type").equals(getString(R.string.pricing))) {
                    setPricingSelected();
                    loadFragment(new ProductListFragment());
                } else if (getIntent().getStringExtra("type").equals(getString(R.string.my_next_meal))) {
                    setHomeSelected();
//                    loadFragment(new HomeFragment());
                    loadFragment(HomeFragmentNew.newInstance());
                }
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_notification:
                navigateToNotificationScreen();
                break;
            case R.id.iv_doctor:
                startActivity(new Intent(this, InstructionActivity.class));
                break;
            case R.id.ll_home:
                setHomeSelected();
                loadFragment(HomeFragmentNew.newInstance());
//                loadFragment(new HomeFragment());
                break;
            case R.id.ll_my_diet:
                FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.SCHEDULE, firebaseAnalytics);
                setMyDietSelected();
                loadFragment(ScheduleFragmentNew.newInstance());
                break;
            case R.id.ll_dashboard:
                setDashboardSelected();
                navigateToDashboard();
                break;
            case R.id.ll_pricing:
                FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.PRODUCTS, firebaseAnalytics);
                setPricingSelected();
                loadFragment(new ProductListFragment());
                break;
            case R.id.ll_my_profile:
//                FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.ACHIEVEMENT, firebaseAnalytics);
                setMyProfileSelected();
                Intent intent1 = new Intent(this, AboutDietQueenActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                break;
        }
    }

    private void navigateToNotificationScreen() {
        FireBaseAnalyticsUtil.screenViewEvent("", firebaseAnalytics);
        Intent intent = new Intent(DashboardSubActivity.this, NotificationListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToDashboard() {
        Intent intent = new Intent(this, DashboardActivity1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setHomeSelected() {
        mLLHome1.setBackground(getDrawable(R.drawable.round_bg_lightpink));
        mLLMyDiet1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLDashboard1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLPricing1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLProfile1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setMyDietSelected() {
        mLLMyDiet1.setBackground(getDrawable(R.drawable.round_bg_lightpink));
        mLLHome1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLDashboard1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLPricing1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLProfile1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setDashboardSelected() {
        mLLDashboard1.setBackground(getDrawable(R.drawable.round_bg_lightpink));
        mLLHome1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLMyDiet1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLPricing1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLProfile1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setPricingSelected() {
        mLLPricing1.setBackground(getDrawable(R.drawable.round_bg_lightpink));
        mLLHome1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLMyDiet1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLDashboard1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLProfile1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setMyProfileSelected() {
        mLLProfile1.setBackground(getDrawable(R.drawable.round_bg_lightpink));
        mLLHome1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLMyDiet1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLDashboard1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
        mLLPricing1.setBackground(getDrawable(R.drawable.round_bg_lightblue));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT) {
////            binding.btnProductSubscribe.setEnabled(true);
//            if (resultCode == RESULT_OK && data != null) {
//                TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
//                        .INTENT_EXTRA_TRANSACTION_RESPONSE);
//
//                ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);
//
//                // Check which object is non-null
//                if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
//                    if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
//                        //Success Transaction
//                        ProductListFragment fragment = (ProductListFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
//                        fragment.subScribe(transactionResponse.getPayuResponse());
//                    } else {
//                        //Failure Transaction
//                        Toast.makeText(this, transactionResponse.getMessage().equals("") ? getString(R.string.payment_failed_please_try_again) : transactionResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//
//                    // Response from Payumoney
//                    String payuResponse = transactionResponse.getPayuResponse();
//                    Log.e(ProductListFragment.TAG, "onActivityResult: " + payuResponse);
//                    // Response from SURl and FURL
//                    String merchantResponse = transactionResponse.getTransactionDetails();
//                    Log.e(ProductListFragment.TAG, "onActivityResult: " + merchantResponse);
//
//                } else if (resultModel != null && resultModel.getError() != null) {
//                    Log.d(ProductListFragment.TAG, "Error response : " + resultModel.getError().getTransactionResponse());
//                } else {
//                    Log.d(ProductListFragment.TAG, "Both objects are null!");
//                }
//            } else {
//                Toast.makeText(this, "Payment failed", Toast.LENGTH_SHORT).show();
//            }
//        } else if (requestCode == PersonalDetailActivity.UPDATE_USER_REQ) {
//            if (resultCode == RESULT_OK) {
//                ProductListFragment fragment = (ProductListFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
//                fragment.updateUser();
//            }
//        }
    }

//    @Override
//    public void onPaymentSuccess(String s) {
//        Log.i("TAG","success data = "+s);
//    }
//
//    @Override
//    public void onPaymentError(int i, String s) {
//        Log.i("TAG","error data = "+s);
//    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        Log.i("TAG","data = "+s);
        Log.i("TAG","data = "+paymentData.getData());
        Log.i("TAG","data = "+paymentData.getOrderId());
        ProductListFragment fragment = (ProductListFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
        fragment.subScribe(s);
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        Log.i("TAG","data = "+s);
        Log.i("TAG","data = "+paymentData.getData());
    }
}
