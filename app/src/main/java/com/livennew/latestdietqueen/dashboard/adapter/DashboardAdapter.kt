package com.livennew.latestdietqueen.dashboard.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import com.livennew.latestdietqueen.dashboard.model.DashboardModel
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import com.livennew.latestdietqueen.R
import androidx.constraintlayout.widget.ConstraintLayout
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView

class DashboardAdapter internal constructor(private val mContext: Context, private val mDashboardList: List<DashboardModel>?, private val mClickListener: (DashboardModel) -> Unit) : RecyclerView.Adapter<DashboardAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dashboard_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dashboardObj = mDashboardList!![position]
        holder.mTvTitle.text = dashboardObj.title

        if (dashboardObj.icon != -1) {
            holder.mIvIcon.setImageResource(dashboardObj.icon)
            holder.mCLMain.visibility = View.VISIBLE
        } else {
            holder.mCLMain.visibility = View.GONE
        }

        if (!TextUtils.isEmpty(dashboardObj.imagePath)) {
            holder.mRLMain.visibility = View.GONE
            Glide.with(mContext)
                    .load(dashboardObj.imagePath)
                    .placeholder(R.drawable.ic_user_profile)
                    .error(R.drawable.ic_user_profile)
                    .apply(RequestOptions.circleCropTransform())
                    .into(holder.mCIvProfile)
            holder.mCIvProfile.visibility = View.VISIBLE
        } else {
            holder.mRLMain.visibility = View.VISIBLE
            holder.mCIvProfile.visibility = View.GONE
        }

        if (dashboardObj.subIcon != -1) {
            holder.mIvSubIcon.setImageResource(dashboardObj.subIcon)
            holder.mIvSubIcon.visibility = View.VISIBLE
        } else {
            holder.mIvSubIcon.visibility = View.GONE
        }

        if (!TextUtils.isEmpty(dashboardObj.subTitle)) {
            holder.mTvSubTitle.text = dashboardObj.subTitle
            holder.mTvSubTitle.visibility = View.VISIBLE
        } else {
            holder.mTvSubTitle.visibility = View.GONE
        }

        if (!TextUtils.isEmpty(dashboardObj.subTitle1)) {
            holder.mTvSubTitle1.text = dashboardObj.subTitle1
            holder.mTvSubTitle1.visibility = View.VISIBLE
        } else {
            holder.mTvSubTitle1.visibility = View.GONE
        }

        if (!TextUtils.isEmpty(dashboardObj.subTitle2)) {
            holder.mTvSubTitle2.text = dashboardObj.subTitle2
            holder.mTvSubTitle2.visibility = View.VISIBLE
        } else {
            holder.mTvSubTitle2.visibility = View.GONE
        }

        if (dashboardObj.isEnabled) {
            holder.mRLMain.background = mContext.getDrawable(R.drawable.round_bg_lightblue)
        } else {
            holder.mRLMain.background = mContext.getDrawable(R.drawable.round_bg_gray)
        }

        holder.mCLMain.setOnClickListener {
            if (dashboardObj.isEnabled) {
                mClickListener(dashboardObj)
            }else{
                Toast.makeText(mContext,"Coming Soon",Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun getItemCount(): Int {
        return if (mDashboardList != null && mDashboardList.isNotEmpty()) {
            mDashboardList.size
        } else {
            0
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var mCLMain: ConstraintLayout
        var mRLMain: RelativeLayout
        var mTvTitle: TextView
        var mTvSubTitle: TextView
        var mTvSubTitle1: TextView
        var mTvSubTitle2: TextView
        var mIvIcon: ImageView
        var mIvSubIcon: ImageView
        var mCIvProfile: CircleImageView

        init {
            mCLMain = view.findViewById(R.id.cl_main)
            mRLMain = view.findViewById(R.id.rl_main)
            mTvTitle = view.findViewById(R.id.tv_title)
            mTvSubTitle = view.findViewById(R.id.tv_sub_title)
            mTvSubTitle1 = view.findViewById(R.id.tv_sub_title_1)
            mTvSubTitle2 = view.findViewById(R.id.tv_sub_title_2)
            mIvIcon = view.findViewById(R.id.iv)
            mIvSubIcon = view.findViewById(R.id.iv_sub)
            mCIvProfile = view.findViewById(R.id.civ_profile)
        }
    }
}