package com.livennew.latestdietqueen.dashboard

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.appupdate.testing.FakeAppUpdateManager
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.android.play.core.tasks.Task
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessaging
import com.livennew.latestdietqueen.*
import com.livennew.latestdietqueen.about_us.AboutDietQueenActivity
import com.livennew.latestdietqueen.about_us.ContactUsActivity
import com.livennew.latestdietqueen.dashboard.adapter.DashboardAdapter
import com.livennew.latestdietqueen.dashboard.model.DashboardModel
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Plan
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.PlanParam
import com.livennew.latestdietqueen.exclusive_youtube.ExclusiveYoutubeMainClass
import com.livennew.latestdietqueen.exclusive_youtube.testAllActivity
import com.livennew.latestdietqueen.faq.FAQActivity
import com.livennew.latestdietqueen.fragments.home.HomeFragment
import com.livennew.latestdietqueen.info_screen.health_screen.HealthActivity
import com.livennew.latestdietqueen.login_screen.LoginActivity
import com.livennew.latestdietqueen.model.*
import com.livennew.latestdietqueen.my_plans.AddMyWeightActivity
import com.livennew.latestdietqueen.my_plans.MyPhysicalActivity
import com.livennew.latestdietqueen.network_support.APIClient
import com.livennew.latestdietqueen.newsblog.NewsBlogListActivity
import com.livennew.latestdietqueen.notification.MyFirebaseMessagingService
import com.livennew.latestdietqueen.notification.NotificationListActivity
import com.livennew.latestdietqueen.notification.model.Notification
import com.livennew.latestdietqueen.retrofit.APIInterface
import com.livennew.latestdietqueen.user_detail.AchievementsActivity
import com.livennew.latestdietqueen.utils.DateUtil
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil.Companion.screenViewEvent
import com.livennew.latestdietqueen.utils.TopBarUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashboardActivity1 : AppCompatActivity() {
    private var toolbar: Toolbar? = null
    var sessionManager: SessionManager? = null
    var apiInterface: APIInterface? = null
    private var user: User? = null
    private var mDashboardList = ArrayList<DashboardModel>()
    private lateinit var mRvDashboard: RecyclerView
    private lateinit var mTvPlanDetail: TextView
    private lateinit var mTvAbout: ImageView
//    private lateinit var mIvNotification: ImageView
//    private lateinit var mIvDoctor: ImageView
    var disposable = CompositeDisposable()
    private lateinit var mProgressBar: ProgressBar
    private lateinit var mPlan: Plan

    companion object {
        private const val REQUEST_UPDATE = 54
        private var firebaseAnalytics: FirebaseAnalytics? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        setContentView(R.layout.activity_dashboard1)

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        sessionManager = SessionManager(applicationContext)
        apiInterface = APIClient.getClient(applicationContext).create(APIInterface::class.java)

        val mIvNotification:ImageView = findViewById(R.id.iv_notification)
        val mIvDoctor:ImageView = findViewById(R.id.iv_doctor)
        val mIvSideMenu:ImageView = findViewById(R.id.iv_menu)
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager!!)

//        initToolbar()
        navigate()
    }

    private fun navigate() {
        if (intent != null && intent.extras != null) {
            if (intent.hasExtra("navigate")) Log.d("jjjj", "jjjjjjjjjjjjjjjjjjjjjjjjjj")
            else if (intent.hasExtra("notification")) {
                val intent = Intent(this, NotificationListActivity::class.java)
                intent.putExtra("notification", getIntent().getParcelableExtra<Parcelable>("notification") as Notification?)
                startActivity(intent)
            }
        }

        init()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        navigate()
    }

    override fun onResume() {
        super.onResume()
        checkForUpdates()
    }

    fun init() {
        mProgressBar = findViewById(R.id.progress_bar)
        mTvPlanDetail = findViewById(R.id.tv_plan_detail)
        mTvAbout = findViewById(R.id.tv_about)
        mTvPlanDetail.visibility = View.GONE

        mRvDashboard = findViewById(R.id.rv_dashboard)
        mRvDashboard.layoutManager = GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false)

        val i = Intent(this, MyFirebaseMessagingService::class.java)
        startService(i)
        FirebaseMessaging.getInstance().subscribeToTopic("breakfast") // Topic Push Notification
        FirebaseMessaging.getInstance().subscribeToTopic("general") // Topic Push Notification

        getUserDetail()
    }

    private fun prepareDashboardData() {
        mTvAbout.visibility = View.VISIBLE
        mTvAbout.setOnClickListener {
           navigateToAboutDQScreen()
        }
        mDashboardList.clear()

        val dashboardModelMyProfile = DashboardModel()
        dashboardModelMyProfile.id = 1
        dashboardModelMyProfile.title = getString(R.string.my_profile)
        dashboardModelMyProfile.icon = resources.getIdentifier("ic_dash_my_health", "drawable", packageName)
        dashboardModelMyProfile.imagePath = if (!TextUtils.isEmpty(user?.profileImage)) user?.profileImage else ""
        mDashboardList.add(dashboardModelMyProfile)

        val dashboardModelMyWeight = DashboardModel()
        dashboardModelMyWeight.id = 2
        dashboardModelMyWeight.title = getString(R.string.my_weight)
        dashboardModelMyWeight.icon = resources.getIdentifier("ic_dash_weigt", "drawable", packageName)
        mDashboardList.add(dashboardModelMyWeight)

        val dashboardModelMyHealth = DashboardModel()
        dashboardModelMyHealth.id = 3
        dashboardModelMyHealth.title = getString(R.string.my_health)
        dashboardModelMyHealth.icon = resources.getIdentifier("ic_dash_my_health", "drawable", packageName)
        mDashboardList.add(dashboardModelMyHealth)

        val dashboardModelMyDietSchedule = DashboardModel()
        dashboardModelMyDietSchedule.id = 4
        dashboardModelMyDietSchedule.title = getString(R.string.my_diet_schedule)
        dashboardModelMyDietSchedule.icon = resources.getIdentifier("ic_dash_my_diet", "drawable", packageName)
        mDashboardList.add(dashboardModelMyDietSchedule)

        val dashboardModelMyNextMeal = DashboardModel()
        dashboardModelMyNextMeal.id = 5
        dashboardModelMyNextMeal.title = getString(R.string.my_next_meal)
        dashboardModelMyNextMeal.icon = resources.getIdentifier("ic_dash_my_next_meal", "drawable", packageName)
        dashboardModelMyNextMeal.subTitle1 = getNextMealTime()
        mDashboardList.add(dashboardModelMyNextMeal)

        val dashboardModelDietStatus = DashboardModel()
        dashboardModelDietStatus.id = 6
        dashboardModelDietStatus.title = getString(R.string.diet_status)
        dashboardModelDietStatus.icon = resources.getIdentifier("ic_dash_my_diet_update", "drawable", packageName)
        dashboardModelDietStatus.isEnabled = true
        mDashboardList.add(dashboardModelDietStatus)

        val dashboardModelPricing = DashboardModel()
        dashboardModelPricing.id = 7
        dashboardModelPricing.title = getString(R.string.pricing)
        dashboardModelPricing.icon = resources.getIdentifier("ic_dash_pricing", "drawable", packageName)
        dashboardModelPricing.subTitle = getString(R.string.basic)
        mDashboardList.add(dashboardModelPricing)

//        val dashboardModelBlog = DashboardModel()
//        dashboardModelBlog.id = 8
//        dashboardModelBlog.title = getString(R.string.blog)
//        dashboardModelBlog.icon = resources.getIdentifier("ic_dash_blog", "drawable", packageName)
//        mDashboardList.add(dashboardModelBlog)

        val dashboardModelPhysicalActivity = DashboardModel()
        dashboardModelPhysicalActivity.id = 9
        dashboardModelPhysicalActivity.title = getString(R.string.my_physical_activity)
        dashboardModelPhysicalActivity.icon = resources.getIdentifier("ic_db_fc", "drawable", packageName)
//        dashboardModelPhysicalActivity.subIcon = resources.getIdentifier("ic_db_fc_alert", "drawable", packageName)
        dashboardModelPhysicalActivity.isEnabled = false
        mDashboardList.add(dashboardModelPhysicalActivity)

        val dashboardModelAwardsAchivementsActivity = DashboardModel()
        dashboardModelAwardsAchivementsActivity.id = 10
        dashboardModelAwardsAchivementsActivity.title = getString(R.string.awards_achivements)
        dashboardModelAwardsAchivementsActivity.icon = resources.getIdentifier("ic_dash_awards", "drawable", packageName)
//        dashboardModelAwardsAchivementsActivity.subIcon = resources.getIdentifier("ic_db_fc_alert", "drawable", packageName)
        dashboardModelAwardsAchivementsActivity.isEnabled = false
        mDashboardList.add(dashboardModelAwardsAchivementsActivity)

//        val dashboardModelVideos = DashboardModel()
//        dashboardModelVideos.id = 10
//        dashboardModelVideos.title = getString(R.string.videos)
//        dashboardModelVideos.icon = resources.getIdentifier("ic_dash_videos", "drawable", packageName)
//        dashboardModelVideos.isEnabled = false
//        mDashboardList.add(dashboardModelVideos)

//        val dashboardModelFaqs = DashboardModel()
//        dashboardModelFaqs.id = 11
//        dashboardModelFaqs.title = getString(R.string.faqs)
//        dashboardModelFaqs.icon = resources.getIdentifier("ic_dash_faqs", "drawable", packageName)
//        mDashboardList.add(dashboardModelFaqs)

//        val dashboardModelContact = DashboardModel()
//        dashboardModelContact.id = 12
//        dashboardModelContact.title = getString(R.string.contact)
//        dashboardModelContact.icon = resources.getIdentifier("ic_dash_contact", "drawable", packageName)
//        mDashboardList.add(dashboardModelContact)

//        val dashboardModelTestimonials = DashboardModel()
//        dashboardModelTestimonials.id = 13
//        dashboardModelTestimonials.title = getString(R.string.testimonials)
//        dashboardModelTestimonials.icon = resources.getIdentifier("ic_dash_testimonials", "drawable", packageName)
//        dashboardModelTestimonials.isEnabled = true
//        mDashboardList.add(dashboardModelTestimonials)

//        val dashboardModelSettings = DashboardModel()
//        dashboardModelSettings.id = 14
//        dashboardModelSettings.title = getString(R.string.action_settings)
//        dashboardModelSettings.icon = resources.getIdentifier("ic_dash_settings", "drawable", packageName)
//        dashboardModelSettings.isEnabled = false
//        mDashboardList.add(dashboardModelSettings)

//        val dashboardModelSettings = DashboardModel()
//        dashboardModelSettings.id = 14
//        dashboardModelSettings.title = getString(R.string.action_logout)
//        dashboardModelSettings.icon = resources.getIdentifier("ic_logout", "drawable", packageName)
//        dashboardModelSettings.isEnabled = true
//        mDashboardList.add(dashboardModelSettings)

        setAdapter()
    }

    private fun setAdapter() {
        mRvDashboard.visibility = View.VISIBLE
        val dashboardAdapter = DashboardAdapter(this, mDashboardList) { dashboardModel: DashboardModel ->
            adapterItemClicked(dashboardModel)
        }
        mRvDashboard.adapter = dashboardAdapter

        setPlanDetails()
    }

    private fun adapterItemClicked(dashboardObj: DashboardModel) {
        Log.v("TAG", "Selected tile: ${dashboardObj.title}")

        when (dashboardObj.title) {
            getString(R.string.my_profile) -> {
                navigateToProfileScreen()
            }
            getString(R.string.my_weight) -> {
                val intent = Intent(this@DashboardActivity1, AddMyWeightActivity::class.java)
                startActivity(intent)
            }
            getString(R.string.my_health) -> {
                navigateToProfileScreen()
//                val intent = Intent(this@DashboardActivity1, HealthActivity::class.java)
//                startActivity(intent)
            }
            getString(R.string.pricing) -> {
                firebaseAnalytics?.let { screenViewEvent(FireBaseAnalyticsUtil.PRODUCTS, it) }
                navigateNextScreen(dashboardObj.title.toString())
            }
            getString(R.string.my_next_meal) -> {
                navigateNextScreen(dashboardObj.title.toString())
            }
            getString(R.string.diet_status) -> {
                firebaseAnalytics?.let { screenViewEvent(FireBaseAnalyticsUtil.SCHEDULE, it) }
                navigateNextScreen(dashboardObj.title.toString())
            }
            getString(R.string.physical_activity) -> {
                val intent = Intent(this@DashboardActivity1, MyPhysicalActivity::class.java)
                startActivity(intent)
            }

            getString(R.string.my_diet_schedule) -> {
                firebaseAnalytics?.let { screenViewEvent(FireBaseAnalyticsUtil.SCHEDULE, it) }
                navigateNextScreen(dashboardObj.title.toString())
            }
            getString(R.string.testimonials)->{
                val intent = Intent(this@DashboardActivity1, testAllActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            getString(R.string.blog) -> {
                firebaseAnalytics?.let { screenViewEvent(FireBaseAnalyticsUtil.BLOGS, it) }
                val intent = Intent(this@DashboardActivity1, NewsBlogListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            getString(R.string.videos) -> {
                firebaseAnalytics?.let { screenViewEvent(FireBaseAnalyticsUtil.VIDEOS, it) }
                startActivity(Intent(this@DashboardActivity1, ExclusiveYoutubeMainClass::class.java))
            }
            getString(R.string.faqs) -> {
                firebaseAnalytics?.let { screenViewEvent(FireBaseAnalyticsUtil.FAQ, it) }
                startActivity(Intent(this@DashboardActivity1, FAQActivity::class.java))
            }
            getString(R.string.contact) -> {
                startActivity(Intent(this@DashboardActivity1, ContactUsActivity::class.java))
            }
            getString(R.string.action_logout) -> {
                logout()
            }
        }
    }

    private fun logout() {
        val builder = AlertDialog.Builder(this, R.style.MyDatePicker)
        builder.setTitle(getString(R.string.logout))
        builder.setMessage(getString(R.string.are_you_sure_you_want))
        builder.setPositiveButton(getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
            // Do nothing but close the dialog
            deregisterFCMToken()
            dialog.dismiss()
        }
        builder.setNegativeButton(
            getString(R.string.no)
        ) { dialog: DialogInterface, which: Int ->
            // Do nothing
            dialog.dismiss()
        }
        val alert = builder.create()
        alert.show()
    }

    private fun deregisterFCMToken() {
        apiInterface!!.deregisterPushToken().enqueue(object : Callback<MyResponse?> {
            override fun onResponse(call: Call<MyResponse?>, response: Response<MyResponse?>) {
                navigateToLoginScreen()
            }

            override fun onFailure(call: Call<MyResponse?>, t: Throwable) {
                navigateToLoginScreen()
            }
        })
    }

    private fun navigateToLoginScreen() {
        sessionManager!!.logout()
        startActivity(Intent(this@DashboardActivity1, LoginActivity::class.java))
        finish()
        overridePendingTransition(
            R.anim.anim_slide_in_right,
            R.anim.anim_slide_out_right
        )
    }

    override fun onBackPressed() {
        val fragmentCurrent = supportFragmentManager.findFragmentById(R.id.frame_container)
        Log.d("TAG", "onBackPressed()  Fragment$fragmentCurrent")
        if (fragmentCurrent is HomeFragment) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    private fun initToolbar() {
//        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
//        toolbar?.navigationIcon = null
//        setSupportActionBar(toolbar)
//        if (supportActionBar != null) {
//            supportActionBar!!.title = ""
//        }
        navigate()
    }

    private fun checkForUpdates() {
        val appUpdateManager: AppUpdateManager
        if (BuildConfig.DEBUG) {
            appUpdateManager = FakeAppUpdateManager(this)
            appUpdateManager.setUpdateAvailable(13)
            appUpdateManager.setUpdatePriority(5)
        } else {
            appUpdateManager = AppUpdateManagerFactory.create(this)
        }
        val appUpdateInfo = appUpdateManager.appUpdateInfo
        appUpdateInfo.addOnSuccessListener { result: AppUpdateInfo -> handleUpdate(result, appUpdateManager, appUpdateInfo) }
    }

    private fun handleUpdate(task: AppUpdateInfo, manager: AppUpdateManager, info: Task<AppUpdateInfo>) {
        if (task.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
            handleImmediateUpdate(manager, info)
        } else if (task.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
            handleFlexibleUpdate(manager, info)
        }
    }

    private fun handleImmediateUpdate(manager: AppUpdateManager, info: Task<AppUpdateInfo>) {
        if ((info.result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE ||
                        info.result.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS)
                && info.result.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
            try {
                manager.startUpdateFlowForResult(info.result, AppUpdateType.IMMEDIATE, this, REQUEST_UPDATE)
            } catch (e: SendIntentException) {
                e.printStackTrace()
            }
        }
    }

    private fun handleFlexibleUpdate(manager: AppUpdateManager, info: Task<AppUpdateInfo>) {
        if ((info.result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE ||
                        info.result.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS)
                && info.result.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
            try {
                manager.startUpdateFlowForResult(info.result, AppUpdateType.FLEXIBLE, this, REQUEST_UPDATE)
            } catch (e: SendIntentException) {
                e.printStackTrace()
            }
            if (BuildConfig.DEBUG) {
                val fakeAppUpdate = manager as FakeAppUpdateManager
                if (fakeAppUpdate.isConfirmationDialogVisible) {
                    fakeAppUpdate.userAcceptsUpdate()
                    fakeAppUpdate.downloadStarts()
                    fakeAppUpdate.downloadCompletes()
                    fakeAppUpdate.completeUpdate()
                    fakeAppUpdate.installCompletes()
                }
            }
        }
    }

    private fun navigateNextScreen(type: String) {
        val intent = Intent(this@DashboardActivity1, DashboardSubActivity::class.java)
        intent.putExtra("type", type)
        startActivity(intent)
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left)
    }

    private fun navigateToProfileScreen() {
        firebaseAnalytics?.let { screenViewEvent(FireBaseAnalyticsUtil.ACHIEVEMENT, it) }
        val intent1 = Intent(this, AchievementsActivity::class.java)
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent1)
    }

    private fun navigateToNotificationScreen() {
        firebaseAnalytics?.let { screenViewEvent("", it) }
        val intent1 = Intent(this, NotificationListActivity::class.java)
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent1)
    }

    private fun getUserDetail() {
        mRvDashboard.visibility = View.GONE
        mProgressBar.visibility = View.VISIBLE
        disposable.add(apiInterface!!.profile
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<UserResp?>() {
                    override fun onSuccess(userResp: UserResp) {
                        if (!userResp.error) {
                            user = userResp.data
                            user?.profileImage = (userResp.others.userImageBase64Path + user?.profileImage)
                            sessionManager!!.user = user

                            Log.v("TAG", "Plan name: ${user?.dietPlan?.dietPlaDetails?.plan_name}")
                        } else {
                            Toast.makeText(applicationContext, Util.getMessageInSingleLine(userResp.message), Toast.LENGTH_LONG).show()
                        }
                        mProgressBar.visibility = View.GONE
                        getTodayDietData()
                    }

                    override fun onError(e: Throwable) {
                        mProgressBar.visibility = View.GONE
                        Toast.makeText(applicationContext, e.message, Toast.LENGTH_SHORT).show()
                        getTodayDietData()
                    }
                }))
    }

    private fun getTodayDietData() {
        Log.d("TAG", "getTodayDietData()  sessionManager.getDietEPlanId(): " + sessionManager!!.dietEPlanId)
        mProgressBar.visibility = View.VISIBLE
        apiInterface!!.getDietPlan(PlanParam(sessionManager!!.dietEPlanId)).enqueue(object : Callback<Plan?> {
            override fun onResponse(call: Call<Plan?>, response: Response<Plan?>) {
                if (response.body() != null) {
                    val versionCode = BuildConfig.VERSION_CODE
                    if (versionCode < response.body()!!.data.aos_version.toInt()) {
                        update()
                    }
                    mProgressBar.visibility = View.GONE
                    if (!response.body()!!.error) {
                        Log.v("TAG", "getTodayDietData success response")
                        mPlan = response.body()!!

                    } else {
                        for (i in response.body()!!.message.indices) {
                            Toast.makeText(applicationContext, response.body()!!.message[i], Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                prepareDashboardData()
            }

            override fun onFailure(call: Call<Plan?>, t: Throwable) {
                mProgressBar.visibility = View.GONE
                Log.v("TAG", "prepareTodayDietData error response: $t")
                Toast.makeText(applicationContext, "Error in retrieving diet details from server $t", Toast.LENGTH_SHORT).show()
                prepareDashboardData()
            }
        })
    }

    private fun setPlanDetails() {
        if(user?.dietPlan?.dietPlaDetails?.id == 60) {
            val planBasic = "${user?.dietPlan?.dietPlaDetails?.plan_name}, "
            val planDetail = "$planBasic \n ${getString(R.string.move_to_advance)}"
            val spannable: Spannable = SpannableString(planDetail)
            spannable.setSpan(ForegroundColorSpan(getColor(R.color.orange_theme)), planBasic.length, planDetail.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            mTvPlanDetail.setText(spannable, TextView.BufferType.SPANNABLE)
        }else{
            mTvPlanDetail.text = user?.dietPlan?.dietPlaDetails?.plan_name
        }
        mTvPlanDetail.visibility = View.GONE
        mTvPlanDetail.setOnClickListener {
            navigateNextScreen(getString(R.string.pricing))
        }
    }

    private fun getNextMealTime() :String {
        var time = "2:00pm"
        try {
            if (!TextUtils.isEmpty(mPlan.data.time)) {
                time = DateUtil.convertTime24To12(mPlan.data.time)
            }
        } catch (ex:Exception) {
            Log.v("TAG", "Exception: ${ex.message}")
        }
        return time
    }


//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT) {
////            binding.btnProductSubscribe.setEnabled(true);
//            if (resultCode == RESULT_OK && data != null) {
//                val transactionResponse: TransactionResponse? = data.getParcelableExtra(PayUmoneyFlowManager.INTENT_EXTRA_TRANSACTION_RESPONSE)
//                val resultModel: ResultModel? = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT)
//
//                // Check which object is non-null
//                if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
//                    if (transactionResponse.transactionStatus == TransactionResponse.TransactionStatus.SUCCESSFUL) {
//                        //Success Transaction
//                        val fragment: ProductListFragment? = supportFragmentManager.findFragmentById(R.id.example_fragment) as ProductListFragment?
//                        fragment!!.subScribe(transactionResponse.getPayuResponse())
//                    } else {
//                        //Failure Transaction
//                        Toast.makeText(this, if (transactionResponse.message == "") getString(R.string.payment_failed_please_try_again) else transactionResponse.message, Toast.LENGTH_SHORT).show()
//                    }
//
//                    // Response from Payumoney
//                    val payuResponse = transactionResponse.getPayuResponse()
//                    Log.e(ProductListFragment.TAG, "onActivityResult: $payuResponse")
//                    // Response from SURl and FURL
//                    val merchantResponse = transactionResponse.transactionDetails
//                    Log.e(ProductListFragment.TAG, "onActivityResult: $merchantResponse")
//                } else if (resultModel != null && resultModel.error != null) {
//                    Log.d(ProductListFragment.TAG, "Error response : " + resultModel.error.transactionResponse)
//                } else {
//                    Log.d(ProductListFragment.TAG, "Both objects are null!")
//                }
//            } else {
//                Toast.makeText(this, "Payment failed", Toast.LENGTH_SHORT).show()
//            }
//        } else if (requestCode == PersonalDetailActivity.UPDATE_USER_REQ) {
//            if (resultCode == RESULT_OK) {
//                user = sessionManager!!.user
//            }
//        }
//    }

//    fun subScribe(payuResponse: String) {
//        val params = HashMap<String, Any>()
//        params["diet_plan_id"] = mSelectedProduct.getId()
//        params["payuResponse"] = payuResponse
//        apiInterface!!.subScribe(params).enqueue(object : Callback<SubScribeResp?> {
//            override fun onResponse(call: Call<SubScribeResp?>, response: Response<SubScribeResp?>) {
//                if (response.isSuccessful) {
//                    if (response.body() != null) {
//                        if (!response.body()!!.error) {
//                            val data = response.body()!!.data
//                            //                            txtProductTitle = data.getDietPlan().getPlanName();
////                            durationMonth = data.getDietPlan().getDurationInMonth();
//                            val dialog = Dialog(this@DashboardActivity1)
//                            dialog.setContentView(R.layout.congratulation_screen)
//                            dialog.setCanceledOnTouchOutside(false)
//                            val tvMsg = dialog.findViewById<TextView>(R.id.tv_detail)
//                            val btnOk = dialog.findViewById<Button>(R.id.btn_ok)
//                            tvMsg.text = Util.fromHtml("Your " + data.dietPlan.durationInMonth + " Months " + "&ldquo; " + data.dietPlan.planName + " &rdquo;" + " Diet Plan Active Now!")
//                            btnOk.setOnClickListener { view: View? ->
//                                dialog.dismiss()
//                                MyApplication.getLocaleManager().setNewLocale(this@DashboardActivity1, user!!.userDetail.language)
//                                val intent = Intent(this@DashboardActivity1, DashboardActivity1::class.java)
//                                startActivity(intent)
//                                finish()
//                            }
//                            dialog.show()
//                            val back = ColorDrawable(Color.TRANSPARENT)
//                            val inset = InsetDrawable(back, 40)
//                            dialog.window!!.setBackgroundDrawable(inset)
//                            dialog.window!!.setLayout(6 * width / 7, LinearLayout.LayoutParams.WRAP_CONTENT)
//                        } else {
//                            Toast.makeText(this@DashboardActivity1, response.body()!!.message[0], Toast.LENGTH_LONG).show()
//                        }
//                    }
//                } else {
//                    Toast.makeText(this@DashboardActivity1, response.message(), Toast.LENGTH_SHORT).show()
//                }
//            }
//
//            override fun onFailure(call: Call<SubScribeResp?>, t: Throwable) {
//                Log.e(ProductListFragment.TAG, "onFailure: ", t)
//                Toast.makeText(this@DashboardActivity1, t.message, Toast.LENGTH_SHORT).show()
//            }
//        })
//    }

    private fun update() {
        val builder = AlertDialog.Builder(this, R.style.MyDatePicker)
        builder.setTitle("Update")
        builder.setMessage("Kindly Update App from Play store for latest changes.")
        builder.setPositiveButton("Update") { dialog: DialogInterface, which: Int ->
            // Do nothing but close the dialog
            val appPackageName = packageName // getPackageName() from Context or Activity object
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
            } catch (anfe: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
            }
            finish()
            dialog.dismiss()
        }
        builder.setCancelable(false)
        val alert = builder.create()
        alert.show()
    }

    private fun navigateToAboutDQScreen() {
        val intent = Intent(this, AboutDietQueenActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

}