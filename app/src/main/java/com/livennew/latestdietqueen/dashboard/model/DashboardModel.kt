package com.livennew.latestdietqueen.dashboard.model

class DashboardModel {
    var title: String? = null
    var subTitle: String? = null
    var subTitle1: String? = null
    var subTitle2: String? = null
    var id = 0
    var icon = -1
    var subIcon = -1
    var isEnabled = true
    var imagePath: String? = null
}