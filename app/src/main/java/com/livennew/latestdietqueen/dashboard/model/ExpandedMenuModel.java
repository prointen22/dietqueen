package com.livennew.latestdietqueen.dashboard.model;

public class ExpandedMenuModel {

    String iconName = "";
    int iconImg = -1; // menu icon resource id
    String name;

    public ExpandedMenuModel(String name, String iconName) {
        this.name = name;
        this.iconName = iconName;
    }

    public ExpandedMenuModel() {

    }

    public ExpandedMenuModel(String name, String iconName, int iconImg) {
        this.name=name;
        this.iconName=iconName;
        this.iconImg=iconImg;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public int getIconImg() {
        return iconImg;
    }

    public void setIconImg(int iconImg) {
        this.iconImg = iconImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}