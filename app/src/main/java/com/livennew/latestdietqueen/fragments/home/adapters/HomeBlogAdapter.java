package com.livennew.latestdietqueen.fragments.home.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.fragments.home.model.Blog;
import com.livennew.latestdietqueen.fragments.home.model.BlogListResp;

import java.util.ArrayList;
import java.util.List;

public class HomeBlogAdapter extends RecyclerView.Adapter<HomeBlogAdapter.MyViewHolder> {

    private List<Blog> list;
    private BlogListResp.Others others;
    private final Context context;
    private Listener listener;

    public HomeBlogAdapter(Context context) {
        this.context = context;
        list = new ArrayList<>();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_home_blog_content, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Blog blog = list.get(position);
        Glide.with(context)
                .load(others.getBlogImagePath() + blog.getUploadFile())
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.ivHomeBlog);
        holder.tvHomeBlogTitle.setText(list.get(position).getTitle());

        if(list.get(position).getAuthor().toString().equals("NULL"))
        {
            holder.tv_home_blog_author.setVisibility(View.GONE);
            holder.tv_home_blog_author.setText("");

        }else {
            holder.tv_home_blog_author.setVisibility(View.VISIBLE);
            holder.tv_home_blog_author.setText(list.get(position).getAuthor());
        }
    holder.tv_home_blog_created_at.setText(list.get(position).getCreatedAt());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(blog);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setData(List<Blog> list, BlogListResp.Others others) {
        this.list = list;
        this.others = others;
        notifyDataSetChanged();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivHomeBlog;
        TextView tvHomeBlogTitle;
        TextView tv_home_blog_author,tv_home_blog_created_at;
        CardView cvHomeBlog;
        private View itemView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivHomeBlog = itemView.findViewById(R.id.iv_home_blog);
            tvHomeBlogTitle = itemView.findViewById(R.id.tv_home_blog_title);
            cvHomeBlog = itemView.findViewById(R.id.cv_home_blog);
           // tv_home_blog_created_at = itemView.findViewById(R.id.tv_home_blog_created_at);
            tv_home_blog_author= itemView.findViewById(R.id.tv_home_blog_author);
            tv_home_blog_created_at= itemView.findViewById(R.id.tv_home_blog_created_at);
            this.itemView = itemView;
        }
    }

    public interface Listener {
        void onItemClick(Blog blog);
    }
}