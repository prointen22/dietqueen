package com.livennew.latestdietqueen.fragments.home.adapters;


import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.fragments.home.model.Recipes;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.fragments.home.model.Testimonials;
import com.livennew.latestdietqueen.retrofit.APIInterface;

import java.util.List;

import javax.inject.Inject;

public class HomeRecepieNutrientAdapter extends RecyclerView.Adapter<HomeRecepieNutrientAdapter.MyViewHolder> {

    private List<Recipes> list;
    private TestimonialListResp.Others others;
    private final Context context;
    private Listener listener;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    public HomeRecepieNutrientAdapter(Context context, List<Recipes> list) {
        this.context = context;
        this.list = list;
        this.others = others;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_recepie_indigrient, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
      Recipes constituent = list.get(position);


      try {



        holder.tvt_raw_constituentt.setText(constituent.getCalories());

        if(constituent.getUnit_name().equals("CALORIE")){
            holder.tvt_raw_constituentt.setTextColor(Color.parseColor("#FF8640"));
            holder.tvt_raw_consti_unit.setText(constituent.getUnit_name());
        }else {
            holder.tvt_raw_constituentt.setTextColor(R.color.black);

            holder.tvt_raw_consti_unit.setText(constituent.getUnit_name());
        }

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(blog);
            }
        });*/

      }catch (Exception e){

      }



    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView im_consti;
        TextView tvt_raw_constituentt,tvt_raw_consti_unit;
        CardView cvHomeBlog;
        private View itemView;
        CardView cv_testimonial;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            im_consti = itemView.findViewById(R.id.im_consti);
            tvt_raw_constituentt = itemView.findViewById(R.id.tvt_raw_constituent);
            tvt_raw_consti_unit = itemView.findViewById(R.id.tvt_raw_consti_unit);
            cv_testimonial = itemView.findViewById(R.id.cv_testimonial);

        }
    }

    public interface Listener {
        void onItemClick(Testimonials id);
    }




}