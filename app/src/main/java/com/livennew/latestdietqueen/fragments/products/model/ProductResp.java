package com.livennew.latestdietqueen.fragments.products.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.model.MyResponse;


public class ProductResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private Product product;

    @SerializedName("others")
    @Expose
    private Others others;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }
}
