package com.livennew.latestdietqueen.fragments.products.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.fragments.products.model.BuyNowModel;

import java.util.ArrayList;

public class ProductBuyNowAdapter extends RecyclerView.Adapter<ProductBuyNowAdapter.MyViewHolder> {

    private ArrayList<BuyNowModel> alProductBuyNow;
    private Context context;


    public ProductBuyNowAdapter(Context context, ArrayList<BuyNowModel> alProductBuyNow) {
        this.context = context;
        this.alProductBuyNow = alProductBuyNow;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.buy_product_content, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final BuyNowModel buyNowModel = alProductBuyNow.get(position);
        if (buyNowModel.isRecommended()) {
            holder.llOuterContainer.setBackgroundColor(context.getResources().getColor(R.color.checktoggle));
            holder.tvSponsered.setVisibility(View.VISIBLE);
        } else {
            holder.llOuterContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.tvSponsered.setVisibility(View.INVISIBLE);
        }
        holder.tvMonthNum.setText(String.valueOf(buyNowModel.getMonth_num()));
        holder.tvMonth.setText(buyNowModel.getMonth());
        holder.tvPrice.setText(String.valueOf(buyNowModel.getProduct_buy_price()).concat("/-"));
        holder.tvDescription.setText(buyNowModel.getDiet_plan_info());
    }

    @Override
    public int getItemCount() {
        return alProductBuyNow.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayoutCompat llOuterContainer;
        TextView tvSponsered, tvMonthNum, tvMonth, tvPrice, tvDescription;
        //CardView cvProductBuy;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSponsered = itemView.findViewById(R.id.tvSponsered);
            tvMonthNum = itemView.findViewById(R.id.tv_month_num);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvPrice = itemView.findViewById(R.id.tv_buy_prod_price);
            tvDescription = itemView.findViewById(R.id.tv_buy_prod_desc);
            llOuterContainer = itemView.findViewById(R.id.ll_outer_container_view);

        }
    }
}
