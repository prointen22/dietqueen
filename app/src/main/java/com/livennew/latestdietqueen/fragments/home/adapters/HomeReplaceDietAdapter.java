package com.livennew.latestdietqueen.fragments.home.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.fragments.home.RecepieActivity;
import com.livennew.latestdietqueen.retrofit.APIInterface;

import java.util.List;

import javax.inject.Inject;

public class HomeReplaceDietAdapter extends RecyclerView.Adapter<HomeReplaceDietAdapter.MyViewHolder> {
    private List<Food> alTodayDietModel;
    private Others others;
    private Context context;
    private HomeReplaceDietAdapter.Listener listener;
    String lastSelectID="",data_e_Data="";
    List<Food> foodid;
    List<Food> foodname;
    List<Food> imgpathFood;
    List<Food> isRecipe;

    @Inject
    APIInterface apiInterface;

    public HomeReplaceDietAdapter(Context context, List<Food> alTodayDietModel,String lastSelectID,String data_e_Data,List<Food>  foodid,List<Food>  foodname,List<Food>  imgpathFood,List<Food>  isRecipe) {
        this.context = context;
        this.alTodayDietModel = alTodayDietModel;
        this.listener = listener;
        this.lastSelectID = lastSelectID;
        this.data_e_Data = data_e_Data;
        this.foodid = foodid;
        this.foodname = foodname;

        this.imgpathFood = imgpathFood;
this.isRecipe=isRecipe;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_replace_item_home, parent, false);

        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT,  LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(10,15,10,15);
     /*   int width = context.getResources().getDisplayMetrics().widthPixels;
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = (int) (width * 0.6);*/
        view.setLayoutParams(lp);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Food todayDietModel = alTodayDietModel.get(position);
        final Food todayDietModelImgPath = imgpathFood.get(position);
        final Food fid = foodid.get(position);
        final Food fname = foodname.get(position);
        final Food isRecepie = isRecipe.get(position);


Log.d("isRecepie","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"+todayDietModel.getFoodGroupId());

      Glide.with(context)
                .load(todayDietModelImgPath.getImage().toString())
                .apply(RequestOptions.circleCropTransform())
              .apply(new RequestOptions().override(80, 80))

              .into(holder.img_item);

if(fname.getFoodMasterName()!=null) {
    holder.tvt_replace_item_name.setText(fname.getFoodMasterName().toString());
}else{
    holder.tvt_replace_item_name.setText("");
}

        holder.lyn_replce_out.setTag(fid.getId().toString());
        holder.lyn_replce_out.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if (listener != null){
            //listener.onDoneClick(position, todayDietMenuModel, others);

            listener.onItemClick(Integer.parseInt(view.getTag().toString()),Integer.parseInt(lastSelectID),Integer.parseInt(data_e_Data));



        }
    }
});

        if(isRecepie.getIs_recipe().equals("Y")){
            holder.tvt_recepie_book.setVisibility(View.VISIBLE);
        }else{
            holder.tvt_recepie_book.setVisibility(View.GONE);
        }
        holder.tvt_recepie_book.setTag(fid.getId().toString()+"##"+lastSelectID+"##"+data_e_Data+"##"+todayDietModel.getFoodGroupId());
        holder.tvt_recepie_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String data=view.getTag().toString();
                    Log.d("kkkkkkk","uuuuuuuuuuuuuuuuuuuuuuuuuuu tvt_recepie_book uuuuuuuuuuuuuuuuuuuuuuuuuuuuu"+view.getTag());
                String[] separated = data.split("##");
                Log.d("kkkkkkk","uuuuuuuuuuuuuuuuuuuuuuuuuuu tvt_recepie_book  Seperated uuuuuuuuuuuuuuuuuuuuuuuuuuuuu"+separated[0]+separated[1]+separated[2]);


                    //listener.onDoneClick(position, todayDietMenuModel, others);

                  //  listener.onItemClick(Integer.parseInt(view.getTag().toString()),Integer.parseInt(lastSelectID),Integer.parseInt(data_e_Data));

Intent i=new Intent(context, RecepieActivity.class);
i.putExtra("newID",separated[0]);
                i.putExtra("lastFoodID",separated[1]);
                i.putExtra("data_e_Data",separated[2]);
                i.putExtra("foodGroupID",separated[3]);

                context.startActivity(i);


            }
        });


    }

    @Override
    public int getItemCount() {
        return alTodayDietModel.size();
    }


    public void setListener(HomeReplaceDietAdapter.Listener listener) {
        this.listener = listener;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvt_replace_item_name,tvt_recepie_book;
        ImageView img_item;
        LinearLayout lyn_replce_out;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            img_item = itemView.findViewById(R.id.img_item);
            tvt_replace_item_name = itemView.findViewById(R.id.tvt_replace_item_name);
            lyn_replce_out= itemView.findViewById(R.id.lyn_replce_out);
            tvt_recepie_book= itemView.findViewById(R.id.tvt_recepie_book);
        }
    }



    public interface Listener {
        void onItemClick(Integer id,Integer lastselectID,Integer dataEData);
    }
}
