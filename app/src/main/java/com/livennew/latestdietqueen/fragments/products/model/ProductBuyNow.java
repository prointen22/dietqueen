package com.livennew.latestdietqueen.fragments.products.model;

import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.fragments.products.adapter.ProductBuyNowAdapter;

import java.util.ArrayList;

public class ProductBuyNow extends FabActivity {
    private Toolbar toolbarProductDetail;
    private RecyclerView rvProductBuyNow;
    private Button btnBuyNow;
    private ImageView ivProductImage;
    private TextView tvInstruction1, tvInstruction2, tvInstruction3, tvInstruction4, tvProductBuyNowTitle;
    private ProductBuyNowAdapter productBuyNowAdapter;
    private ArrayList<BuyNowModel> alBuyNow;
    private NestedScrollView nScrollViewProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_buy_now);
        init();
        setSupportActionBar(toolbarProductDetail);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbarProductDetail.setNavigationIcon(R.drawable.ic_navigation_before_pink_24dp);
        onSetVideoRecyclerView();
        /*nScrollViewProduct.post(new Runnable() {
            public void run() {
                nScrollViewProduct.fullScroll(Ho.FOCUS_RIGHT);
            }
        });*/
        rvProductBuyNow.setFocusable(false);
        ivProductImage.requestFocus();
    }

    private void init() {
        toolbarProductDetail = findViewById(R.id.toolbar_productbuy_now_detail);
        btnBuyNow = findViewById(R.id.btn_product_buy);
        ivProductImage = findViewById(R.id.iv_product);
        tvProductBuyNowTitle = findViewById(R.id.tv_product_nm);
        tvInstruction1 = findViewById(R.id.tv_instruction1);
        tvInstruction2 = findViewById(R.id.tv_instruction2);
        tvInstruction3 = findViewById(R.id.tv_instruction3);
        tvInstruction4 = findViewById(R.id.tv_instruction4);
        rvProductBuyNow = findViewById(R.id.rv_diet_plan);
        nScrollViewProduct = findViewById(R.id.nScroolViewProduct);
    }

    public void onSetVideoRecyclerView() {
        alBuyNow = new ArrayList<>();
        productBuyNowAdapter = new ProductBuyNowAdapter(this, alBuyNow);
        RecyclerView.LayoutManager videoLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvProductBuyNow.setLayoutManager(videoLayoutManager);
        rvProductBuyNow.setAdapter(productBuyNowAdapter);
        prepareBuyNowData();
    }

    private void prepareBuyNowData() {
        BuyNowModel buyNowModel = new BuyNowModel(1, false, 3, "Months", 999, "Additional One Month free");
        alBuyNow.add(buyNowModel);

        buyNowModel = new BuyNowModel(2, true, 6, "Months", 2999, "Additional Two Month free");
        alBuyNow.add(buyNowModel);

        buyNowModel = new BuyNowModel(3, false, 12, "Months", 11999, "Additional Three Month free");
        alBuyNow.add(buyNowModel);

        productBuyNowAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
