package com.livennew.latestdietqueen.fragments.home.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.fragments.home.model.Testimonials;
import com.livennew.latestdietqueen.retrofit.APIInterface;

import java.util.List;

import javax.inject.Inject;

public class HomeRecepieAdapter extends RecyclerView.Adapter<HomeRecepieAdapter.MyViewHolder> {

    private List<Testimonials> list;
    private TestimonialListResp.Others others;
    private final Context context;
    private Listener listener;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    public HomeRecepieAdapter(Context context, List<Testimonials> list, TestimonialListResp.Others others) {
        this.context = context;
        this.list = list;
        this.others = others;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_home_testimonial_content, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
      Testimonials blog = list.get(position);
        Log.d("testimonial",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+others.getTestimonialImagePath() + blog.getUploadFile());
        Log.d("testimonial",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+list.get(position).getTitle());
        Log.d("testimonial",">>>>>>>>>>>>>>>>>>>>>>>>>>> getId >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+list.get(position).getId());
        Log.d("testimonial",">>>>>>>>>>>>>>>>>>>>>>>>>>> getId >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+list.get(position).getCreatedAt());
        Log.d("testimonial",">>>>>>>>>>>>>>>>>>>>>>>>>>> getId >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+list.get(position).getAuthor_details());

        Glide.with(context)
                .load(others.getTestimonialImagePath() + list.get(position).getUploadFile())
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.ivHomeBlog);
        holder.tvHomeBlogTitle.setText(list.get(position).getTitle());

        holder.cv_testimonial.setTag(list.get(position).getId());
        holder.tv_home_testimonial_createt_at.setText(list.get(position).getCreatedAt());
        holder.tv_home_testimonial_author.setText(list.get(position).getAuthor_details());

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(blog);
            }
        });*/



        holder.cv_testimonial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null){
                    //listener.onDoneClick(position, todayDietMenuModel, others);
                    listener.onItemClick(blog);
                  //  listener.onItemClick();
                    Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxx   xxxxxxxxxxxxxxxxxxxxxxxxxx" +blog.getId());
                    Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");



                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivHomeBlog;
        TextView tvHomeBlogTitle,tv_home_testimonial_author,tv_home_testimonial_createt_at;
        CardView cvHomeBlog;
        private View itemView;
        CardView cv_testimonial;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivHomeBlog = itemView.findViewById(R.id.iv_home_blog);
            tvHomeBlogTitle = itemView.findViewById(R.id.tv_home_blog_title);
            cvHomeBlog = itemView.findViewById(R.id.cv_home_blog);
            cv_testimonial = itemView.findViewById(R.id.cv_testimonial);
            tv_home_testimonial_author= itemView.findViewById(R.id.tv_home_testimonial_author);
                    tv_home_testimonial_createt_at= itemView.findViewById(R.id.tv_home_testimonial_createt_at);
            this.itemView = itemView;
        }
    }

    public interface Listener {
        void onItemClick(Testimonials id);
    }




}