package com.livennew.latestdietqueen.fragments.home;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.dashboard.DashboardActivity1;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeRecepieConstituentAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeRecepieNutrientAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeRecepiedetailsAdapter;
import com.livennew.latestdietqueen.fragments.home.model.RecepieList;
import com.livennew.latestdietqueen.fragments.home.model.Recipes;
import com.livennew.latestdietqueen.fragments.home.model.Testimonials;
import com.livennew.latestdietqueen.fragments.products.model.ProductResp;
import com.livennew.latestdietqueen.model.FoodReplaceResp;
import com.livennew.latestdietqueen.model.ReplaceFoodParam;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.DateUtil;


import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class RecepieActivity extends AppCompatActivity  implements View.OnClickListener{
    private static final String TAG = RecepieActivity.class.getSimpleName();
    private Video item;
    private String tagGetVideoDetails = "getVideoDetails";
    private Testimonials binding;
    Toolbar toolbar;
    Integer foood_master_id,previous,next;
    List<Recipes> listingConstituenst = new ArrayList<>();
    List<Recipes> listingIndigrients = new ArrayList<>();

    List<Recipes> listingReceipeDetails = new ArrayList<>();

        List<String> recipeID = new ArrayList<>();
        String is_like_status="N";
    @Inject
    SessionManager sessionManager;
    private LinearLayoutCompat llFooter;
    @Inject
    APIInterface apiInterface;
    private User user;
    RecyclerView rv_home_video,rv_constituents,rv_recepie_summary,rv_nutritiens;
//    RecyclerViewAdapter recyclerViewAdapter;
    HomeRecepieConstituentAdapter recyclerAdapterConstituent;
    HomeRecepieNutrientAdapter recyclerAdapterNutritient;
    HomeRecepiedetailsAdapter recyclerAdapterDetails;
    ProgressBar progress_bar;
    Button btn_replace_button;
    String data_e_Data,lastFoodID,newID,food_group_id;
    TextView tvt_heart,tvt_back,tvt_forward,tvt_backword,tvt_seeall;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recepie);


        Intent intent = getIntent();
        if(intent!=null){
            newID= intent.getStringExtra("newID");
            lastFoodID= intent.getStringExtra("lastFoodID");
            data_e_Data= intent.getStringExtra("data_e_Data");
            food_group_id= intent.getStringExtra("foodGroupID");
Log.d("ggggg","yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy food_group_id yyyyyyyyyyyyyyyyyyyyyyyy"+food_group_id);

            recipeID.add(newID);
        }


        rv_constituents=(RecyclerView)findViewById(R.id.rv_constituents);
        rv_recepie_summary=(RecyclerView)findViewById(R.id.rv_recepie_summary);
        rv_nutritiens=(RecyclerView)findViewById(R.id.rv_nutritiens);
        btn_replace_button=(Button)findViewById(R.id.btn_replace_button);
        tvt_heart=(TextView) findViewById(R.id.tvt_heart);
        tvt_seeall=(TextView) findViewById(R.id.tvt_seeall);
        tvt_back=(TextView)findViewById(R.id.tvt_back);
        tvt_forward=(TextView)findViewById(R.id.tvt_forward);
                tvt_backword=(TextView)findViewById(R.id.tvt_backword);
        tvt_heart.setOnClickListener(this::onClick);
        tvt_back.setOnClickListener(this::onClick);
        tvt_forward.setOnClickListener(this::onClick);
        tvt_backword.setOnClickListener(this::onClick);
        tvt_seeall.setOnClickListener(this::onClick);
        btn_replace_button.setOnClickListener(this);
        getDetaisl(newID);

    }



public void getDetaisl(String FoodID){

    listingReceipeDetails.clear();
    listingIndigrients.clear();
    listingConstituenst.clear();


    Log.d("mmm", "/////////////////////////////// FoodID  //////////////////// "+FoodID);
       /* recyclerAdapterNutritient.notifyDataSetChanged();
        recyclerAdapterConstituent.notifyDataSetChanged();
        recyclerAdapterDetails.notifyDataSetChanged();
*/

    Map<String, Integer> params = new HashMap<String, Integer>();
    params.put("id",Integer.parseInt(FoodID));
    params.put("perPage", 3);
    params.put("food_group_id",Integer.parseInt(food_group_id));

    Log.d("mmm", "/////////////////////////////////////////////////// testimonial 1");
    apiInterface.getRecepie(params).enqueue(new Callback<RecepieList>() {
        @Override
        public void onResponse(@NotNull Call<RecepieList> call, @NotNull retrofit2.Response<RecepieList> response) {

            RecepieList rlist=response.body();
          //  TestimonialListResp blogResp1 = response.body();


            try {
               // String img_path = rlist.getOthers().getRecipeImagePath().toString();

                foood_master_id = rlist.getData().getRecipes().get(0).getId();
                newID = "";
                newID = String.valueOf(foood_master_id);

                previous = response.body().getData().getPrevious();
                next = response.body().getData().getNext();

                if (previous == null && previous.equals("")) {

                    tvt_backword.setVisibility(View.GONE);
                } else {
                    tvt_backword.setVisibility(View.VISIBLE);


                }

                if (next == null && next.equals("")) {
                    tvt_forward.setVisibility(View.GONE);
                } else {
                    tvt_backword.setVisibility(View.VISIBLE);
                }

                Log.d("mmm", "/////////////////////////////////////////////////// testimonial previous" + previous);
                Log.d("mmm", "/////////////////////////////////////////////////// testimonial next" + next);

                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2" + rlist.getData().getCurrentPage());
                Log.d("mmm", "////////////////////////////// getRecipeImagePath ///////////////////// testimonial 2" + rlist.getOthers().getRecipeImagePath());
                Log.d("mmm", "////////////////////////////// getRecipeImagePath ///////////////////// testimonial 2" + rlist.getOthers().getFoodImagePath());
                Log.d("mmm", "////////////////////////////// getRecipeImagePath  getImage///////////////////// testimonial 2" + rlist.getData().getRecipes().get(0).getImage());
                Log.d("mmm", "////////////////////////////// getRecipeImagePath  getMaking_description///////////////////// testimonial 2" + rlist.getData().getRecipes().get(0).getMaking_description());
                Log.d("mmm", "////////////////////////////// getRecipeImagePath  getFood_master_name///////////////////// testimonial 2" + rlist.getData().getRecipes().get(0).getFood_master_name());
                Log.d("dddd", "mmmmmmmmmmmmmmmmmmmmm  Data getIs_like mmmmmmmmmm" + response.body().getData().getRecipes().get(0).getIs_like());

                is_like_status = response.body().getData().getRecipes().get(0).getIs_like();
                if (response.body().getData().getRecipes().get(0).getIs_like().equals("N")) {
                    //tvt_heart.setBackgroundDrawable(R.id.tvt_heart);
                    tvt_heart.setBackgroundResource(R.drawable.ic_heart_outline);

                } else {
                    tvt_heart.setBackgroundResource(R.drawable.ic_heart_filled);

                }


                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2" + rlist.getData().getRecipes().get(0).getId());
                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2" + rlist.getData().getRecipes().get(0).getConversion_factor().get(0).getQuantity());

                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2" + rlist.getData().getRecipes().get(0).getConversion_factor().get(0).getRaw_food());

           /* if (!blogResp1.getError()) {

                // binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2 " + blogResp1.getData().getTitle());
                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2 " + blogResp1.getOthers().getTestimonialImagePath());

            }*/
                Recipes recipeConstituent1 = new Recipes();
                recipeConstituent1.setRecepieImage( rlist.getOthers().getRecipeImagePath()+rlist.getData().getRecipes().get(0).getImage());
                recipeConstituent1.setRecepieName(rlist.getData().getRecipes().get(0).getFood_master_name());
                recipeConstituent1.setRecepieDescription(rlist.getData().getRecipes().get(0).getMaking_description());


                listingReceipeDetails.add(recipeConstituent1);
                recyclerAdapterDetails = new HomeRecepiedetailsAdapter(getApplicationContext(), listingReceipeDetails);
                RecyclerView.LayoutManager recycev1 = new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false);
                //    recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));

                rv_recepie_summary.setLayoutManager(recycev1);
                rv_recepie_summary.setItemAnimator(new DefaultItemAnimator());
                rv_recepie_summary.setAdapter(null);
                rv_recepie_summary.setAdapter(recyclerAdapterDetails);

                // ****************************************************************    Nutritient ***************************************************
                Integer nutritientSize = rlist.getData().getRecipes().get(0).getIngredients().size();


                if (nutritientSize > 0) {
                    for (int j = 0; j < nutritientSize; j++) {
                        Recipes recipeConstituent = new Recipes();
                        if ((rlist.getData().getRecipes().get(0).getIngredients().get(j).getCalories() != null && rlist.getData().getRecipes().get(0).getIngredients().get(j).getCalories() != "")) {
                            long calories = Math.round(Double.parseDouble(rlist.getData().getRecipes().get(0).getIngredients().get(j).getCalories()));
                            String cal = String.valueOf(calories);

                            if (rlist.getData().getRecipes().get(0).getIngredients().get(j).getUnit_name().equals("CALORIE")) {
                                recipeConstituent.setCalories(cal);
                                recipeConstituent.setUnit_name(rlist.getData().getRecipes().get(0).getIngredients().get(j).getUnit_name());

                            } else {

                                recipeConstituent.setCalories(cal + "" + rlist.getData().getRecipes().get(0).getIngredients().get(j).getUnit_name());
                                recipeConstituent.setUnit_name(rlist.getData().getRecipes().get(0).getIngredients().get(j).getIngredient_name());
                                // recipeConstituent.setConstituentUnit(rlist.getData().getRecipes().get(0).getConversion_factor().get(i).getRaw_food().getUnit().getName());

                            }
                        }

                        listingIndigrients.add(recipeConstituent);
                    }


                    recyclerAdapterNutritient = new HomeRecepieNutrientAdapter(getApplicationContext(), listingIndigrients);

                    //    RecyclerView.LayoutManager recyce = new GridLayoutManager(getActivity(),2);
                    RecyclerView.LayoutManager recycev_nutri = new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false);
                    //    recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));

                    rv_nutritiens.setLayoutManager(recycev_nutri);
                    rv_nutritiens.setItemAnimator(new DefaultItemAnimator());
                    rv_nutritiens.setAdapter(null);
                    rv_nutritiens.setAdapter(recyclerAdapterNutritient);


                }
                // ****************************************************************    Constituent ***************************************************

                Integer constituentSize = rlist.getData().getRecipes().get(0).getConversion_factor().size();


                if (constituentSize > 0) {
                    for (int i = 0; i < constituentSize; i++) {
                        Recipes recipeConstituent = new Recipes();

                        Log.d("Consti", "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Consti  >>>>>>>>>>>>>>>>>>>>>>>>>>" + rlist.getData().getRecipes().get(0).getConversion_factor().get(i).getRaw_food().getImage());
                        Log.d("Consti", "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Consti  >>>>>>>>>>>>>>>>>>>>>>>>>>" + rlist.getData().getRecipes().get(0).getConversion_factor().get(i).getRaw_food().getUnit().getName());
                        Log.d("Consti", "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Consti  >>>>>>>>>>>>>>>>>>>>>>>>>>" + rlist.getData().getRecipes().get(0).getConversion_factor().get(i).getRaw_food().getFood_master_name());
                        recipeConstituent.setConstituentimage( rlist.getOthers().getFoodImagePath()+rlist.getData().getRecipes().get(0).getConversion_factor().get(i).getRaw_food().getImage());
                        recipeConstituent.setConstituent_food_master_name(rlist.getData().getRecipes().get(0).getConversion_factor().get(i).getRaw_food().getFood_master_name());
                        recipeConstituent.setConstituentUnit(rlist.getData().getRecipes().get(0).getConversion_factor().get(i).getQuantity() + rlist.getData().getRecipes().get(0).getConversion_factor().get(i).getRaw_food().getUnit().getName());


                        listingConstituenst.add(recipeConstituent);
                    }


                    recyclerAdapterConstituent = new HomeRecepieConstituentAdapter(getApplicationContext(), listingConstituenst);

                    //    RecyclerView.LayoutManager recyce = new GridLayoutManager(getActivity(),2);
                    RecyclerView.LayoutManager recycev = new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false);
                    //    recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));

                    rv_constituents.setLayoutManager(recycev);
                    rv_constituents.setItemAnimator(new DefaultItemAnimator());
                    rv_constituents.setAdapter(null);
                    rv_constituents.setAdapter(recyclerAdapterConstituent);
                }
            }catch (Exception e){



            }

        }

        @Override
        public void onFailure(@NotNull Call<RecepieList> call, @NotNull Throwable t) {
            Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2" + t);

        }
    });
}


    @Override
    public void onClick(View v) {

        switch (v.getId()){


            case R.id.btn_replace_button:

                replace(Integer.parseInt(newID),Integer.parseInt(lastFoodID),Integer.parseInt(data_e_Data));
                break;

            case R.id.tvt_back:
                super.onBackPressed();

                break;
            case R.id.tvt_backword:
                if(previous>0) {
                    getDetaisl(String.valueOf(previous));
                }
                break;

            case R.id.tvt_forward:
                if(next > 0) {
                    getDetaisl(String.valueOf(next));
                }
                break;

            case R.id.tvt_heart:

                if(is_like_status.equals("N")){
                    is_like_status="Y";
                    tvt_heart.setBackgroundResource(R.drawable.ic_heart_filled);


                }else{
                    tvt_heart.setBackgroundResource(R.drawable.ic_heart_outline);

                    is_like_status="N";

                }

                islike_dislike(is_like_status,foood_master_id);
                break;
            case R.id.tvt_seeall:

                super.onBackPressed();
                break;
        }

    }

    public void replace(Integer newFoodId,Integer oldFoodId,Integer dietEDataId) {
        ProgressDialog pd=new ProgressDialog(this);

        pd.show();

        //    apiInterface.replaceFood(new ReplaceFoodParam(id, lastselectID,dataEData, DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
        //  int oldFoodId, int newFoodId, int dietEDataId, String date
        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt oldFoodId"+oldFoodId);
        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt newFoodId"+newFoodId);
        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt dietEDataId"+dietEDataId);
        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt DateUtil.getTodayDate()"+DateUtil.getTodayDate());

        apiInterface.replaceFood(new ReplaceFoodParam(oldFoodId,newFoodId,dietEDataId, DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
            @Override
            public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
                pd.dismiss();
                if (response.body() != null) {
                    if (response.body().getError()) {
                        for (int i = 0; i < response.body().getMessage().size(); i++)
                            //Toast.makeText(this, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                            Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt getFood"+ response.body().getMessage().get(i));
                        pgDialog(response.body().getMessage().get(0).toString(),"true");


                    } else {
                        //todayDietMenu.setFood(response.body().getData().getFood());
                        //  myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt getFood"+ response.body().getMessage());
                        pgDialog(response.body().getMessage().get(0).toString(),"false");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                pd.dismiss();
            }
        });






//current_iten_id,last_select_item_id,diet_eplan_id,time
  /*    apiInterface.replaceFood(new ReplaceFoodParam(todayDietMenu.getFood().getId(), replaceItemAdapter.getLastSelectedFood().getId(), todayDietMenu.getDietEDataId(), DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
          @Override
          public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
              if (response.body() != null) {
                  if (response.body().getError()) {
                      for (int i = 0; i < response.body().getMessage().size(); i++)
                          Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                  } else {
                      todayDietMenu.setFood(response.body().getData().getFood());
                      myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                  }
              }
              dialog.dismiss();
          }

          @Override
          public void onFailure(@NonNull Call call, @NonNull Throwable t) {
              dialog.dismiss();
          }
      });

  }*/
    }



    public  void pgDialog(String msg,String error){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View alertDialogView = inflater.inflate(R.layout.aler_dialog_item_replace, null);
        alertDialog.setView(alertDialogView);

        TextView textDialog = (TextView) alertDialogView.findViewById(R.id.replace_item);

        if(error.equals("true")){

            textDialog.setText(msg);
        }else {


            if (msg.equals("You Have Already Updated This Food")) {
                textDialog.setText("You Have Already Updated This Food");
            } else {
                textDialog.setText("You have replaced an Item");
            }
        }
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                Intent i=new Intent(getApplicationContext(), DashboardActivity1.class);
                startActivity(i);

            }
        });
        alertDialog.show();



    }

    public void islike_dislike(String islike,Integer foood_master_id){


        Map<String, String> params = new HashMap<String, String>();
        params.put("food_master_id",String.valueOf(foood_master_id));
        params.put("is_like",islike);


        Log.d("mmm", "/////////////////////////////////////////////////// testimonial 1");
        apiInterface.is_like(params).enqueue(new Callback<ProductResp>() {
            @Override
            public void onResponse(@NotNull Call<ProductResp> call, @NotNull retrofit2.Response<ProductResp> response) {

                ProductResp rlist=response.body();
                Log.d("mmm", "/////////////   islike Response////////////////////////////////////// testimonial 2" );


                //  TestimonialListResp blogResp1 = response.body();


            }

            @Override
            public void onFailure(@NotNull Call<ProductResp> call, @NotNull Throwable t) {
                Log.d("mmm", "/////////////   islike Response fail ////////////////////////////////////// testimonial 2"+t );

            }
        });
    }



}
