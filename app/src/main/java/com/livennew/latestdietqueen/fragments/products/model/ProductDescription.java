package com.livennew.latestdietqueen.fragments.products.model;

public class ProductDescription {
    String description;
    Boolean isIncluded;

    public ProductDescription() {
    }

    public ProductDescription(String desc, Boolean included) {
        this.description = desc;
        this.isIncluded = included;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIncluded() {
        return isIncluded;
    }

    public void setIncluded(Boolean included) {
        isIncluded = included;
    }
}
