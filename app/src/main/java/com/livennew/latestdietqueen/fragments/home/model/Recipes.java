package com.livennew.latestdietqueen.fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Recipes {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("is_like")
    @Expose
    private String is_like;


    public String getIs_like() {
        return is_like;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    private String Constituentimage,Constituent_food_master_name,ConstituentUnit,recepieImage,recepieName,recepieDescription,calories,unit_name;

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public String getRecepieImage() {
        return recepieImage;
    }

    public void setRecepieImage(String recepieImage) {
        this.recepieImage = recepieImage;
    }

    public String getRecepieName() {
        return recepieName;
    }

    public void setRecepieName(String recepieName) {
        this.recepieName = recepieName;
    }

    public String getRecepieDescription() {
        return recepieDescription;
    }

    public void setRecepieDescription(String recepieDescription) {
        this.recepieDescription = recepieDescription;
    }

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("food_master_name")
    @Expose
    private String food_master_name;


    @SerializedName("making_description")
    @Expose
    private String making_description;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFood_master_name() {
        return food_master_name;
    }

    public void setFood_master_name(String food_master_name) {
        this.food_master_name = food_master_name;
    }

    public String getMaking_description() {
        return making_description;
    }

    public void setMaking_description(String making_description) {
        this.making_description = making_description;
    }



    @SerializedName("food_group")
    @Expose
    private Food_Group food_group;

    @SerializedName("unit")
    @Expose
    private unit unit;




    @SerializedName("conversion_factor")
    @Expose
    private List<conversion_factor> conversion_factor = null;



    @SerializedName("ingredients")
    @Expose
    private List<Ingredients> ingredients = null;

    public List<Ingredients> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredients> ingredients) {
        this.ingredients = ingredients;
    }

    public class Food_Group {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

    }

    public class unit {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class conversion_factor {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("quantity")
        @Expose
        private String quantity;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        @SerializedName("raw_food")
        @Expose
        private Raw_food raw_food;


        public class Raw_food{

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("food_master_name")
            @Expose
            private String food_master_name;

            @SerializedName("image")
            @Expose
            private String image;

            @SerializedName("unit")
            @Expose
            private Unit unit;

            public Unit getUnit() {
                return unit;
            }

            public void setUnit(Unit unit) {
                this.unit = unit;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getFood_master_name() {
                return food_master_name;
            }

            public void setFood_master_name(String food_master_name) {
                this.food_master_name = food_master_name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public class Unit {
                @SerializedName("id")
                @Expose
                private String id;
                @SerializedName("name")
                @Expose
                private String name;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }
            }
        }
        public Raw_food getRaw_food() {
            return raw_food;
        }

        public void setRaw_food(Raw_food raw_food) {
            this.raw_food = raw_food;
        }


    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Food_Group getFood_group() {
        return food_group;
    }

    public void setFood_group(Food_Group food_group) {
        this.food_group = food_group;
    }

    public Recipes.unit getUnit() {
        return unit;
    }

    public void setUnit(Recipes.unit unit) {
        this.unit = unit;
    }
    public List<conversion_factor> getConversion_factor() {
        return conversion_factor;
    }

    public void setConversion_factor(List<conversion_factor> conversion_factor) {
        this.conversion_factor = conversion_factor;
    }


    public class Ingredients{

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("food_master_id")
        @Expose
        private String food_master_id;

        @SerializedName("ingredient_id")
        @Expose
        private String ingredient_id;

        @SerializedName("calories")
        @Expose
        private String calories;

        @SerializedName("calorie_category_id")
        @Expose
        private String calorie_category_id;


        @SerializedName("conditional_category_id")
        @Expose
        private String conditional_category_id;

        @SerializedName("created_at")
        @Expose
        private String created_at;

        @SerializedName("updated_at")
        @Expose
        private String updated_at;

        @SerializedName("ingredient_name")
        @Expose
        private String ingredient_name;

        @SerializedName("unit_name")
        @Expose
        private String unit_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFood_master_id() {
            return food_master_id;
        }

        public void setFood_master_id(String food_master_id) {
            this.food_master_id = food_master_id;
        }

        public String getIngredient_id() {
            return ingredient_id;
        }

        public void setIngredient_id(String ingredient_id) {
            this.ingredient_id = ingredient_id;
        }

        public String getCalories() {
            return calories;
        }

        public void setCalories(String calories) {
            this.calories = calories;
        }

        public String getCalorie_category_id() {
            return calorie_category_id;
        }

        public void setCalorie_category_id(String calorie_category_id) {
            this.calorie_category_id = calorie_category_id;
        }

        public String getConditional_category_id() {
            return conditional_category_id;
        }

        public void setConditional_category_id(String conditional_category_id) {
            this.conditional_category_id = conditional_category_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getIngredient_name() {
            return ingredient_name;
        }

        public void setIngredient_name(String ingredient_name) {
            this.ingredient_name = ingredient_name;
        }

        public String getUnit_name() {
            return unit_name;
        }

        public void setUnit_name(String unit_name) {
            this.unit_name = unit_name;
        }
    }
    public String getConstituentimage() {
        return Constituentimage;
    }

    public void setConstituentimage(String constituentimage) {
        Constituentimage = constituentimage;
    }

    public String getConstituent_food_master_name() {
        return Constituent_food_master_name;
    }

    public void setConstituent_food_master_name(String constituent_food_master_name) {
        Constituent_food_master_name = constituent_food_master_name;
    }

    public String getConstituentUnit() {
        return ConstituentUnit;
    }

    public void setConstituentUnit(String constituentUnit) {
        ConstituentUnit = constituentUnit;
    }

}
