package com.livennew.latestdietqueen.fragments.products.model;

import java.util.ArrayList;

public class ProductDietPlanModel {
    int id;
    String dietPlanImage, dietCurrentPlan, dietPlanTitle, dietPlanDesc, dietPlanDesc1, dietPlanOffer, dietPlanDuration, dietPlanPrice,
            dietPlanDiscount, discountPer, planName;
    ArrayList<ProductDescription> productDescriptions;

    public String getDietCurrentPlan() {
        return dietCurrentPlan;
    }

    public void setDietCurrentPlan(String dietCurrentPlan) {
        this.dietCurrentPlan = dietCurrentPlan;
    }

    public String getDietPlanDesc1() {
        return dietPlanDesc1;
    }

    public void setDietPlanDesc1(String dietPlanDesc1) {
        this.dietPlanDesc1 = dietPlanDesc1;
    }

    public ProductDietPlanModel() {
    }

    public String getDietPlanDuration() {
        return dietPlanDuration;
    }

    public void setDietPlanDuration(String dietPlanDuration) {
        this.dietPlanDuration = dietPlanDuration;
    }

    public String getDietPlanDiscount() {
        return dietPlanDiscount;
    }

    public void setDietPlanDiscount(String dietPlanDiscount) {
        this.dietPlanDiscount = dietPlanDiscount;
    }
/*public ProductDietPlanModel(int id, int dietPlanImage, String dietPlanTitle, String dietPlanDesc, String dietPlanOffer, String dietPlanPrice, String backgroundColor, String offerColor) {
        this.id = id;
        this.dietPlanImage = dietPlanImage;
        this.dietPlanTitle = dietPlanTitle;
        this.dietPlanDesc = dietPlanDesc;
        this.dietPlanOffer = dietPlanOffer;
        this.dietPlanPrice = dietPlanPrice;
        this.backgroundColor = backgroundColor;
        this.offerColor = offerColor;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDietPlanImage() {
        return dietPlanImage;
    }

    public void setDietPlanImage(String dietPlanImage) {
        this.dietPlanImage = dietPlanImage;
    }

    public String getDietPlanTitle() {
        return dietPlanTitle;
    }

    public void setDietPlanTitle(String dietPlanTitle) {
        this.dietPlanTitle = dietPlanTitle;
    }

    public String getDietPlanDesc() {
        return dietPlanDesc;
    }

    public void setDietPlanDesc(String dietPlanDesc) {
        this.dietPlanDesc = dietPlanDesc;
    }

    public String getDietPlanOffer() {
        return dietPlanOffer;
    }

    public void setDietPlanOffer(String dietPlanOffer) {
        this.dietPlanOffer = dietPlanOffer;
    }

    public String getDietPlanPrice() {
        return dietPlanPrice;
    }

    public void setDietPlanPrice(String dietPlanPrice) {
        this.dietPlanPrice = dietPlanPrice;
    }

    public String getDiscountPer() {
        return discountPer;
    }

    public void setDiscountPer(String discountPer) {
        this.discountPer = discountPer;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public ArrayList<ProductDescription> getProductDescriptions() {
        return productDescriptions;
    }

    public void setProductDescriptions(ArrayList<ProductDescription> productDescriptions) {
        this.productDescriptions = productDescriptions;
    }
}
