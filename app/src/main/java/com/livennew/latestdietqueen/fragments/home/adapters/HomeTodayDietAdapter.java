package com.livennew.latestdietqueen.fragments.home.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.ReplaceItemsActivity;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.DietItemMenuAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Plan;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.PlanParam;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.adapter.VideoListAdapter;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;
import com.livennew.latestdietqueen.fragments.home.model.Blog;
import com.livennew.latestdietqueen.fragments.home.model.TodayDietModel;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.DateUtil;
import com.livennew.latestdietqueen.utils.RecyclerItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeTodayDietAdapter extends RecyclerView.Adapter<HomeTodayDietAdapter.MyViewHolder> {
    private List<DietEPlanFood> alTodayDietModel;
    private Others others;
    private Context context;
    private HomeTodayDietAdapter.Listener listener;

    @Inject
    APIInterface apiInterface;

    public HomeTodayDietAdapter(Context context, List<DietEPlanFood> alTodayDietModel) {
        this.context = context;
        this.alTodayDietModel = alTodayDietModel;
        this.listener = listener;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_today_diet_content, parent, false);
        int width = context.getResources().getDisplayMetrics().widthPixels;
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = (int) (width * 0.6);
        view.setLayoutParams(params);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DietEPlanFood todayDietModel = alTodayDietModel.get(position);
        holder.tvHomeFoodNm.setText(todayDietModel.getFood().getFoodMasterName()+ " "+getDietQty(Util.calculateQty(todayDietModel, others), todayDietModel.getFood().getUnit().getName()));


        Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxx todayDietModel.getDietEDataId() xxxxxxxxxxxxxxxxxxxxxxx"+todayDietModel.getDietEDataId());
        //Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxx todayDietModel.getDietEDataId() xxxxxxxxxxxxxxxxxxxxxxx"+getDietQty(Util.calculateQty(todayDietModel, others));
        Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxx todayDietModel.getDietEDataId() xxxxxxxxxxxxxxxxxxxxxxx"+todayDietModel.getFood().getUnit().getName());



        ;

        holder.tvHomeFoodNm.setTag(todayDietModel.getDietEDataId());
        holder.ivHomeFood.setTag(todayDietModel.getDietEDataId());
        holder.tvHomeFoodNm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null){
                    //listener.onDoneClick(position, todayDietMenuModel, others);

                    listener.onItemClick(todayDietModel.getFood(),view.getTag().toString());
                    Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"+todayDietModel.getFood());
                    Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"+todayDietModel.getFood());



                }
            }
        });

        holder.ivHomeFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null){
                    //listener.onDoneClick(position, todayDietMenuModel, others);

                    listener.onItemClick(todayDietModel.getFood(),view.getTag().toString());
                    Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"+todayDietModel.getFood());
                    Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"+todayDietModel.getFood());



                }
            }
        });



        Glide.with(context)
                .load(others.getDietPlanImagePath() + todayDietModel.getFood().getImage())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivHomeFood);
      //  holder.tvQty.setText(getDietQty(Util.calculateQty(todayDietModel, others), todayDietModel.getFood().getUnit().getName()));
    }

    @Override
    public int getItemCount() {
        return alTodayDietModel.size();
    }


    public void setListener(HomeTodayDietAdapter.Listener listener) {
        this.listener = listener;
    }

    public void addAll(List<DietEPlanFood> dietEPlanFood, Others others) {
        this.alTodayDietModel = dietEPlanFood;
        this.others = others;
        notifyDataSetChanged();
    }

    private String getDietQty(long calculateQty, String name) {
        if (calculateQty == 0)
            return "NA";
        return calculateQty + " " + name;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvQty;
        ImageView ivHomeFood;
        TextView tvHomeFoodNm;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivHomeFood = itemView.findViewById(R.id.iv_home_food);
            tvHomeFoodNm = itemView.findViewById(R.id.tv_diet);
            tvQty = itemView.findViewById(R.id.tvQty);
        }
    }



    public interface Listener {
        void onItemClick(Food item,String diet_e_data_id);
    }
}
