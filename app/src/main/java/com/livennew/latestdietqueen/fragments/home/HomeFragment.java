package com.livennew.latestdietqueen.fragments.home;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.livennew.latestdietqueen.BuildConfig;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.about_us.AboutUs;
import com.livennew.latestdietqueen.about_us.AboutUsResp;
import com.livennew.latestdietqueen.databinding.FragmentDashboardHomeBinding;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DoneParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Plan;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.PlanParam;
import com.livennew.latestdietqueen.exclusive_youtube.YouTubePlayerActivity;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;
import com.livennew.latestdietqueen.exclusive_youtube.testAllActivity;
import com.livennew.latestdietqueen.exclusive_youtube.testDetailActivity;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeBlogAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeReplaceDietAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTestimonialAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTodayDietAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeVideosAdapter;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.fragments.home.model.Testimonials;
import com.livennew.latestdietqueen.model.MyResponse;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.newsblog.NewsBlogListActivity;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.user_detail.AchievementsActivity;
import com.livennew.latestdietqueen.utils.CenterZoomLayoutManager;
import com.livennew.latestdietqueen.utils.DateUtil;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.MODE_PRIVATE;


@AndroidEntryPoint
public class HomeFragment extends Fragment {
    private static final String TAG = "HomeFragment";

    private HomeTodayDietAdapter homeTodayDietAdapter;
    private HomeReplaceDietAdapter homeReplaceDietAdapter;
    private ArrayList<DietEPlanFood> alHomeTodayDiet;
    private ArrayList<Food> alHomeReplaceTodayDiet;

    List<DietEPlanFood> listingvID = new ArrayList<DietEPlanFood>();
    String Fodgid = "";
    private HomeBlogAdapter homeBlogAdapter;
    HomeTestimonialAdapter homeTestAdapter;
    Integer DietEDataId = 0;
    private HomeVideosAdapter homeVideosAdapter;
    //    private VideoResp videos;
    private FragmentActivity activity;
    private FragmentDashboardHomeBinding binding;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    User user;

    SharedPreferences mPrefsobj;
    String prahar = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        user = sessionManager.getUser();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDashboardHomeBinding.inflate(inflater, container, false);


        mPrefsobj = getActivity().getPreferences(MODE_PRIVATE);

        onSetTodayDietRecyclerView();
        binding.llTodayDiet.setOnClickListener(View -> {
        });
        binding.civHomeProfile.setOnClickListener(View -> {
            callAchivmentScreen();
        });
        binding.tvHomeUserNm.setOnClickListener(View -> {
            callAchivmentScreen();
        });
        homeVideosAdapter = new HomeVideosAdapter(activity, new ArrayList<>());
        binding.incBlogVideo.rvHomeVideo.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        binding.incBlogVideo.rvHomeVideo.setAdapter(homeVideosAdapter);
        homeVideosAdapter.setListener(new HomeVideosAdapter.Listener() {
            @Override
            public void onItemClick(Video item, int position) {
                getVideoDetails(item);
            }
        });
//        homeBlogAdapter = new HomeBlogAdapter(activity);
//        binding.incBlogVideo.rvHomeBlog.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
//        binding.incBlogVideo.rvHomeBlog.setAdapter(homeBlogAdapter);
//        homeBlogAdapter.setListener(new HomeBlogAdapter.Listener() {
//            @Override
//            public void onItemClick(Blog blog) {
//                Intent intent = new Intent(activity, BlogDetailActivity.class);
//                intent.putExtra("blog_id", blog.getId());
//                intent.putExtra("type", "blog");
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//            }
//
//        });
        if (SupportClass.checkConnection(activity)) {
//            blogList();
            VideoList();
        } else {
            SupportClass.noInternetConnectionToast(activity);
        }

        /**News video list shows*/
        binding.incBlogVideo.tvShowVideoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, testAllActivity.class);
                startActivity(intent);
                activity.overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);


            }
        });

        binding.tvtUpcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gotoDialog();


            }
        });

        binding.incBlogVideo.tvShowBlogList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, NewsBlogListActivity.class);
                startActivity(intent);
                activity.overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        });


        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (sessionManager.getUser().getName() != null) {
            binding.tvHomeUserNm.setText(SupportClass.getUserFirstName(sessionManager.getUser().getName()));
        }
        Glide
                .with(this)
                .load(sessionManager.getUser().getProfileImage())
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(binding.civHomeProfile);

    }


    /**
     * Today's diet recycler view with CenterZoomLayoutManager class implemented
     */
    private void onSetTodayDietRecyclerView() {
        alHomeTodayDiet = new ArrayList<>();
        alHomeReplaceTodayDiet = new ArrayList<>();
        homeTodayDietAdapter = new HomeTodayDietAdapter(activity, alHomeTodayDiet);


        CenterZoomLayoutManager todayDietLayoutManager = new CenterZoomLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        binding.rvHomeTodayDiet.setLayoutManager(todayDietLayoutManager);
        binding.rvHomeTodayDiet.setAdapter(homeTodayDietAdapter);
        final int radius = getResources().getDimensionPixelSize(R.dimen.rad);
        final int dotsHeight = getResources().getDimensionPixelSize(R.dimen.indicator);
        final int color = ContextCompat.getColor(getContext(), R.color.indicator_off);
        final int color1 = ContextCompat.getColor(getContext(), R.color.indicator_on);
        binding.rvHomeTodayDiet.addItemDecoration(new DotsIndicatorDecoration(radius, 20, dotsHeight, color, color1));
        new PagerSnapHelper().attachToRecyclerView(binding.rvHomeTodayDiet);
        todayDietLayoutManager.scrollToPosition(alHomeTodayDiet.size() / 2);
        prepareTodayDietData();

        if (sessionManager.isTermsAccepted() == 0) {
            Log.d("ddddd", "TEST");
              getAboutUs();
        }
//        getAboutUs();

        homeTodayDietAdapter.setListener(new HomeTodayDietAdapter.Listener() {
            @Override
            public void onItemClick(Food item, String diet_e_data_id) {

                Log.d("dddd", "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm" + item.getId()); //last item id
                Log.d("dddd", "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm" + item.getFoodMasterName());
                Log.d("dddd", "mmmmmmmmmmmmmmm diet_e_data_id mmmmmmmmmmmmmmmm" + diet_e_data_id); // diet e data id
                List<String> s1 = new ArrayList<>();//foodMasterName
                List<String> foodId = new ArrayList<>();//foodMasterName

                List<String> foodMasterName = new ArrayList<>();//foodMasterName

                List<String> imgpath = new ArrayList<>();//foodMasterName
                List<String> id = new ArrayList<>();//foodMasterName
                List<String> isRecipe = new ArrayList<>();


                String replaceData = item.getId() + "##" + diet_e_data_id;


                // Log.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxxx others.getDietPlanImagePath() xxxxxxxxxxxxxxxxxxxxxx"+others.getDietPlanImagePath());
                //og.d("fff","xxxxxxxxxxxxxxxxxxxxxxxxxxxxx others.getDietPlanImagePath() xxxxxxxxxxxxxxxxxxxxxx"+todayDietModel.getFood().getImage() );

                //   todayDietMenu.getFood().getId(), replaceItemAdapter.getLastSelectedFood().getId(), todayDietMenu.getDietEDataId(), DateUtil.getTodayDate()

                apiInterface.getFoodsByGroup(new FoodGroupParam(item)).enqueue(new Callback<Foods>() {
                    @Override
                    public void onResponse(@NonNull Call<Foods> call, @NonNull retrofit2.Response<Foods> response) {
                        if (response.body() != null) {
                            //    Glide.with(mContext).load(others.getDietPlanImagePath() + replaceItemModel.getImage()).into(holder.ivReplaceMenu);

                            if (response.body().getData().size() > 0) {
                                Integer size = response.body().getData().size();
                                for (int i = 0; i < size; i++) {

                                    Food f = new Food();
                                    Log.d("dddd", "mmmmmmmmmmmmmmmmmmmmm  Data  fgid mmmmmmmmmm" + response.body().getData().get(i).getFoodGroupId());
                                    Log.d("dddd", "mmmmmmmmmmmmmmmmmmmmm  Data mmmmmmmmmm" + response.body().getData().get(i).getId()); //current item id


                                    Log.d("dddd", "mmmmmmmmmmmmmmmmmmmmm  Data mmmmmmmmmm" + response.body().getData().get(i).getFoodMasterName());
                                    Log.d("dddd", "mmmmmmmmmmmmmmmmmmmmm  Data others mmmmmmmmmm" + response.body().getOthers().getDietPlanImagePath());
                                    Log.d("dddd", "mmmmmmmmmmmmmmmmmmmmm  Data mmmmmmmmmm" + response.body().getData().get(i).getImage());
                                    Fodgid = response.body().getData().get(0).getFoodGroupId().toString();
                                    foodId.add(response.body().getData().get(i).getFoodGroupId());
                                    foodMasterName.add(response.body().getData().get(i).getFoodMasterName());
                                    imgpath.add(response.body().getOthers().getDietPlanImagePath() + response.body().getData().get(i).getImage());
                                    id.add(response.body().getData().get(i).getId().toString());
                                    isRecipe.add(response.body().getData().get(i).getIs_recipe().toString());
                                    //current_iten_id,last_select_item_id,diet_eplan_id,time


                                }


                            }
                            Log.d("hhhhhhh", "uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu");

 /*Intent i=new Intent(getActivity(),ReplaceItemsActivity.class);
 i.putExtra("s",s1.toString());
 i.putExtra("replaceData",replaceData);
 startActivity(i);*/
                            ReplaceItemFragment fragment = new ReplaceItemFragment();
                            Bundle b = new Bundle();
                            b.putString("foodId", foodId.toString());
                            b.putString("foodMasterName", foodMasterName.toString());
                            b.putString("imgpath", imgpath.toString());
                            b.putString("id", id.toString());
                            b.putString("replaceData", replaceData);
                            b.putString("isRecipe", isRecipe.toString());
                            b.putString("Fodgid", Fodgid);
                            fragment.setArguments(b);
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_container, fragment);
                            transaction.addToBackStack("nav_home");
                            transaction.commit();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Foods> call, @NonNull Throwable t) {
                        Log.e(TAG, "onFailure: " + t.getMessage());
                    }
                });

            }

        });

    }

    private void prepareTodayDietData() {

        Log.d("ddddd", "yyyyyyyyyyyy prepareTodayDietData  sessionManager.getDietEPlanId()yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" + sessionManager.getDietEPlanId());
        binding.llTodayDiet.setVisibility(View.GONE);
        binding.progressBarTime.setVisibility(View.VISIBLE);
        apiInterface.getDietPlan(new PlanParam(sessionManager.getDietEPlanId())).enqueue(new Callback<Plan>() {
            @Override
            public void onResponse(@NonNull Call<Plan> call, @NonNull retrofit2.Response<Plan> response) {
                if (response.body() != null) {
                    if (!response.body().getError()) {
                        Log.d("ddddd", "yyyyyyyyyyyy prepareTodayDietData   Respone responseyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                        int versionCode = BuildConfig.VERSION_CODE;
                        if (versionCode < Integer.parseInt(response.body().getData().getAos_version())) {
                            Update();
                        }


                        binding.llTodayDiet.setVisibility(View.VISIBLE);
                        homeTodayDietAdapter.addAll(response.body().getData().getDietEPlanFood(), response.body().getOthers());
                        String time = new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(Calendar.getInstance().getTime());
                        String time1 = DateUtil.convertTime24To12(response.body().getData().getTime());
                        Log.d("ddddd", "yyyyyyyyyyyy prepareTodayDietData   time  responsey response.body().getData().getTime() yyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" + response.body().getData().getTime());
                        Log.d("ddddd", "yyyyyyyyyyyy prepareTodayDietData   getDietEDataId   responseyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" + response.body().getData().getDietEPlanFood().get(0).getDietEDataId());
                        Log.d("ddddd", "yyyyyyyyyyyy prepareTodayDietData   response.body().getData().getIs_diet_done()  responseyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" + response.body().getData().getIs_diet_done());

                        DietEDataId = response.body().getData().getDietEPlanFood().get(0).getDietEDataId();
                        try {

                            if (response.body().getData().getIs_diet_done().equals("N")) {

                                binding.tvtUpcoming.setText("Rescheduled…");
                                binding.tvtUpcoming.setEnabled(false);
                            } else {
                                binding.tvtUpcoming.setText("Upcoming");
                                binding.tvtUpcoming.setEnabled(true);
                            }
                        } catch (Exception e) {

                        }



                  /*      SimpleDateFormat format = new SimpleDateFormat("hh:mm");
                        Date date1 = null;
                            //date1.setTime(Long.parseLong(time1));
                        try {
                            date1 = format.parse(time1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Date date2 = null;
                        try {
                            date2 = format.parse(time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Log.d("ddddd","yyyyyyyyyyyy prepareTodayDietData    date1.getTime()  responseyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" + date1.getTime());

                        Log.d("ddddd","yyyyyyyyyyyy prepareTodayDietData   date2.getTime()  responseyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" +date2.getTime());

                        long millis = date1.getTime() - date2.getTime();
                        int hours = (int) (millis / (1000 * 60 * 60));
                        int mins = (int) ((millis / (1000 * 60)) % 60);

                        String diff = hours + ":" + mins;
                        Log.d("ddddd","yyyyyyyyyyyy prepareTodayDietData   diff  responseyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" +diff);

                if(hours>0) {
                    binding.dietTime.setText(" in  " + hours+ " Hours");

                }else{


                }*/

                        binding.dietTime.setText(response.body().getData().getTime_difference());
                        binding.tvTodayDietTime.setText(response.body().getData().getPlanSubcategory());
                        prahar = response.body().getData().getPlanSubcategory();
                        //    binding.tvTodayDietTime.setText(result+ " " + response.body().getData().getPlanSubcategory());

                    } else {
                        binding.llTodayDiet.setVisibility(View.GONE);
                        for (int i = 0; i < response.body().getMessage().size(); i++) {
                            Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                        }
                    }
                    binding.progressBarTime.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<Plan> call, @NotNull Throwable t) {
                binding.llTodayDiet.setVisibility(View.GONE);
                binding.progressBarTime.setVisibility(View.GONE);
                Log.d("ddddd", "yyyyyyyyyyyy prepareTodayDietData   Respone  responseyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" + t);

            }
        });

    }

    private void getAboutUs() {
//        binding.llProgress.setVisibility(View.VISIBLE);
        apiInterface.getAboutUs().enqueue(new Callback<AboutUsResp>() {
            @Override
            public void onResponse(@NonNull Call<AboutUsResp> call, @NonNull retrofit2.Response<AboutUsResp> response) {
//                binding.llProgress.setVisibility(View.GONE);
//                binding.viewpager.setAdapter(new AboutUsActivity.MyAdapter(getSupportFragmentManager(), response.body().getData()));
//                binding.tabs.post(() -> binding.tabs.setupWithViewPager(binding.viewpager));
//                if (getIntent() != null) {
//                    binding.viewpager.setCurrentItem(getIntent().getIntExtra("pageNo", 0));
//                }
                openTermsCustomeDialog(response.body().getData());
            }

            @Override
            public void onFailure(@NonNull Call<AboutUsResp> call, @NonNull Throwable t) {
//                binding.llProgress.setVisibility(View.GONE);
            }
        });
    }

    private void openTermsCustomeDialog(List<AboutUs> data) {
        Dialog dialog = new Dialog(activity, R.style.theme_sms_receive_dialog);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setContentView(R.layout.terms_dialog);
        TextView tvAboutApp = dialog.findViewById(R.id.tvAboutApp);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvAboutApp.setText(Html.fromHtml(data.get(0).getContent(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvAboutApp.setText(Html.fromHtml(data.get(0).getContent()));
        }

        ImageView ivClose = dialog.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        MaterialCheckBox agreeCheckedBox = dialog.findViewById(R.id.agreeCheckedBox);
        agreeCheckedBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                       @Override
                                                       public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                           if (isChecked) {
                                                               //api call
                                                               Log.i("ddddd", "isChecked = " + isChecked);
                                                               Map<String, Integer> params = new HashMap<String, Integer>();
                                                               params.put("is_terms_accepted", 1);
                                                               params.put("id", sessionManager.getUser().getId());
                                                               apiInterface.updateAgreeTerms(params).enqueue(new Callback<MyResponse>() {
                                                                   @Override
                                                                   public void onResponse(@NonNull Call<MyResponse> call, @NonNull retrofit2.Response<MyResponse> response) {
                                                                       sessionManager.setIsTermsAccepted(1);
                                                                       dialog.dismiss();


                                                                   }

                                                                   @Override
                                                                   public void onFailure(Call<MyResponse> call, Throwable t) {
                                                                       t.printStackTrace();
                                                                       Log.i("ddddd",""+t.getMessage());

                                                                   }
                                                               });

                                                           }

                                                       }
                                                   }
        );

        dialog.show();
    }

//    private void blogList() {
//        binding.incBlogVideo.llDashboardBlog.setVisibility(View.GONE);
//        binding.incBlogVideo.icBlogProgress.setVisibility(View.VISIBLE);
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("type", "blog");
//        params.put("perPage",3);
//
//        apiInterface.getBlogs(params).enqueue(new Callback<BlogListResp>() {
//            @Override
//            public void onResponse(@NotNull Call<BlogListResp> call, @NotNull retrofit2.Response<BlogListResp> response) {
//                BlogListResp blogResp = response.body();
//                if (!blogResp.getError()) {
//                    homeBlogAdapter.setData(blogResp.getData().getBlogExclusive(), blogResp.getOthers());
//                    binding.incBlogVideo.llDashboardBlog.setVisibility(View.VISIBLE);
//                    binding.incBlogVideo.icBlogProgress.setVisibility(View.GONE);
//                } else {
//                    Toast.makeText(activity, blogResp.getMessage().get(0), Toast.LENGTH_LONG).show();
//                }
//
//            }
//
//            @Override
//            public void onFailure(@NotNull Call<BlogListResp> call, @NotNull Throwable t) {
//
//                Log.d("kkkkk","kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk blog error"+t);
//                binding.incBlogVideo.llDashboardBlog.setVisibility(View.GONE);
//                binding.incBlogVideo.icBlogProgress.setVisibility(View.GONE);
//            }
//        });
//    }

    private void VideoList() {
      /*  binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
        binding.incBlogVideo.icVideoProgress.setVisibility(View.VISIBLE);
        apiInterface.getVideos(getString(R.string.channel_id), getString(R.string.youtube_key)).enqueue(new Callback<VideoResp>() {
            @Override
            public void onResponse(@NotNull Call<VideoResp> call, @NotNull retrofit2.Response<VideoResp> response) {
                try {
                    if (response.body() != null) {
                        List<Video> videos = response.body().getItems();
                        Iterator<Video> i = videos.iterator();
                        while (i.hasNext()) {
                            Video item = i.next();
                            if (item.getId().getVideoId() == null)
                                i.remove();
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (videos != null && videos.size() > 1) {
                                    binding.incBlogVideo.llDashboardVideo.setVisibility(View.VISIBLE);
                                    homeVideosAdapter.setData(videos);
                                }
                                binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);

                            }
                        }, 2000);
                    } else {
                        binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(@NotNull Call<VideoResp> call, @NotNull Throwable t) {
                binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
                binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
            }
        });
*/

        binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
//        binding.incBlogVideo.icVideoProgress.setVisibility(View.VISIBLE);

        Map<String, Integer> params = new HashMap<String, Integer>();
        params.put("id", 0);
        params.put("perPage", 3);

        Log.d("mmm", "/////////////////////////////////////////////////// testimonial 1");
        apiInterface.getTestimonials(params).enqueue(new Callback<TestimonialListResp>() {
            @Override
            public void onResponse(@NotNull Call<TestimonialListResp> call, @NotNull retrofit2.Response<TestimonialListResp> response) {
                TestimonialListResp blogResp1 = response.body();
                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2" + blogResp1.getError());
                if (!blogResp1.getError()) {

                    // binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                    Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2 " + blogResp1.getData().getFirstPageUrl());
                    Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2 " + blogResp1.getData().getTestimonials().size());
                    Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2 " + blogResp1.getOthers().getTestimonialImagePath());

                    binding.incBlogVideo.llDashboardVideo.setVisibility(View.VISIBLE);

//                    binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);

                    homeTestAdapter = new HomeTestimonialAdapter(getContext(), blogResp1.getData().getTestimonials(), blogResp1.getOthers());


                    homeTestAdapter.setListener(new HomeTestimonialAdapter.Listener() {


                        @Override
                        public void onItemClick(Testimonials testi) {
                            Log.d("mmm", "//////////////////////// ff /////////////////////////// testimonial" + testi.getId());


                            Intent intent = new Intent(activity, testDetailActivity.class);
                            intent.putExtra("testimonial_id", String.valueOf(testi.getId()));
                            // intent.putExtra("type", "blog");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);


                        }
                    });
                    //    RecyclerView.LayoutManager recyce = new GridLayoutManager(getActivity(),2);
                    RecyclerView.LayoutManager recycev = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
                    //    recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));
                    binding.incBlogVideo.rvHomeVideo.setLayoutManager(recycev);
                    binding.incBlogVideo.rvHomeVideo.setItemAnimator(new DefaultItemAnimator());
                    binding.incBlogVideo.rvHomeVideo.setAdapter(homeTestAdapter);


                } else {
                    Toast.makeText(activity, blogResp1.getMessage().get(0), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(@NotNull Call<TestimonialListResp> call, @NotNull Throwable t) {
                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2" + t);

                binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
//                binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
            }
        });


    }

    private void getVideoDetails(Video item) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "https://www.googleapis.com/youtube/v3/videos?id=" + item.getId().getVideoId() + "&key=" + getString(R.string.youtube_key) + "&part=snippet,contentDetails,statistics,status", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String desc = response.getJSONArray("items").getJSONObject(0).getJSONObject("snippet").getString("description");
                    item.getSnippet().setDescription(desc);
                    callVideoPlayer(item);
                } catch (JSONException e) {
                    e.printStackTrace();
                    callVideoPlayer(item);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        //  AppController.getInstance().addToRequestQueue(jsonObjectRequest, tagGetVideoDetails);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void callVideoPlayer(Video item) {
        Intent intent = new Intent(activity, YouTubePlayerActivity.class);
        intent.putExtra("video", item);
        startActivity(intent);
    }

    private void callAchivmentScreen() {
        Intent intent = new Intent(activity, AchievementsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void gotoDialog() {

        final Dialog dialog = new Dialog(this.activity);
        //  dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialogbox_status);
        //   dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        Button btn_submit = (Button) dialog.findViewById(R.id.btn_submit);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        TextView tvt_heading = (TextView) dialog.findViewById(R.id.tvt_heading);
        TextView tvt_message = (TextView) dialog.findViewById(R.id.tvt_message);

        tvt_message.setText("Have you had your " + prahar);
        tvt_heading.setText(prahar);

        // btn_submit.setTag(et_comment.getText().toString());
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();


                //String status_comment_tag=v.getTag().toString();
                // Log.d("gggg",":vvvnhnnnnnnnnnnnnnnnnnnnnnn 1 nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"+status_comment_tag.length());

                gotoDialog1();


                dialog.dismiss();


            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();

                dietDone("Y");
                dialog.dismiss();


            }
        });

        dialog.show();
    }

    public void dietDone(String done) {

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String formattedDate = df.format(c);

        Log.d("kkkkk", "******************************** formattedDate *************************** Diet Done Home" + formattedDate);


//        apiInterface.setDietDone(new DoneParam(DietEDataId, formattedDate, done)).enqueue(new Callback<MyResponse>() {
//            @Override
//            public void onResponse(@NonNull Call<MyResponse> call, @NonNull retrofit2.Response<MyResponse> response) {
//                if (response.body() != null && !response.body().getError()) {
//                    HomeFragment fragment = new HomeFragment();
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.frame_container, fragment);
//                    fragmentTransaction.addToBackStack(null);
//                    fragmentTransaction.commit();
//
//
//                    Log.d("kkkkk", "*********************************************************** Diet Done Home" + response.body().getMessage());
//                } else {
//                    if (response.body() != null) {
//                        for (int i = 0; i < response.body().getMessage().size(); i++)
//                            Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
//
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
//                Log.e(TAG, "onFailure: " + t.getMessage());
//                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    public void gotoDialog1() {

        final Dialog dialog = new Dialog(this.activity);
        //  dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  dialog.setCancelable(false);
        dialog.setContentView(R.layout.coustom_lay_home);
        //   dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        Button btn_submit = (Button) dialog.findViewById(R.id.btn_submit);
        ;


        // btn_submit.setTag(et_comment.getText().toString());
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();


                //String status_comment_tag=v.getTag().toString();
                // Log.d("gggg",":vvvnhnnnnnnnnnnnnnnnnnnnnnn 1 nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"+status_comment_tag.length());


                dietDone("N");
                binding.tvtUpcoming.setText("Rescheduled…");
                binding.tvtUpcoming.setEnabled(false);
                dialog.dismiss();


            }
        });


        dialog.show();
    }


    private void Update() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MyDatePicker);
        builder.setTitle("Update");
        builder.setMessage("Kindly Update App from Play store for latest changes.");
        builder.setPositiveButton("Update", (dialog, which) -> {
            // Do nothing but close the dialog
            final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
            activity.finish();
            dialog.dismiss();
        });
        builder.setCancelable(false);
        AlertDialog alert = builder.create();
        alert.show();
    }


}
