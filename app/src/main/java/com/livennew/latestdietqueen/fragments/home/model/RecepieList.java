package com.livennew.latestdietqueen.fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

public class RecepieList extends MyResponse {


    @SerializedName("data")
    @Expose
    private RecepieData data;


    @SerializedName("others")
    @Expose
    private Others others;


    public RecepieData getData() {
        return data;
    }



    public void setData(RecepieData data) {
        this.data = data;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }

    public class Others {
        @SerializedName("recipeImagePath")
        @Expose
        private String recipeImagePath;



        @SerializedName("foodImagePath")
        @Expose
        private String foodImagePath;


        public String getFoodImagePath() {
            return foodImagePath;
        }

        public void setFoodImagePath(String foodImagePath) {
            this.foodImagePath = foodImagePath;
        }

        public String getRecipeImagePath() {
            return recipeImagePath;
        }

        public void setRecipeImagePath(String recipeImagePath) {
            this.recipeImagePath = recipeImagePath;
        }
    }
}
