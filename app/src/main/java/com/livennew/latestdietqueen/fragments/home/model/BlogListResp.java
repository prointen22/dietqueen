package com.livennew.latestdietqueen.fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

public class BlogListResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private BlogData data;

    @SerializedName("others")
    @Expose
    private Others others;

    public BlogData getData() {
        return data;
    }

    public void setData(BlogData data) {
        this.data = data;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }

    public class Others {
        @SerializedName("blogImagePath")
        @Expose
        private String blogImagePath;
        @SerializedName("exclusiveImagePath")
        @Expose
        private String exclusiveImagePath;

        public String getBlogImagePath() {
            return blogImagePath;
        }

        public void setBlogImagePath(String blogImagePath) {
            this.blogImagePath = blogImagePath;
        }

        public String getExclusiveImagePath() {
            return exclusiveImagePath;
        }

        public void setExclusiveImagePath(String exclusiveImagePath) {
            this.exclusiveImagePath = exclusiveImagePath;
        }
    }
}
