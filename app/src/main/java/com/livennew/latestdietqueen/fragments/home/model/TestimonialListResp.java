package com.livennew.latestdietqueen.fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

import java.util.List;

public class TestimonialListResp extends MyResponse {

    @SerializedName("data")
    @Expose
    private TestimonialData data;




    @SerializedName("others")
    @Expose
    private Others others;

    public TestimonialData getData() {
        return data;
    }

    public void setData(TestimonialData data) {
        this.data = data;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }

    public class Others {
        @SerializedName("testimonialImagePath")
        @Expose
        private String testimonialImagePath;

        public String getTestimonialImagePath() {
            return testimonialImagePath;
        }

        public void setTestimonialImagePath(String testimonialImagePath) {
            this.testimonialImagePath = testimonialImagePath;
        }
    }
}
