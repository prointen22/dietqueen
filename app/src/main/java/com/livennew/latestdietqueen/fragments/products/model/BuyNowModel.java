package com.livennew.latestdietqueen.fragments.products.model;

public class BuyNowModel {

    private int buy_now_id, product_buy_price, month_num;
    private String diet_plan_info, month;
    private boolean isRecommended;

    public BuyNowModel(int buy_now_id, boolean isRecommended, int month_num, String month, int product_buy_price, String diet_plan_info) {
        this.buy_now_id = buy_now_id;
        this.isRecommended = isRecommended;
        this.month_num = month_num;
        this.month = month;
        this.product_buy_price = product_buy_price;
        this.diet_plan_info = diet_plan_info;
    }

    public int getBuy_now_id() {
        return buy_now_id;
    }

    public void setBuy_now_id(int buy_now_id) {
        this.buy_now_id = buy_now_id;
    }

    public int getProduct_buy_price() {
        return product_buy_price;
    }

    public void setProduct_buy_price(int product_buy_price) {
        this.product_buy_price = product_buy_price;
    }

    public int getMonth_num() {
        return month_num;
    }

    public void setMonth_num(int month_num) {
        this.month_num = month_num;
    }

    public String getDiet_plan_info() {
        return diet_plan_info;
    }

    public void setDiet_plan_info(String diet_plan_info) {
        this.diet_plan_info = diet_plan_info;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public boolean isRecommended() {
        return isRecommended;
    }

    public void setRecommended(boolean recommended) {
        isRecommended = recommended;
    }
}
