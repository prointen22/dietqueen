package com.livennew.latestdietqueen.fragments.products;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.dashboard.DashboardActivity1;
import com.livennew.latestdietqueen.databinding.ActivityProductListBinding;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ProductListActivity extends BaseActivity implements PaymentResultWithDataListener {

    private ActivityProductListBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProductListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
    }

    public void showProductDetails(int id) {
        Intent intent = new Intent(ProductListActivity.this, ProductDetailActivity.class);
        intent.putExtra("product_id", id);
        startActivityForResult(intent, ProductDetailActivity.PRODUCT_DETAILS_REQ);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ProductDetailActivity.PRODUCT_DETAILS_REQ: {
                if (resultCode == RESULT_OK) {
                    startActivity(new Intent(this, DashboardActivity1.class));
                    finish();
                }
            }
        }
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        Log.i("TAG","data = "+s);
        Log.i("TAG","data = "+paymentData.getData());
        Log.i("TAG","data = "+paymentData.getOrderId());
        ProductListFragment fragment = (ProductListFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
        fragment.subScribe(s);
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        Log.i("TAG","data = "+s);
        Log.i("TAG","data = "+paymentData.getData());
    }
}