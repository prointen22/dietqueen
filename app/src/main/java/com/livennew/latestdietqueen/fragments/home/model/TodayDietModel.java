package com.livennew.latestdietqueen.fragments.home.model;

public class TodayDietModel {
    int id;
    String dietName;
    int image;
    boolean isEditable, isDone;

    public TodayDietModel(int id, String dietName, int image) {
        this.id = id;
        this.dietName = dietName;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDietName() {
        return dietName;
    }

    public void setDietName(String dietName) {
        this.dietName = dietName;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

}
