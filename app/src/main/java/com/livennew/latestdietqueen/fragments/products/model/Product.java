package com.livennew.latestdietqueen.fragments.products.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("plan_name")
    @Expose
    private String planName;
    @SerializedName("duration_in_month")
    @Expose
    private String durationInMonth;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("discount_amount")
    @Expose
    private String discountAmount;
    @SerializedName("discount_per")
    @Expose
    private String discountPer;
    @SerializedName("feature")
    @Expose
    private String feature;
    @SerializedName("benefit")
    @Expose
    private String benefit;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("plan_type")
    @Expose
    private String planType;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("description1")
    @Expose
    private String description1;

    public String getDescription4() {
        return description4;
    }

    public void setDescription4(String description4) {
        this.description4 = description4;
    }

    @SerializedName("description4")
    @Expose
    private String description4;

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getDescription3() {
        return description3;
    }

    public void setDescription3(String description3) {
        this.description3 = description3;
    }

    @SerializedName("description2")
    @Expose
    private String description2;

    @SerializedName("description3")
    @Expose
    private String description3;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getDurationInMonth() {
        return durationInMonth;
    }

    public void setDurationInMonth(String durationInMonth) {
        this.durationInMonth = durationInMonth;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountPer() {
        return discountPer;
    }

    public void setDiscountPer(String discountPer) {
        this.discountPer = discountPer;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        this.benefit = benefit;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
