package com.livennew.latestdietqueen.fragments.home.model;

public class VideoModel {
    private int id;
    private String VideoURL, VideoTitle, VideoDesc, timeDuration, status;


    public VideoModel() {
    }

    public VideoModel(int id, String videoURL, String videoDesc, String timeDuration, String status) {
        VideoURL = videoURL;
        VideoDesc = videoDesc;
        this.id = id;
        this.timeDuration = timeDuration;
        this.status = status;
    }

    public String getTimeDuration() {
        return timeDuration;
    }

    public String getVideoTitle() {
        return VideoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        VideoTitle = videoTitle;
    }

    public void setTimeDuration(String timeDuration) {
        this.timeDuration = timeDuration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideoURL() {
        return VideoURL;
    }

    public void setVideoURL(String videoURL) {
        VideoURL = videoURL;
    }

    public String getVideoDesc() {
        return VideoDesc;
    }

    public void setVideoDesc(String videoDesc) {
        VideoDesc = videoDesc;
    }
}
