package com.livennew.latestdietqueen.fragments.products.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.databinding.RvProductContentBinding;
import com.livennew.latestdietqueen.diet_info.adapter.ScheduleItemMenuAdapter;
import com.livennew.latestdietqueen.fragments.products.model.ProductDietPlanModel;


import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import javax.inject.Inject;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    private final ArrayList<ProductDietPlanModel> list;
    private final Context context;
    int totalAmount;
    private Listener listener;
    private int selectedPlanId = -1;

    public ProductAdapter(Context context, ArrayList<ProductDietPlanModel> list,int selectedPlanId, Listener mListener) {
        this.context = context;
        this.list = list;
        this.listener = mListener;
        this.selectedPlanId = selectedPlanId;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(RvProductContentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ProductDietPlanModel productDietPlanModel = list.get(position);
        //   holder.binding.tvPrice.setPaintFlags(holder.binding.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
       /* Glide.with(context)
                .load(productDietPlanModel.getDietPlanImage())
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.binding.image);*/
        holder.binding.tvTitle.setText(productDietPlanModel.getDietPlanTitle());

        Log.d("jkkkkkkk", ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> dietPlane" + productDietPlanModel.getDietCurrentPlan());
        String[] aa = productDietPlanModel.getDietPlanDesc().split("##");
        //String desc= Html.fromHtml("<fontcolor='#E89B6C'>"+aa[1]+"</font>");
//        holder.binding.tvDesc.setText(aa[0]);
//        holder.binding.tvDesc1.setText(aa[1]);

        String upperString = productDietPlanModel.getDietPlanOffer().substring(0, 1).toUpperCase() + productDietPlanModel.getDietPlanOffer().substring(1).toLowerCase();
        // holder.binding.tvPaidOrFree.setText(upperString);
        totalAmount = Integer.parseInt(productDietPlanModel.getDietPlanPrice()) - Integer.parseInt(productDietPlanModel.getDietPlanDiscount());
//        holder.binding.tvUsp.setText(productDietPlanModel.getDietPlanDesc1());

        holder.binding.tvPlanName.setText(productDietPlanModel.getPlanName());



        try {
            if (productDietPlanModel.getDietCurrentPlan() != "") {
                if (productDietPlanModel.getDietCurrentPlan().equals("Y")) {
                    holder.binding.llMain.setBackgroundResource(R.drawable.card_view_border);
                    holder.binding.tvTitle1.setVisibility(View.VISIBLE);
                    holder.binding.btnBuyNow.setVisibility(View.INVISIBLE);

                } else {
                    if(selectedPlanId ==-1){
                        holder.binding.llMain.setBackgroundResource(R.drawable.bg_blue_rounded_corners_1);
                        holder.binding.tvTitle1.setVisibility(View.INVISIBLE);
                        holder.binding.btnBuyNow.setVisibility(View.VISIBLE);
                    }else {
                        if (selectedPlanId != 60) {
                            if (productDietPlanModel.getId() == 60) {
                                holder.binding.llMain.setBackgroundResource(R.drawable.card_view_inactive_border);
                                holder.binding.tvTitle1.setVisibility(View.INVISIBLE);
                                holder.binding.btnBuyNow.setVisibility(View.INVISIBLE);
                            } else {
                                holder.binding.llMain.setBackgroundResource(R.drawable.bg_blue_rounded_corners_1);
                                holder.binding.tvTitle1.setVisibility(View.INVISIBLE);
                                holder.binding.btnBuyNow.setVisibility(View.VISIBLE);
                            }
                        }
                    }
//                    holder.binding.llMain.setBackgroundResource(R.drawable.bg_blue_rounded_corners_1);
//                    holder.binding.tvTitle1.setVisibility(View.INVISIBLE);
//                    holder.binding.btnBuyNow.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            Log.v("TAG", "Exception in binding data: "+e.getMessage());
        }

        if (totalAmount == 0.0){
            holder.binding.tvAmount.setText("FREE");
            holder.binding.btnBuyNow.setText("Continue");
        }else {
            Spanned rupeeSymbol = Html.fromHtml("<fontcolor='#FD139A'>" + context.getString(R.string.rupee_symbol) + "</font>");
            String planPrice = rupeeSymbol + String.valueOf(totalAmount);
            holder.binding.tvAmount.setText(planPrice);
            holder.binding.btnBuyNow.setText("Buy Now");

        }

//        holder.binding.btnBuyNow.setBackgroundResource(R.drawable.submit_login_button);
        holder.binding.btnBuyNow.setBackground(ContextCompat.getDrawable(context, R.drawable.submit_login_button));

        holder.binding.btnBuyNow.setOnClickListener(view -> {
            if (listener != null)
                listener.onItemClick(productDietPlanModel.getId());
        });

        holder.adapter = new DescriptionAdapter(context, productDietPlanModel.getProductDescriptions());
        holder.layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.binding.rvDesc.setLayoutManager(holder.layoutManager);
        holder.binding.rvDesc.setAdapter(holder.adapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setListener(Listener listener) {

        this.listener = listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private final com.livennew.latestdietqueen.databinding.RvProductContentBinding binding;
        public DescriptionAdapter adapter;
        public RecyclerView.LayoutManager layoutManager;

        public MyViewHolder(@NonNull @NotNull RvProductContentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface Listener {
        void onItemClick(int id);
    }
}