package com.livennew.latestdietqueen.fragments.products.adapter;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.fragments.products.model.ProductDietPlanModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ProductAdapterNew extends RecyclerView.Adapter<ProductAdapterNew.MyViewHolder> {
    private final ArrayList<ProductDietPlanModel> list;
    private final Context context;
    int totalAmount;
    private Listener listener;

    public ProductAdapterNew(Context context, ArrayList<ProductDietPlanModel> list, Listener mListener) {
        this.context = context;
        this.list = list;
        this.listener = mListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_product_content, parent, false);
        return new ProductAdapterNew.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ProductDietPlanModel productDietPlanModel = list.get(position);
        holder.tvTitle.setText(productDietPlanModel.getDietPlanTitle());

        totalAmount = Integer.parseInt(productDietPlanModel.getDietPlanPrice()) - Integer.parseInt(productDietPlanModel.getDietPlanDiscount());

        holder.tvPlanName.setText(productDietPlanModel.getPlanName());

        Spanned rupeeSymbol= Html.fromHtml("<fontcolor='#FD139A'>"+context.getString(R.string.rupee_symbol)+"</font>");
        String planPrice = rupeeSymbol + String.valueOf(totalAmount);
        holder.tvAmount.setText(planPrice);

        try {
            if (productDietPlanModel.getDietCurrentPlan() != "") {
                if (productDietPlanModel.getDietCurrentPlan().equals("Y")) {
                    holder.llMain.setBackgroundResource(R.drawable.card_view_border);
                    holder.tvTitle1.setVisibility(View.VISIBLE);
                } else {
                    holder.llMain.setBackgroundResource(R.drawable.bg_blue_rounded_corners_1);
                    holder.tvTitle1.setVisibility(View.INVISIBLE);
                }
            }
        } catch (Exception e) {
            Log.v("TAG", "Exception in binding data: "+e.getMessage());
        }

        holder.btnBuyNow.setBackground(ContextCompat.getDrawable(context, R.drawable.submit_login_button));

        holder.btnBuyNow.setOnClickListener(view -> {
            if (listener != null)
                listener.onItemClick(productDietPlanModel.getId());
        });

        holder.adapter = new DescriptionAdapter(context, productDietPlanModel.getProductDescriptions());
        holder.layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.rvDesc.setLayoutManager(holder.layoutManager);
        holder.rvDesc.setAdapter(holder.adapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public DescriptionAdapter adapter;
        public RecyclerView.LayoutManager layoutManager;
        public TextView tvTitle, tvTitle1, tvPlanName, tvAmount;
        public RelativeLayout llMain;
        public Button btnBuyNow;
        public RecyclerView rvDesc;

        public MyViewHolder(View view) {
            super(view);
            llMain = view.findViewById(R.id.ll_main);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvTitle1 = view.findViewById(R.id.tvTitle1);
            tvPlanName = view.findViewById(R.id.tv_plan_name);
            tvAmount = view.findViewById(R.id.tv_amount);
            btnBuyNow = view.findViewById(R.id.btn_buy_now);
            rvDesc = view.findViewById(R.id.rv_desc);
        }
    }

    public interface Listener {
        void onItemClick(int id);
    }
}