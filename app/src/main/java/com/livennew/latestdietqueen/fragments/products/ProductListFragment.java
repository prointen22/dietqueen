package com.livennew.latestdietqueen.fragments.products;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.livennew.latestdietqueen.MyApplication;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.dashboard.DashboardActivity1;
import com.livennew.latestdietqueen.fragments.home.HomeFragment;
import com.livennew.latestdietqueen.fragments.products.adapter.ProductAdapter;
import com.livennew.latestdietqueen.fragments.products.adapter.ProductAdapterNew;
import com.livennew.latestdietqueen.fragments.products.model.ProductDescription;
import com.livennew.latestdietqueen.fragments.products.model.ProductDietPlanModel;
import com.livennew.latestdietqueen.login_screen.LoginActivity;
import com.livennew.latestdietqueen.model.SubScribe;
import com.livennew.latestdietqueen.model.SubScribeResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.payu.AppEnvironment;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.user_detail.PersonalDetailActivity;
import com.livennew.latestdietqueen.utils.CenterZoomLayoutManager;
import com.livennew.latestdietqueen.utils.SupportClass;
//import com.payumoney.core.PayUmoneyConfig;
//import com.payumoney.core.PayUmoneyConstants;
//import com.payumoney.core.PayUmoneySdkInitializer;
//import com.payumoney.core.entity.TransactionResponse;
//import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
//import com.payumoney.sdkui.ui.utils.ResultModel;
import com.payu.base.models.ErrorResponse;
import com.payu.base.models.PayUPaymentParams;
import com.payu.base.models.PaymentMode;
import com.payu.base.models.PaymentType;
import com.payu.checkoutpro.PayUCheckoutPro;
import com.payu.checkoutpro.models.PayUCheckoutProConfig;
import com.payu.checkoutpro.utils.PayUCheckoutProConstants;
import com.payu.ui.model.listeners.PayUCheckoutProListener;
import com.payu.ui.model.listeners.PayUHashGenerationListener;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultListener;
import com.razorpay.PaymentResultWithDataListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

@AndroidEntryPoint
public class ProductListFragment extends Fragment implements PaymentResultWithDataListener, PaymentResultListener {
    private RecyclerView rvProductDietPlan;
    private ProductAdapter productDietPlanAdapter;
    private ArrayList<ProductDietPlanModel> alProductDietPlan;
    public static final String TAG = "Erros";
    private LinearLayoutCompat llProgress;
    private Button btnNewsVideoReload;
    private FragmentActivity activity;
    private int lastPage = 0, currentPlanId;
    private String curapgeNo = "1";
    private Handler handler;
    private int currentItem, totalItems, scrollOutItems, width;
    private Boolean isScrolling = false;
    private CenterZoomLayoutManager recyclerLayoutManager;
    private ProgressBar progressBar;
    private CardView cvCurrentDietPlan;
    private TextView tvDietPlanTitle, tvDietPlanPrice, tvHeader, tvExpired, tvCurrentPlan, tvtBack;
    private ImageView ivDietPlan;
    private boolean isExpired;
    private LinearLayoutCompat ll_current_plan;
    TableLayout tbl_bottom;
    @Inject
    SessionManager sessionManager;
    User user;
//    private ViewPager mVpDietPlan;
//    private ProductAdapterNew productDietPlanAdapterNew;

    private ProductDietPlanModel mSelectedProduct;
    private int total;
    List<String> excludeMobiles = new ArrayList<>();

    @Inject
    APIInterface apiInterface;

    public ProductListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        user = sessionManager.getUser();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        init(view);
        /**Today's Diet*/

        rvProductDietPlan.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = recyclerLayoutManager.getChildCount();
                totalItems = recyclerLayoutManager.getItemCount();
                scrollOutItems = recyclerLayoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItem + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int page = Integer.parseInt(curapgeNo);
                            page++;
                            curapgeNo = page + "";
                            if (page <= lastPage) {
                                if (SupportClass.checkConnection(activity)) {
                                    onProductDietPlanPagination();
                                } else {
                                    SupportClass.noInternetConnectionToast(activity);
                                }
                            }
                        }
                    }, 2000);

                }
            }
        });
        btnNewsVideoReload.setOnClickListener(v -> {
            if (SupportClass.checkConnection(activity)) {
                onProductDietPlan();
                btnNewsVideoReload.setVisibility(View.GONE);
                rvProductDietPlan.setVisibility(View.VISIBLE);
//                mVpDietPlan.setVisibility(View.VISIBLE);
            } else {
                SupportClass.noInternetConnectionToast(activity);
                btnNewsVideoReload.setVisibility(View.VISIBLE);
                rvProductDietPlan.setVisibility(View.GONE);
//                mVpDietPlan.setVisibility(View.GONE);
            }
        });


     /*   ll_current_plan.setOnClickListener(View -> {
            if (activity instanceof DashboardActivity)
                ((DashboardActivity) activity).showProductDetails(currentPlanId, true);
            else if (activity instanceof ProductListActivity)
                ((ProductListActivity) activity).showProductDetails(currentPlanId);

        });*/

        excludeMobiles.add("2123111111");
        excludeMobiles.add("2123222222");
        excludeMobiles.add("2123333333");
        excludeMobiles.add("2123444444");
        excludeMobiles.add("2123555555");
        excludeMobiles.add("2123666666");
        excludeMobiles.add("2123777777");
        excludeMobiles.add("9767667670");
        excludeMobiles.add("8484090486");
        excludeMobiles.add("8329835469");

        return view;
    }

    public void init(View v) {
        alProductDietPlan = new ArrayList<>();
        alProductDietPlan.clear();
        rvProductDietPlan = v.findViewById(R.id.rv_diet_plan);
        tvtBack = v.findViewById(R.id.tvtBack);
        handler = new Handler();
        llProgress = v.findViewById(R.id.ll_progress);
        btnNewsVideoReload = v.findViewById(R.id.btn_news_video_reload);
        progressBar = v.findViewById(R.id.progress_bar);
        tvHeader = v.findViewById(R.id.tv_header);

        Checkout.preload(getApplicationContext());
//        mVpDietPlan = v.findViewById(R.id.vp_diet_plan);

       /*  ivDietPlan = v.findViewById(R.id.iv_current_diet_plan);
        tvDietPlanTitle = v.findViewById(R.id.tv_current_diet_plan_title);
        tvDietPlanPrice = v.findViewById(R.id.tv_current_diet_plan_price);
        cvCurrentDietPlan = v.findViewById(R.id.cv_current_plan);
        tvExpired = v.findViewById(R.id.tv_expired);
        tvCurrentPlan = v.findViewById(R.id.tv_current_plan);
        ll_current_plan = v.findViewById(R.id.ll_current_plan);*/


        tvtBack.setOnClickListener(view -> {
            //getActivity().onBackPressed();
            HomeFragment home = new HomeFragment();
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container, home);
            transaction.addToBackStack(null);
            transaction.commit();
        });

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        width = metrics.widthPixels;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SupportClass.checkConnection(activity)) {
            curapgeNo = "1";
            onProductDietPlan();
            btnNewsVideoReload.setVisibility(View.GONE);
            rvProductDietPlan.setVisibility(View.VISIBLE);
//            mVpDietPlan.setVisibility(View.VISIBLE);
        } else {
            SupportClass.noInternetConnectionToast(activity);
            btnNewsVideoReload.setVisibility(View.GONE);
            rvProductDietPlan.setVisibility(View.GONE);
//            mVpDietPlan.setVisibility(View.GONE);
        }
    }

    private void onProductDietPlan() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            rvProductDietPlan.setVisibility(View.GONE);
//            mVpDietPlan.setVisibility(View.GONE);
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.GET, Api.MAIN_URL + Api.DIETPLAN + "page=" + Integer.parseInt(curapgeNo), null, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                progressBar.setVisibility(View.GONE);
                rvProductDietPlan.setVisibility(View.VISIBLE);
//                mVpDietPlan.setVisibility(View.VISIBLE);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    // NewTractors newTractors = new Gson().fromJson(response.toString(), NewTractors.class);
                    //if (!newTractors.isError()) {
                    if (!error) {
                        JSONObject jsonObject1 = jObj.getJSONObject("data");
                        lastPage = jsonObject1.getInt("last_page");
                        JSONArray jsonArrayData = jsonObject1.getJSONArray("diet_plan");
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
                        String dietImagePath = jsonObjectOther.getString("dietPlanImagePath");
                        //alProductDietPlan = new ArrayList<>();
                        alProductDietPlan.clear();
                        int selectedPlanId = -1;
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);
                            ProductDietPlanModel productDietPlanModel = new ProductDietPlanModel();
                            // String dietPlanOffer, String dietPlanPrice
                            productDietPlanModel.setId(jsonObjectData.getInt("id"));
                            productDietPlanModel.setDietPlanTitle(jsonObjectData.getString("plan_name"));
//String desc=jsonObjectData.getString("description");
                            // Html.fromHtml("<fontcolor='#E89B6C'>"+desc+"</font>"Html.fromHtml("<fontcolor='#E89B6C'>"+desc+"</font>"
                            productDietPlanModel.setDietPlanDesc(jsonObjectData.getString("description1") + "##" + jsonObjectData.getString("description"));


                            productDietPlanModel.setDietPlanOffer(jsonObjectData.getString("plan_type"));
                            productDietPlanModel.setDietPlanDuration(jsonObjectData.getString("duration_in_month"));
                            productDietPlanModel.setDietPlanPrice(jsonObjectData.getString("amount"));
                            productDietPlanModel.setDietPlanDiscount(jsonObjectData.getString("discount_amount"));
                            productDietPlanModel.setDietPlanImage(dietImagePath.concat(jsonObjectData.getString("image")));
                            productDietPlanModel.setDiscountPer(jsonObjectData.getString("discount_per"));
                            productDietPlanModel.setDietPlanDesc1(jsonObjectData.getString("description2"));
                            if (sessionManager.getNoPlan() == 0) {
                                productDietPlanModel.setDietCurrentPlan("N");
                            }else {
                                productDietPlanModel.setDietCurrentPlan((jsonObjectData.getString("is_current_plan")));
                            }
                            productDietPlanModel.setPlanName(jsonObjectData.getString("plan_name"));

                            Log.v("TAG","Description 3: "+jsonObjectData.getString("description3"));
                            Log.v("TAG","Description 4: "+jsonObjectData.getString("description4"));

                            ArrayList<ProductDescription> alProductDescriptions = new ArrayList<>();

                            String[] arPlanInclusion = jsonObjectData.getString("description3").split("\\r\\n|\\n|\\r");
                            if (arPlanInclusion.length > 0){
                                for (String s : arPlanInclusion) {
                                    alProductDescriptions.add(new ProductDescription(s, true));
                                }
                            }

                            String[] arPlanExclusion = jsonObjectData.getString("description4").split("\\r\\n|\\n|\\r");
                            if (arPlanExclusion.length > 0){
                                for (String s : arPlanExclusion) {
                                    alProductDescriptions.add(new ProductDescription(s, false));
                                }
                            }

                            productDietPlanModel.setProductDescriptions(alProductDescriptions);

                            //Set layout manager to position the items
                            if (productDietPlanModel.getDietCurrentPlan() != "") {
                                if (productDietPlanModel.getDietCurrentPlan().equals("Y")) {
                                    selectedPlanId = productDietPlanModel.getId();
                                }
                                }

                            alProductDietPlan.add(productDietPlanModel);
                        }

                        productDietPlanAdapter = new ProductAdapter(activity, alProductDietPlan,selectedPlanId, this::buyButtonClick);
//                        productDietPlanAdapterNew = new ProductAdapterNew(activity, alProductDietPlan);
                        recyclerLayoutManager = new CenterZoomLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

                        rvProductDietPlan.setLayoutManager(recyclerLayoutManager);
                        rvProductDietPlan.setAdapter(productDietPlanAdapter);
//                        mVpDietPlan.setAdapter(productDietPlanAdapterNew);
                        final int radius = getResources().getDimensionPixelSize(R.dimen.rad);
                        final int dotsHeight = getResources().getDimensionPixelSize(R.dimen.indicator);
                        final int color = ContextCompat.getColor(getContext(), R.color.dark_grey);
                        final int color1 = ContextCompat.getColor(getContext(), R.color.orange_theme);
                        rvProductDietPlan.addItemDecoration(new DotsIndicatorDecoration(radius, 20, dotsHeight, color, color1));
                        new PagerSnapHelper().attachToRecyclerView(rvProductDietPlan);
//                        recyclerLayoutManager.scrollToPosition(alProductDietPlan.size() / 2);

//                        productDietPlanAdapter.setListener(this::buyButtonClick);

//                        productDietPlanAdapterNew.setListener(id -> {
//                            if (activity instanceof DashboardActivity)
//                                ((DashboardActivity) activity).showProductDetails(id, false);
//                            else if (activity instanceof ProductListActivity)
//                                ((ProductListActivity) activity).showProductDetails(id);
//
//                        });

                        if (!jsonObject1.isNull("current_plan") && jsonObject1.has("current_plan")) {
                            //cvCurrentDietPlan.setVisibility(View.VISIBLE);
                            JSONObject jsonArrayDataCurrent = jsonObject1.getJSONObject("current_plan");
                            isExpired = jsonArrayDataCurrent.getBoolean("is_expire");
                            if (!jsonArrayDataCurrent.isNull("diet_plan") && jsonArrayDataCurrent.has("diet_plan")) {
                                //cvCurrentDietPlan.setVisibility(View.VISIBLE);
                                //   tvCurrentPlan.setVisibility(View.VISIBLE);
                                //  tvHeader.setText(getResources().getString(R.string.upgrade_plan));
                                JSONObject jsonObjectCurrentDiet = jsonArrayDataCurrent.getJSONObject("diet_plan");
                                currentPlanId = jsonObjectCurrentDiet.getInt("id");
                                tvDietPlanTitle.setText(jsonObjectCurrentDiet.getString("plan_name"));
                                String amount = jsonObjectCurrentDiet.getString("amount");
                                String month = jsonObjectCurrentDiet.getString("duration_in_month");
                                Picasso.get().load(dietImagePath.concat(jsonObjectCurrentDiet.getString("image"))).placeholder(R.drawable.ic_award).error(R.drawable.ic_award).into(ivDietPlan, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        Log.d("TAG", "onSuccess");
                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        //  Toast.makeText(activity, "An error occurred", Toast.LENGTH_SHORT).show();
                                    }
                                });

                                if (isExpired) {
                                    tvExpired.setVisibility(View.VISIBLE);
                                    tvDietPlanPrice.setVisibility(View.GONE);
                                } else {
                                    tvExpired.setVisibility(View.GONE);
                                    tvDietPlanPrice.setVisibility(View.VISIBLE);
                                    tvDietPlanPrice.setText(month.concat(" " + "Month " + (amount == null || Integer.parseInt(amount) == 0 ? "" : amount)));
                                }
                            } else {
                                // cvCurrentDietPlan.setVisibility(View.GONE);
                                //  tvCurrentPlan.setVisibility(View.GONE);
                                //     tvHeader.setText(getResources().getString(R.string.weight_loss_plan));
                            }
                        } else {
                            // tvCurrentPlan.setVisibility(View.GONE);
                            //   cvCurrentDietPlan.setVisibility(View.GONE);
                            //   tvHeader.setText(getResources().getString(R.string.diet_plan));
                        }
                    } else {
                        //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                        Toast.makeText(activity, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void onProductDietPlanPagination() {
        try {
            llProgress.setVisibility(View.VISIBLE);
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.GET, Api.MAIN_URL + Api.DIETPLAN + "page=" + Integer.parseInt(curapgeNo), null, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject jsonObject1 = jObj.getJSONObject("data");
                        lastPage = jsonObject1.getInt("last_page");
                        JSONArray jsonArrayData = jsonObject1.getJSONArray("diet_plan");
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
                        String dietImagePath = jsonObjectOther.getString("dietPlanImagePath");
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);
                            ProductDietPlanModel productDietPlanModel = new ProductDietPlanModel();
                            // String dietPlanOffer, String dietPlanPrice
                            productDietPlanModel.setId(jsonObjectData.getInt("id"));
                            productDietPlanModel.setDietPlanTitle(jsonObjectData.getString("plan_name"));

                            String desc = jsonObjectData.getString("description");

                            productDietPlanModel.setDietPlanDesc(jsonObjectData.getString("description1") + "##" + jsonObjectData.getString("description"));
                            productDietPlanModel.setDietPlanOffer(jsonObjectData.getString("plan_type"));
                            productDietPlanModel.setDietPlanDuration(jsonObjectData.getString("duration_in_month"));
                            productDietPlanModel.setDietPlanPrice(jsonObjectData.getString("amount"));
                            productDietPlanModel.setDietPlanDiscount(jsonObjectData.getString("discount_amount"));
                            productDietPlanModel.setDietPlanImage(dietImagePath.concat(jsonObjectData.getString("image")));
                            productDietPlanModel.setDietCurrentPlan((jsonObjectData.getString("is_current_plan")));
                            productDietPlanModel.setPlanName(jsonObjectData.getString("plan_name"));

                            Log.v("TAG","Description 3: "+jsonObjectData.getString("description3"));
                            Log.v("TAG","Description 4: "+jsonObjectData.getString("description4"));

                            ArrayList<ProductDescription> alProductDescriptions = new ArrayList<>();

                            String[] arPlanInclusion = jsonObjectData.getString("description3").split("\\r\\n|\\n|\\r");
                            if (arPlanInclusion.length > 0){
                                for (String s : arPlanInclusion) {
                                    alProductDescriptions.add(new ProductDescription(s, true));
                                }
                            }

                            String[] arPlanExclusion = jsonObjectData.getString("description4").split("\\r\\n|\\n|\\r");
                            if (arPlanExclusion.length > 0){
                                for (String s : arPlanExclusion) {
                                    alProductDescriptions.add(new ProductDescription(s, false));
                                }
                            }

                            productDietPlanModel.setProductDescriptions(alProductDescriptions);

                            //Set layout manager to position the items
                            alProductDietPlan.add(productDietPlanModel);
                        }
                        productDietPlanAdapter.notifyDataSetChanged();
//                        productDietPlanAdapterNew.notifyDataSetChanged();
                        llProgress.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(activity, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void buyButtonClick(int id) {
        for (ProductDietPlanModel productObj:alProductDietPlan) {
            if (productObj.getId() == id) {
                mSelectedProduct = productObj;
                break;
            }
        }

        total = Integer.parseInt(mSelectedProduct.getDietPlanPrice()) - Integer.parseInt(mSelectedProduct.getDietPlanDiscount());

        if (isValidate()) {
            if (SupportClass.checkConnection(activity)) {
//                                    binding.btnProductSubscribe.setEnabled(false);
                if (total == 0.0 || excludeMobiles.contains(user.getMobile())) {
                    subScribe("");
                } else {
//                    launchPayUMoneyFlow();
//                    launchPayUMoneyProFlow();
                    initiateRazorpay();
                }
            } else {
                SupportClass.noInternetConnectionToast(activity);
            }
        } else {
            startActivityForResult(new Intent(activity, PersonalDetailActivity.class), PersonalDetailActivity.UPDATE_USER_REQ);
        }
    }

    private void initiateRazorpay() {


//        

//        checkout.setKeyID("<YOUR_KEY_ID>");
        /**
         * Instantiate Checkout
         */
        Checkout checkout = new Checkout();
        checkout.setKeyID("rzp_live_dK9qQuINobjeca");
//        checkout.setKeyID("rzp_test_wTkqcTKmWSLP1X");

        /**
         * Set your logo here
         */
        checkout.setImage(R.mipmap.ic_launcher);

        /**
         * Reference to current activity
         */
        final Activity activity = getActivity();

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();

            options.put("name", user.getName());
            options.put("description", mSelectedProduct.getDietPlanTitle());
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
//            options.put("order_id", "order_DBJOWzybf0sJbb");//from response of step 3.
            options.put("theme.color", "#F37022");
            options.put("send_sms_hash", true);
            options.put("allow_rotation", false);
            options.put("currency", "INR");
            options.put("amount", (total*100));//pass amount in currency subunits
            options.put("prefill.email", user.getEmail());
            options.put("prefill.contact",user.getMobile());
            JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

            checkout.open(activity, options);

        } catch(Exception e) {
            Log.e(TAG, "Error in starting Razorpay Checkout", e);
        }
    }


    private boolean isValidate() {
        if (Integer.parseInt(mSelectedProduct.getDietPlanPrice()) == 0)
            return true;
        else if (TextUtils.isEmpty(user.getName()) || TextUtils.isEmpty(user.getEmail()) || user.getState() == null || user.getCity() == null) {
            Toast.makeText(activity, getString(R.string.please_complete_profile_first), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void subScribe(String payuResponse) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("diet_plan_id", mSelectedProduct.getId());
        params.put("razorpay_txn_id", payuResponse);
        apiInterface.subScribe(params).enqueue(new retrofit2.Callback<SubScribeResp>() {
            @Override
            public void onResponse(@NotNull Call<SubScribeResp> call, @NotNull Response<SubScribeResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (!response.body().getError()) {
                            SubScribe data = response.body().getData();
//                            txtProductTitle = data.getDietPlan().getPlanName();
//                            durationMonth = data.getDietPlan().getDurationInMonth();
                            MyApplication.getLocaleManager().setNewLocale(activity, user.getUserDetail().getLanguage());
                            Intent intent = new Intent(activity, DashboardActivity1.class);
                            startActivity(intent);
                            activity.finish();

//                            Dialog dialog = new Dialog(activity);
//                            dialog.setContentView(R.layout.congratulation_screen);
//                            dialog.setCanceledOnTouchOutside(false);
//                            TextView tvMsg = dialog.findViewById(R.id.tv_detail);
//                            Button btnOk = dialog.findViewById(R.id.btn_ok);
////                            tvMsg.setText(Util.fromHtml("Your ".concat(data.getDietPlan().getDurationInMonth() + " Months " + "&ldquo; " + data.getDietPlan().getPlanName() + " &rdquo;" + " Diet Plan Active Now!")));
//                            tvMsg.setText(Util.fromHtml("Your " + "&ldquo;" + data.getDietPlan().getPlanName() + "&rdquo;" + " is Active."));
//                            btnOk.setOnClickListener(view -> {
//                                dialog.dismiss();
//                                MyApplication.getLocaleManager().setNewLocale(activity, user.getUserDetail().getLanguage());
//                                Intent intent = new Intent(activity, DashboardActivity1.class);
//                                startActivity(intent);
//                                activity.finish();
//
////                                Intent returnIntent = new Intent();
////                                activity.setResult(RESULT_OK, returnIntent);
////                                activity.finish();
//                            });
//                            dialog.show();
//                            ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
//                            InsetDrawable inset = new InsetDrawable(back, 40);
//                            dialog.getWindow().setBackgroundDrawable(inset);
//                            dialog.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
                        } else {
                            Toast.makeText(activity, response.body().getMessage().get(0), Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(@NotNull Call<SubScribeResp> call, @NotNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * This function prepares the data for payment and launches payumoney plug n play sdk
     */


    private void launchPayUMoneyProFlow() {
        PayUCheckoutProConfig payUCheckoutProConfig = new PayUCheckoutProConfig();
        payUCheckoutProConfig.setMerchantName("PayU");
        payUCheckoutProConfig.setMerchantSmsPermission(false);
        payUCheckoutProConfig.setAutoApprove(true);
        payUCheckoutProConfig.setMerchantResponseTimeout(15000); // for 15 seconds timeout
        payUCheckoutProConfig.setWaitingTime(45000);
        ArrayList<PaymentMode> checkoutOrderList = new ArrayList<>();
        checkoutOrderList.add(new PaymentMode(PaymentType.UPI, PayUCheckoutProConstants.CP_GOOGLE_PAY));
        checkoutOrderList.add(new PaymentMode(PaymentType.WALLET, PayUCheckoutProConstants.CP_PHONEPE));
        checkoutOrderList.add(new PaymentMode(PaymentType.WALLET, PayUCheckoutProConstants.CP_PAYTM));
        payUCheckoutProConfig.setPaymentModesOrder(checkoutOrderList);

        double amount = 0;
        try {
            amount = total;

        } catch (Exception e) {
            e.printStackTrace();
        }
        AppEnvironment appEnvironment = ((MyApplication) activity.getApplication()).getAppEnvironment();

        HashMap<String, Object> additionalParams = new HashMap<>();
        additionalParams.put(PayUCheckoutProConstants.CP_UDF1, "udf1");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF2, "udf2");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF3, "udf3");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF4, "udf4");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF5, "udf5");
        String transId = System.currentTimeMillis() + "";
        PayUPaymentParams.Builder builder = new PayUPaymentParams.Builder();
        builder.setAmount(String.valueOf(amount))
                .setIsProduction(true)
                .setProductInfo(mSelectedProduct.getPlanName())
                .setKey(appEnvironment.merchant_Key())
                .setPhone(user.getMobile().trim())
                .setTransactionId(transId)
                .setFirstName(user.getName() != null ? user.getName() : "dietQueen")
                .setEmail(user.getEmail() != null ? user.getEmail().trim() : "devkhetigaadi@gmail.com")
                .setSurl(appEnvironment.surl())
                .setFurl(appEnvironment.furl())
//                .setUserCredential(<String>)
                .setAdditionalParams(additionalParams); //Optional, can contain any additional PG params

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(appEnvironment.merchant_Key() + "|");
        stringBuilder.append(transId + "|");
        stringBuilder.append(amount + "|");
        stringBuilder.append(mSelectedProduct.getPlanName() + "|");
        stringBuilder.append(user.getName() != null ? user.getName() : "dietQueen");
        stringBuilder.append("|");
        stringBuilder.append(user.getEmail() != null ? user.getEmail().trim() : "devkhetigaadi@gmail.com" + "|");
        stringBuilder.append("|");
//        stringBuilder.append("|||||");
        stringBuilder.append("|||||");
//        stringBuilder.append(additionalParams.get(PayUCheckoutProConstants.CP_UDF1) + "|");
//        stringBuilder.append(additionalParams.get(PayUCheckoutProConstants.CP_UDF2) + "|");
//        stringBuilder.append(additionalParams.get(PayUCheckoutProConstants.CP_UDF3) + "|");
//        stringBuilder.append(additionalParams.get(PayUCheckoutProConstants.CP_UDF4) + "|");
//        stringBuilder.append(additionalParams.get(PayUCheckoutProConstants.CP_UDF5) + "||||||");
      //suAZO7|1650889271552|299.0|1 Month Weight Loss (Discount)|Suril Shah|surilshah08@gmail.com|||||||||||hWbVu2jC
   //SHA512(a2cqBC|fa3359f205d621c07383|2|Product Info|Payu-Admin|test@example.com|||||||||||
        // {"billingAmount": "150.00","billingCurrency": "INR",
        // "billingCycle": "WEEKLY","billingInterval": 1,"paymentStartDate": "2019-09-18","paymentEndDate": "2020-10-20"}
        // |<Please_add_salt_here>)

        stringBuilder.append(appEnvironment.salt());
        PayUPaymentParams payUPaymentParams = builder.build();

        PayUCheckoutPro.open(
                getActivity(),
                payUPaymentParams,
                payUCheckoutProConfig,
                new PayUCheckoutProListener() {

                    @Override
                    public void onPaymentSuccess(Object response) {
                        //Cast response object to HashMap
                        HashMap<String, Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String) result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
                        Log.i("TAG","payuResponse = "+payuResponse);
                        Log.i("TAG","merchantResponse = "+merchantResponse);
                        subScribe(payuResponse);
                    }

                    @Override
                    public void onPaymentFailure(Object response) {
                        //Cast response object to HashMap
                        HashMap<String, Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String) result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
                        Log.i("TAG","payuResponse = "+payuResponse);
                        Log.i("TAG","merchantResponse = "+merchantResponse);

                    }

                    @Override
                    public void onPaymentCancel(boolean isTxnInitiated) {
                        Log.i("TAG","isTxnInitiated = "+isTxnInitiated);
                    }

                    @Override
                    public void onError(ErrorResponse errorResponse) {
                        String errorMessage = errorResponse.getErrorMessage();
                        Log.i("TAG","errorMessage = "+errorMessage);

                    }

                    @Override
                    public void setWebViewProperties(@Nullable WebView webView, @Nullable Object o) {
                        //For setting webview properties, if any. Check Customized Integration section for more details on this
                    }

                    @Override
                    public void generateHash(HashMap<String, String> valueMap, PayUHashGenerationListener hashGenerationListener) {
                        String hashName = valueMap.get(PayUCheckoutProConstants.CP_HASH_NAME);
                        String hashData = valueMap.get(PayUCheckoutProConstants.CP_HASH_STRING);
                        if (!TextUtils.isEmpty(hashName) && !TextUtils.isEmpty(hashData)) {
                            //Do not generate hash from local, it needs to be calculated from server side only. Here, hashString contains hash created from your server side.
                            Log.i("TAG","str = "+stringBuilder);
                            String hash = hashCal(String.valueOf(stringBuilder));
//                            String hash = "ea1da4d3084b5e6010ef1f7a59e14e9a3f51c9b100a2c126a26ad2e159d97cd3f9e0ef3d26146a4136bcae66bca896616556ea940504e1f83912699201bfcf16";
//                            String hash = "17544038f972d9e6433b0844f5dcbd790af395eb1e2655827ad0fc9df3f0e344cde5f3db31b79d3f478a60435f681930d95427e667ac724853132e5fa2657a53";
                            Log.i("TAG","hash = "+hash);
                            if (!TextUtils.isEmpty(hash)) {
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put(hashName, hash);
                                hashGenerationListener.onHashGenerated(hashMap);
                            }

//                            HashMap<String, String> dataMap = new HashMap<>();
//                            dataMap.put(hashName, hash);
//                            hashGenerationListener.onHashGenerated(dataMap);

                        }
                    }
                }
        );

    }

//    private void launchPayUMoneyFlow() {
//        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();
//        payUmoneyConfig.setDoneButtonText(getString(R.string.pay_now));
//        payUmoneyConfig.setPayUmoneyActivityTitle(getString(R.string.payment));
//        payUmoneyConfig.disableExitConfirmation(false);
//        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();
//        double amount = 0;
//        try {
//            amount = total;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        String txnId = System.currentTimeMillis() + "";
//        String phone = user.getMobile().trim();
//        String productName = mSelectedProduct.getPlanName();
//        String firstName = user.getName() != null ? user.getName() : "dietQueen";
//        String email = user.getEmail() != null ? user.getEmail().trim() : "devkhetigaadi@gmail.com";
//        String udf1 = "";
//        String udf2 = "";
//        String udf3 = "";
//        String udf4 = "";
//        String udf5 = "";
//        String udf6 = "";
//        String udf7 = "";
//        String udf8 = "";
//        String udf9 = "";
//        String udf10 = "";
//        AppEnvironment appEnvironment = ((MyApplication) activity.getApplication()).getAppEnvironment();
//        builder.setAmount(String.valueOf(amount))
//                .setTxnId(txnId)
//                .setPhone(phone)
//                .setProductName(productName)
//                .setFirstName(firstName)
//                .setEmail(email)
//                .setsUrl(appEnvironment.surl())
//                .setfUrl(appEnvironment.furl())
//                .setUdf1(udf1)
//                .setUdf2(udf2)
//                .setUdf3(udf3)
//                .setUdf4(udf4)
//                .setUdf5(udf5)
//                .setUdf6(udf6)
//                .setUdf7(udf7)
//                .setUdf8(udf8)
//                .setUdf9(udf9)
//                .setUdf10(udf10)
//                .setIsDebug(appEnvironment.debug())
//                .setKey(appEnvironment.merchant_Key())
//                .setMerchantId(appEnvironment.merchant_ID());
//
//
//        Log.d("PauuMoney Amount",String.valueOf(amount));
//        Log.d("PauuMoney txnId",String.valueOf(txnId));
//        Log.d("PauuMoney phone",String.valueOf(phone));
//        Log.d("PauuMoney firstName",String.valueOf(firstName));
//        Log.d("PauuMoney email",String.valueOf(email));
//        Log.d("Pauuy surl()",appEnvironment.surl());
//        Log.d("PauuMoney furl",appEnvironment.furl());
//        Log.d("PauuMoney key",appEnvironment.merchant_Key());
//        Log.d("PauuMoney MID",appEnvironment.merchant_ID());
//
//        try {
//            PayUmoneySdkInitializer.PaymentParam mPaymentParams = builder.build();
//            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);
//            PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, activity, R.style.AppTheme_Pink, true);
//        } catch (Exception e) {
//            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
//            Log.e(TAG, "launchPayUMoneyFlow: " + e.getMessage());
////            binding.btnProductSubscribe.setEnabled(true);
//        }
//    }

//    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {
//        StringBuilder stringBuilder = new StringBuilder();
//        HashMap<String, String> params = paymentParam.getParams();
//        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");
//
//        AppEnvironment appEnvironment = ((MyApplication) activity.getApplication()).getAppEnvironment();
//        stringBuilder.append(appEnvironment.salt());
//
//        String hash = hashCal(stringBuilder.toString());
//        paymentParam.setMerchantHash(hash);
//
//        return paymentParam;
//    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT) {
////            binding.btnProductSubscribe.setEnabled(true);
//            if (resultCode == RESULT_OK && data != null) {
//                TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
//                        .INTENT_EXTRA_TRANSACTION_RESPONSE);
//
//                ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);
//
//                // Check which object is non-null
//                if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
//                    if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
//                        //Success Transaction
//                        subScribe(transactionResponse.getPayuResponse());
//                    } else {
//                        //Failure Transaction
//                        Toast.makeText(activity, transactionResponse.getMessage().equals("") ? getString(R.string.payment_failed_please_try_again) : transactionResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//
//                    // Response from Payumoney
//                    String payuResponse = transactionResponse.getPayuResponse();
//                    Log.e(TAG, "onActivityResult: " + payuResponse);
//                    // Response from SURl and FURL
//                    String merchantResponse = transactionResponse.getTransactionDetails();
//                    Log.e(TAG, "onActivityResult: " + merchantResponse);
//
//                } else if (resultModel != null && resultModel.getError() != null) {
//                    Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
//                } else {
//                    Log.d(TAG, "Both objects are null!");
//                }
//            } else {
//                Toast.makeText(activity, "Payment failed", Toast.LENGTH_SHORT).show();
//            }
//        } else
            if (requestCode == PersonalDetailActivity.UPDATE_USER_REQ) {
            if (resultCode == RESULT_OK) {
                user = sessionManager.getUser();
                if (mSelectedProduct!=null) {
                    total = Integer.parseInt(mSelectedProduct.getDietPlanPrice()) - Integer.parseInt(mSelectedProduct.getDietPlanDiscount());

                    if (isValidate()) {
                        if (SupportClass.checkConnection(activity)) {
//                                    binding.btnProductSubscribe.setEnabled(false);
                            if (total == 0.0 || excludeMobiles.contains(user.getMobile())) {
                                subScribe("");
                            } else {
//                                launchPayUMoneyFlow();
                            }
                        } else {
                            SupportClass.noInternetConnectionToast(activity);
                        }
                    } else {
                        startActivityForResult(new Intent(activity, PersonalDetailActivity.class), PersonalDetailActivity.UPDATE_USER_REQ);
                    }
                }

            }
        }
    }

    public void updateUser() {
        user = sessionManager.getUser();
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        Log.i("TAG","data = "+s);
        Log.i("TAG","data = "+paymentData.getData());
        Log.i("TAG","data = "+paymentData.getOrderId());
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        Log.i("TAG","data = "+s);
        Log.i("TAG","data = "+paymentData.getData());
    }

    @Override
    public void onPaymentSuccess(String s) {
        Log.i("TAG","data = "+s);
    }

    @Override
    public void onPaymentError(int i, String s) {
        Log.i("TAG","data = "+s);
    }
}

class DotsIndicatorDecoration extends RecyclerView.ItemDecoration {

    private final int indicatorHeight;
    private final int indicatorItemPadding;
    private final int radius;

    private final Paint inactivePaint = new Paint();
    private final Paint activePaint = new Paint();

    public DotsIndicatorDecoration(int radius, int padding, int indicatorHeight, @ColorInt int colorInactive, @ColorInt int colorActive) {
        float strokeWidth = Resources.getSystem().getDisplayMetrics().density * 1;
        this.radius = radius;
        inactivePaint.setStrokeCap(Paint.Cap.ROUND);
        inactivePaint.setStrokeWidth(strokeWidth);
        inactivePaint.setStyle(Paint.Style.FILL);
        inactivePaint.setAntiAlias(true);
        inactivePaint.setColor(colorInactive);

        activePaint.setStrokeCap(Paint.Cap.ROUND);
        activePaint.setStrokeWidth(strokeWidth);
        activePaint.setStyle(Paint.Style.FILL);
        activePaint.setAntiAlias(true);
        activePaint.setColor(colorActive);

        this.indicatorItemPadding = padding;
        this.indicatorHeight = indicatorHeight;
    }

    @Override
    public void onDrawOver(@NotNull Canvas c, @NotNull RecyclerView parent, @NotNull RecyclerView.State state) {
        super.onDrawOver(c, parent, state);

        final RecyclerView.Adapter adapter = parent.getAdapter();

        if (adapter == null) {
            return;
        }

        int itemCount = adapter.getItemCount();

        // center horizontally, calculate width and subtract half from center
        float totalLength = this.radius * 2 * itemCount;
        float paddingBetweenItems = Math.max(0, itemCount - 1) * indicatorItemPadding;
        float indicatorTotalWidth = totalLength + paddingBetweenItems;
        float indicatorStartX = (parent.getWidth() - indicatorTotalWidth) / 2f;

        // center vertically in the allotted space
        float indicatorPosY = parent.getHeight() - indicatorHeight / 2f;

        drawInactiveDots(c, indicatorStartX, indicatorPosY, itemCount);

        int activePosition;

        if (parent.getLayoutManager() instanceof GridLayoutManager) {
            activePosition = ((GridLayoutManager) parent.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        } else if (parent.getLayoutManager() instanceof LinearLayoutManager) {
            activePosition = ((LinearLayoutManager) parent.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        } else {
            // not supported layout manager
            return;
        }

        if (activePosition == RecyclerView.NO_POSITION) {
            return;
        }

        // find offset of active page if the user is scrolling
        final View activeChild = parent.getLayoutManager().findViewByPosition(activePosition);
        if (activeChild == null) {
            return;
        }

        drawActiveDot(c, indicatorStartX, indicatorPosY, activePosition);
    }

    private void drawInactiveDots(Canvas c, float indicatorStartX, float indicatorPosY, int itemCount) {
        // width of item indicator including padding
        final float itemWidth = this.radius * 2 + indicatorItemPadding;

        float start = indicatorStartX + radius;
        for (int i = 0; i < itemCount; i++) {
            c.drawCircle(start, indicatorPosY, radius, inactivePaint);
            start += itemWidth;
        }
    }

    private void drawActiveDot(Canvas c, float indicatorStartX, float indicatorPosY,
                               int highlightPosition) {
        // width of item indicator including padding
        final float itemWidth = this.radius * 2 + indicatorItemPadding;
        float highlightStart = indicatorStartX + radius + itemWidth * highlightPosition;
        c.drawCircle(highlightStart, indicatorPosY, radius, activePaint);
    }

    @Override
    public void getItemOffsets(@NotNull Rect outRect, @NotNull View view, @NotNull RecyclerView parent, @NotNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = indicatorHeight;
    }
}