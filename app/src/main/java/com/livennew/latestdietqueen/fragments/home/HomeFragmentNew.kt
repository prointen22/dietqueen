package com.livennew.latestdietqueen.fragments.home

import android.app.Dialog
import android.content.*
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.livennew.latestdietqueen.BuildConfig
import com.livennew.latestdietqueen.R
import com.livennew.latestdietqueen.SessionManager
import com.livennew.latestdietqueen.Util
import com.livennew.latestdietqueen.databinding.AddDietMenuReplaceBinding
import com.livennew.latestdietqueen.databinding.FragmentHomeNewBinding
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.ReplaceItemAdapter
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.*
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTodayDietAdapterNew
import com.livennew.latestdietqueen.model.FoodReplaceResp
import com.livennew.latestdietqueen.model.ReplaceFoodParam
import com.livennew.latestdietqueen.model.User
import com.livennew.latestdietqueen.network_support.APIClient
import com.livennew.latestdietqueen.retrofit.APIInterface
import com.livennew.latestdietqueen.utils.CenterZoomLayoutManager
import com.livennew.latestdietqueen.utils.DateUtil
import com.livennew.latestdietqueen.utils.DateUtilNew
import com.livennew.latestdietqueen.utils.DotsIndicatorDecoration
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragmentNew : Fragment() {
    private var mTodayMealAdapter: HomeTodayDietAdapterNew? = null
    private var alHomeTodayDiet: ArrayList<DietEPlanFood>? = null
    private var alHomeReplaceTodayDiet: ArrayList<Food>? = null
    private var binding: FragmentHomeNewBinding? = null
    var apiInterface: APIInterface? = null
    var sessionManager: SessionManager? = null
    var user: User? = null
    var mPrefsobj: SharedPreferences? = null
    var prahar = ""
    ////
    lateinit var mCountdownTimer: CountDownTimer
    var isRunning: Boolean = false
    var mTimeInMilliSeconds = 0L

    lateinit var mSelectedMealImageUrl: String
    lateinit var mSelectedFoodObj: Food
    lateinit var mSelectedDietId: String
    lateinit var mSelectedDietPlanObj: DietEPlanFood
    lateinit var mSelectedOthersObj: Others
    lateinit var mResponsePlan: Plan

    companion object {
        @JvmStatic
        fun newInstance(): Fragment {
            return HomeFragmentNew()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(requireActivity().applicationContext)
        apiInterface = APIClient.getClient(requireActivity().applicationContext).create(APIInterface::class.java)
        user = sessionManager!!.user
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentHomeNewBinding.inflate(inflater, container, false)
        mPrefsobj = requireActivity().getPreferences(Context.MODE_PRIVATE)
        if (user!=null && !TextUtils.isEmpty(user?.name)) {
            binding?.tvTitle?.text = "Hi ${user?.name}"
        }

        onSetTodayDietRecyclerView()

        binding!!.llcReplace.setOnClickListener { if (!TextUtils.isEmpty(mSelectedDietId)) {
            onFoodReplace(mSelectedDietPlanObj)
        } }

        return binding!!.root
    }

    private fun startTimer(time_in_seconds: Long) {
        mCountdownTimer = object : CountDownTimer(time_in_seconds, 1000) {
            override fun onFinish() {
            }

            override fun onTick(p0: Long) {
                mTimeInMilliSeconds = p0
                updateTextUI()
            }
        }
        mCountdownTimer.start()

        isRunning = true
    }

    private fun updateTextUI() {
        val minute = (mTimeInMilliSeconds / 1000) / 60
        val seconds = (mTimeInMilliSeconds / 1000) % 60

        binding?.tvMinute?.text  = "$minute"
        binding?.tvSecond?.text = "$seconds"
    }

    /**
     * Today's diet recycler view with CenterZoomLayoutManager class implemented
     */
    private fun onSetTodayDietRecyclerView() {
        alHomeTodayDiet = ArrayList()
        alHomeReplaceTodayDiet = ArrayList()
        mTodayMealAdapter = HomeTodayDietAdapterNew(requireActivity(), alHomeTodayDiet!!){ imageUrl: String, foodObj:Food, dietId:String, dietPlanFoodObj:DietEPlanFood ->
            adapterItemClicked(imageUrl, foodObj, dietId, dietPlanFoodObj)
        }
        val todayDietLayoutManager = CenterZoomLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding?.rvMeal?.layoutManager = todayDietLayoutManager
        binding?.rvMeal?.adapter = mTodayMealAdapter
        val radius = resources.getDimensionPixelSize(R.dimen.rad)
        val dotsHeight = resources.getDimensionPixelSize(R.dimen.indicator)
        val color = ContextCompat.getColor(requireContext(), R.color.devider_color)
        val color1 = ContextCompat.getColor(requireContext(), R.color.orange_theme)
        binding?.rvMeal?.addItemDecoration(DotsIndicatorDecoration(radius, 20, dotsHeight, color, color1))
        PagerSnapHelper().attachToRecyclerView(binding?.rvMeal)
        prepareTodayDietData()
        if (sessionManager!!.isTermsAccepted == 0) {
            Log.d("ddddd", "TEST")
        }
    }

    private fun prepareTodayDietData() {
        Log.d("TAG", "prepareTodayDietData  sessionManager.getDietEPlanId(): " + sessionManager!!.dietEPlanId)
        binding?.rvMeal?.visibility = View.GONE
        binding?.progressBar?.visibility = View.VISIBLE
        apiInterface!!.getDietPlan(PlanParam(sessionManager!!.dietEPlanId)).enqueue(object : Callback<Plan?> {
            override fun onResponse(call: Call<Plan?>, response: Response<Plan?>) {
                if (response.body() != null) {
                    if (!response.body()!!.error) {
                        Log.d("TAG", "prepareTodayDietData success response")
                        val versionCode = BuildConfig.VERSION_CODE
                        if (versionCode < response.body()!!.data.aos_version.toInt()) {
                            update()
                        }

                        if (response.body() == null || response.body()!!.data == null) {
                            binding?.progressBar?.visibility = View.GONE
                            Log.v("TAG", "Empty data from response")
                            return
                        }

                        mResponsePlan = response.body()!!

                        if (!TextUtils.isEmpty(mResponsePlan.data.planSubcategory)) {
                            binding?.tvMealTitle?.text = mResponsePlan.data.planSubcategory
                        }

                        if (response.body()!!.data.dietEPlanFood.isNotEmpty()) {
                            val mealItemCountMsg = "${response.body()!!.data.dietEPlanFood.size} ${getString(R.string.item_meal)}"
                            binding?.tvMealSubTitle?.text = mealItemCountMsg
                        }

                        binding?.rvMeal?.visibility = View.VISIBLE
                        mSelectedOthersObj = response.body()!!.others
                        mTodayMealAdapter?.addAll(response.body()!!.data.dietEPlanFood, response.body()!!.others)

                        Log.d("TAG", "prepareTodayDietData   time response.body().getData().getTime():" + response.body()!!.data.time)
                        Log.d("TAG", "Time diff (response.body()!!.data.time_difference):" + response.body()!!.data.time_difference)
                        Log.d("TAG", "prepareTodayDietData   getDietEDataId: " + response.body()!!.data.dietEPlanFood[0].dietEDataId)
                        Log.d("TAG", "prepareTodayDietData   response.body().getData().getIs_diet_done(): " + response.body()!!.data.is_diet_done)

                        val time1 = DateUtil.convertTime24To12(response.body()!!.data.time)
                        if (!TextUtils.isEmpty(time1)) {
                            val nextMealText = getString(R.string.next_meal)
                            val nextMealTime ="$nextMealText $time1"
                            val spannable: Spannable = SpannableString(nextMealTime)
                            spannable.setSpan(ForegroundColorSpan(context!!.getColor(R.color.orange_theme)), nextMealText.length, nextMealTime.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

                            binding!!.tvNextMeal.setText(spannable, TextView.BufferType.SPANNABLE)

                            val mTimeDiff = DateUtilNew.getMealTimeDiffWithCurrentTimeInMinutes(time1)

                            binding!!.tvMinute.text = mTimeDiff.toString()
                            mTimeInMilliSeconds = binding!!.tvMinute.text.toString().toLong() *60000L
                            startTimer(mTimeInMilliSeconds)
                        }

                        setDefaultData()
                    } else {
                        binding?.rvMeal?.visibility = View.GONE
                        for (i in response.body()!!.message.indices) {
                            Toast.makeText(activity, response.body()!!.message[i], Toast.LENGTH_SHORT).show()
                        }
                    }
                    binding?.progressBar?.visibility = View.GONE
                }
            }

            override fun onFailure(call: Call<Plan?>, t: Throwable) {
                binding?.rvMeal?.visibility = View.GONE
                binding?.progressBar?.visibility = View.GONE
                Log.d("TAG", "prepareTodayDietData error response: $t")
            }
        })
    }

    private fun adapterItemClicked(imageUrl: String, foodObj: Food, dietId: String, dietPlanFoodObj: DietEPlanFood) {
        mSelectedMealImageUrl = imageUrl
        mSelectedFoodObj = foodObj
        mSelectedDietId = dietId
        mSelectedDietPlanObj = dietPlanFoodObj

        setMealItemImage(imageUrl)
    }

    private fun setMealItemImage(url:String) {
        Glide.with(requireActivity())
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(binding!!.civMeal)

        binding!!.tvMealItem.text = mSelectedDietPlanObj.food.foodMasterName
        binding!!.tvMealQty.text = getDietQty(Util.calculateQty(mSelectedDietPlanObj, mSelectedOthersObj), mSelectedDietPlanObj.food.unit.name)
    }

    private fun getDietQty(calculateQty: Long, name: String): String? {
        return if (calculateQty == 0L) "NA" else "$calculateQty $name"
    }

    private fun onFoodReplace(todayDietMenu: DietEPlanFood) {
        val dialog = Dialog(requireActivity(), R.style.theme_sms_receive_dialog)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        val binding: AddDietMenuReplaceBinding = AddDietMenuReplaceBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root) //add_diet_menu_replace
        dialog.setCanceledOnTouchOutside(false)
        //binding.tvReplcItmHeader.text = "Replace " + todayDietMenu.food.foodMasterName
        val alReplaceItemModel = ArrayList<Food>()
        val blogLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.rvReplaceItemList.layoutManager = blogLayoutManager
        val replaceItemAdapter = ReplaceItemAdapter(activity, alReplaceItemModel) { food: Food? ->
            if (food != null) {
                var oldFoodId = 0
                oldFoodId = if (todayDietMenu.oldFoodId == 0) {
                    todayDietMenu.food.id
                } else {
                    todayDietMenu.oldFoodId
                }
                apiInterface!!.replaceFood(ReplaceFoodParam(oldFoodId, food.id, todayDietMenu.dietEDataId, DateUtil.getTodayDate())).enqueue(object : Callback<FoodReplaceResp?> {
                    override fun onResponse(call: Call<FoodReplaceResp?>, response: Response<FoodReplaceResp?>) {
                        if (response.body() != null) {
                            if (response.body()!!.error) {
                                for (i in response.body()!!.message.indices) Toast.makeText(activity, response.body()!!.message[i], Toast.LENGTH_SHORT).show()
                            } else {
//                                todayDietMenu.setOldFoodId(response.body()!!.data.oldFoodId)
//                                todayDietMenu.
                                todayDietMenu.oldFoodId = response.body()!!.data.oldFoodId
                                todayDietMenu.food = response.body()!!.data.food
                                mSelectedFoodObj = todayDietMenu.food
                                mSelectedMealImageUrl = mSelectedOthersObj.dietPlanImagePath + mSelectedFoodObj.image
                                mTodayMealAdapter?.notifyDataSetChanged()
                                adapterItemClicked(mSelectedMealImageUrl, mSelectedFoodObj, mSelectedDietId, mSelectedDietPlanObj)
                            }
                        }
                        dialog.dismiss()
                    }

                    override fun onFailure(call: Call<FoodReplaceResp?>, t: Throwable) {
                        dialog.dismiss()
                    }
                })
            } else {
                dialog.dismiss()
            }
        };
        binding.rvReplaceItemList.adapter = replaceItemAdapter
        binding.progressBar.visibility = View.VISIBLE
        binding.ivClose.setOnClickListener { dialog.dismiss() }
        /*binding.btnReplaceItemOk.setOnClickListener {
            if (replaceItemAdapter.lastSelectedFood != null) {
                apiInterface!!.replaceFood(ReplaceFoodParam(todayDietMenu.food.id, replaceItemAdapter.lastSelectedFood.id, todayDietMenu.dietEDataId, DateUtil.getTodayDate())).enqueue(object : Callback<FoodReplaceResp?> {
                    override fun onResponse(call: Call<FoodReplaceResp?>, response: Response<FoodReplaceResp?>) {
                        if (response.body() != null) {
                            if (response.body()!!.error) {
                                for (i in response.body()!!.message.indices) Toast.makeText(activity, response.body()!!.message[i], Toast.LENGTH_SHORT).show()
                            } else {
                                todayDietMenu.food = response.body()!!.data.food
                                mSelectedFoodObj = todayDietMenu.food
                                mSelectedMealImageUrl = mSelectedOthersObj.dietPlanImagePath + mSelectedFoodObj.image
                                mTodayMealAdapter?.notifyDataSetChanged()
                                adapterItemClicked(mSelectedMealImageUrl, mSelectedFoodObj, mSelectedDietId, mSelectedDietPlanObj)
                            }
                        }
                        dialog.dismiss()
                    }

                    override fun onFailure(call: Call<FoodReplaceResp?>, t: Throwable) {
                        dialog.dismiss()
                    }

                })
            } else {
                dialog.dismiss()
            }
        }*/
        apiInterface!!.getFoodsByGroup(FoodGroupParam(todayDietMenu.food)).enqueue(object : Callback<Foods?> {
            override fun onResponse(call: Call<Foods?>, response: Response<Foods?>) {
                if (response.body() != null) {
                    if (response.body()!!.data != null && response.body()!!.data.size > 0) {
                        val i = response.body()!!.data.iterator()
                        while (i.hasNext()) {
                            val e = i.next()
                            if (e.id == todayDietMenu.food.id) {
                                i.remove()
                            }
                        }
                        if (response.body()!!.data.size == 0) {
                            dialog.dismiss()
                            Toast.makeText(activity, getString(R.string.there_is_no_food_to_replace), Toast.LENGTH_SHORT).show()
                        }
                        replaceItemAdapter.addAll(response.body()!!.data, response.body()!!.others)
                        binding.progressBar.visibility = View.GONE
                    } else {
                        dialog.dismiss()
                        Toast.makeText(activity, getString(R.string.there_is_no_food_to_replace), Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<Foods?>, t: Throwable) {
                Log.e("TAG", "onFailure: " + t.message)
                dialog.dismiss()
            }
        })
        dialog.show()
        dialog.window!!.setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT)
    }

    private fun setDefaultData() {
        try {
            prahar = mResponsePlan.data.planSubcategory
            if (mResponsePlan.data.dietEPlanFood[0].food.image != null) {
                mSelectedMealImageUrl = mResponsePlan.others.dietPlanImagePath + mResponsePlan.data.dietEPlanFood[0].food.image
            }
            mSelectedFoodObj = mResponsePlan.data.dietEPlanFood[0].food
            mSelectedDietId = mResponsePlan.data.dietEPlanFood[0].dietEDataId.toString()
            mSelectedDietPlanObj = mResponsePlan.data.dietEPlanFood[0]
            setMealItemImage(mSelectedMealImageUrl)
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    private fun update() {
        val builder = AlertDialog.Builder(requireActivity(), R.style.MyDatePicker)
        builder.setTitle("Update")
        builder.setMessage("Kindly Update App from Play store for latest changes.")
        builder.setPositiveButton("Update") { dialog: DialogInterface, which: Int ->
            // Do nothing but close the dialog
            val appPackageName = requireActivity().packageName // getPackageName() from Context or Activity object
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
            } catch (anfe: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
            }
            requireActivity().finish()
            dialog.dismiss()
        }
        builder.setCancelable(false)
        val alert = builder.create()
        alert.show()
    }

}