package com.livennew.latestdietqueen.fragments.home.adapters;

import android.content.Context;

import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;

import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.adapter.VideoListAdapter;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;

import java.util.List;


public class HomeVideosAdapter extends RecyclerView.Adapter<HomeVideosAdapter.MyViewHolder> {
    private static final String TAG = "HomeVideosAdapter";
    private final Context context;
    private List<Video> list;
    private Listener listener;

    public HomeVideosAdapter(Context context, List<Video> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_video_list_item, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        Video item = list.get(position);

            holder.tvName.setText(Util.fromHtml(item.getSnippet().getTitle()));
        Glide.with(context).load(item.getSnippet().getThumbnails().getMedium().getUrl()).into((holder.ivVideo));
        holder.view.setOnClickListener(view -> {
            if (listener != null)
                listener.onItemClick(item, position);
        });

    }

    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return list.size();

    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setData(List<Video> list) {
        this.list = list;
        notifyDataSetChanged();
        Log.e(TAG, "setData: ");
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView ivVideo;
        private final TextView tvName;
        private View view;


        public MyViewHolder(View view) {
            super(view);
            // TODO Auto-generated constructor stub
            ivVideo = (ImageView) view.findViewById(R.id.ivVideo);
            tvName = (TextView) view.findViewById(R.id.tvName);
            this.view = view;
        }
    }

    public interface Listener {
        void onItemClick(Video currentTractor, int position);
    }
}
