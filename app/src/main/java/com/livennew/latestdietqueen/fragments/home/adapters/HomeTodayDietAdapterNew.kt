package com.livennew.latestdietqueen.fragments.home.adapters

import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import com.livennew.latestdietqueen.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others
import de.hdodenhof.circleimageview.CircleImageView

class HomeTodayDietAdapterNew(private val context: Context, private var alTodayDietModel: List<DietEPlanFood>, private val mClickListener: (String, Food, String, DietEPlanFood) -> Unit) : RecyclerView.Adapter<HomeTodayDietAdapterNew.MyViewHolder>() {
    private var others: Others? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.home_meal_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val todayDietModel = alTodayDietModel[position]
        Glide.with(context)
                .load(others!!.dietPlanImagePath + todayDietModel.food.image)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.ivMealItem)

        holder.ivMealItem.setOnClickListener { mClickListener(others!!.dietPlanImagePath + todayDietModel.food.image, todayDietModel.food, todayDietModel.dietEDataId.toString(), todayDietModel) }
    }

    override fun getItemCount(): Int {
        return alTodayDietModel.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(dietEPlanFood: List<DietEPlanFood>, others: Others?) {
        alTodayDietModel = dietEPlanFood
        this.others = others
        notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ivMealItem: CircleImageView

        init {
            ivMealItem = itemView.findViewById(R.id.civ_main)
        }
    }
}