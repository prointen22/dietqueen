package com.livennew.latestdietqueen.fragments.products;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.MyApplication;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.ActivityProductDetailsBinding;
import com.livennew.latestdietqueen.fragments.products.model.ProductResp;
import com.livennew.latestdietqueen.model.SubScribe;
import com.livennew.latestdietqueen.model.SubScribeResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.payu.AppEnvironment;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.user_detail.PersonalDetailActivity;
import com.livennew.latestdietqueen.utils.SupportClass;
//import com.payumoney.core.PayUmoneyConfig;
//import com.payumoney.core.PayUmoneyConstants;
//import com.payumoney.core.PayUmoneySdkInitializer;
//import com.payumoney.core.entity.TransactionResponse;
//import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
//import com.payumoney.sdkui.ui.utils.ResultModel;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class ProductDetailActivity extends FabActivity {
    public static final int PRODUCT_DETAILS_REQ = 67;
    private String txtProductTitle = "";
    private String durationMonth = "";
    private int id;
    private int total;
    private static final String TAG = "Erros";
    private int width;
    private boolean isCurrentPlan;
    TextView tvt_descriptionCut,tvt_description3,tvt_description2,tvt_description1,tvt_description;
    private ActivityProductDetailsBinding binding;
    @Inject
    SessionManager sessionManager;
    User user;

    @Inject
    APIInterface apiInterface;
    private ProductResp productResp;
    List<String> excludeMobiles = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProductDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        user = sessionManager.getUser();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        width = metrics.widthPixels;
      //  setSupportActionBar(binding.toolbarProductDesc);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    //    binding.toolbarProductDesc.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
       // binding.tvPrice.setPaintFlags(binding.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id = extras.getInt("product_id");
            isCurrentPlan = extras.getBoolean("isCurrentPlan");
        } else {
            Toast.makeText(this, "Data not available", Toast.LENGTH_SHORT).show();
        }
        if (SupportClass.checkConnection(this)) {
            getProductDetail(id);
        } else {
            SupportClass.noInternetConnectionToast(this);
        }



      //  binding.toolbarLayout.setTitle(" ");
        binding.btnProductSubscribe.setOnClickListener(v -> {
            if (isValidate()) {
                if (SupportClass.checkConnection(ProductDetailActivity.this)) {
                    binding.btnProductSubscribe.setEnabled(false);
                    if (total == 0.0 || excludeMobiles.contains(user.getMobile())) {
                        subScribe("");
                    } else {
//                        launchPayUMoneyFlow();
                    }
                } else {
                    SupportClass.noInternetConnectionToast(ProductDetailActivity.this);
                }
            } else {
                startActivityForResult(new Intent(ProductDetailActivity.this, PersonalDetailActivity.class), PersonalDetailActivity.UPDATE_USER_REQ);
            }
        });
   /*     binding.appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    binding.toolbarLayout.setTitle(txtProductTitle);
                    isShow = true;
                } else if (isShow) {
                    binding.toolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        }); */
        binding.tvtBack.setOnClickListener(v -> {
          super.onBackPressed();


        });
        excludeMobiles.add("2123111111");
        excludeMobiles.add("2123222222");
        excludeMobiles.add("2123333333");
        excludeMobiles.add("2123444444");
        excludeMobiles.add("2123555555");
        excludeMobiles.add("2123666666");
        excludeMobiles.add("2123777777");
        excludeMobiles.add("9767667670");
        excludeMobiles.add("8484090486");
        excludeMobiles.add("8329835469");

    }

    private boolean isValidate() {
        if (Integer.parseInt(productResp.getProduct().getAmount()) == 0)
            return true;
        else if (TextUtils.isEmpty(user.getName()) || TextUtils.isEmpty(user.getEmail()) || user.getState() == null || user.getCity() == null) {
            Toast.makeText(this, getString(R.string.please_complete_profile_first), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        setFabMargin(80);
    }

    private void getProductDetail(int id) {
        binding.btnProductSubscribe.setEnabled(false);
        binding.btnProductSubscribe.setVisibility(View.GONE);
//        binding.icProgress.setVisibility(View.VISIBLE);
        apiInterface.getProductDetails(id).enqueue(new Callback<ProductResp>() {
            @Override
            public void onResponse(@NotNull Call<ProductResp> call, @NotNull Response<ProductResp> response) {
                binding.btnProductSubscribe.setEnabled(true);
                productResp = response.body();
                if (!productResp.getError()) {
                    binding.tvTitle.setText(productResp.getProduct().getPlanName());
                    // binding.tvDesc.setText(productResp.getProduct().getFeature());
                    total = Integer.parseInt(productResp.getProduct().getAmount()) - Integer.parseInt(productResp.getProduct().getDiscountAmount());
                    binding.tvPrice.setText(getText(R.string.rs) + "" + productResp.getProduct().getAmount() + ".00");
                    Log.d("description", "jjjjjjjjjjjjjjjjjjjjjjjjjjjj 1 jjjjjjjjjjjjjjjjjjjj" + productResp.getProduct().getDescription());
                    Log.d("description", "jjjjjjjjjjjjjjjjjjjjjjjjjjjjj 2 jjjjjjjjjjjjjjjjjjj" + productResp.getProduct().getDescription1());
                    Log.d("description", "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjj 3 jjjjjjjjjjjjjjjjjj" + productResp.getProduct().getDescription2());
                    Log.d("description", "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj 4 jjjjjjjjjjjjjjjjj" + productResp.getProduct().getDescription3());
                    binding.tvDiscPrice.setText(getText(R.string.rs) + "" + total + ".00");
                    Integer featureFlag = 5;
                    Integer counter = 0;

                    String[] aa = productResp.getProduct().getDescription3().split("\\r\\n|\\n|\\r");

                    Log.d("description", "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj aa  split   above jjjjjjjjjjjjjjjjj" + aa);
                    if (aa.length > 0){
                        for (int a = 0; a < aa.length; a++) {

                            Log.d("description", "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj aa  split   jjjjjjjjjjjjjjjjj" + a + " >>>>>>>" + aa[a]);


                            if (a == 0 && aa[a].toString().length()>1) {
                                binding.tvtDescription1.setVisibility(View.VISIBLE);
                                binding.tvtDescription1.setText(aa[a].toString());

                            }
                            if (a == 1) {
                                binding.tvtDescription2.setVisibility(View.VISIBLE);
                                binding.tvtDescription2.setText(aa[a]);

                            }
                            if (a == 2) {
                                binding.tvtDescription3.setVisibility(View.VISIBLE);
                                binding.tvtDescription3.setText(aa[a]);

                            }
                            if (a == 3) {
                                binding.tvtDescription4.setVisibility(View.VISIBLE);
                                binding.tvtDescription4.setText(aa[a]);

                            }

                            if (a == 4) {
                                binding.tvtDescription5.setVisibility(View.VISIBLE);
                                binding.tvtDescription5.setText(aa[a]);

                            }




                            //if (featureFlag == 5) {
                              //  break; // A unlabeled break is enough. You don't need a labeled break here.
                           // }
                        }

                }


                  /*  if (!TextUtils.isEmpty(productResp.getProduct().getDescription2().toString())) {
                        binding.tvtDescription1.setText(productResp.getProduct().getDescription2().toString());

                    } else {
                        binding.tvtDescription1.setVisibility(View.GONE);
                    }*/


              /*      if (!TextUtils.isEmpty(productResp.getProduct().getDescription4().toString())) {
                        binding.tvtDescriptioncut.setText(productResp.getProduct().getDescription4().toString());

                    } else {
                        binding.tvtDescriptioncut.setVisibility(View.GONE);
                    } */
                     counter = 0;

                    String[] aa1=productResp.getProduct().getDescription4().split("\\r\\n|\\n|\\r");

                    Log.d("description","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj featureFlag jjjjjjjjjjjjjjjjj"+featureFlag);
                    Log.d("description","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj featureFlag jjjjjjjjjjjjjjjjj"+aa1.length);
                        for (int a1 = 0; a1 < aa1.length; a1++) {

                  //        Log.d("description", "jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj aa  split   jjjjjjjjjjjjjjjjj" + a1 + " >>>>>>>" + aa1[counter]);

                            if (a1 == 0 && aa1[a1].toString().length()>1) {
                                binding.tvtDescriptioncut.setVisibility(View.VISIBLE);
                                binding.tvtDescriptioncut.setText(aa1[a1].toString());

                            }
                           if (a1 == 1 && aa1[a1].toString().length()>1) {
                                binding.tvtDescriptioncut1.setVisibility(View.VISIBLE);
                                binding.tvtDescriptioncut1.setText(aa1[a1]);

                            }
                            if (a1 == 2 && aa1[a1].toString().length()>1) {
                                binding.tvtDescriptioncut2.setVisibility(View.VISIBLE);
                                binding.tvtDescriptioncut2.setText(aa1[a1]);

                            }
                            if (a1 == 3 && aa1[a1].toString().length()>1) {
                                binding.tvtDescriptioncut3.setVisibility(View.VISIBLE);
                                binding.tvtDescriptioncut3.setText(aa1[a1]);

                            }
                            if (a1 == 4 && aa1[a1].toString().length()>1) {
                                binding.tvtDescriptioncut4.setVisibility(View.VISIBLE);
                                binding.tvtDescriptioncut4.setText(aa1[counter]);

                            }


                        }

                    Log.d("description","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj featureFlag last jjjjjjjjjjjjjjjjj"+featureFlag);

                    // binding.tvDiscPer.setText("(" + productResp.getProduct().getDiscountPer() + "% Off)");
                   // binding.details.tvProductDetailFeat.setText(productResp.getProduct().getFeature());
                   // binding.details.tvProductDetailBenf.setText(productResp.getProduct().getBenefit());
                   // Glide.with(ProductDetailActivity.this).load(productResp.getOthers().getDietPlanImagePath() + productResp.getProduct().getImage()).error(R.mipmap.ic_launcher).into(binding.ivProductImage);
                    if (Integer.parseInt(productResp.getProduct().getDiscountAmount()) == 0)
                        binding.tvPrice.setVisibility(View.GONE);
                    else
                        binding.tvPrice.setVisibility(View.VISIBLE);
                    if (total == 0)
                          // binding.clPriceStrip.setVisibility(View.GONE);
                    Log.d("dddd","dddd");
                } else {
                    for (int i = 0; i < productResp.getMessage().size(); i++) {
                        Toast.makeText(ProductDetailActivity.this, productResp.getMessage().get(i), Toast.LENGTH_LONG).show();
                    }
                }
                binding.btnProductSubscribe.setVisibility(View.VISIBLE);
//                binding.icProgress.setVisibility(View.GONE);
                if (isCurrentPlan) {
                    binding.btnProductSubscribe.setVisibility(View.GONE);
                } else {
                    binding.btnProductSubscribe.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProductResp> call, @NotNull Throwable t) {
                Log.e(TAG, "response Error: " + t.getMessage());
                binding.btnProductSubscribe.setEnabled(true);
//                binding.icProgress.setVisibility(View.GONE);
            }
        });

    }

    /**
     * This function prepares the data for payment and launches payumoney plug n play sdk
     */
//    private void launchPayUMoneyFlow() {
//        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();
//        payUmoneyConfig.setDoneButtonText(getString(R.string.pay_now));
//        payUmoneyConfig.setPayUmoneyActivityTitle(getString(R.string.payment));
//        payUmoneyConfig.disableExitConfirmation(false);
//        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();
//        double amount = 0;
//        try {
//            amount = total;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        String txnId = System.currentTimeMillis() + "";
//        String phone = user.getMobile().trim();
//       String productName = binding.tvTitle.getText().toString();
//        String firstName = user.getName() != null ? user.getName() : "dietQueen";
//        String email = user.getEmail() != null ? user.getEmail().trim() : "devkhetigaadi@gmail.com";
//        String udf1 = "";
//        String udf2 = "";
//        String udf3 = "";
//        String udf4 = "";
//        String udf5 = "";
//        String udf6 = "";
//        String udf7 = "";
//        String udf8 = "";
//        String udf9 = "";
//        String udf10 = "";
//        AppEnvironment appEnvironment = ((MyApplication) getApplication()).getAppEnvironment();
//        builder.setAmount(String.valueOf(amount))
//                .setTxnId(txnId)
//                .setPhone(phone)
//                .setProductName(productName)
//                .setFirstName(firstName)
//                .setEmail(email)
//                .setsUrl(appEnvironment.surl())
//                .setfUrl(appEnvironment.furl())
//                .setUdf1(udf1)
//                .setUdf2(udf2)
//                .setUdf3(udf3)
//                .setUdf4(udf4)
//                .setUdf5(udf5)
//                .setUdf6(udf6)
//                .setUdf7(udf7)
//                .setUdf8(udf8)
//                .setUdf9(udf9)
//                .setUdf10(udf10)
//                .setIsDebug(appEnvironment.debug())
//                .setKey(appEnvironment.merchant_Key())
//                .setMerchantId(appEnvironment.merchant_ID());
//
//
//        Log.d("PauuMoney Amount",String.valueOf(amount));
//        Log.d("PauuMoney txnId",String.valueOf(txnId));
//        Log.d("PauuMoney phone",String.valueOf(phone));
//        Log.d("PauuMoney firstName",String.valueOf(firstName));
//        Log.d("PauuMoney email",String.valueOf(email));
//        Log.d("Pauuy surl()",appEnvironment.surl());
//        Log.d("PauuMoney furl",appEnvironment.furl());
//        Log.d("PauuMoney key",appEnvironment.merchant_Key());
//        Log.d("PauuMoney MID",appEnvironment.merchant_ID());
//
//
//        try {
//            PayUmoneySdkInitializer.PaymentParam mPaymentParams = builder.build();
//            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);
//            PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, ProductDetailActivity.this, R.style.AppTheme_Pink, true);
//        } catch (Exception e) {
//            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
//            Log.e(TAG, "launchPayUMoneyFlow: " + e.getMessage());
//            binding.btnProductSubscribe.setEnabled(true);
//        }
//    }
//    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {
//        StringBuilder stringBuilder = new StringBuilder();
//        HashMap<String, String> params = paymentParam.getParams();
//        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
//        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");
//
//        AppEnvironment appEnvironment = ((MyApplication) getApplication()).getAppEnvironment();
//        stringBuilder.append(appEnvironment.salt());
//
//        String hash = hashCal(stringBuilder.toString());
//        paymentParam.setMerchantHash(hash);
//
//        return paymentParam;
//    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }

    private void subScribe(String payuResponse) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("diet_plan_id", id);
        params.put("payuResponse", payuResponse);
        apiInterface.subScribe(params).enqueue(new Callback<SubScribeResp>() {
            @Override
            public void onResponse(@NotNull Call<SubScribeResp> call, @NotNull Response<SubScribeResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (!response.body().getError()) {
                            SubScribe data = response.body().getData();
                            txtProductTitle = data.getDietPlan().getPlanName();
                            durationMonth = data.getDietPlan().getDurationInMonth();
                            Dialog dialog = new Dialog(ProductDetailActivity.this);
                            dialog.setContentView(R.layout.congratulation_screen);
                            dialog.setCanceledOnTouchOutside(false);
                            TextView tvMsg = dialog.findViewById(R.id.tv_detail);
                            Button btnOk = dialog.findViewById(R.id.btn_ok);
                            tvMsg.setText(Util.fromHtml("Your ".concat(durationMonth + " Months " + "&ldquo; " + txtProductTitle + " &rdquo;" + " Diet Plan Active Now!")));
                            btnOk.setOnClickListener(view -> {
                                dialog.dismiss();
                                Intent returnIntent = new Intent();
                                setResult(RESULT_OK, returnIntent);
                                finish();
                            });
                            dialog.show();
                            ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
                            InsetDrawable inset = new InsetDrawable(back, 40);
                            dialog.getWindow().setBackgroundDrawable(inset);
                            dialog.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
                        } else {
                            Toast.makeText(ProductDetailActivity.this, response.body().getMessage().get(0), Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(ProductDetailActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(@NotNull Call<SubScribeResp> call, @NotNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Toast.makeText(ProductDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT) {
//            binding.btnProductSubscribe.setEnabled(true);
//            if (resultCode == RESULT_OK && data != null) {
//                TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
//                        .INTENT_EXTRA_TRANSACTION_RESPONSE);
//
//                ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);
//
//                // Check which object is non-null
//                if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
//                    if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
//                        //Success Transaction
//                        subScribe(transactionResponse.getPayuResponse());
//                    } else {
//                        //Failure Transaction
//                        Toast.makeText(this, transactionResponse.getMessage().equals("") ? getString(R.string.payment_failed_please_try_again) : transactionResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//
//                    // Response from Payumoney
//                    String payuResponse = transactionResponse.getPayuResponse();
//                    Log.e(TAG, "onActivityResult: " + payuResponse);
//                    // Response from SURl and FURL
//                    String merchantResponse = transactionResponse.getTransactionDetails();
//                    Log.e(TAG, "onActivityResult: " + merchantResponse);
//
//                } else if (resultModel != null && resultModel.getError() != null) {
//                    Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
//                } else {
//                    Log.d(TAG, "Both objects are null!");
//                }
//            } else {
//                Toast.makeText(this, "Payment failed", Toast.LENGTH_SHORT).show();
//            }
//        }
//        else if (requestCode == PersonalDetailActivity.UPDATE_USER_REQ) {
//            if (resultCode == RESULT_OK) {
//                user = sessionManager.getUser();
//            }
//        }
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}