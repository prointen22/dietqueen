package com.livennew.latestdietqueen.fragments.home.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.fragments.home.model.Recipes;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.fragments.home.model.Testimonials;
import com.livennew.latestdietqueen.retrofit.APIInterface;

import java.util.List;

import javax.inject.Inject;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class HomeRecepiedetailsAdapter extends RecyclerView.Adapter<HomeRecepiedetailsAdapter.MyViewHolder> {

    private List<Recipes> list;
    private TestimonialListResp.Others others;
    private final Context context;
    private Listener listener;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    public HomeRecepiedetailsAdapter(Context context, List<Recipes> list) {
        this.context = context;
        this.list = list;
        this.others = others;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_recepie_nutri_details, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
      Recipes constituent = list.get(position);
        Log.d("testimonial",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+constituent.getConstituent_food_master_name());
        Log.d("testimonial",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+constituent.getConstituentimage());
        Log.d("testimonial",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+constituent.getConstituentUnit());


        Glide.with(context)
                .load(constituent.getRecepieImage())
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)

                .into(holder.img_recepie);

        holder.tvt_recepie_name.setText(constituent.getRecepieName());
        holder.tvt_recepie_description.setText(constituent.getRecepieDescription());

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(blog);
            }
        });*/





    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_recepie;
        TextView tvt_recepie_name,tvt_raw_consti_unit,tvt_recepie_description;
        CardView cvHomeBlog;
        private View itemView;
        CardView cv_testimonial;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img_recepie = itemView.findViewById(R.id.img_recepie);
            tvt_recepie_name = itemView.findViewById(R.id.tvt_recepie_name);
            tvt_recepie_description= itemView.findViewById(R.id.tvt_recepie_description);

        }
    }

    public interface Listener {
        void onItemClick(Testimonials id);
    }




}