package com.livennew.latestdietqueen.fragments.graph;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.jakewharton.threetenabp.AndroidThreeTen;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.AddDietMenuReplaceBinding;
import com.livennew.latestdietqueen.databinding.FragmentGraphBinding;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.DietItemMenuAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.MyTodayScheduleRecyclerViewAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.ReplaceItemAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Diet;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DoneParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.PlanParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Plans;
import com.livennew.latestdietqueen.model.FoodReplaceResp;
import com.livennew.latestdietqueen.model.MyResponse;
import com.livennew.latestdietqueen.model.ReplaceFoodParam;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.DateUtil;
import com.livennew.latestdietqueen.utils.EventDecorator;
import com.livennew.latestdietqueen.utils.SupportClass;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.jetbrains.annotations.NotNull;
import org.threeten.bp.LocalDate;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class GraphFragment extends Fragment implements OnDateSelectedListener {
    private static final String TAG = "GraphFragment";

    String DATE_FORMAT = "yyyy-MM-dd";

    private ArrayList<Diet> todayDietModelArrayList;
    private FragmentActivity activity;
    public MyTodayScheduleRecyclerViewAdapter myTodayScheduleRecyclerViewAdapter;
    private FragmentGraphBinding binding;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    User user;
    private ArrayList<Food> alReplaceItemModel;

    public GraphFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        user = sessionManager.getUser();
        AndroidThreeTen.init(activity);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentGraphBinding.inflate(inflater, container, false);

        binding.calendarView.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        /**Line chart view*/
        binding.calendarView.setSelectedDate(CalendarDay.today());
        binding.calendarView.setOnDateChangedListener(this);
        rvOnSetDatewiseDiet();
        getDiets(DateUtil.getTodayDate());
        return binding.getRoot();
    }

    private void getDiets(String date) {
        PlanParam planParam = new PlanParam(sessionManager.getDietEPlanId(), date);
        binding.progress.setVisibility(View.VISIBLE);
        binding.rvCalenderList.setVisibility(View.GONE);
        apiInterface.getDietPlans(planParam).enqueue(new Callback<Plans>() {
            @Override
            public void onResponse(@NonNull Call<Plans> call, @NonNull Response<Plans> response) {
                response.body().getOthers().setDate(planParam.getDate());
                myTodayScheduleRecyclerViewAdapter.updateData(response.body().getData(), response.body().getOthers(), DateUtil.isTodayDate(date));
                binding.progress.setVisibility(View.GONE);
                binding.rvCalenderList.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<Plans> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                binding.progress.setVisibility(View.GONE);
                binding.rvCalenderList.setVisibility(View.VISIBLE);
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    void setEvent(CalendarDay day) {
        List<CalendarDay> datesIndependent = new ArrayList<>();
        datesIndependent.add(day);
        setDecor(datesIndependent, R.drawable.g_independent);
    }

    void setDecor(List<CalendarDay> calendarDayList, int drawable) {
        binding.calendarView.addDecorators(new EventDecorator(activity, drawable, calendarDayList));
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        getDiets(DateUtil.getDate(date.getDate()));
        if (!DateUtil.getDate(date.getDate()).equalsIgnoreCase(DateUtil.getTodayDate())) {
            setEvent(CalendarDay.today());
        } else {
            binding.calendarView.removeDecorators();
            binding.calendarView.setSelectedDate(CalendarDay.today());
        }
    }

    /**
     * Today's diet recycler view with CenterZoomLayoutManager class implemented
     */
    private void rvOnSetDatewiseDiet() {
        todayDietModelArrayList = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        binding.rvCalenderList.setLayoutManager(layoutManager);
        // Scroll to the position we want to snap to
        myTodayScheduleRecyclerViewAdapter = new MyTodayScheduleRecyclerViewAdapter(activity, todayDietModelArrayList);
        binding.rvCalenderList.setAdapter(myTodayScheduleRecyclerViewAdapter);
        myTodayScheduleRecyclerViewAdapter.setListener(new MyTodayScheduleRecyclerViewAdapter.Listener() {
            @Override
            public void onDoneClick(int position, Diet todayDietMenuModel, Others others) {
                apiInterface.setDietDone(new DoneParam(todayDietMenuModel.getId(), others.getDate())).enqueue(new Callback<MyResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<MyResponse> call, @NonNull Response<MyResponse> response) {
                        if (response.body() != null && !response.body().getError()) {
                            myTodayScheduleRecyclerViewAdapter.getList().get(position).setIsDone(true);
                            myTodayScheduleRecyclerViewAdapter.notifyItemChanged(position);
                        } else {
                            if (response.body() != null) {
                                for (int i = 0; i < response.body().getMessage().size(); i++)
                                    Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                        Log.e(TAG, "onFailure: " + t.getMessage());
                        Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

        }, new DietItemMenuAdapter.Listener() {
            @Override
            public void onReplaceClick(int mainItemPosition, int subItemPosition, DietEPlanFood todayDietMenu, Others others) {
                if (SupportClass.checkConnection(activity)) {
                    onFoodReplace(mainItemPosition, subItemPosition, todayDietMenu, others);
                } else {
                    SupportClass.noInternetConnectionToast(activity);
                }
            }
        });
    }

    private void onFoodReplace(int mainItemPosition, int subItemPosition, DietEPlanFood todayDietMenu, Others others) {
        Dialog dialog = new Dialog(activity);
        AddDietMenuReplaceBinding binding = AddDietMenuReplaceBinding.inflate(getLayoutInflater());
        dialog.setContentView(binding.getRoot());//add_diet_menu_replace
        dialog.setCanceledOnTouchOutside(false);
        binding.tvReplcItmHeader.setText("Replace " + todayDietMenu.getFood().getFoodMasterName() + " \nwith below item");
        alReplaceItemModel = new ArrayList<>();
        LinearLayoutManager blogLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.rvReplaceItemList.setLayoutManager(blogLayoutManager);
        ReplaceItemAdapter replaceItemAdapter = new ReplaceItemAdapter(activity, alReplaceItemModel, food -> {
            if (food!=null) {
                apiInterface.replaceFood(new ReplaceFoodParam(todayDietMenu.getFood().getId(), food.getId(), todayDietMenu.getDietEDataId(), DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
                    @Override
                    public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
                        if (response.body() != null) {
                            if (response.body().getError()) {
                                for (int i = 0; i < response.body().getMessage().size(); i++)
                                    Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                            } else {
                                todayDietMenu.setFood(response.body().getData().getFood());
                                myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                            }
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                        dialog.dismiss();
                    }
                });
            } else {
                dialog.dismiss();
            }
        });
        binding.rvReplaceItemList.setAdapter(replaceItemAdapter);
        binding.progressBar.setVisibility(View.VISIBLE);
        binding.ivClose.setOnClickListener(view -> dialog.dismiss());
        /*binding.btnReplaceItemOk.setOnClickListener(v -> {
            if (replaceItemAdapter.getLastSelectedFood() != null) {
                apiInterface.replaceFood(new ReplaceFoodParam(todayDietMenu.getFood().getId(), replaceItemAdapter.getLastSelectedFood().getId(), todayDietMenu.getDietEDataId(), DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
                    @Override
                    public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
                        if (response.body() != null) {
                            if (response.body().getError()) {
                                for (int i = 0; i < response.body().getMessage().size(); i++)
                                    Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                            } else {
                                todayDietMenu.setFood(response.body().getData().getFood());
                                myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                            }
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                        dialog.dismiss();
                    }
                });
            } else {
                dialog.dismiss();
            }
        });*/
        apiInterface.getFoodsByGroup(new FoodGroupParam(todayDietMenu.getFood())).enqueue(new Callback<Foods>() {
            @Override
            public void onResponse(@NonNull Call<Foods> call, @NonNull Response<Foods> response) {
                if (response.body() != null) {
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        Iterator<Food> i = response.body().getData().iterator();
                        while (i.hasNext()) {
                            Food e = i.next();
                            if (e.getId().equals(todayDietMenu.getFood().getId())) {
                                i.remove();
                            }
                        }
                        if (response.body().getData().size() == 0) {
                            dialog.dismiss();
                            Toast.makeText(activity, getString(R.string.there_is_no_food_to_replace), Toast.LENGTH_SHORT).show();
                        }
                        replaceItemAdapter.addAll(response.body().getData(), response.body().getOthers());
                        binding.progressBar.setVisibility(View.GONE);
                    } else {
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Foods> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
    }


}
