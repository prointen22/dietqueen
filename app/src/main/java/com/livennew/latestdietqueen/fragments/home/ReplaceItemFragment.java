package com.livennew.latestdietqueen.fragments.home;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.threetenabp.AndroidThreeTen;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeReplaceDietAdapter;
import com.livennew.latestdietqueen.model.FoodReplaceResp;
import com.livennew.latestdietqueen.model.ReplaceFoodParam;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.DateUtil;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class ReplaceItemFragment extends Fragment implements View.OnClickListener {
    List<Food> listingvID = new ArrayList<>();
    List<Food> foodname = new ArrayList<>();
    List<Food> foodid = new ArrayList<>();
    List<Food> imgpathFood = new ArrayList<>();
    List<Food> isRecepie = new ArrayList<>();
EditText et_search;
String FoodGroupId="";
    List<String> listingReplaceID = new ArrayList<>();
    List<DietEPlanFood> listingimgpath = new ArrayList<>();
    RecyclerView rv_home_today_alter_diet;
    List<Food> a = new ArrayList<>();
    SharedPreferences mPrefsobj;
    String[]  rData=null;
    String lastId="";
    String dataEdatatId="";
    HomeReplaceDietAdapter recyclerAdapterVegi;
    String replaceData;
    private FragmentActivity activity;
    String Fodgid="",isRecipe="";
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    User user;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = getActivity();
//        user = sessionManager.getUser();
        AndroidThreeTen.init(activity);
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.replace_layout, null, false);

       // search();
        TextView tvtBack=view.findViewById(R.id.tvtBack);
        et_search=view.findViewById(R.id.et_search);
        tvtBack.setOnClickListener(this);
        tvtBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeFragment fragment = new HomeFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String foodId = bundle.getString("foodId");
            String replaceData = bundle.getString("replaceData");
            String foodMasterName = bundle.getString("foodMasterName");
            String id = bundle.getString("id");
            String imgPath = bundle.getString("imgpath");
             Fodgid = bundle.getString("Fodgid");
            isRecipe =bundle.getString("isRecipe");



           Log.d("ccccc nnnnnnnnnnnnnnnn","jjjjjjjjjjjjjjjjjj  isRecipe jjjjjjjjjjjjjjjjjjjjjjj"+">>>>>>>>>"+isRecipe);

            Log.d("ccccc nnnnnnnnnnnnnnnn","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"+">>>>>>>>>"+replaceData);
            String[] fgid=foodId.replaceAll("\\[", "").replaceAll("\\]", "").split(",") ;
            for (int i = 1; i <fgid.length;i++) {

                Log.d("foodGroupId : ","llllllllllllllllllllllllll cdata.length lllllllllllllllllllllllll"+fgid[i]);
                Food f = new Food();
                f.setFoodGroupId(fgid[i]);
                listingvID.add(f);
            }
            String[] fname=foodMasterName.replaceAll("\\[", "").replaceAll("\\]", "").split(",") ;
            for (int i = 1; i <fname.length;i++) {

                Log.d("foodName : ","llllllllllllllllllllllllll cdata.length lllllllllllllllllllllllll"+fname[i]);
                Food f = new Food();
                f.setFoodMasterName(fname[i]);
                foodname.add(f);
            }
            String[] rid=id.replaceAll("\\[", "").replaceAll("\\]", "").split(",") ;
            for (int i = 1; i <rid.length;i++) {

                System.out.println(rid[i]);
                Log.d("foodGroupId : ","llllllllllllllllllllllllll cdata.length lllllllllllllllllllllllll"+rid[i]);
                Food f = new Food();
                Integer y=Integer.parseInt(rid[i].toString().replaceAll(" ", ""));
                f.setId(y);
                foodid.add(f);
            }
            String[] img=imgPath.replaceAll("\\[", "").replaceAll("\\]", "").split(",") ;
            for (int i = 1; i <img.length;i++) {

                Log.d("foodGroupId : ","llllllllllllllllllllllllll cdata.length lllllllllllllllllllllllll"+img[i]);
                Food f = new Food();
                f.setImage(img[i]);
                imgpathFood.add(f);
            }

            String[] isRec=isRecipe.replaceAll("\\[", "").replaceAll("\\]", "").split(",") ;
            for (int i = 1; i <isRec.length;i++) {

                Log.d("foodGroupId : ","llllllllllllllllllllllllll isRec.length lllllllllllllllllllllllll"+isRec[i]);
                Food f = new Food();
                f.setIs_recipe(isRec[i]);
                isRecepie.add(f);
            }

/*
             //   String[] cdata = s.530data.replaceAll("\\[", "").replaceAll("\\]", "").split(",");
             Log.d("foodGroupId : ","llllllllllllllllllllllllll cdata.length lllllllllllllllllllllllll"+cdata.length);
            for (int i = 1; i < cdata.length;) {

                Log.d("ccccc nnnnnnnnnnnnnnnn",">>>>>>>>>"+i+">>>"+cdata[i]);
                String[] tt = cdata[i].split("##");
                // Log.d("foodGroupId : ","llllllllllllllllllllllllll tt  lllllllllllllllllllllllll"+tt);
                //Log.d("ccccc", i + ">>>>>>>>>" + tt[0]);
                //Log.d("ccccc", i + ">>>>>>>>>" + tt[1]);
                //Log.d("ccccc", i + ">>>>>>>>>" + tt[2]);
                //Log.d("ccccc", i + ">>>>>>>>>" + tt[3]);
                String fgid = tt[0];
                String fname = tt[1];
                String fimage = tt[2];
                Integer fid = Integer.parseInt(tt[3].toString());

                Food f = new Food();
                f.setFoodGroupId(fgid);
                f.setFoodMasterName(fname);
                f.setImage(fimage);
                f.setId(fid);
                listingvID.add(f);

                i=i+1;

                Log.d("ccccc nnnnnnnnnnnnnnnn",">>>>>>>>>"+i);
            }

 */

            rData=replaceData.split("##");
            Log.d("ccccc", ">>>>>>>>> rdata 0" + rData[0]);
            Log.d("ccccc  444 ",  ",,,,,,,,,,,  rdata 1" + rData[1]);

            lastId=rData[0];
            dataEdatatId=rData[1];
        }

        rv_home_today_alter_diet = view.findViewById(R.id.rv_home_today_alter_diet);
        HomeReplaceDietAdapter recyclerAdapterVegi = new HomeReplaceDietAdapter(getActivity(),listingvID,lastId,dataEdatatId,foodid,foodname,imgpathFood,isRecepie
        );

        //    RecyclerView.LayoutManager recyce = new GridLayoutManager(getActivity(),2);
        RecyclerView.LayoutManager recycev = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        //    recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));

        rv_home_today_alter_diet.setLayoutManager(recycev);
        rv_home_today_alter_diet.setItemAnimator(new DefaultItemAnimator());
        rv_home_today_alter_diet.setAdapter(recyclerAdapterVegi);


        recyclerAdapterVegi.setListener(new HomeReplaceDietAdapter.Listener() {


            @Override
            public void onItemClick(Integer id, Integer lastselectID, Integer dataEData) {

                Log.d("ddddddd","vvvvvvvvvvv lastselectID"+id+"."+lastselectID+"."+dataEData);
                replace(id,lastselectID,dataEData);


            }
        });

        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("yyyyy",">>>>>>>>>>>>>>>>>>>>>>>>>>> afterTextChange");
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                Log.d("yyyyy",">>>>>>>>>>>>>>>>>>>>>>>>>>> beforeTextChanged");

               // et_search.setHint("");
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                Log.d("yyyyy",">>>>>>>>>>>>>>>>>>>>>>>>>>> onTextChanged");

                if(s.length() > 0)
                    //field2.setText("");
                    search();
            }
        });

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        search();

        return view;
    }

    public void replace(Integer newFoodId,Integer oldFoodId,Integer dietEDataId) {
        ProgressDialog pd=new ProgressDialog(this.getActivity());

        pd.show();

    //    apiInterface.replaceFood(new ReplaceFoodParam(id, lastselectID,dataEData, DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
      //  int oldFoodId, int newFoodId, int dietEDataId, String date
Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt oldFoodId"+oldFoodId);
        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt newFoodId"+newFoodId);
        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt dietEDataId"+dietEDataId);
        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt DateUtil.getTodayDate()"+DateUtil.getTodayDate());

        apiInterface.replaceFood(new ReplaceFoodParam(oldFoodId,newFoodId,dietEDataId, DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
                @Override
            public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
                pd.dismiss();
                if (response.body() != null) {
                    if (response.body().getError()) {
                          for (int i = 0; i < response.body().getMessage().size(); i++)
                        //Toast.makeText(this, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt getFood"+ response.body().getMessage().get(i));
                        pgDialog(response.body().getMessage().get(0).toString(),"true");


                    } else {
                        //todayDietMenu.setFood(response.body().getData().getFood());
                        //  myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                        Log.d("kkkkkkkk","tttttttttttttttttttttttttttttttt getFood"+ response.body().getMessage());
                        pgDialog(response.body().getMessage().get(0).toString(),"false");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                pd.dismiss();
            }
        });






//current_iten_id,last_select_item_id,diet_eplan_id,time
  /*    apiInterface.replaceFood(new ReplaceFoodParam(todayDietMenu.getFood().getId(), replaceItemAdapter.getLastSelectedFood().getId(), todayDietMenu.getDietEDataId(), DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
          @Override
          public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
              if (response.body() != null) {
                  if (response.body().getError()) {
                      for (int i = 0; i < response.body().getMessage().size(); i++)
                          Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                  } else {
                      todayDietMenu.setFood(response.body().getData().getFood());
                      myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                  }
              }
              dialog.dismiss();
          }

          @Override
          public void onFailure(@NonNull Call call, @NonNull Throwable t) {
              dialog.dismiss();
          }
      });

  }*/
    }


    @Override
    public void onClick(View v) {

    }

    public  void pgDialog(String msg,String error){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertDialogView = inflater.inflate(R.layout.aler_dialog_item_replace, null);
        alertDialog.setView(alertDialogView);

        TextView textDialog = (TextView) alertDialogView.findViewById(R.id.replace_item);

        if(error.equals("true")){

            textDialog.setText(msg);
        }else {


            if (msg.equals("You Have Already Updated This Food")) {
                textDialog.setText("You Have Already Updated This Food");
            } else {
                textDialog.setText("You have replaced an Item");
            }
        }
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                HomeFragment fragment = new HomeFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        alertDialog.show();



    }

    public void search(){
        Log.d("dddd","mmmmmmmmmmmmmmmmmmmmm  search outer mmmmmmmmmm");

        //rv_home_today_alter_diet.removeAllViewsInLayout();
            listingvID.clear();
                foodid.clear();
            foodname.clear();
                imgpathFood.clear();
        isRecepie.clear();
        rv_home_today_alter_diet.removeAllViews();
   //     rv_home_today_alter_diet.notify();
        Log.d("dddd","mmmmmmmmmmmmmmmmmmmmm  search outer 1 FoodGroupId mmmmmmmmmm"+Fodgid);
        Food food = new Food();
        String abc=et_search.getText().toString();
        food.setFoodGroupId(Fodgid);
        food.setSearch(et_search.getText().toString());
        apiInterface.getFoodsByGroupSearch(new FoodGroupParam(food),abc).enqueue(new Callback<Foods>() {
            @Override
            public void onResponse(@NonNull Call<Foods> call, @NonNull retrofit2.Response<Foods> response) {
                if (response.body() != null) {
                    //    Glide.with(mContext).load(others.getDietPlanImagePath() + replaceItemModel.getImage()).into(holder.ivReplaceMenu);

                    if(response.body().getData().size()>0){
                        Integer size=response.body().getData().size();
                        for(int i=0;i<size;i++){

                            Food f=new Food();
                            Log.d("dddd","mmmmmmmmmmmmmmmmmmmmm  search response mmmmmmmmmm"+response.body().getData().get(i).getFoodGroupId());
                            Log.d("dddd","mmmmmmmmmmmmmmmmmmmmm  Data mmmmmmmmmm"+response.body().getData().get(i).getId()); //current item id


                            Log.d("dddd","mmmmmmmmmmmmmmmmmmmmm  Data mmmmmmmmmm"+response.body().getData().get(i).getFoodMasterName());

                            Log.d("dddd","mmmmmmmmmmmmmmmmmmmmm  Data mmmmmmmmmm"+response.body().getData().get(i).getFoodMasterName());

                            Log.d("dddd","mmmmmmmmmmmmmmmmmmmmm  Data others mmmmmmmmmm"+response.body().getOthers().getDietPlanImagePath());
                            Log.d("dddd","mmmmmmmmmmmmmmmmmmmmm  Data mmmmmmmmmm"+response.body().getData().get(i).getImage());

                            Food f1= new Food();
                            f1.setFoodGroupId(response.body().getData().get(i).getFoodGroupId());
                            listingvID.add(f1);

                            Food f2= new Food();
                            f2.setId(response.body().getData().get(i).getId());
                            foodid.add(f2);

                            Food f3= new Food();
                            f3.setFoodMasterName(response.body().getData().get(i).getFoodMasterName());
                            foodname.add(f3);

                            Food f4= new Food();
                            f4.setImage(response.body().getOthers().getDietPlanImagePath()+response.body().getData().get(i).getImage());
                            imgpathFood.add(f4);

                            Food f5= new Food();
                            f5.setIs_recipe(response.body().getData().get(i).getIs_recipe().toString());
                            isRecepie.add(f5);




                        //foodId.add(response.body().getData().get(i).getFoodGroupId());
                            //foodMasterName.add(response.body().getData().get(i).getFoodMasterName());
                            //imgpath.add(response.body().getOthers().getDietPlanImagePath()+response.body().getData().get(i).getImage());
                           // id.add(response.body().getData().get(i).getId().toString());

                            //current_iten_id,last_select_item_id,diet_eplan_id,time


                        }


                    }
                    Log.d("hhhhhhh","uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu");

 /*Intent i=new Intent(getActivity(),ReplaceItemsActivity.class);
 i.putExtra("s",s1.toString());
 i.putExtra("replaceData",replaceData);
 startActivity(i);*/
                    recyclerAdapterVegi = new HomeReplaceDietAdapter(getActivity(),listingvID,lastId,dataEdatatId,foodid,foodname,imgpathFood,isRecepie
                    );

                    //    RecyclerView.LayoutManager recyce = new GridLayoutManager(getActivity(),2);
                    RecyclerView.LayoutManager recycev = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
                    //    recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));

                    rv_home_today_alter_diet.setLayoutManager(recycev);
                    rv_home_today_alter_diet.setItemAnimator(new DefaultItemAnimator());
                    rv_home_today_alter_diet.setAdapter(null);
                    rv_home_today_alter_diet.setAdapter(recyclerAdapterVegi);


                    recyclerAdapterVegi.setListener(new HomeReplaceDietAdapter.Listener() {


                        @Override
                        public void onItemClick(Integer id, Integer lastselectID, Integer dataEData) {

                            Log.d("ddddddd","vvvvvvvvvvv lastselectID"+id+"."+lastselectID+"."+dataEData);
                            replace(id,lastselectID,dataEData);


                        }
                    });



                }
            }

            @Override
            public void onFailure(@NonNull Call<Foods> call, @NonNull Throwable t) {
                Log.e("jjj", "onFailure: " + t.getMessage());
            }
        });

    }
}
class DotsIndicatorDecoration extends RecyclerView.ItemDecoration {

    private final int indicatorHeight;
    private final int indicatorItemPadding;
    private final int radius;

    private final Paint inactivePaint = new Paint();
    private final Paint activePaint = new Paint();

    public DotsIndicatorDecoration(int radius, int padding, int indicatorHeight, @ColorInt int colorInactive, @ColorInt int colorActive) {
        float strokeWidth = Resources.getSystem().getDisplayMetrics().density * 1;
        this.radius = radius;
        inactivePaint.setStrokeCap(Paint.Cap.ROUND);
        inactivePaint.setStrokeWidth(strokeWidth);
        inactivePaint.setStyle(Paint.Style.FILL);
        inactivePaint.setAntiAlias(true);
        inactivePaint.setColor(colorInactive);

        activePaint.setStrokeCap(Paint.Cap.ROUND);
        activePaint.setStrokeWidth(strokeWidth);
        activePaint.setStyle(Paint.Style.FILL);
        activePaint.setAntiAlias(true);
        activePaint.setColor(colorActive);

        this.indicatorItemPadding = padding;
        this.indicatorHeight = indicatorHeight;
    }

    @Override
    public void onDrawOver(@NotNull Canvas c, @NotNull RecyclerView parent, @NotNull RecyclerView.State state) {
        super.onDrawOver(c, parent, state);

        final RecyclerView.Adapter adapter = parent.getAdapter();

        if (adapter == null) {
            return;
        }

        int itemCount = adapter.getItemCount();

        // center horizontally, calculate width and subtract half from center
        float totalLength = this.radius * 2 * itemCount;
        float paddingBetweenItems = Math.max(0, itemCount - 1) * indicatorItemPadding;
        float indicatorTotalWidth = totalLength + paddingBetweenItems;
        float indicatorStartX = (parent.getWidth() - indicatorTotalWidth) / 2f;

        // center vertically in the allotted space
        float indicatorPosY = parent.getHeight() - indicatorHeight / 2f;

        drawInactiveDots(c, indicatorStartX, indicatorPosY, itemCount);

        final int activePosition;

        if (parent.getLayoutManager() instanceof GridLayoutManager) {
            activePosition = ((GridLayoutManager) parent.getLayoutManager()).findFirstVisibleItemPosition();
        } else if (parent.getLayoutManager() instanceof LinearLayoutManager) {
            activePosition = ((LinearLayoutManager) parent.getLayoutManager()).findFirstVisibleItemPosition();
        } else {
            // not supported layout manager
            return;
        }

        if (activePosition == RecyclerView.NO_POSITION) {
            return;
        }

        // find offset of active page if the user is scrolling
        final View activeChild = parent.getLayoutManager().findViewByPosition(activePosition);
        if (activeChild == null) {
            return;
        }

        drawActiveDot(c, indicatorStartX, indicatorPosY, activePosition);
    }

    private void drawInactiveDots(Canvas c, float indicatorStartX, float indicatorPosY, int itemCount) {
        // width of item indicator including padding
        final float itemWidth = this.radius * 2 + indicatorItemPadding;

        float start = indicatorStartX + radius;
        for (int i = 0; i < itemCount; i++) {
            c.drawCircle(start, indicatorPosY, radius, inactivePaint);
            start += itemWidth;
        }
    }

    private void drawActiveDot(Canvas c, float indicatorStartX, float indicatorPosY,
                               int highlightPosition) {
        // width of item indicator including padding
        final float itemWidth = this.radius * 2 + indicatorItemPadding;
        float highlightStart = indicatorStartX + radius + itemWidth * highlightPosition;
        c.drawCircle(highlightStart, indicatorPosY, radius, activePaint);
    }

    @Override
    public void getItemOffsets(@NotNull Rect outRect, @NotNull View view, @NotNull RecyclerView parent, @NotNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = indicatorHeight;
    }
}