package com.livennew.latestdietqueen.fragments.products.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.diet_info.adapter.ScheduleItemMenuAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Diet;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;
import com.livennew.latestdietqueen.fragments.products.model.ProductDescription;
import com.livennew.latestdietqueen.utils.DateUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DescriptionAdapter extends RecyclerView.Adapter<DescriptionAdapter.ViewHolder> {
    private ArrayList<ProductDescription> mDescList;
    private final Context mContext;

    public DescriptionAdapter(Context mContext, ArrayList<ProductDescription> descList) {
        this.mContext = mContext;
        this.mDescList = descList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.plan_description_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ProductDescription descObj = mDescList.get(position);

        holder.mTvDesc.setText(descObj.getDescription());

        if (descObj.getIncluded()) {
            holder.mIv.setImageResource(R.drawable.ic_check);
            holder.mTvDesc.setTextColor(mContext.getColor(R.color.black));
        } else {
            holder.mIv.setImageResource(R.drawable.ic_close);
            holder.mTvDesc.setTextColor(mContext.getColor(R.color.light_gray));
        }

    }

    @Override
    public int getItemCount() {
        if (mDescList != null && !mDescList.isEmpty()) {
            return mDescList.size();
        } else {
            return 0;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTvDesc;
        ImageView mIv;

        public ViewHolder(View view) {
            super(view);
            mTvDesc = view.findViewById(R.id.tv_desc);
            mIv= view.findViewById(R.id.iv);
        }
    }
}
