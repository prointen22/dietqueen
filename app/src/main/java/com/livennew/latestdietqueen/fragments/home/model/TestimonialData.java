package com.livennew.latestdietqueen.fragments.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TestimonialData {
    @SerializedName("current_page")
    @Expose
    private int currentPage;

    @SerializedName("first_page_url")
    @Expose
    private String firstPageUrl;

    @SerializedName("from")
    @Expose
    private int from;

    @SerializedName("last_page")
    @Expose
    private int lastPage;

    @SerializedName("last_page_url")
    @Expose
    private String lastPageUrl;

    @SerializedName("next_page_url")
    @Expose
    private String nextPageUrl;

    @SerializedName("path")
    @Expose
    private String path;

    @SerializedName("per_page")
    @Expose
    private int perPage;

    @SerializedName("prev_page_url")
    @Expose
    private String prevPageUrl;

    @SerializedName("to")
    @Expose
    private int to;

    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("blog_exclusive")
    @Expose
    private int blog_exclusive;


    @SerializedName("id")
    @Expose
    private int id;


    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("author")
    @Expose
    private String author;


    @Expose
    @SerializedName("upload_file")
    private String upload_file;

    @Expose
    @SerializedName("created_at")
    private String created_at;




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUpload_file() {
        return upload_file;
    }

    public void setUpload_file(String upload_file) {
        this.upload_file = upload_file;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }




    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getFirstPageUrl() {
        return firstPageUrl;
    }

    public void setFirstPageUrl(String firstPageUrl) {
        this.firstPageUrl = firstPageUrl;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public String getLastPageUrl() {
        return lastPageUrl;
    }

    public void setLastPageUrl(String lastPageUrl) {
        this.lastPageUrl = lastPageUrl;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getPrevPageUrl() {
        return prevPageUrl;
    }

    public void setPrevPageUrl(String prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }



    @SerializedName("testimonials")
    @Expose
    private List<Testimonials> testimonials = null;

    public List<Testimonials> getTestimonials() {
        return testimonials;
    }

    public void setTestimonials(List<Testimonials> testimonials) {
        this.testimonials = testimonials;
    }
}
