package com.livennew.latestdietqueen.login_screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.FCMCallback;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.ActivitySocialMediaLoginOtpBinding;
import com.livennew.latestdietqueen.language.LanguageActivity;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.receiver.MySMSBroadcastReceiver;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.NumericKeyBoardTransformationMethod;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class SocialMediaLoginOTP extends BaseActivity {
    private String mobile = "";
    private String mobile_otp = "";
    private int counter = 40;
    ProgressDialog pDialog;
    private static final String TAG = "Erros";
    private boolean error;
    StringBuilder sb;
    boolean appendSeparator = false;
    @Inject
    SessionManager sessionManager;
    private User user;
    @Inject
    APIInterface apiInterface;
    private ActivitySocialMediaLoginOtpBinding binding;
    private MySMSBroadcastReceiver smsBroadcast;
    private MySMSBroadcastReceiver smsBroadcastReceivers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySocialMediaLoginOtpBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        user = sessionManager.getUser();
        init();
        binding.etMobile.setInputType(InputType.TYPE_CLASS_NUMBER);
        binding.etMobile.setTransformationMethod(new NumericKeyBoardTransformationMethod());
        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobile = binding.etMobile.getText().toString();
                if (SupportClass.checkConnection(SocialMediaLoginOTP.this)) {
                    if (!SupportClass.isValidPhoneNumber(mobile)) {
                        SupportClass.hideKeyboardFrom(SocialMediaLoginOTP.this);
                        loginWithOtp(mobile);
                        startSmsListener();
                        binding.etMobile.setEnabled(false);
                        binding.btnSubmit.setVisibility(View.GONE);
                        binding.etOtp.setVisibility(View.VISIBLE);
                        binding.btnVerifyOtp.setVisibility(View.VISIBLE);
                        binding.llTimer.setVisibility(View.VISIBLE);
                        binding.tvTimer.setVisibility(View.GONE);
                        binding.tvResendOtp.setVisibility(View.VISIBLE);
                    } else {
                        SupportClass.invalidMobileNumberToast(SocialMediaLoginOTP.this);
                    }
                } else {
                    SupportClass.noInternetConnectionToast(SocialMediaLoginOTP.this);
                }
            }
        });
        binding.tvChangeMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.etOtp.setText("");
                binding.etMobile.setEnabled(true);
                binding.btnSubmit.setVisibility(View.VISIBLE);
                binding.etOtp.setVisibility(View.GONE);
                binding.btnVerifyOtp.setVisibility(View.GONE);
                binding.llTimer.setVisibility(View.GONE);
            }
        });
        binding.btnVerifyOtp.setOnClickListener(e -> {
            if (!SupportClass.isValidPhoneNumber(mobile)) {
                checkOtp();
            } else {
                SupportClass.invalidMobileNumberToast(SocialMediaLoginOTP.this);
            }
        });

        binding.btnSubmit.setEnabled(false);
        binding.etMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.btnSubmit.setEnabled(charSequence.length() == 10);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.btnVerifyOtp.setEnabled(false);
        binding.etOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.btnVerifyOtp.setEnabled(charSequence.length() == 4);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.tvResendOtp.setOnClickListener(e -> {
            if (!SupportClass.isValidPhoneNumber(mobile)) {
                if (SupportClass.checkConnection(SocialMediaLoginOTP.this)) {
                    binding.tvResendOtp.setVisibility(View.GONE);
                    binding.tvTimer.setVisibility(View.VISIBLE);
                    counter = 40;
                    loginWithOtp(mobile);
                    new CountDownTimer(40000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            counter--;
                            binding.tvTimer.setText(getString(R.string.timer_messages, counter));
                        }

                        public void onFinish() {
                            binding.tvResendOtp.setVisibility(View.VISIBLE);
                            binding.tvTimer.setVisibility(View.GONE);
                        }
                    }.start();
                } else {
                    SupportClass.noInternetConnectionToast(this);
                }
            } else {
                SupportClass.invalidMobileNumberToast(this);
            }
        });
        smsBroadcast = new MySMSBroadcastReceiver();
        smsBroadcast.initOTPListener(new MySMSBroadcastReceiver.OTPReceiveListener() {
            @Override
            public void onOTPReceived(String otp) {
                Log.e(TAG, "onOTPReceived: " + otp);
//                inputCode.setText(otp);
            }

            @Override
            public void onOTPTimeOut() {

            }
        });
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        getApplicationContext().registerReceiver(smsBroadcast, intentFilter);
    }

    private void startSmsListener() {
        smsBroadcastReceivers = new MySMSBroadcastReceiver();
        smsBroadcastReceivers.initOTPListener(new MySMSBroadcastReceiver.OTPReceiveListener() {
            @Override
            public void onOTPReceived(String otp) {
                binding.etOtp.setText(otp);
                Log.d("Dddddd","ddddddddddddd onOTPReceived ddddddddddddddddddddd");
            }

            @Override
            public void onOTPTimeOut() {
                Log.d("Dddddd","ddddddddddddd Time out ddddddddddddddddddddd");

            }
        });
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        this.registerReceiver(smsBroadcastReceivers, intentFilter);
        SmsRetriever.getClient(this)
                .startSmsRetriever()
                .addOnSuccessListener(aVoid -> Log.e(TAG, "Successfully started retriever"))
                .addOnFailureListener(e -> Log.e(TAG, "Failed to start retriever"));
    }

    public void loginWithOtp(String mobile) {
        pDialog.setMessage("Loading...");
        pDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("id", user.getId() + "");
        params.put("mobile_no", mobile);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.USER_SOCIALMEDIA_OTP, new JSONObject(params),
                response -> {
                    Log.d(TAG, response.toString());
                    parseOtpSocialRegistrationResponse(response);
                    pDialog.dismiss();
                }, error -> {
                    Log.d(TAG, error.toString());
                    pDialog.dismiss();
                    if (error instanceof NoConnectionError)
                        Toast.makeText(SocialMediaLoginOTP.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                });
        RequestQueue MyRequestQueue = Volley.newRequestQueue(SocialMediaLoginOTP.this);
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyRequestQueue.add(jsonObjReq);
    }

    public void parseOtpSocialRegistrationResponse(JSONObject response) {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(response));
            error = jsonObject.getBoolean("error");
            JSONObject jsonData = jsonObject.getJSONObject("data");
            JSONArray jsonArray = jsonObject.getJSONArray("message");

            if (!error) {
                mobile_otp = jsonData.getString("otp");
                if (!jsonData.isNull("mobile_no") && jsonData.has("mobile_no")) {
                    sessionManager.getUser().setId(jsonData.getInt("id"));
                    sessionManager.getUser().setMobile(jsonData.getString("mobile_no"));
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                }
            } else {
                for (int i = 0; i < jsonArray.length(); i++) {
                    Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void checkOtp() {
        if (binding.etOtp.getText().toString().equalsIgnoreCase(mobile_otp)) {
            if (SupportClass.checkConnection(this)) {
                updateProfile(mobile);
            } else {
                SupportClass.noInternetConnectionToast(this);
            }
        } else {
            Toast.makeText(SocialMediaLoginOTP.this, R.string.please_check_otp, Toast.LENGTH_LONG).show();
            Log.i("msg", "check id");
        }
    }

    public void updateProfile(String mobile) {
        pDialog.setMessage("Loading...");
        pDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("id", sessionManager.getUser().getId() + "");
        params.put("mobile_no", mobile);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.USER_UPDATE_MOBILE, new JSONObject(params),
                response -> {
                    Log.d(TAG, response.toString());
                    parseUserUpdateProfileResponse(response);
                    pDialog.dismiss();
                }, error -> {
                    Log.d(TAG, error.toString());
                    pDialog.dismiss();
                    if (error instanceof NoConnectionError)
                        Toast.makeText(SocialMediaLoginOTP.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                headers.put("userId", sessionManager.getUser().getId() + "");
                return headers;
            }
        };
        RequestQueue MyRequestQueue = Volley.newRequestQueue(SocialMediaLoginOTP.this);
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        MyRequestQueue.add(jsonObjReq);
    }

    public void parseUserUpdateProfileResponse(JSONObject response) {
        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(response));
            int success = jsonObject.getInt("status_code");
            if (success == 200) {
                JSONObject jsonData = jsonObject.getJSONObject("data");
                // message = jsonObject.getString("message");
                error = jsonObject.getBoolean("error");
                JSONArray jsonArray = jsonObject.getJSONArray("message");
                sb.setLength(0);
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (appendSeparator)
                        sb.append('\n'); // a comma
                    appendSeparator = true;
                    sb.append(jsonArray.getString(i));
                }
                if (!error) {
                    FcmApi.registerFCMToken(apiInterface, sessionManager, new FCMCallback() {
                        @Override
                        public void onComplete() {
                            try {
                                sessionManager.getUser().setId(jsonData.getInt("id"));
                                mobile = jsonData.getString("mobile_no");
                                sessionManager.getUser().setMobile(mobile);
                                sessionManager.getUser().setToken(jsonData.getString("token"));
                                Intent intent = new Intent(SocialMediaLoginOTP.this, LanguageActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        pDialog = new ProgressDialog(this);
        sb = new StringBuilder();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SocialMediaLoginOTP.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (smsBroadcastReceivers != null) {
            smsBroadcastReceivers.initOTPListener(null);
            unregisterReceiver(smsBroadcastReceivers);
        }
    }
}
