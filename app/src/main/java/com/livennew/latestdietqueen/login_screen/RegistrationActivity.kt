package com.livennew.latestdietqueen.login_screen

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.livennew.latestdietqueen.R
import com.livennew.latestdietqueen.SessionManager
import com.livennew.latestdietqueen.databinding.ActivityRegistrationBinding
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceActivityNew
import com.livennew.latestdietqueen.model.User
import com.livennew.latestdietqueen.model.UserResp
import com.livennew.latestdietqueen.network_support.APIClient
import com.livennew.latestdietqueen.retrofit.APIInterface
import com.livennew.latestdietqueen.utils.SupportClass
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegistrationActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityRegistrationBinding
    var sessionManager: SessionManager? = null
    var apiInterface: APIInterface? = null
    private var user: User? = null
    private var pDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        mBinding = ActivityRegistrationBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        sessionManager = SessionManager(applicationContext)
        apiInterface = APIClient.getClient(applicationContext).create(APIInterface::class.java)

        if (intent != null) {
            if (intent.hasExtra("screen")) {
                if (intent.getStringExtra("screen") == "signup") {
                    user = intent.getParcelableExtra("user")
                }
            }
        } else {
            Log.i("check age", "Please check your age")
        }
    }

    override fun onResume() {
        super.onResume()
//        user = sessionManager?.user
        pDialog = ProgressDialog(this)
        init()
    }

    private fun init() {
//        mBinding.etName.im
        mBinding.etName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (mBinding.etName.text.toString().startsWith(" ")) {
                    mBinding.etName.error = "Name can't be started with space"
                } else if (mBinding.etName.text.toString().endsWith(" ")) {
                    mBinding.etName.error = "Name can't be ended with space"
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        mBinding.btnNext.setOnClickListener {
            if (isValidate()) {
                user?.name = mBinding.etName.text.toString()
                if (!TextUtils.isEmpty(mBinding.etEmail.text)) {
                    user?.email = mBinding.etEmail.text.toString()
                }
                updateUserDetails()


            }
        }

        mBinding.tvBack.setOnClickListener{ onBackPressed()}
    }

    private fun updateUserDetails() {
        mBinding.btnNext.setEnabled(false)
        pDialog!!.setMessage("Loading...")
        pDialog!!.setCancelable(false)
        pDialog!!.show()
//        mBinding..setVisibility(View.VISIBLE)
        apiInterface!!.updateUserWorkDetails(user).enqueue(object : Callback<UserResp?> {
            override fun onResponse(call: Call<UserResp?>, response: Response<UserResp?>) {
                mBinding.btnNext.setEnabled(true)
                val userResp = response.body()
                if (!userResp!!.error) {
                    Toast.makeText(this@RegistrationActivity, getMessageInSingleLine(userResp!!.message), Toast.LENGTH_SHORT).show()
                    try {
                        userResp!!.data.profileImage = userResp!!.others.userImageBase64Path + userResp!!.data.profileImage
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                    pDialog!!.dismiss()
                    sessionManager!!.user = userResp!!.data
                    infoSubmit()
//                    val returnIntent = Intent()
//                    setResult(RESULT_OK, returnIntent)
//                    finish()
//                    rlProgressBar.setVisibility(View.GONE)
                } else {
                    for (i in userResp!!.message.indices) {
                        Toast.makeText(this@RegistrationActivity, userResp!!.message[i], Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<UserResp?>, t: Throwable) {
                mBinding.btnNext.setEnabled(true)
            }
        })
    }

    private fun isValidate(): Boolean {
        if (TextUtils.isEmpty(mBinding.etName.text)) {
            mBinding.etName.error = "Name can't be empty"
            return false
        } else if (!TextUtils.isEmpty(mBinding.etName.text) && mBinding.etName.text.toString().startsWith(" ")) {
            mBinding.etName.error = "Name can't be started with space"
            return false
        } else if (!TextUtils.isEmpty(mBinding.etName.text) && mBinding.etName.text.toString().endsWith(" ")) {
            mBinding.etName.error = "Name can't be ended with space"
            return false
        } else if (!TextUtils.isEmpty(mBinding.etName.text) && (mBinding.etName.text.toString().trim().length < 3 || mBinding.etName.text.toString().trim().length > 100)) {
            mBinding.etName.error = "Name should be withing the range of 3 to 100 characters"
            return false
        }else if (TextUtils.isEmpty(mBinding.etEmail.text)) {
            mBinding.etName.error = "Email can't be empty"
            return false
        }else if (!TextUtils.isEmpty(mBinding.etEmail.text) && !isValidEmail(mBinding.etEmail.text.toString())) {
            mBinding.etEmail.error = "Please enter valid email!"
            return false
        }

        mBinding.etName.error = null
        mBinding.etEmail.error = null

        return true
    }

    private fun isValidEmail(email: CharSequence?): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun infoSubmit() {

        if (!SupportClass.checkConnection(this)) {
            SupportClass.noInternetConnectionToast(this)
            return
        }

        pDialog!!.setMessage("Loading...")
        pDialog!!.setCancelable(false)
        pDialog!!.show()
        removeLastTwoCharForHeight();
        removeLastTwoCharForHour();
        removeLastTwoCharForWeight()
        apiInterface!!.updateUser(user).enqueue(object : Callback<UserResp?> {
            override fun onResponse(call: Call<UserResp?>, response: Response<UserResp?>) {
                try {
                    val userResp = response.body()
                    pDialog!!.dismiss()
                    if (!userResp!!.error) {
//                        Toast.makeText(this@RegistrationActivity, getMessageInSingleLine(userResp.message), Toast.LENGTH_SHORT).show()
                        userResp.data.profileImage = userResp.others.userImageBase64Path + userResp.data.profileImage
                        sessionManager!!.user = userResp.data
                        val intent = Intent(this@RegistrationActivity, FitnessAdviceActivityNew::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left)
                    } else {
                        val dialog = AlertDialog.Builder(this@RegistrationActivity)
                        dialog.setMessage(getMessageInSingleLine(userResp.message))
                        dialog.setCancelable(false)
                        dialog.setPositiveButton(android.R.string.ok) { dialog1: DialogInterface, which: Int -> dialog1.dismiss() }.show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<UserResp?>, t: Throwable) {
                Toast.makeText(this@RegistrationActivity, t.localizedMessage, Toast.LENGTH_SHORT).show()
                pDialog!!.dismiss()
            }
        })
    }

    fun getMessageInSingleLine(message: List<String?>): StringBuilder? {
        val sb = StringBuilder()
        var appendSeparator = false
        for (i in message.indices) {
            if (appendSeparator) sb.append('\n') // a comma
            appendSeparator = true
            sb.append(message[i])
        }
        return sb
    }

    fun removeLastTwoCharForHeight() {
        user!!.userDetail.height =
            user!!.userDetail.height.substring(0, user!!.userDetail.height.length - 2).toDouble()
                .toString() + ""
    }



    fun removeLastTwoCharForHour() {
        user!!.userDetail.hoursOfSleep =
            user!!.userDetail.hoursOfSleep.substring(0, user!!.userDetail.hoursOfSleep.length - 2)
                .toDouble().toString() + ""
    }
    fun removeLastTwoCharForWeight() {
        if (!TextUtils.isEmpty(user!!.userDetail.weight)) {
            user!!.userDetail.weight =
                user!!.userDetail.weight.substring(0, user!!.userDetail.weight.length - 2)
                    .toDouble().toString() + ""
        }
    }
}