package com.livennew.latestdietqueen.login_screen;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.livennew.latestdietqueen.FCMCallback;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.model.FCMParam;
import com.livennew.latestdietqueen.model.MyResponse;
import com.livennew.latestdietqueen.retrofit.APIInterface;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FcmApi {
    private static final String TAG = "FcmApi";

    public static void registerFCMToken(APIInterface apiInterface, SessionManager sessionManager, FCMCallback fcmCallback) {
        String token = sessionManager.getFCMToken();
//        if (TextUtils.isEmpty(token)) {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }
                        // Get new FCM registration token
                        sessionManager.setPushToken(task.getResult());
                        callApi(apiInterface, task.getResult(), fcmCallback);
                    }
                });
       /* } else {
            callApi(apiInterface, token, fcmCallback);
        }*/
    }

    private static void callApi(APIInterface apiInterface, String token, FCMCallback fcmCallback) {
        Log.e(TAG, "token" + token);
        apiInterface.registerPushToken(new FCMParam(token)).enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(@NotNull Call<MyResponse> call, @NotNull Response<MyResponse> response) {
                if (fcmCallback != null)
                    fcmCallback.onComplete();
            }

            @Override
            public void onFailure(@NotNull Call<MyResponse> call, @NotNull Throwable t) {
                if (fcmCallback != null)
                    fcmCallback.onComplete();
            }
        });
    }
}
