package com.livennew.latestdietqueen.login_screen;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.livennew.latestdietqueen.BuildConfig;
import com.livennew.latestdietqueen.MyApplication;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.dashboard.DashboardActivity1;
import com.livennew.latestdietqueen.databinding.ActivityVerifyOtpBinding;
import com.livennew.latestdietqueen.fragments.products.ProductListActivity;
import com.livennew.latestdietqueen.info_screen.age_screen.AgeActivity;
import com.livennew.latestdietqueen.language.LanguageActivity;
import com.livennew.latestdietqueen.model.DietEPlanResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.receiver.MySMSBroadcastReceiver;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil;
import com.livennew.latestdietqueen.utils.SharedPreferenceUtils;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;
import retrofit2.Call;
import retrofit2.Callback;

@AndroidEntryPoint
public class VerifyOtpActivity extends AppCompatActivity {
    private static final String TAG = "Erros";
    private final CompositeDisposable disposable = new CompositeDisposable();
    private int counter = 40;
    private boolean error;
    private User user;
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    private ProgressDialog pDialog;
    private MySMSBroadcastReceiver smsBroadcastReceivers;
    private ActivityVerifyOtpBinding binding;
    private String mPIN = "";
    private static FirebaseAnalytics firebaseAnalytics;
    private SharedPreferenceUtils sharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        binding = ActivityVerifyOtpBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        sharedPreference = SharedPreferenceUtils.getInstance(this);
        Intent intent = getIntent();
        if (intent != null) {
            user = intent.getParcelableExtra("user");
            String otpSentMsg = getString(R.string.enter_4_digit_otp_that_was_sent) +" "+ user.getIsd_code()+" "+ user.getMobile();
            binding.phonetext.setText(otpSentMsg);
        }

        binding.button.setOnClickListener(e -> checkOtp());
        binding.btnChangeNo.setOnClickListener(e -> {
            Intent intent1 = new Intent(VerifyOtpActivity.this, LoginActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent1.putExtra("user", user);
            startActivity(intent1);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        });
        binding.llResendOtp.setOnClickListener(e -> {
            if (SupportClass.checkConnection(VerifyOtpActivity.this)) {
                binding.llResendOtp.setVisibility(View.GONE);
                binding.tvTimer.setVisibility(View.VISIBLE);
                counter = 40;
                getOtp(user.getMobile(),user.getIsd_code());
                new CountDownTimer(40000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        counter--;
                        binding.tvTimer.setText(getString(R.string.timer_messages, counter));
                    }

                    public void onFinish() {
                        binding.llResendOtp.setVisibility(View.VISIBLE);
                        binding.tvTimer.setVisibility(View.GONE);
                    }
                }.start();
            } else {
                SupportClass.noInternetConnectionToast(this);
            }
        });
        binding.llBack.setOnClickListener(e-> onBackPressed());
        getOtp(user.getMobile(),user.getIsd_code());
        startSmsListener();

        binding.otp.setPinViewEventListener((pinview, fromUser) -> {
            Context var10000 = (Context)VerifyOtpActivity.this;
            if (pinview == null) {
                Intrinsics.throwNpe();
            }

            mPIN = pinview.getValue();
            Toast.makeText(var10000, pinview.getValue(), Toast.LENGTH_SHORT).show();
        });
    }


    private void startSmsListener() {
        smsBroadcastReceivers = new MySMSBroadcastReceiver();
        smsBroadcastReceivers.initOTPListener(new MySMSBroadcastReceiver.OTPReceiveListener() {
            @Override
            public void onOTPReceived(String otp) {
                Log.e(TAG, "onOTPReceived: " + otp);
//                binding.otp.setText(otp);
                binding.otp.setValue(otp);
            }

            @Override
            public void onOTPTimeOut() {

            }
        });
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        this.registerReceiver(smsBroadcastReceivers, intentFilter);
        SmsRetriever.getClient(this)
                .startSmsRetriever()
                .addOnSuccessListener(aVoid -> Log.e(TAG, "Successfully started retriever"))
                .addOnFailureListener(e -> Log.e(TAG, "Failed to start retriever"));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(VerifyOtpActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }

    /**
     * Mobile number through Login OTP
     **/
    public void getOtp(String mobile_number, String isdCode) {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        User userParam = new User();
        userParam.setMobile(mobile_number);
        userParam.setIsd_code(isdCode);
        apiInterface.loginWithMobile(userParam).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NotNull Call<UserResp> call, @NotNull retrofit2.Response<UserResp> response) {
                pDialog.dismiss();
                if (!response.body().getError()) {
                    User user1 = response.body().getData();
                    user1.setProfileImage(response.body().getOthers().getUserImageBase64Path() + user1.getProfileImage());
                    user = user1;
                    int versionCode = BuildConfig.VERSION_CODE;
                    if(versionCode < Integer.parseInt(user.getAos_version())) {
                        Update();
                    }
//                    binding.otpNumber.setText("OTP: "+user1.getOtp());
                } else {
                    for (int i = 0; i < response.body().getMessage().size(); i++) {
                        Toast.makeText(VerifyOtpActivity.this, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserResp> call, @NotNull Throwable t) {
                pDialog.dismiss();
                new AlertDialog.Builder(VerifyOtpActivity.this, R.style.MyDatePicker).setMessage(
                        t.getMessage()).show();
            }
        });

    }

    public void checkOtp() {
        binding.button.setEnabled(false);
        if (mPIN.equalsIgnoreCase(user.getOtp())) {
            binding.button.setEnabled(false);
            sessionManager.setUser(user);
            apiInterface.getDietEPlan()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<DietEPlanResp>() {
                        @Override
                        public void onSuccess(@NonNull DietEPlanResp dietEPlanResp) {
                            binding.button.setEnabled(true);
                            if (dietEPlanResp.getError()) {
                                user.setDietPlan(null);
                            } else {
                                user.setDietEPlanId(dietEPlanResp.getData().getId());
                            }
//                            if(dietEPlanResp.getData().getIs_terms_accepted() == 0){
                                Log.d("dietqueen","terms pending = "+dietEPlanResp.getData().getIs_terms_accepted());
                                sessionManager.setIsTermsAccepted(dietEPlanResp.getData().getIs_terms_accepted());
//                            }
                            FcmApi.registerFCMToken(apiInterface, sessionManager, () -> {
                                Intent intent = null;
                                if (user.getUserDetail() == null || user.getUserDetail().getLanguage() == null) {
                                    intent = new Intent(getApplicationContext(), LanguageActivity.class);
//                                    intent.putExtra("achievment","signup");
                                } else if (user.getAge() == null || Integer.parseInt(user.getAge()) < 5
                                        || Integer.parseInt(user.getAge()) > 100 || user.getUserDetail().getHeight() == null
                                        || user.getUserDetail().getWeight() == null || user.getUserDetail().getHoursOfSleep() == null
                                        || user.getUserDetail().getRegion() == null || user.getUserDetail().getFood() == null
                                        || TextUtils.isEmpty(user.getUserDetail().getFood()) || user.getUserDetail().getProfession() == null) {
                                    MyApplication.getLocaleManager().setNewLocale(VerifyOtpActivity.this, user.getUserDetail().getLanguage());
                                    intent = new Intent(VerifyOtpActivity.this, AgeActivity.class);
                                } else if (user.getDietPlan() == null) {
                                    MyApplication.getLocaleManager().setNewLocale(VerifyOtpActivity.this, user.getUserDetail().getLanguage());
                                    intent = new Intent(VerifyOtpActivity.this, ProductListActivity.class);
                                    intent.putExtra("plan","expired");
                                    sessionManager.setNoPlan(0);
                                } else {
                                    MyApplication.getLocaleManager().setNewLocale(VerifyOtpActivity.this, user.getUserDetail().getLanguage());
                                    FireBaseAnalyticsUtil.loginEvent(FireBaseAnalyticsUtil.LOGIN,firebaseAnalytics);
                                    FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.DASHBOARD,firebaseAnalytics);
                                    intent = new Intent(VerifyOtpActivity.this, DashboardActivity1.class);
                                }
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.anim_slide_in_left,
                                        R.anim.anim_slide_out_left);
                            });
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Log.e(TAG, "onError: ", e);
                            Toast.makeText(VerifyOtpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            binding.button.setEnabled(true);
                        }
                    });

        } else {
            Toast.makeText(VerifyOtpActivity.this, "Please check otp", Toast.LENGTH_LONG).show();
            binding.button.setEnabled(true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (smsBroadcastReceivers != null) {
                smsBroadcastReceivers.initOTPListener(null);
                unregisterReceiver(smsBroadcastReceivers);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
    private void Update() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this, R.style.MyDatePicker);
        builder.setTitle("Update");
        builder.setMessage("Kindly Update App from Play store for latest changes.");
        builder.setPositiveButton("Update", (dialog, which) -> {
            // Do nothing but close the dialog
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
            finish();
            dialog.dismiss();
        });
        builder.setCancelable(false);
        androidx.appcompat.app.AlertDialog alert = builder.create();
        alert.show();
    }
}