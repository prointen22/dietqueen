package com.livennew.latestdietqueen.login_screen;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hbb20.CountryCodePicker;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.MyApplication;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.dashboard.DashboardActivity1;
import com.livennew.latestdietqueen.databinding.ActivityLoginBinding;
import com.livennew.latestdietqueen.fragments.products.ProductListActivity;
import com.livennew.latestdietqueen.info_screen.age_screen.AgeActivity;
import com.livennew.latestdietqueen.intro_screen.MainActivity;
import com.livennew.latestdietqueen.language.LanguageActivity;
import com.livennew.latestdietqueen.model.CountryCodes;
import com.livennew.latestdietqueen.model.DietEPlanResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.DateUtil;
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil;
import com.livennew.latestdietqueen.utils.NumericKeyBoardTransformationMethod;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;

@AndroidEntryPoint
public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "Erros";
    private final CompositeDisposable disposable = new CompositeDisposable();
    private EditText etSignIn;
    private Button btnSubmit, btnFB, btnGmail;
    private LinearLayout LLCountryCode;
    private String mobile_no = "", email = "", name = "";
    private ProgressDialog pDialog;
    private CallbackManager callbackManager;
    AccessToken access_token;
    LoginButton facebookLogin;
    SignInButton btnGmailLogin;
    private static final String EMAIL = "email";
    private static final String PUBLIC_PROFILE = "public_profile";
    private GoogleSignInClient googleApiClient;
    private static final int RC_SIGN_IN = 007;
    private static FirebaseAnalytics firebaseAnalytics;
    StringBuilder sb;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    private User user;
    private CountryCodePicker ccp;
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        callbackManager = CallbackManager.Factory.create();
        if (getIntent() != null) {
            user = getIntent().getParcelableExtra("user");
        }
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        init();

        etSignIn.setInputType(InputType.TYPE_CLASS_NUMBER);
        etSignIn.setTransformationMethod(new NumericKeyBoardTransformationMethod());
        btnSubmit.setVisibility(View.GONE);
        etSignIn.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() >=4 && s.length()<= 12) {
                    btnSubmit.setVisibility(View.VISIBLE);
                } else {
                    btnSubmit.setVisibility(View.GONE);
                }
            }
        });


        btnSubmit.setOnClickListener(e -> {
            if (SupportClass.checkConnection(LoginActivity.this)) {
                mobile_no = etSignIn.getText().toString();
                if (!mobile_no.isEmpty()) {
                    if (SupportClass.isValidPhoneNumber(mobile_no)) {
                        SupportClass.invalidMobileNumberToast(LoginActivity.this);
                    } else {
                        otpLogin(mobile_no);
                    }
                } else {
                    SupportClass.invalidMobileNumberToast(this);
                }
            } else {
                SupportClass.noInternetConnectionToast(this);
            }
        });
        facebookLogin.setOnClickListener(e -> {
            if (SupportClass.checkConnection(LoginActivity.this)) {
                facebookLoginSignUp();
            } else {
                SupportClass.noInternetConnectionToast(this);
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = GoogleSignIn.getClient(this, gso);


        btnGmail.setOnClickListener(v -> {
            if (SupportClass.checkConnection(LoginActivity.this)) {
                googleSignIn();
            } else {
                SupportClass.noInternetConnectionToast(LoginActivity.this);
            }
        });

    }

    public void googleSignIn() {
//        pDialog.show();
        Intent signInIntent = googleApiClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.e(TAG, "display name: " + acct.getDisplayName());
            name = acct.getDisplayName();
            email = acct.getEmail();
            sendSocialLogin(name, email);
            GoogleSignInOptions gso = new GoogleSignInOptions.
                    Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                    build();
            GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, gso);
            googleSignInClient.signOut();
            Log.e(TAG, "Name: " + name + ", email: " + email);
        } else {
            pDialog.dismiss();
        }
    }

    public void onClick(View v) {
        if (v == btnFB) {
            facebookLogin.performClick();
        }
    }

    public void init() {
        etSignIn = findViewById(R.id.sing_phone);
        btnGmail = findViewById(R.id.google_sign_in_button);
        btnSubmit = findViewById(R.id.otp_submit_button);
        btnFB = findViewById(R.id.sing_facebook);
        pDialog = new ProgressDialog(this);
        btnGmailLogin = findViewById(R.id.btn_sign_in);
        facebookLogin = findViewById(R.id.login_button);
        facebookLogin.setPermissions(Arrays.asList(PUBLIC_PROFILE, EMAIL));
        LLCountryCode = findViewById(R.id.ll_country_code);
        ccp = findViewById(R.id.ccp);
        ccp.setDefaultCountryUsingNameCode("IN");
//        ccp.registerCarrierNumberEditText(etSignIn);
//        ccp.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
//            @Override
//            public void onValidityChanged(boolean isValidNumber) {
//                // your code
//                Log.i("Number","isValidNumber = "+isValidNumber);
//                if(isValidNumber){
//                    btnSubmit.setVisibility(View.VISIBLE);
//                } else {
//                    btnSubmit.setVisibility(View.GONE);
//                }
//            }
//        });
        if (user != null) {
            etSignIn.setText(user.getMobile());
            btnSubmit.setVisibility(View.VISIBLE);
        }
        sb = new StringBuilder();
        LLCountryCode.setOnClickListener(v -> {
                String jsonFileString = DateUtil.getJsonFromAssets(getApplicationContext(), "countrycodes.json");
                Gson gson = new Gson();
                Type listUserType = new TypeToken<List<CountryCodes>>() { }.getType();

                List<CountryCodes> countries = gson.fromJson(jsonFileString, listUserType);
                for (int i = 0; i < countries.size(); i++) {
                    Log.i("data", "> Item " + i + "\n" + countries.get(i));
                }
                showCustomDialog(countries);
        });
    }

    private void showCustomDialog(List<CountryCodes> countries) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_countrylist);
        RecyclerView rvCountryItemList = (RecyclerView)dialog.findViewById(R.id.rv_country_item_list);
        LinearLayoutManager blogLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvCountryItemList.setLayoutManager(blogLayoutManager);
        CountryItemAdapter countryItemAdapter = new CountryItemAdapter(this, countries);
//        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
//        text.setText(msg);
//
//        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
//        dialogButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });

        dialog.show();
    }

    /**
     * Mobile number through Login OTP
     **/
    public void otpLogin(String mobile_number) {
        FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.VERIFYOTP,firebaseAnalytics);
        Intent intent = new Intent(LoginActivity.this, VerifyOtpActivity.class);
        User user1 = new User();
        user1.setMobile(mobile_number);
        Log.i("isd_code","country code = "+ccp.getSelectedCountryCode());
        user1.setIsd_code("+"+ccp.getSelectedCountryCode());
        intent.putExtra("user", user1);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public void facebookLoginSignUp() {
        facebookLogin.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("response Success", "Login");
                        access_token = loginResult.getAccessToken();
                        Log.d("response access_token", access_token.toString());
                        loadUserProfile(access_token);
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /**Facebook code*/
        if (requestCode == 64206) {
            Log.v("MyApp", getClass().toString() + " onActivityResult:If Facebook");
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
        /**Gmail code*/
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result != null) {
                handleSignInResult(result);
            }
        }
    }

    private void loadUserProfile(AccessToken newAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(newAccessToken, (object, response) -> {
            try {
                name = object.getString("name");
                if (object.has("email")) {
                    email = object.getString("email");
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.dontAnimate();

                    LoginManager.getInstance().logOut();
                    sendSocialLogin(name, email);
                } else {
                    CheckMobile();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                pDialog.dismiss();
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,first_name,last_name,link,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void sendSocialLogin(String name, String email) {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        apiInterface.loginWithSocial(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NotNull Call<UserResp> call, @NotNull retrofit2.Response<UserResp> response) {
                if (!response.body().getError()) {
                    response.body().getData().setProfileImage(response.body().getOthers().getUserImageBase64Path() + response.body().getData().getProfileImage());
                    sessionManager.setUser(response.body().getData());
                    getDietEPlan(response.body());

                } else {
                    Toast.makeText(LoginActivity.this, getMessageInSingleLine(response.body().getMessage()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserResp> call, @NotNull Throwable t) {
                if (pDialog != null && pDialog.isShowing())
                    pDialog.dismiss();
            }
        });

    }

    private void getDietEPlan(UserResp response) {
        apiInterface.getDietEPlan()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<DietEPlanResp>() {
                    @Override
                    public void onSuccess(@NonNull DietEPlanResp dietEPlanResp) {
                        if (pDialog != null && pDialog.isShowing())
                            pDialog.dismiss();
                        user = response.getData();
                        if (dietEPlanResp.getError()) {
                            user.setDietPlan(null);
                        } else {
                            user.setDietEPlanId(dietEPlanResp.getData().getId());
                        }
                        sessionManager.setUser(user);
                        FcmApi.registerFCMToken(apiInterface, sessionManager, () -> {
                            Intent intent = null;
                            if (user.getMobile() == null) {
                                CheckMobile();
                            } else if (user.getUserDetail() == null || user.getUserDetail().getLanguage() == null) {
                                intent = new Intent(getApplicationContext(), LanguageActivity.class);
                            } else if (user.getAge() == null || Integer.parseInt(user.getAge()) < 5
                                    || Integer.parseInt(user.getAge()) > 100 || user.getUserDetail().getHeight() == null
                                    || user.getUserDetail().getWeight() == null || user.getUserDetail().getHoursOfSleep() == null
                                    || user.getUserDetail().getRegion() == null || user.getUserDetail().getFood() == null
                                    || TextUtils.isEmpty(user.getUserDetail().getFood()) || user.getUserDetail().getProfession() == null) {
                                MyApplication.getLocaleManager().setNewLocale(LoginActivity.this, user.getUserDetail().getLanguage());
                                intent = new Intent(LoginActivity.this, AgeActivity.class);
                            } else if (user.getDietPlan() == null) {
                                MyApplication.getLocaleManager().setNewLocale(LoginActivity.this, user.getUserDetail().getLanguage());
                                intent = new Intent(LoginActivity.this, ProductListActivity.class);
                            } else {
                                MyApplication.getLocaleManager().setNewLocale(LoginActivity.this, user.getUserDetail().getLanguage());
                                intent = new Intent(LoginActivity.this, DashboardActivity1.class);
                            }
                            if (intent != null) {
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.anim_slide_in_left,
                                        R.anim.anim_slide_out_left);
                            }


                        });
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "onError: ", e);
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public void CheckMobile() {
        Intent intent = new Intent(LoginActivity.this, SocialMediaLoginOTP.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
    }


}
