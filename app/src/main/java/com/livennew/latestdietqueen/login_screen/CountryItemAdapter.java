package com.livennew.latestdietqueen.login_screen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.databinding.ReplaceMennuLayoutBinding;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.adapter.ReplaceItemAdapter;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.model.CountryCodes;

import java.util.List;

public class CountryItemAdapter extends RecyclerView.Adapter<CountryItemAdapter.ViewHolder> {
    private List<CountryCodes> countries;
    private Foods.FoodsOthers others;
    private Context mContext;

    public CountryItemAdapter(Context context, List<CountryCodes> countries) {
        this.mContext = context;
        this.countries = countries;
    }



    private int lastSelectedPosition = -1;



    @Override
    public CountryItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ReplaceMennuLayoutBinding view = ReplaceMennuLayoutBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new CountryItemAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CountryItemAdapter.ViewHolder holder, int position) {
        CountryCodes replaceItemModel = countries.get(position);
//        Glide.with(mContext).load(others.getDietPlanImagePath() + replaceItemModel.getImage()).into(holder.ivReplaceMenu);
        holder.tvReplaceMenuNm.setText(replaceItemModel.getName());
        if (lastSelectedPosition == position) {
//            holder.llReplaceMenuContent.setBackground(mContext.getResources().
//            getDrawable(R.drawable.button_background));
            holder.ivReplaceMenuCheck.setImageResource(R.drawable.ic_replace_round_selector);
//            replaceItem = replaceItemModel.getTvReplaceItem();
        } else {
//            holder.llReplaceMenuContent.setBackgroundResource(0);
            holder.ivReplaceMenuCheck.setImageResource(R.drawable.ic_replace_round);
//            holder.ivReplaceMenuCheck.setVisibility(View.GONE);
        }
        // rest of the code here

        holder.llReplaceMenuContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lastSelectedPosition == position) {
                    lastSelectedPosition = -1;
//                    notifyDataSetChanged();
                    return;
                }
                lastSelectedPosition = position;
//                notifyDataSetChanged();

            }
        });
    }

    @Override
    public int getItemCount() {
        if (countries != null && !countries.isEmpty()) {
            return countries.size();
        } else {
            return 0;
        }
    }

    public void addAll(List<CountryCodes> data, Foods.FoodsOthers others) {
        this.countries = data;
//        this.others = others;
//        notifyDataSetChanged();
    }

//    public Food getLastSelectedFood() {
//        if (lastSelectedPosition != -1) {
//            return alReplaceItemModel.get(lastSelectedPosition);
//        }
//        return null;
//    }

//    public void updateItem(Food lastSelectedFood, int position) {
//        alReplaceItemModel.set(position, lastSelectedFood);
//        lastSelectedPosition = -1;
//        notifyItemChanged(position);
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final public TextView tvReplaceMenuNm;
        final public LinearLayoutCompat llReplaceMenuContent;
        final public ImageView ivReplaceMenu, ivReplaceMenuCheck;

        public ViewHolder(ReplaceMennuLayoutBinding view) {
            super(view.getRoot());
            llReplaceMenuContent = view.llReplaceItem;
            ivReplaceMenu = view.ivReplaceMenu;
            tvReplaceMenuNm = view.tvReplaceMenuNm;
            ivReplaceMenuCheck = view.ivCheckReplaceMenu;
            //rvDietHorizantal = view.findViewById(R.id.rv_hori_diet_item);

        }
    }
}
