package com.livennew.latestdietqueen.language.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.model.Language;

import java.util.ArrayList;
import java.util.List;

public class LanguageListAdapter extends RecyclerView.Adapter<LanguageListAdapter.MyViewHolder> {
    private List<Language> alLanguage;
    private SessionManager sessionManager;
    private ClickLister clickLister;
    private Context context;
    private int lastSelectedPosition = -1;
    private ItemFilter mFilter = new ItemFilter();

    public LanguageListAdapter(Context context, List<Language> alLanguage, SessionManager sessionManager, ClickLister clickLister) {
        this.context = context;
        this.alLanguage = alLanguage;
        this.sessionManager = sessionManager;
        this.clickLister = clickLister;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_language_content, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.rbSelectLang.setChecked(lastSelectedPosition == position);
        holder.tvLanguage.setText(alLanguage.get(position).getName());
        holder.rbSelectLang.setChecked(alLanguage.get(position).isSelected());
        holder.itemView.setOnClickListener(view -> clickLister.onItemClick(alLanguage.get(position)));
        holder.rbSelectLang.setOnClickListener(view -> holder.itemView.callOnClick());

        if (alLanguage.get(position).isSelected()) {
            holder.mLLC.setBackgroundColor(ContextCompat.getColor(context, R.color.selector_color));
            holder.tvLanguage.setTextColor(ContextCompat.getColor(context, R.color.orange_theme));
        } else {
            holder.mLLC.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            holder.tvLanguage.setTextColor(ContextCompat.getColor(context, R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return alLanguage.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvLanguage;
        public RadioButton rbSelectLang;
        private View itemView;
        private String languageToLoad;
        private LinearLayoutCompat mLLC;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvLanguage = itemView.findViewById(R.id.tv_language);
            rbSelectLang = itemView.findViewById(R.id.rb_check_lang);
            mLLC = itemView.findViewById(R.id.llc_row);

            /*rbSelectLang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    switch (tvLanguage.getText().toString()) {
                        case "Hindi": {
                            languageToLoad = "hi";
                            break;
                        }
                        case "Marathi": {
                            languageToLoad = "mr";
                            break;
                        }
                        case "Gujarathi": {
                            languageToLoad = "gu";
                            break;
                        }
                        case "Kannada": {
                            languageToLoad = "kn";
                            break;
                        }
                        case "Telugu": {
                            languageToLoad = "te";
                            break;
                        }
                        case "Tamil": {
                            languageToLoad = "ta";
                            break;
                        }
                        case "Punjabi": {
                            languageToLoad = "pa";
                            break;
                        }
                        case "Bangali": {
                            languageToLoad = "bn";
                            break;
                        }
                        case "Malayalam": {
                            languageToLoad = "ml";
                            break;
                        }
                        case "mahato": {
                            languageToLoad = "ma";
                            break;
                        }
                        default: {
                            languageToLoad = "en";
                            break;
                        }
                    }
                    clickLister.onItemClick(alLanguage.get(getAdapterPosition()));
                }
            });*/
            this.itemView = itemView;
        }
    }

    public interface ClickLister {
        void onItemClick(Language language);
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            String filterString = constraint.toString().toLowerCase();

            if (TextUtils.isEmpty(filterString)) {
                results.values = alLanguage;
                results.count = alLanguage.size();

                return results;
            }

            final List<Language> list = alLanguage;

            int count = list.size();
            final ArrayList<Language> nlist = new ArrayList<>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getName();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressLint("NotifyDataSetChanged")
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            alLanguage = (ArrayList<Language>) results.values;
            notifyDataSetChanged();
        }

    }
}
