package com.livennew.latestdietqueen.language;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.livennew.latestdietqueen.MyApplication;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.dashboard.DashboardActivity1;
import com.livennew.latestdietqueen.databinding.ActivityLanguageBinding;
import com.livennew.latestdietqueen.info_screen.age_screen.AgeActivity;
import com.livennew.latestdietqueen.language.adapter.LanguageListAdapter;
import com.livennew.latestdietqueen.login_screen.LoginActivity;
import com.livennew.latestdietqueen.model.Language;
import com.livennew.latestdietqueen.model.LanguageCode;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class LanguageActivity extends AppCompatActivity {
    List<Language> alLanguage;
    private ProgressDialog pDialog;
    private static final String TAG = "Erros";
    LanguageListAdapter languageListAdapter;
    private Button btnReload;
    StringBuilder sb;
    boolean appendSeparator = false;
    private String screen;
    @Inject
    SessionManager sessionManager;
    User user;
    private ActivityLanguageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        binding = ActivityLanguageBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorAccent));
        user = sessionManager.getUser();

//        setSupportActionBar(binding.toolbarLanguage);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayShowHomeEnabled(true);
//        }
//        binding.toolbarLanguage.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);

        if (getIntent().getExtras() != null) {
            screen = getIntent().getExtras().getString("achievment");

            Log.d("Screen",">>>>>>>>>>>>>>>> >>>>>>>>>>>>>>>>>>>>>>");
        }
        init();
        if (SupportClass.checkConnection(this)) {
            languageList();
            btnReload.setVisibility(View.GONE);
            binding.rvLanguage.setVisibility(View.VISIBLE);
        } else {
            SupportClass.noInternetConnectionToast(this);
            btnReload.setVisibility(View.VISIBLE);
            binding.rvLanguage.setVisibility(View.GONE);
        }
        btnReload.setOnClickListener(v -> {
            if (SupportClass.checkConnection(LanguageActivity.this)) {
                languageList();
                btnReload.setVisibility(View.GONE);
                binding.rvLanguage.setVisibility(View.VISIBLE);
            } else {
                SupportClass.noInternetConnectionToast(LanguageActivity.this);
                btnReload.setVisibility(View.VISIBLE);
                binding.rvLanguage.setVisibility(View.GONE);
            }
        });
        binding.llBack.setOnClickListener(e-> onBackPressed());

        binding.etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (cs.length() ==0 ) {
                    setAdapter();
                    return;
                }
                languageListAdapter.getFilter().filter(cs);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }
        });
    }

    public void init() {
        alLanguage = new ArrayList<>();
        pDialog = new ProgressDialog(this);
        btnReload = findViewById(R.id.btn_reload);
        sb = new StringBuilder();
        if (screen==null){
          binding.llBack.setVisibility(View.GONE);
        }else{
            binding.llBack.setVisibility(View.VISIBLE);
            binding.btnNext.setText("Save");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (screen != null) {
            finish();
        } else {
            if (!isFinishing()) {
                callDialog();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (screen != null) {
            finish();
            super.onBackPressed();
        } else {
            if (!isFinishing()) {
                callDialog();
            }
        }

    }

    private void updateLanguage(Language language) {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", language.getId());
            JSONObject params = new JSONObject();
            params.put("language", jsonObject);
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.UPDATELANGUAGE, params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                pDialog.dismiss();
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONArray jsonArray = jObj.getJSONArray("message");
                        Toast.makeText(this, jsonArray.get(0).toString(), Toast.LENGTH_SHORT).show();
                        MyApplication.getLocaleManager().setNewLocale(this, language);
                        if (screen != null) {
                            if (screen.equals("achievment")){
                                finish();
                            }else {
                                Intent intent = new Intent(LanguageActivity.this, DashboardActivity1.class);
                                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                finish();
                            }
                        } else {
                            Intent intent = new Intent(LanguageActivity.this, AgeActivity.class);
                            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            finish();
                            overridePendingTransition(R.anim.anim_slide_in_left,
                                    R.anim.anim_slide_out_left);
                        }
                    } else {
                        JSONArray jsonArray = jObj.getJSONArray("message");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> {
                pDialog.dismiss();
                Log.e(TAG, "response Error: " + error.getMessage());
            }) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(LanguageActivity.this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void languageList() {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        HashMap<String, String> params = new HashMap<>();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, Api.MAIN_URL + Api.LANGUAGE_LIST, new JSONObject(params),
                response -> {
                    Log.d(TAG, response.toString());
                    languageListResponse(response);
                    pDialog.dismiss();
                }, error -> {
            Log.d(TAG, error.toString());
            pDialog.dismiss();
            Toast.makeText(LanguageActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            btnReload.setVisibility(View.VISIBLE);
            binding.rvLanguage.setVisibility(View.GONE);
        });
        RequestQueue MyRequestQueue = Volley.newRequestQueue(LanguageActivity.this);
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        MyRequestQueue.add(jsonObjReq);
    }

    private void languageListResponse(JSONObject response) {

        try {
            JSONObject jsonObject = new JSONObject(String.valueOf(response));
            int success = jsonObject.getInt("status_code");
            if (success == 200) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                boolean error = jsonObject.getBoolean("error");
                JSONArray jsonArray1 = jsonObject.getJSONArray("message");
                sb.setLength(0);
                for (int i = 0; i < jsonArray1.length(); i++) {
                    if (appendSeparator)
                        sb.append('\n'); // a comma
                    appendSeparator = true;
                    sb.append(jsonArray1.getString(i));
                }
                if (!error) {
                    user = sessionManager.getUser();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        Language languagePojo = new Language();
                        languagePojo.setId(jsonObject1.getInt("id"));
                        languagePojo.setName(jsonObject1.getString("name"));
                        languagePojo.setCode(getLanguageCode(languagePojo.getName()));
                        if (user.getUserDetail() != null && user.getUserDetail().getLanguage() != null && user.getUserDetail().getLanguage().getId() == languagePojo.getId())
                            languagePojo.setSelected(true);
                        alLanguage.add(languagePojo);
                    }
                    setAdapter();
                } else {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(LanguageActivity.this);
                    dialog.setMessage(sb);
                    dialog.setCancelable(false);
                    dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                }
            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    public static String getLanguageCode(String name) {
        switch (name) {
            case "Hindi":
                return LanguageCode.HINDI;
            case "Marathi":
                return LanguageCode.MARATHI;
            case "Gujarati":
                return LanguageCode.GUJARATI;
            case "Telugu":
                return LanguageCode.TELUGU;
            case "Tamil":
                return LanguageCode.TAMIL;
            case "Kannada":
                return LanguageCode.KANNADA;
            case "Punjabi":
                return LanguageCode.PUNJABI;
            case "Bangali":
                return LanguageCode.BENGALI;
            default:
                return LanguageCode.ENGLISH;
        }
    }

    private void callDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(LanguageActivity.this, R.style.MyDatePicker);
        dialog.setTitle(R.string.logout);
        dialog.setMessage(R.string.are_you_sure_you_want);
        dialog.setCancelable(false);
        dialog.setPositiveButton(R.string.yes, (dialog12, which) -> {
            dialog12.dismiss();
            Intent intent = new Intent(LanguageActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
            sessionManager.logout();
        });
        dialog.setNegativeButton(R.string.no, (dialog1, which) -> dialog1.dismiss()).show();
    }

    private void setAdapter() {
        languageListAdapter = new LanguageListAdapter(this, alLanguage, sessionManager, language -> {
            for (Language item : alLanguage) {
                item.setSelected(item.getId() == language.getId());
            }
            languageListAdapter.notifyDataSetChanged();
            updateLanguage(language);
        });
        binding.rvLanguage.setAdapter(languageListAdapter);
    }
}
