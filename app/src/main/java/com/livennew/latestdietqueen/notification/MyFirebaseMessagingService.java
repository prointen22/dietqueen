package com.livennew.latestdietqueen.notification;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.intro_screen.MainActivity;
import com.livennew.latestdietqueen.login_screen.FcmApi;
import com.livennew.latestdietqueen.notification.model.Notification;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.splash_screen.SplashActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "NotificationGenie";
    private static final int NOTIFICATION_REQUEST_CODE = 85;
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    private static int counter;
  Map<String,String> dataMap;

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
        Log.e(TAG, "-: " + token);
        Log.d("My mesage",">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FCM");
        if (sessionManager.isLoggedIn()) {
            FcmApi.registerFCMToken(apiInterface, sessionManager, () -> {
            });
        }
        sessionManager.setPushToken(token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d("My mesage",">>>>>>>>>>>>>>>>>>>>>>>>>>>>  On Message Received >>>>>>>>>>>>>>>>>>>>>>FCM"+remoteMessage.getData().size());
        Log.d("My mesage",">>>>>>>>>>>>>>>>>>>>>>>>>>>>  On Message Received >>>>>>>>>>>>>>>>>>>>>>FCM"+ remoteMessage.getData());


        if (remoteMessage.getData().size()>0) {

            dataMap = remoteMessage.getData();
//           if (foregroud){

                Log.d(TAG, "Message data payloa  " + remoteMessage.getData());

       // }else{
                Log.d(TAG, "Message data payloa  " + remoteMessage.getData());
                Notification notification = new Notification();
                notification.setImage(remoteMessage.getData().get("icon"));
                notification.setTitle(remoteMessage.getData().get("title"));
                notification.setDescription(remoteMessage.getData().get("body"));

            if (notification.getImage() != null) {
                    new sendNotification(getApplicationContext()).execute(notification);
                } else {

                Bitmap myLogo = ((BitmapDrawable)getResources().getDrawable(R.drawable.pushimage)).getBitmap();

                imageNotification(getApplicationContext(), notification, myLogo,remoteMessage.getData().get("title"));
                }
           // }
        }
    }

    private void imageNotification(Context mContext, Notification notification, Bitmap image,String title) {

        Log.d("My mesage",">>>>>>>>>>>>>>>>>>>>>>>>>>>> imageNotification>>>>>>>>>>>>>>>>>>>>>>FCM"+title);
        NotificationManager mNotificationManager =
                (NotificationManager) MyFirebaseMessagingService.this.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder nb = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("DietQueen_Notification", getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(notificationChannel);
            nb = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
            nb.setSmallIcon(R.mipmap.ic_launcher);

        } else {
            nb = new NotificationCompat.Builder(getApplicationContext());
            nb.setSmallIcon(R.mipmap.ic_launcher);
        }
      /*  NotificationCompat.BigPictureStyle notifystyle = new NotificationCompat.BigPictureStyle();
        notifystyle.bigPicture(bitmap);
        */
        //Bitmap bitmap = getBitmapFromURL(image_url);
        //********************************************************************************
        NotificationCompat.BigPictureStyle notifystyle = new NotificationCompat.BigPictureStyle();
        notifystyle.bigPicture(image);

        RemoteViews contentView = new RemoteViews(getApplication().getPackageName(), R.layout.custom_notification);
        contentView.setImageViewBitmap(R.id.image, image);
        contentView.setTextViewText(R.id.title, title);
//****************************************************************************************************
        nb.setColor(getResources().getColor(R.color.red));
        nb.setContentTitle(notification.getTitle());
        nb.setContentText((notification.getDescription()));
        nb.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),
                R.mipmap.ic_launcher));
        //*****************************************************************
        nb.setStyle(notifystyle);
        nb.setCustomBigContentView(contentView);
        //********************************************************
        Intent resultIntent = new Intent(mContext, SplashActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (image != null) {
            nb.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(image));
        }
        resultIntent.putExtra("notification", notification);
        TaskStackBuilder TSB = TaskStackBuilder.create(MyFirebaseMessagingService.this);
        TSB.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        TSB.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = TSB.getPendingIntent(NOTIFICATION_REQUEST_CODE, PendingIntent.FLAG_UPDATE_CURRENT);
        nb.setContentIntent(resultPendingIntent);
        nb.setAutoCancel(true);

        // mId allows you to update the notification later on.
     //   mNotificationManager.notify(++counter, nb.build());
        mNotificationManager.notify(0, nb.build());

    }

    private class sendNotification extends AsyncTask<Notification, Void, Notification> {


        Context context;

        public sendNotification(Context context) {
            super();
            this.context = context;
            Log.d("My mesage",">>>>>>>>>>>>>>>>>>>>>>>>>>>> sendNotification>>>>>>>>>>>>>>>>>>>>>>FCM");

        }

        @Override
        protected Notification doInBackground(Notification... params) {
            Log.d("My mesage",">>>>>>>>>>>>>>>>>>>>>>>>>>>> doInBackground>>>>>>>>>>>>>>>>>>>>>>FCM");

            InputStream in;
            try {
                URL url = new URL(params[0].getImage());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                params[0].setBitmapImage(myBitmap);
                return params[0];
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Notification result) {
            super.onPostExecute(result);
            Log.d("My mesage",">>>>>>>>>>>>>>>>>>>>>>>>>>>> onPostExecute>>>>>>>>>>>>>>>>>>>>>>FCM");

            try {
                imageNotification(context, result, result.getBitmapImage(),result.getTitle());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



}
