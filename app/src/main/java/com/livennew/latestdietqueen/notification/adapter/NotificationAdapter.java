package com.livennew.latestdietqueen.notification.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.NotificationContentBinding;
import com.livennew.latestdietqueen.notification.model.Notification;
import com.livennew.latestdietqueen.notification.model.NotificationResp;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private List<Notification> list;
    private NotificationResp.Others others;
    private Context context;
    private Listener listener;


    public NotificationAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(NotificationContentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Notification item = list.get(position);
        RequestOptions requestOptions = new RequestOptions();
        Glide.with(context)
                .load(others.getImagePath() + item.getImage())
                .error(R.mipmap.ic_launcher)
                .apply(requestOptions)
                .into(holder.binding.image);
        holder.binding.tvTitle.setText(item.getTitle());
        holder.binding.tvDesc.setText(Util.fromHtml(item.getDescription()));
        if (item.getUrl()!=null) {
            holder.binding.tvURL.setVisibility(View.VISIBLE);
//            holder.binding.tvURL.setText(Util.fromHtml("<u>External link - Click to open</u>"));
            holder.binding.tvURL.setText(Util.fromHtml("<u>Watch here - Click to open</u>"));
            holder.binding.tvURL.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getUrl()));
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("www.youtube.com"));
                context.startActivity(browserIntent);
            });
        }else{
            holder.binding.tvURL.setVisibility(View.GONE);
        }
//        holder.binding.cvNotification.setBackgroundColor(context.getResources().getColor(R.color.white));
//        holder.binding.tvURL.setTextColor(context.getResources().getColor(R.color.colorAccent));
//        if(position %2 == 1)
//        {
//            holder.binding.cvNotification.setBackgroundColor(context.getResources().getColor(R.color.gray_1000));
//            holder.binding.tvURL.setTextColor(context.getResources().getColor(R.color.purple_color1));
//        }else{
//            holder.binding.cvNotification.setBackgroundColor(context.getResources().getColor(R.color.white));
//            holder.binding.tvURL.setTextColor(context.getResources().getColor(R.color.colorAccent));
//        }
        if (listener != null) {
            holder.itemView.setOnClickListener(view -> listener.onItemClick(item, others));
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateData(List<Notification> notification, NotificationResp.Others others) {
        this.list = notification;
        this.others = others;
        notifyDataSetChanged();
    }

    public void setOnClickListener(Listener listener) {

        this.listener = listener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private final NotificationContentBinding binding;

        public MyViewHolder(@NonNull NotificationContentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface Listener {
        void onItemClick(Notification notification, NotificationResp.Others others);
    }
}
