package com.livennew.latestdietqueen.notification.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

public class NotificationResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private NotificationData data;

    @SerializedName("others")
    @Expose
    private Others others;

    public NotificationData getData() {
        return data;
    }

    public void setData(NotificationData data) {
        this.data = data;
    }

    public Others getOthers() {
        return others;
    }

    public void setOthers(Others others) {
        this.others = others;
    }

    public static class Others {
        @SerializedName("notificationImagePath")
        @Expose
        private String imagePath;

        public String getImagePath() {
            return imagePath;
        }

        public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
        }
    }
}
