package com.livennew.latestdietqueen.notification;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.ActivityNotificationDetailsBinding;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.notification.model.Notification;
import com.livennew.latestdietqueen.utils.TopBarUtil;

import javax.inject.Inject;

public class NotificationDetailsActivity extends FabActivity {

    private ActivityNotificationDetailsBinding binding;
    @Inject
    SessionManager sessionManager;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNotificationDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUser();
//        setSupportActionBar(binding.toolbar);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayShowHomeEnabled(true);
//            getSupportActionBar().setTitle(R.string.notifications);
//        }
//        binding.toolbar.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        init(getIntent().getParcelableExtra("notification"), getIntent().getStringExtra("others"));
        ImageView mIvNotification = findViewById(R.id.iv_notification);
        ImageView mIvDoctor = findViewById(R.id.iv_doctor);
        ImageView mIvSideMenu = findViewById(R.id.iv_menu);
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void init(Notification notification, String others) {
        if (notification.getImage() != null)
            Glide.with(this)
                    .load(others != null ? others + notification.getImage() : notification.getImage())
                    .into(binding.image);
        binding.tvTitle.setText(Util.fromHtml(notification.getTitle()));
        binding.tvDesc.setText(Util.fromHtml(notification.getDescription()));

        if (notification.getUrl()!=null) {
            binding.tvURL.setVisibility(View.VISIBLE);
//            binding.tvURL.setText(Util.fromHtml("<u>External link - Click to open</u>"));
            binding.tvURL.setText(Util.fromHtml("<u>Watch here - Click to open</u>"));
            binding.tvURL.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(notification.getUrl()));
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("www.youtube.com"));
                startActivity(browserIntent);
            });
        }else{
            binding.tvURL.setVisibility(View.GONE);
        }
        binding.tvURL.setTextColor(getResources().getColor(R.color.colorAccent));
    }
}