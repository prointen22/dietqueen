package com.livennew.latestdietqueen.notification;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.notification.adapter.NotificationAdapter;
import com.livennew.latestdietqueen.notification.model.Notification;
import com.livennew.latestdietqueen.notification.model.NotificationResp;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.SupportClass;
import com.livennew.latestdietqueen.utils.TopBarUtil;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class NotificationListActivity extends BaseActivity {
//    private Toolbar toolbarNotificationList;
    private RecyclerView rvNotificationList;
    private ProgressDialog pDialog;
    private NotificationAdapter notificationAdapter;
    private Button btnNotificationReload;
    private ProgressBar progressBar, progressBarPag;
    private LinearLayoutManager recyclerLayoutManager;
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    User user;
    private TextView tvNoRecords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUser();
        init();
//        setSupportActionBar(toolbarNotificationList);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.notifications);
        }
//        toolbarNotificationList.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        notificationAdapter = new NotificationAdapter(NotificationListActivity.this);
        recyclerLayoutManager = new LinearLayoutManager(NotificationListActivity.this);
        rvNotificationList.setLayoutManager(recyclerLayoutManager);
        rvNotificationList.setAdapter(notificationAdapter);
        ImageView mIvNotification = findViewById(R.id.iv_notification);
        ImageView mIvDoctor = findViewById(R.id.iv_doctor);
        ImageView mIvSideMenu = findViewById(R.id.iv_menu);
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager);
        notificationAdapter.setOnClickListener((notification, others) -> {
            Intent intent = new Intent(this, NotificationDetailsActivity.class);
            intent.putExtra("notification", notification);
            intent.putExtra("others", others.getImagePath());
            startActivity(intent);
        });
        if (SupportClass.checkConnection(this)) {
            notificationList();
            btnNotificationReload.setVisibility(View.GONE);
            rvNotificationList.setVisibility(View.VISIBLE);
        } else {
            SupportClass.noInternetConnectionToast(this);
            btnNotificationReload.setVisibility(View.VISIBLE);
            rvNotificationList.setVisibility(View.GONE);
        }

        btnNotificationReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SupportClass.checkConnection(NotificationListActivity.this)) {
                    notificationList();
                    btnNotificationReload.setVisibility(View.GONE);
                    rvNotificationList.setVisibility(View.VISIBLE);
                } else {
                    SupportClass.noInternetConnectionToast(NotificationListActivity.this);
                    btnNotificationReload.setVisibility(View.VISIBLE);
                    rvNotificationList.setVisibility(View.GONE);
                }
            }
        });
        if (getIntent().hasExtra("notification")) {
            Intent intent = new Intent(this, NotificationDetailsActivity.class);
            intent.putExtra("notification", (Notification) getIntent().getParcelableExtra("notification"));
            startActivity(intent);
        }
    }

    private void init() {
//        toolbarNotificationList = findViewById(R.id.toolbar_notification);
        pDialog = new ProgressDialog(this);
        rvNotificationList = findViewById(R.id.rv_notification_list);
        btnNotificationReload = findViewById(R.id.btn_notification_reload);
        progressBar = findViewById(R.id.progress_bar);
        progressBarPag = findViewById(R.id.progress_bar_pag);
        tvNoRecords = (TextView) findViewById(R.id.tvNoRecords);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Blog data list declared
     * passing
     */
    public void notificationList() {
        progressBar.setVisibility(View.VISIBLE);
        rvNotificationList.setVisibility(View.GONE);
        apiInterface.getNotifications().enqueue(new Callback<NotificationResp>() {
            @Override
            public void onResponse(@NotNull Call<NotificationResp> call, @NotNull Response<NotificationResp> response) {
                progressBar.setVisibility(View.GONE);
                rvNotificationList.setVisibility(View.VISIBLE);
                if (!response.body().getError()) {
                    notificationAdapter.updateData(response.body().getData().getNotification(), response.body().getOthers());
                    if (response.body().getData().getNotification().size() == 0)
                        tvNoRecords.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(NotificationListActivity.this, response.body().getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<NotificationResp> call, @NotNull Throwable t) {
                Toast.makeText(NotificationListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
