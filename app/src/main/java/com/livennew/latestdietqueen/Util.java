package com.livennew.latestdietqueen;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;

import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.MealsDataItem;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Others;

import java.util.List;

public class Util {
    public static long calculateQty(DietEPlanFood todayDietMenu, Others others) {
        if (todayDietMenu.getFood().getEnergyCalorie() == null)
            return 0;
        if (todayDietMenu.getFood().getEnergyCalorie().getCalories() == 0 || todayDietMenu.getFood().getEnergyCalorie().getCalories() == 0.0)
            return 0;
        try {
            double result = (others.getRequiredEnergy() * todayDietMenu.getPercentage() / 100);
            long qty = Math.round((result * todayDietMenu.getFood().getQuantity()) / todayDietMenu.getFood().getEnergyCalorie().getCalories());
            return qty;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static long calculateQty(MealsDataItem todayDietMenu, Others others) {
        if (todayDietMenu.getFood().getEnergyCalorie() == null)
            return 0;
        if (todayDietMenu.getFood().getEnergyCalorie().getCalories() == 0 || todayDietMenu.getFood().getEnergyCalorie().getCalories() == 0.0)
            return 0;
        try {
            double result = (others.getRequiredEnergy() * todayDietMenu.getPercentage() / 100);
            long qty = Math.round((result * todayDietMenu.getFood().getQuantity()) / todayDietMenu.getFood().getEnergyCalorie().getCalories());
            return qty;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (html == null) {
            // return an empty spannable if the html is null
            return new SpannableString("");
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // FROM_HTML_MODE_LEGACY is the behaviour that was used for versions below android N
            // we are using this flag to give a consistent behaviour
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static int convertDipToPixels(Context context, float dips) {
        return (int) (dips * context.getResources().getDisplayMetrics().density + 0.5f);
    }
    public static boolean isAtLeastVersion(int version) {
        return Build.VERSION.SDK_INT >= version;
    }

    public static StringBuilder getMessageInSingleLine(List<String> message) {
        boolean appendSeparator = false;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < message.size(); i++) {
            if (appendSeparator)
                sb.append('\n'); // a comma
            appendSeparator = true;
            sb.append(message.get(i));
        }
        return sb;
    }
}
