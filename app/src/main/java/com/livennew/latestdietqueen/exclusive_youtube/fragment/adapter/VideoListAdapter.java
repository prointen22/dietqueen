package com.livennew.latestdietqueen.exclusive_youtube.fragment.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.VideoListItemBinding;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {
    private final Context context;
    private List<Video> list = new ArrayList<>();
    private Listener listener;

    public VideoListAdapter(Context context) {
        this.context = context;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@Nullable ViewGroup parent, int viewType) {
        return new ViewHolder(VideoListItemBinding.inflate(LayoutInflater.from(context), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Video item = list.get(position);

            ((ViewHolder) holder).binding.tvName.setText(Util.fromHtml(item.getSnippet().getTitle()));
        Glide.with(context).load(item.getSnippet().getThumbnails().getMedium().getUrl()).into(((ViewHolder) holder).binding.ivVideo);
        ((ViewHolder) holder).binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(list.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void addAll(List<Video> list) {
        if (this.list == null)
            this.list = new ArrayList<>();
        for (Video item : list) {
            if (item.getId().getVideoId() != null)
                this.list.add(item);
        }
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private com.livennew.latestdietqueen.databinding.VideoListItemBinding binding;

        public ViewHolder(@NotNull VideoListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface Listener {
        void onItemClick(Video item, int position);
    }
}

