package com.livennew.latestdietqueen.exclusive_youtube;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTestimonialAdapter;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.fragments.home.model.Testimonials;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.retrofit.APIInterface;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;

@AndroidEntryPoint
public class testDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = testDetailActivity.class.getSimpleName();
    private Video item;
    private String tagGetVideoDetails = "getVideoDetails";
    private Testimonials binding;
    Toolbar toolbar;
    @Inject
    SessionManager sessionManager;
    private LinearLayoutCompat llFooter;
    @Inject
    APIInterface apiInterface;
    private User user;
    RecyclerView rv_home_video;
//    RecyclerViewAdapter recyclerViewAdapter;
    ProgressBar progress_bar;
    String testimonial_id;
    TextView tvt_title,tvt_author,tvt_description;
    TextView tvt_back;
    ImageView img_testimonialImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testimonial_details);


        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Testimonials");


        tvt_title=(TextView) findViewById(R.id.tvt_title);
        tvt_author=(TextView) findViewById(R.id.tvt_author);
        tvt_back=(TextView) findViewById(R.id.tvt_back);
        tvt_description=(TextView) findViewById(R.id.tvt_description);
        img_testimonialImage=(ImageView) findViewById(R.id.img_testimonialImage);
        tvt_back.setOnClickListener(this);
        Intent intent = getIntent();
        if(intent!=null){

            String testimonial_id = getIntent().getStringExtra("testimonial_id");
            Log.d("mmm", "/////////////////////////////////////////////////// testimonial_id 1"+testimonial_id);

            VideoList(testimonial_id);
        }


    }
    private void VideoList(String id) {
        Map<String, Integer> params = new HashMap<String, Integer>();
        params.put("id",Integer.parseInt(id));
        //params.put("perPage", 3);

        Log.d("mmm", "/////////////////////////////////////////////////// testimonial 1");
        apiInterface.getTestimonials(params).enqueue(new Callback<TestimonialListResp>() {
            @Override
            public void onResponse(@NotNull Call<TestimonialListResp> call, @NotNull retrofit2.Response<TestimonialListResp> response) {
                TestimonialListResp blogResp1 = response.body();
                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2" + blogResp1.getError());
                if (!blogResp1.getError()) {

                    // binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                    Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2 " + blogResp1.getData().getTitle());
                    Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2 " + blogResp1.getOthers().getTestimonialImagePath());
                    tvt_title.setText(blogResp1.getData().getTitle());
                    tvt_author.setText(blogResp1.getData().getAuthor());
                    tvt_description.setText(blogResp1.getData().getDescription());

                    Glide.with(getApplicationContext())
                            .load(blogResp1.getOthers().getTestimonialImagePath()+blogResp1.getData().getUpload_file())
                            .error(R.mipmap.ic_launcher)
                            .placeholder(R.mipmap.ic_launcher)
                            .into(img_testimonialImage);


                } else {
                   // Toast.makeText(activity, blogResp1.getMessage().get(0), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(@NotNull Call<TestimonialListResp> call, @NotNull Throwable t) {
                Log.d("mmm", "/////////////////////////////////////////////////// testimonial 2" + t);

            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.tvt_back:
                super.onBackPressed();

                break;
        }
    }




/*    @Override
    protected void onStop() {
        super.onStop();
        if (AppController.getInstance().getRequestQueue() != null) {
            AppController.getInstance().cancelPendingRequests(tagGetVideoDetails);
        }
    }*/
}
