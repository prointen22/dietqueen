package com.livennew.latestdietqueen.exclusive_youtube.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.FragmentDashboardHomeBinding;
import com.livennew.latestdietqueen.databinding.FragmentYoutubeBinding;
import com.livennew.latestdietqueen.exclusive_youtube.YouTubePlayerActivity;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.adapter.VideoListAdapter;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.VideoResp;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeBlogAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTestimonialAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeVideosAdapter;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;


@AndroidEntryPoint
public class YoutubeFragment extends Fragment {
    private FragmentActivity activity;
    private LinearLayoutManager mLayoutManager;
    private VideoResp videos;
    private VideoListAdapter mAdapter;
    private String lastToken = "";
    private Handler handler;
    private int currentItem, totalItems, scrollOutItems;
    private Boolean isScrolling = false;
    private LinearLayoutManager linearLayoutManager;
    private FragmentYoutubeBinding binding;



    private HomeBlogAdapter homeBlogAdapter;

    private HomeVideosAdapter homeVideosAdapter;
    //    private VideoResp videos;

    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    User user;

    SharedPreferences mPrefsobj ;
    public YoutubeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentYoutubeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        binding.rvYoutube.setHasFixedSize(true);
        handler = new Handler();
        mAdapter = new VideoListAdapter(activity);
        linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.rvYoutube.setLayoutManager(linearLayoutManager);
        binding.rvYoutube.setAdapter(mAdapter);
        mAdapter.setListener(new VideoListAdapter.Listener() {
            @Override
            public void onItemClick(Video item, int position) {
                getVideoDetails(item);
            }
        });
        if (SupportClass.checkConnection(activity)) {
           // getVideos();
            VideoList();

        } else {
            SupportClass.noInternetConnectionToast(activity);
        }

        binding.rvYoutube.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                totalItems = linearLayoutManager.getItemCount();
                scrollOutItems = linearLayoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItem + scrollOutItems == totalItems) && (!TextUtils.isEmpty(lastToken))) {
                    isScrolling = false;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (SupportClass.checkConnection(activity)) {
                                getVideosPagination();
                            } else {
                                SupportClass.noInternetConnectionToast(activity);
                            }
                        }
                    }, 2000);

                }
            }
        });

        return view;
    }

/*
    private void getVideos() {
        binding.llProgress.setVisibility(View.VISIBLE);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId=" + getString(R.string.channel_id) + "&maxResults=10&key=" + getString(R.string.youtube_key) + "&pageToken=" + lastToken, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                binding.llProgress.setVisibility(View.GONE);
                videos = new Gson().fromJson(response.toString(), VideoResp.class);
                lastToken = videos.getNextPageToken();
                if (videos.getItems() == null || videos.getItems().size() == 0)
                    binding.tvEmpty.setVisibility(View.VISIBLE);
                else
                    binding.tvEmpty.setVisibility(View.GONE);
                mAdapter.addAll(videos.getItems());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                binding.llProgress.setVisibility(View.GONE);
                binding.tvEmpty.setVisibility(View.VISIBLE);
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }*/


    private void getVideosPagination() {
        binding.progressBar.setVisibility(View.VISIBLE);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId=" + getString(R.string.channel_id) + "&maxResults=10&key=" + getString(R.string.youtube_key) + "&pageToken=" + lastToken, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //llProgress.setVisibility(View.GONE);
                videos = new Gson().fromJson(response.toString(), VideoResp.class);
                lastToken = videos.getNextPageToken();
                mAdapter.addAll(videos.getItems());
                binding.progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(activity, error.toString(), Toast.LENGTH_SHORT).show();
                Log.d("Youtube Error", error.toString());
              //  binding.llProgress.setVisibility(View.GONE);
            }
        });
        // AppController.getInstance().addToRequestQueue(jsonObjectRequest, tagGetVideos);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }


    private void getVideoDetails(Video item) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, "https://www.googleapis.com/youtube/v3/videos?id=" + item.getId() + "&key=" + getString(R.string.youtube_key) + "&part=snippet,contentDetails,statistics,status", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String desc = response.getJSONArray("items").getJSONObject(0).getJSONObject("snippet").getString("description");
                    item.getSnippet().setDescription(desc);
                    callVideoPlayer(item);
                } catch (JSONException e) {
                    e.printStackTrace();
                    callVideoPlayer(item);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        //  AppController.getInstance().addToRequestQueue(jsonObjectRequest, tagGetVideoDetails);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void callVideoPlayer(Video item) {
        Intent intent = new Intent(activity, YouTubePlayerActivity.class);
        intent.putExtra("video", item);
        startActivity(intent);
    }

    private void VideoList() {
      /*  binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
        binding.incBlogVideo.icVideoProgress.setVisibility(View.VISIBLE);
        apiInterface.getVideos(getString(R.string.channel_id), getString(R.string.youtube_key)).enqueue(new Callback<VideoResp>() {
            @Override
            public void onResponse(@NotNull Call<VideoResp> call, @NotNull retrofit2.Response<VideoResp> response) {
                try {
                    if (response.body() != null) {
                        List<Video> videos = response.body().getItems();
                        Iterator<Video> i = videos.iterator();
                        while (i.hasNext()) {
                            Video item = i.next();
                            if (item.getId().getVideoId() == null)
                                i.remove();
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (videos != null && videos.size() > 1) {
                                    binding.incBlogVideo.llDashboardVideo.setVisibility(View.VISIBLE);
                                    homeVideosAdapter.setData(videos);
                                }
                                binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);

                            }
                        }, 2000);
                    } else {
                        binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(@NotNull Call<VideoResp> call, @NotNull Throwable t) {
                binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
                binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
            }
        });
*/


        Map<String, Integer> params = new HashMap<String, Integer>();
        params.put("id",0);
        params.put("perPage", 3);

        Log.d("mmm","/////////////////////////////////////////////////// testimonial 1");
        apiInterface.getTestimonials(params).enqueue(new Callback<TestimonialListResp>() {
            @Override
            public void onResponse(@NotNull Call<TestimonialListResp> call, @NotNull retrofit2.Response<TestimonialListResp> response) {
                TestimonialListResp blogResp1 = response.body();
                Log.d("mmm","/////////////////////////////////////////////////// testimonial 2"+blogResp1.getError());
                if (!blogResp1.getError()) {

                    // binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                    Log.d("mmm","/////////////////////////////////////////////////// testimonial 2 "+blogResp1.getData().getFirstPageUrl());
                    Log.d("mmm","/////////////////////////////////////////////////// testimonial 2 "+blogResp1.getData().getTestimonials().size());
                    Log.d("mmm","/////////////////////////////////////////////////// testimonial 2 "+blogResp1.getOthers().getTestimonialImagePath());


                    HomeTestimonialAdapter homeTestAdapter = new HomeTestimonialAdapter(getContext(), blogResp1.getData().getTestimonials(), blogResp1.getOthers());
                    //    RecyclerView.LayoutManager recyce = new GridLayoutManager(getActivity(),2);
                    RecyclerView.LayoutManager recycev = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL, false);
                    //    recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));
                    binding.rvHomeVideo.setLayoutManager(recycev);
                    binding.rvHomeVideo.setItemAnimator(new DefaultItemAnimator());
                    binding.rvHomeVideo.setAdapter(homeTestAdapter);


                } else {
                    Toast.makeText(activity, blogResp1.getMessage().get(0), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(@NotNull Call<TestimonialListResp> call, @NotNull Throwable t) {
                Log.d("mmm","/////////////////////////////////////////////////// testimonial 2"+t);

            }
        });
    }
}
