package com.livennew.latestdietqueen.exclusive_youtube;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.andexert.library.RippleView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.ActivityExclusiveYoutubeBinding;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.ExclusiveFragment;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.YoutubeFragment;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ExclusiveYoutubeMainClass extends FabActivity {
    private RippleView btnYoutube;
    private RippleView btnExclusive;
    private Button btnYoutube1;
    private Button btnExclusive1;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private FrameLayout contentView;
    private LinearLayout llProgress;
    private ImageView ivMainVideo;
    private Toolbar toolbarVideosExcl;
    private AppBarLayout mAppBarLayout;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private static final String TAG = "Erros";
    @Inject
    SessionManager sessionManager;
    User user;
    private ActivityExclusiveYoutubeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExclusiveYoutubeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        user = sessionManager.getUser();
        init();
        setSupportActionBar(toolbarVideosExcl);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbarVideosExcl.setNavigationIcon(R.drawable.ic_navigation_before_pink_24dp);
        collapsingToolbarLayout.setTitle(" ");

        if (SupportClass.checkConnection(getApplicationContext())) {
            GetBanner();
        } else {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }

        btnYoutube1.setTextColor(getResources().getColor(R.color.white));
        btnExclusive1.setTextColor(getResources().getColor(R.color.gray_300));
        openYoutubeScreen();
        btnYoutube.setOnRippleCompleteListener(rippleView -> {
            btnYoutube1.setTextColor(getResources().getColor(R.color.white));
            btnExclusive1.setTextColor(getResources().getColor(R.color.gray_300));
            if (SupportClass.checkConnection(getApplicationContext())) {
                //AppController.getInstance().cancelPendingRequests(tag_string_reqgetNutrition);
                //AppController.getInstance().cancelPendingRequests(tag_string_reqgetSpecDetails);
                openYoutubeScreen();
            } else {
                Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }
        });
        //   Glide.with(this).load(R.drawable.image1).error(R.drawable.ic_award).into(ivMainVideo);
        btnExclusive.setOnRippleCompleteListener(rippleView -> {
            btnExclusive1.setTextColor(getResources().getColor(R.color.white));
            btnYoutube1.setTextColor(getResources().getColor(R.color.gray_300));
            if (SupportClass.checkConnection(getApplicationContext())) {
                //AppController.getInstance().cancelPendingRequests(tag_string_reqgetSchedule);
                //AppController.getInstance().cancelPendingRequests(tag_string_reqgetSpecDetails);
                openExclusiveScreen();
            } else {
                Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }
        });
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });
        mAppBarLayout.setBackgroundColor(getResources().getColor(R.color.white));
    }


    private void init() {
        btnYoutube = findViewById(R.id.btnYoutube);
        btnYoutube1 = findViewById(R.id.btnYoutube1);
        btnExclusive = findViewById(R.id.btnExclusive);
        btnExclusive1 = findViewById(R.id.btnExclusive1);
        llProgress = findViewById(R.id.llProgress);
        ivMainVideo = findViewById(R.id.ivBlog);
        toolbarVideosExcl = findViewById(R.id.toolbar);
        mAppBarLayout = findViewById(R.id.appBarLayout);
        collapsingToolbarLayout = findViewById(R.id.collapToolbatLayout);
    }

    private void GetBanner() {
        try {
            //   progressBar1.setVisibility(View.VISIBLE);
            JSONObject params = new JSONObject();
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.GETBANNER, params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject jsonObject = jObj.getJSONObject("data");
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
//                        Glide.get(ExclusiveYoutubeMainClass.this).
                        Glide
                                .with(this)
                                .load(jsonObjectOther.getString("banner_image_path") + jsonObject.getString("banner"))
                                .placeholder(R.drawable.image_background_dark)
                                .error(R.drawable.image_background_dark)
                                .into(ivMainVideo);
                    } else {
                        //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                        JSONArray jsonArray = jObj.getJSONArray("message");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(ExclusiveYoutubeMainClass.this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openYoutubeScreen() {
        YoutubeFragment frgment = new YoutubeFragment();
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        mFragmentTransaction.replace(R.id.containerViewVideos, frgment).commitAllowingStateLoss();
    }

    private void openExclusiveScreen() {
        //Toast.makeText(this, "Developer is working!!!", Toast.LENGTH_SHORT).show();
        ExclusiveFragment frgment = new ExclusiveFragment();
        //Bundle bundle = new Bundle();
        //    bundle.putString("tractor", tractor.toString());
        // bundle.putString("tractor_name_image", tractorNameImage.toString());
        //frgment.setArguments(bundle);
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        mFragmentTransaction.replace(R.id.containerViewVideos, frgment).commitAllowingStateLoss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


}
