package com.livennew.latestdietqueen.exclusive_youtube.fragment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoDetailResp {
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("etag")
    @Expose
    private String etag;
    @SerializedName("items")
    @Expose
    private List<VideoDetail> items = null;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public List<VideoDetail> getItems() {
        return items;
    }

    public void setItems(List<VideoDetail> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "VideoDetailResp{" +
                "kind='" + kind + '\'' +
                ", etag='" + etag + '\'' +
                ", items=" + items +
                '}';
    }
}
