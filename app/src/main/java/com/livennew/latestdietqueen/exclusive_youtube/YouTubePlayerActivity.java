package com.livennew.latestdietqueen.exclusive_youtube;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.ActivityYouTubePlayerBinding;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceActivity;
import com.livennew.latestdietqueen.network_support.Api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class YouTubePlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private static final String TAG = YouTubePlayerActivity.class.getSimpleName();
    private Video item;
    private String tagGetVideoDetails = "getVideoDetails";
    private ActivityYouTubePlayerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityYouTubePlayerBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        item = getIntent().getExtras().getParcelable("video");
        init();
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        youTubePlayerView.initialize(Api.GoogleAppId, this);
    }

    private void init() {
        try {
//            initToolbar();

            binding.tvName.setText(Util.fromHtml(item.getSnippet().getTitle()));
            binding.tvDesc.setText(Util.fromHtml(item.getSnippet().getDescription()));

            if (!item.getSnippet().getPublishedAt().isEmpty()) {
                binding.tvDate.setText(getString(R.string.published_on).concat(" " + getDate(item.getSnippet().getPublishedAt())));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getDate(String publishedAt) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat("dd-mm-yyyy",Locale.US);
        try {
            Date date = input.parse(publishedAt);
            return output.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }


    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (null == player) return;
        try {
            // Start buffering
            if (!wasRestored) {
                player.cueVideo(item.getId().getVideoId());
                player.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
                    @Override
                    public void onLoading() {

                    }

                    @Override
                    public void onLoaded(String s) {
                        player.play();
                    }

                    @Override
                    public void onAdStarted() {

                    }

                    @Override
                    public void onVideoStarted() {

                    }

                    @Override
                    public void onVideoEnded() {

                    }

                    @Override
                    public void onError(YouTubePlayer.ErrorReason errorReason) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


/*    @Override
    protected void onStop() {
        super.onStop();
        if (AppController.getInstance().getRequestQueue() != null) {
            AppController.getInstance().cancelPendingRequests(tagGetVideoDetails);
        }
    }*/
}
