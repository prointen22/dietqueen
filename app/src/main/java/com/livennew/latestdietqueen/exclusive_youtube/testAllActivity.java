package com.livennew.latestdietqueen.exclusive_youtube;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.ActivityYouTubePlayerBinding;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTestimonialAdapter;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.fragments.home.model.Testimonials;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.retrofit.APIInterface;


import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;

@AndroidEntryPoint
public class testAllActivity  extends BaseActivity {
    private static final String TAG = testAllActivity.class.getSimpleName();
    private Video item;
    private String tagGetVideoDetails = "getVideoDetails";
    private Testimonials binding;
    Toolbar toolbar;
    @Inject
    SessionManager sessionManager;
    private LinearLayoutCompat llFooter;
    @Inject
    APIInterface apiInterface;
    private User user;
    RecyclerView rv_home_video;
//    RecyclerViewAdapter recyclerViewAdapter;
    ProgressBar progress_bar;
    HomeTestimonialAdapter homeTestAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testimonial);

        rv_home_video=(RecyclerView) findViewById(R.id.rv_home_video);
        progress_bar=(ProgressBar) findViewById(R.id.progress_bar);
        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Testimonials");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white,null));
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do something when the corky2 is clicked
                testAllActivity.super.onBackPressed();
            }
        });
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.orange_theme));
        VideoList();

    }
    private void VideoList() {
      /*  binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
        binding.incBlogVideo.icVideoProgress.setVisibility(View.VISIBLE);
        apiInterface.getVideos(getString(R.string.channel_id), getString(R.string.youtube_key)).enqueue(new Callback<VideoResp>() {
            @Override
            public void onResponse(@NotNull Call<VideoResp> call, @NotNull retrofit2.Response<VideoResp> response) {
                try {
                    if (response.body() != null) {
                        List<Video> videos = response.body().getItems();
                        Iterator<Video> i = videos.iterator();
                        while (i.hasNext()) {
                            Video item = i.next();
                            if (item.getId().getVideoId() == null)
                                i.remove();
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (videos != null && videos.size() > 1) {
                                    binding.incBlogVideo.llDashboardVideo.setVisibility(View.VISIBLE);
                                    homeVideosAdapter.setData(videos);
                                }
                                binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);

                            }
                        }, 2000);
                    } else {
                        binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(@NotNull Call<VideoResp> call, @NotNull Throwable t) {
                binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
                binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
            }
        });
*/

        Map<String, Integer> params = new HashMap<String, Integer>();
        params.put("id",0);
      //  params.put("perPage", 3);

        Log.d("mmm","/////////////////////////////////////////////////// testimonial 1");
        apiInterface.getTestimonials(params).enqueue(new Callback<TestimonialListResp>() {
            @Override
            public void onResponse(@NotNull Call<TestimonialListResp> call, @NotNull retrofit2.Response<TestimonialListResp> response) {
                TestimonialListResp blogResp1 = response.body();
                Log.d("mmm","/////////////////////////////////////////////////// testimonial 2"+blogResp1.getError());
                progress_bar.setVisibility(View.GONE);
                if (!blogResp1.getError()) {

                    // binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                    Log.d("mmm","/////////////////////////////////////////////////// testimonial 2 "+blogResp1.getData().getFirstPageUrl());
                    Log.d("mmm","/////////////////////////////////////////////////// testimonial 2 "+blogResp1.getData().getTestimonials().size());
                    Log.d("mmm","/////////////////////////////////////////////////// testimonial 2 "+blogResp1.getOthers().getTestimonialImagePath());
                    homeTestAdapter = new HomeTestimonialAdapter(getApplicationContext(), blogResp1.getData().getTestimonials(), blogResp1.getOthers());


                    homeTestAdapter.setListener(new HomeTestimonialAdapter.Listener() {


                        @Override
                        public void onItemClick(Testimonials testi) {
                            Log.d("mmm", "//////////////////////// ff /////////////////////////// testimonial"+testi.getId());


                            Intent intent = new Intent(getApplicationContext(),testDetailActivity.class);
                            intent.putExtra("testimonial_id",String.valueOf(testi.getId()));
                            // intent.putExtra("type", "blog");
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);


                        }
                    });



                    //    RecyclerView.LayoutManager recyce = new GridLayoutManager(getActivity(),2);
                    RecyclerView.LayoutManager recycev = new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL, false);
                    //    recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));
                    rv_home_video.setLayoutManager(recycev);
                    rv_home_video.setItemAnimator(new DefaultItemAnimator());
                    rv_home_video.setAdapter(homeTestAdapter);


                } else {
                    Toast.makeText(getApplicationContext(), blogResp1.getMessage().get(0), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(@NotNull Call<TestimonialListResp> call, @NotNull Throwable t) {
                Log.d("mmm","/////////////////////////////////////////////////// testimonial 2"+t);
                progress_bar.setVisibility(View.GONE);

            }
        });
    }




/*    @Override
    protected void onStop() {
        super.onStop();
        if (AppController.getInstance().getRequestQueue() != null) {
            AppController.getInstance().cancelPendingRequests(tagGetVideoDetails);
        }
    }*/
}
