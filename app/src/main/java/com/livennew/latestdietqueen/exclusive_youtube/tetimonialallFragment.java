package com.livennew.latestdietqueen.exclusive_youtube;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.FragmentDashboardHomeBinding;
import com.livennew.latestdietqueen.databinding.FragmentYoutubeBinding;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Plan;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.PlanParam;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.YoutubeFragment;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;
import com.livennew.latestdietqueen.fragments.home.ReplaceItemFragment;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeBlogAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeReplaceDietAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTestimonialAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTodayDietAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeVideosAdapter;
import com.livennew.latestdietqueen.fragments.home.model.Blog;
import com.livennew.latestdietqueen.fragments.home.model.BlogListResp;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.newsblog.BlogDetailActivity;
import com.livennew.latestdietqueen.newsblog.NewsBlogListActivity;
import com.livennew.latestdietqueen.newsvideo.VideoList;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.user_detail.AchievementsActivity;
import com.livennew.latestdietqueen.utils.CenterZoomLayoutManager;
import com.livennew.latestdietqueen.utils.DateUtil;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.MODE_PRIVATE;


@AndroidEntryPoint
public class tetimonialallFragment extends Fragment {
    private static final String TAG = "HomeFragment";

    private HomeTodayDietAdapter homeTodayDietAdapter;
    private HomeReplaceDietAdapter homeReplaceDietAdapter;
    private ArrayList<DietEPlanFood> alHomeTodayDiet;
    private ArrayList<Food> alHomeReplaceTodayDiet;

    List<DietEPlanFood> listingvID = new ArrayList<DietEPlanFood>();
    String Fodgid="";
    private HomeBlogAdapter homeBlogAdapter;

    private HomeVideosAdapter homeVideosAdapter;
    //    private VideoResp videos;
    private FragmentActivity activity;
    private FragmentYoutubeBinding binding;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    User user;

    SharedPreferences mPrefsobj ;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        user = sessionManager.getUser();

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentYoutubeBinding.inflate(inflater, container, false);


        mPrefsobj = getActivity().getPreferences(MODE_PRIVATE);

        VideoList();
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (sessionManager.getUser().getName() != null) {
          //  binding.tvHomeUserNm.setText(SupportClass.getUserFirstName(sessionManager.getUser().getName()));
        }
        Glide
                .with(this)
                .load(sessionManager.getUser().getProfileImage())
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher);
          //      .into(binding.civHomeProfile);

    }


    /**
     * Today's diet recycler view with CenterZoomLayoutManager class implemented
     */

    private void VideoList() {
      /*  binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
        binding.incBlogVideo.icVideoProgress.setVisibility(View.VISIBLE);
        apiInterface.getVideos(getString(R.string.channel_id), getString(R.string.youtube_key)).enqueue(new Callback<VideoResp>() {
            @Override
            public void onResponse(@NotNull Call<VideoResp> call, @NotNull retrofit2.Response<VideoResp> response) {
                try {
                    if (response.body() != null) {
                        List<Video> videos = response.body().getItems();
                        Iterator<Video> i = videos.iterator();
                        while (i.hasNext()) {
                            Video item = i.next();
                            if (item.getId().getVideoId() == null)
                                i.remove();
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (videos != null && videos.size() > 1) {
                                    binding.incBlogVideo.llDashboardVideo.setVisibility(View.VISIBLE);
                                    homeVideosAdapter.setData(videos);
                                }
                                binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);

                            }
                        }, 2000);
                    } else {
                        binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(@NotNull Call<VideoResp> call, @NotNull Throwable t) {
                binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
                binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
            }
        });
*/

      //  binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
    //    binding.incBlogVideo.icVideoProgress.setVisibility(View.VISIBLE);

        Map<String, Integer> params = new HashMap<String, Integer>();
        params.put("id",0);
      //  params.put("perPage",);

        Log.d("mmm","/////////////////////////////////////////////////// testimonial 1");
        apiInterface.getTestimonials(params).enqueue(new Callback<TestimonialListResp>() {
            @Override
            public void onResponse(@NotNull Call<TestimonialListResp> call, @NotNull retrofit2.Response<TestimonialListResp> response) {
                TestimonialListResp blogResp1 = response.body();
               Log.d("mmm","/////////////////////////////////////////////////// testimonial 2"+blogResp1.getError());
                if (!blogResp1.getError()) {

                   // binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
                    Log.d("mmm","/////////////////////////////////////////////////// testimonial 2 "+blogResp1.getData().getFirstPageUrl());
                    Log.d("mmm","/////////////////////////////////////////////////// testimonial 2 "+blogResp1.getData().getTestimonials().size());
                    Log.d("mmm","/////////////////////////////////////////////////// testimonial 2 "+blogResp1.getOthers().getTestimonialImagePath());

                   // binding.incBlogVideo.llDashboardVideo.setVisibility(View.VISIBLE);

                    //binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);

         HomeTestimonialAdapter homeTestAdapter = new HomeTestimonialAdapter(getContext(), blogResp1.getData().getTestimonials(), blogResp1.getOthers());
                    //    RecyclerView.LayoutManager recyce = new GridLayoutManager(getActivity(),2);
                    RecyclerView.LayoutManager recycev = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL, false);
                    //    recyclerView.addItemDecoration(new GridSpacingdecoration(2, dpToPx(10), true));
                    binding.rvHomeVideo.setLayoutManager(recycev);
                    binding.rvHomeVideo.setItemAnimator(new DefaultItemAnimator());
                    binding.rvHomeVideo.setAdapter(homeTestAdapter);


                } else {
                    Toast.makeText(activity, blogResp1.getMessage().get(0), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(@NotNull Call<TestimonialListResp> call, @NotNull Throwable t) {
                Log.d("mmm","/////////////////////////////////////////////////// testimonial 2"+t);

             //   binding.incBlogVideo.llDashboardVideo.setVisibility(View.GONE);
             //   binding.incBlogVideo.icVideoProgress.setVisibility(View.GONE);
            }
        });
    }



}
