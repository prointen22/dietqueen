package com.livennew.latestdietqueen.exclusive_youtube.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.FragmentExclusiveBinding;
import com.livennew.latestdietqueen.fragments.home.model.VideoModel;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.newsvideo.NewsVideosDetail;
import com.livennew.latestdietqueen.newsvideo.adapter.NewsVideoListAdapter;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ExclusiveFragment extends Fragment {
    private FragmentActivity activity;
    private Toolbar toolbarVideoList;
    private ArrayList<VideoModel> alNewsVideoList;
    private ProgressDialog pDialog;
    private static final String TAG = "Erros";
    private NewsVideoListAdapter newsVideoListAdapter;
    private boolean error;
    private StringBuilder sb;
    private boolean appendSeparator = false;
    private int lastPage = 0;
    private String curapgeNo = "1";
    private Handler handler;
    private int currentItem, totalItems, scrollOutItems;
    private Boolean isScrolling = false;
    private LinearLayoutManager recyclerLayoutManager;
    @Inject
    SessionManager sessionManager;
    User user;
    private FragmentExclusiveBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        user = sessionManager.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentExclusiveBinding.inflate(getLayoutInflater(), container, false);
        View view = binding.getRoot();
        init(view);

        if (SupportClass.checkConnection(activity)) {
            //  videoList();
            binding.btnReload.setVisibility(View.GONE);
            binding.recyclerView.setVisibility(View.VISIBLE);
            curapgeNo = "1";
            ExclusiveList();
        } else {
            SupportClass.noInternetConnectionToast(activity);
            binding.btnReload.setVisibility(View.VISIBLE);
            binding.recyclerView.setVisibility(View.GONE);
        }

        binding.btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SupportClass.checkConnection(activity)) {
                    // videoList();
                    binding.btnReload.setVisibility(View.GONE);
                    binding.recyclerView.setVisibility(View.VISIBLE);
                    curapgeNo = "1";
                    ExclusiveList();
                } else {
                    SupportClass.noInternetConnectionToast(activity);
                    binding.btnReload.setVisibility(View.VISIBLE);
                    binding.recyclerView.setVisibility(View.GONE);
                }
            }
        });
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = recyclerLayoutManager.getChildCount();
                totalItems = recyclerLayoutManager.getItemCount();
                scrollOutItems = recyclerLayoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItem + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //   remove progress item
                            /*if (alProductDietPlan != null && alProductDietPlan.size() != 1) {
                                alProductDietPlan.remove(alProductDietPlan.size() - 1);
                                productDietPlanAdapter.notifyItemRemoved(alProductDietPlan.size());*/
                            int page = Integer.parseInt(curapgeNo);
                            page++;
                            curapgeNo = page + "";
                            if (page <= lastPage) {
                                if (SupportClass.checkConnection(activity)) {
                                    ExclusiveListPagination();
                                } else {
                                    SupportClass.noInternetConnectionToast(activity);
                                }
                            }
                            //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                            //}
                        }
                    }, 2000);

                }
            }
        });

        return view;
    }

    private void ExclusiveList() {
        try {
            binding.llProgress.setVisibility(View.VISIBLE);
            binding.recyclerView.setVisibility(View.GONE);
            JSONObject params = new JSONObject();
            params.put("type", "exclusive");
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.EXCLUSIVE + "page=" + Integer.parseInt(curapgeNo), params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                binding.llProgress.setVisibility(View.GONE);
                binding.recyclerView.setVisibility(View.VISIBLE);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    // NewTractors newTractors = new Gson().fromJson(response.toString(), NewTractors.class);
                    //if (!newTractors.isError()) {
                    if (!error) {
                        JSONObject jsonObject1 = jObj.getJSONObject("data");
                        lastPage = jsonObject1.getInt("last_page");
                        //totPagesTrac = newTractors.getTotPages();
                        JSONArray jsonArrayData = jsonObject1.getJSONArray("blog_exclusive");
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
                        String exclusiveImagePath = jsonObjectOther.getString("exclusiveImagePath");
                        alNewsVideoList.clear();
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);
                            VideoModel videoModel = new VideoModel();

                            videoModel.setId(jsonObjectData.getInt("id"));
                            videoModel.setVideoTitle(jsonObjectData.getString("title"));
                            videoModel.setVideoDesc(jsonObjectData.getString("description"));
                            videoModel.setVideoURL(exclusiveImagePath.concat(jsonObjectData.getString("upload_file")));
                            //Set layout manager to position the items
                            alNewsVideoList.add(videoModel);
                        }
                        if (alNewsVideoList.size() == 0)
                            binding.tvNoRecords.setVisibility(View.VISIBLE);
                        newsVideoListAdapter = new NewsVideoListAdapter(activity, alNewsVideoList);
                        recyclerLayoutManager = new LinearLayoutManager(activity);
                        binding.recyclerView.setLayoutManager(recyclerLayoutManager);
                        binding.recyclerView.setAdapter(newsVideoListAdapter);
                        newsVideoListAdapter.setListener(new NewsVideoListAdapter.Listener() {
                            @Override
                            public void onItemClick(VideoModel videoModel, int position) {
                                Intent intent = new Intent(activity, NewsVideosDetail.class);
                                intent.putExtra("video_id", alNewsVideoList.get(position).getId());
                                intent.putExtra("type", "exclusive");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        });
                    } else {
                        //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                        Toast.makeText(activity, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void ExclusiveListPagination() {
        try {
            binding.progressBar.setVisibility(View.VISIBLE);
            //rvNewsVideoList.setVisibility(View.GONE);
            JSONObject params = new JSONObject();
            params.put("type", "exclusive");
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.EXCLUSIVE + "page=" + Integer.parseInt(curapgeNo), params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                binding.progressBar.setVisibility(View.GONE);
                //rvNewsVideoList.setVisibility(View.VISIBLE);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    // NewTractors newTractors = new Gson().fromJson(response.toString(), NewTractors.class);
                    //if (!newTractors.isError()) {
                    if (!error) {
                        JSONObject jsonObject1 = jObj.getJSONObject("data");
                        lastPage = jsonObject1.getInt("last_page");
                        //totPagesTrac = newTractors.getTotPages();
                        JSONArray jsonArrayData = jsonObject1.getJSONArray("blog_exclusive");
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
                        String exclusiveImagePath = jsonObjectOther.getString("exclusiveImagePath");
                        //                   alNewsVideoList.clear();
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);
                            VideoModel videoModel = new VideoModel();

                            videoModel.setId(jsonObjectData.getInt("id"));
                            videoModel.setVideoTitle(jsonObjectData.getString("title"));
                            videoModel.setVideoDesc(jsonObjectData.getString("description"));
                            videoModel.setVideoURL(exclusiveImagePath.concat(jsonObjectData.getString("upload_file")));
                            //Set layout manager to position the items
                            alNewsVideoList.add(videoModel);
                        }
                        newsVideoListAdapter.notifyDataSetChanged();
                        binding.progressBar.setVisibility(View.GONE);
                    } else {
                        //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                        Toast.makeText(activity, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void init(View v) {
        sb = new StringBuilder();
        alNewsVideoList = new ArrayList<VideoModel>();
        alNewsVideoList.clear();
        handler = new Handler();
    }
}
