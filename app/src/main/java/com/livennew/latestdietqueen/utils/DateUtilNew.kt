package com.livennew.latestdietqueen.utils

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

class DateUtilNew {
    companion object {
        @JvmStatic
        fun getDateFromDateObject(dateObject: Date): String {
            return SimpleDateFormat("dd", Locale.US).format(dateObject) as String
        }

        @JvmStatic
        fun getDayFromDateObject(dateObject: Date): String {
            return SimpleDateFormat("EEE", Locale.US).format(dateObject) as String
        }

        @JvmStatic
        fun getMonthFromDateObject(dateObject: Date): String {
            return SimpleDateFormat("MMM", Locale.US).format(dateObject) as String
        }

        @JvmStatic
        fun getFullDateFromDateObject(dateObject: Date): String {
            return SimpleDateFormat("yyyy-MM-dd", Locale.US).format(dateObject) as String
        }

        @JvmStatic
        fun getCurrentTimeFromDateObject(dateObject: Date): String {
            return SimpleDateFormat("HH:mm", Locale.US).format(dateObject) as String
        }

        @JvmStatic
        fun getCurrentTimeFromDateObject12(dateObject: Date): String {
            return SimpleDateFormat("hh:mm a", Locale.US).format(dateObject) as String
//            return "08:52 AM"
        }

        @JvmStatic
        fun compareDietTimeWithCurrentTime(dietTime: String): Int {
            val currentTime = getCurrentTimeFromDateObject(Date())

            val simpleDateFormat = SimpleDateFormat("hh:mm a")

            val mCurrentTime = simpleDateFormat.parse(DateUtil.convertTime24To12(currentTime))
            val mDietTime = simpleDateFormat.parse(DateUtil.convertTime24To12(dietTime))

            Log.v("TAG", "Current time: $mCurrentTime")
            Log.v("TAG", "Diet time: $mDietTime")

            val difference = mDietTime.time - mCurrentTime.time
            val days = (difference.toInt() / (1000 * 60 * 60 * 24))
            var hours = ((difference.toInt() - 1000 * 60 * 60 * 24 * days) / (1000 * 60 * 60))
            val min = (difference.toInt() - 1000 * 60 * 60 * 24 * days - 1000 * 60 * 60 * hours) / (1000 * 60)
//            hours = if (hours < 0) -hours else hours

//            Log.v("TAG", "Time Diff in days: $days \n Time diff in hours: $hours \n Time diff in min: $min")

            if (hours == 0 && min < 0) {
                hours = -1
            }

            return hours

        }

        @JvmStatic
        fun getMealTimeDiffWithCurrentTimeInMinutes(dietTime: String): Int {
            val currentTime = getCurrentTimeFromDateObject12(Date())

            try {
                val simpleDateFormat = SimpleDateFormat("hh:mm a")

                val mCurrentTime = simpleDateFormat.parse(currentTime)
                val mDietTime = simpleDateFormat.parse(dietTime)

                Log.v("TAG", "Current time: $mCurrentTime")
                Log.v("TAG", "Diet time: $mDietTime")

                val difference = mDietTime.time - mCurrentTime.time
                val days = (difference.toInt() / (1000 * 60 * 60 * 24))
                val hours = ((difference.toInt() - 1000 * 60 * 60 * 24 * days) / (1000 * 60 * 60))
                var min = (difference.toInt() - 1000 * 60 * 60 * 24 * days - 1000 * 60 * 60 * hours) / (1000 * 60)
                if (hours > 0) {
                    min += (hours * 60)
                }
                min = if (min < 0) 0 else min
                return min
            } catch (e:Exception) {
                Log.v("TAG", "Exception in getting meal time difference: ${e.message}")
            }

            return 0
        }

        @JvmStatic
        fun isFutureDate(date: String): Boolean {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

            val mCurrentDate = simpleDateFormat.parse(getFullDateFromDateObject(Date()))
            val mDate = simpleDateFormat.parse(date)

            if (mDate.after(mCurrentDate)) {
                Log.v("TAG1", "$mDate is after $mCurrentDate")
                return true
            }

            return false
        }

        @JvmStatic
        fun isPastDate(date: String): Boolean {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")

            val mCurrentDate = simpleDateFormat.parse(getFullDateFromDateObject(Date()))
            val mDate = simpleDateFormat.parse(date)

            if (mDate.before(mCurrentDate)) {
                Log.v("TAG1", "$mDate is after $mCurrentDate")
                return true
            }

            return false
        }
    }
}