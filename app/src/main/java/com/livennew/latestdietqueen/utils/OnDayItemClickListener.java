package com.livennew.latestdietqueen.utils;

import com.livennew.latestdietqueen.diet_info.model.Day;

public interface OnDayItemClickListener {
    void onItemClick(Day item, int position);
}
