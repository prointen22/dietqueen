package com.livennew.latestdietqueen.utils;

public interface OnLoadMoreListener {
    void onLoadMore();
}
