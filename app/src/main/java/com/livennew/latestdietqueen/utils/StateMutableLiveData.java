package com.livennew.latestdietqueen.utils;

import androidx.lifecycle.MutableLiveData;

public class StateMutableLiveData<T> extends MutableLiveData<StateData<T>> {

    /**
     * Use this to put the Data on a LOADING Status
     */
    public void postLoading() {
        postValue(new StateData<T>().loading());
    }

    /**
     * Use this to put the Data on a ERROR DataStatus
     *
     * @param message the error to be handled
     */
    public void postError(String message) {
        postValue(new StateData<T>().error(message));
    }

    /**
     * Use this to put the Data on a SUCCESS DataStatus
     *
     * @param data
     */
    public void postSuccess(T data) {
        postValue(new StateData<T>().success(data));
    }

}
