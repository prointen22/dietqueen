package com.livennew.latestdietqueen.utils;

import android.content.Context;

import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    private static final long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    public static String getJsonFromAssets(Context context, String fileName) {
        String jsonString;
        try {
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }

    public static String getYesterdayDate() {
        return new SimpleDateFormat(DATE_FORMAT, Locale.US).format(findPrevDay(new Date()).getTime());
    }

    public static String getTomorrowDate() {
        return new SimpleDateFormat(DATE_FORMAT,Locale.US).format(findNextDay(new Date()).getTime());
    }

    public static String getTodayDate() {
        return new SimpleDateFormat(DATE_FORMAT,Locale.US).format(new Date().getTime());
    }

    public static Date findNextDay(Date date) {
        return new Date(date.getTime() + MILLIS_IN_A_DAY);
    }

    public static Date findPrevDay(Date date) {
        return new Date(date.getTime() - MILLIS_IN_A_DAY);
    }

    public static String convertTime24To12(String time) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm",Locale.US);
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a",Locale.US);
        Date date = null;
        try {
            date = parseFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date != null;
        return displayFormat.format(date).toLowerCase(Locale.ROOT);
    }

    public static String getDate(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern(DATE_FORMAT));
    }

    public static boolean isTodayDate(String date) {
        return date.equals(getTodayDate());
    }
}
