package com.livennew.latestdietqueen.utils;

import android.view.View;

public interface MyClickListener {
    public void onItemClick(int position, View v);
}
