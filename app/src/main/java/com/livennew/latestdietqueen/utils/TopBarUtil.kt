package com.livennew.latestdietqueen.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.livennew.latestdietqueen.BuildConfig
import com.livennew.latestdietqueen.R
import com.livennew.latestdietqueen.SessionManager
import com.livennew.latestdietqueen.about_us.AboutDietQueenActivity
import com.livennew.latestdietqueen.diet_info.InstructionActivity
import com.livennew.latestdietqueen.faq.FAQActivity
import com.livennew.latestdietqueen.info_screen.WaistlineActivity
import com.livennew.latestdietqueen.language.LanguageActivity
import com.livennew.latestdietqueen.login_screen.LoginActivity
import com.livennew.latestdietqueen.model.MyResponse
import com.livennew.latestdietqueen.network_support.APIClient
import com.livennew.latestdietqueen.notification.NotificationListActivity
import com.livennew.latestdietqueen.retrofit.APIInterface
import com.livennew.latestdietqueen.user_detail.AchievementsActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class TopBarUtil {
    companion object {

        @JvmStatic
        fun handleClick(context: Context, ivNotification: ImageView, ivDoctor: ImageView, ivMenu: ImageView, sessionManager: SessionManager) {
            ivNotification.setOnClickListener { navigateToNotificationScreen(context) }

            ivDoctor.setOnClickListener {
                context.startActivity(Intent(context, InstructionActivity::class.java))
            }

            ivMenu.setOnClickListener { showMenuDialog(context, sessionManager) }
        }

        private fun navigateToNotificationScreen(context: Context) {
            val intent1 = Intent(context, NotificationListActivity::class.java)
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent1)
        }

        private fun navigateToProfileScreen(context: Context) {
            val intent1 = Intent(context, AchievementsActivity::class.java)
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent1)
        }

        private fun showMenuDialog(context: Context, sessionManager: SessionManager) {
            // Create an alert builder
            val builder: AlertDialog.Builder = AlertDialog.Builder(context)

            // set the custom layout
            val customLayout: View = LayoutInflater.from(context).inflate(
                    R.layout.menu_dialog,
                    null)
            builder.setView(customLayout)
            val dialog = builder.create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val mIvProfileImage = customLayout.findViewById<ImageView>(R.id.iv_profile)
            try {
                Glide
                        .with(context)
                        .load(sessionManager.user.profileImage)
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(mIvProfileImage)
            } catch (e: Exception) {
                Log.v("TAG", "Exception in loading profile image: ${e.message}")
            }

            val mTvName = customLayout.findViewById<TextView>(R.id.tv_name)
            mTvName.text = sessionManager.user.name

            val mTvId = customLayout.findViewById<TextView>(R.id.tv_id)

            if (sessionManager.user.id != 0) {
                val planBasic = "Id. "
                val planDetail = "$planBasic ${sessionManager.user.id}"
                val spannable: Spannable = SpannableString(planDetail)
                spannable.setSpan(ForegroundColorSpan(context.getColor(R.color.orange_theme)), planBasic.length, planDetail.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

                mTvId.setText(spannable, TextView.BufferType.SPANNABLE)
                mTvId.visibility = View.VISIBLE
            } else {
                mTvId.visibility = View.GONE
            }

            val mIvClose = customLayout.findViewById<ImageView>(R.id.iv_close)
            mIvClose.setOnClickListener { dialog.dismiss() }

            val mTvProfile = customLayout.findViewById<TextView>(R.id.tv_my_profile)
            mTvProfile.setOnClickListener {
                dialog.dismiss()
                navigateToProfileScreen(context)
            }

            val mTvChangeLanguage = customLayout.findViewById<TextView>(R.id.tv_change_language)
            if (!TextUtils.isEmpty(sessionManager.user.userDetail.language.name)) {
                mTvChangeLanguage.text = "${context.getString(R.string.change_language)} (${sessionManager.user.userDetail.language.name})"
            }

            mTvChangeLanguage.setOnClickListener {
                dialog.dismiss()
                navigateToLanguageScreen(context)
            }

            val mTvContactUs = customLayout.findViewById<TextView>(R.id.tv_contact_us)
            mTvContactUs.setOnClickListener {
                dialog.dismiss()
                context.startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + context.getString(R.string.whats_app_no))))
            }

            val mTvShare = customLayout.findViewById<TextView>(R.id.tv_share)
            mTvShare.setOnClickListener {
                dialog.dismiss()
                shareApp(context)
            }

            val mTvFaqs = customLayout.findViewById<TextView>(R.id.tv_faq)
            mTvFaqs.setOnClickListener {
                dialog.dismiss()
                context.startActivity(Intent(context, FAQActivity::class.java))
            }

            val mTvWebsite = customLayout.findViewById<TextView>(R.id.tv_website)
            mTvWebsite.setOnClickListener {
                dialog.dismiss()
                navigateToWebsite(context)
            }

            val mTvLogout = customLayout.findViewById<TextView>(R.id.tv_logout)
            mTvLogout.setOnClickListener {
                logout(context, sessionManager)
            }

            val mTvAbout = customLayout.findViewById<ImageView>(R.id.tv_about)
            mTvAbout.setOnClickListener {
                dialog.dismiss()
                navigateToAboutDQScreen(context)
//                context.startActivity(Intent(context, WaistlineActivity::class.java))

            }
            dialog.show()
        }

        private fun navigateToLanguageScreen(context: Context) {
            val intent = Intent(context, LanguageActivity::class.java)
            intent.putExtra("achievment", "achievment")
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        }

        private fun navigateToLoginScreen(context: Context, sessionManager: SessionManager) {
            sessionManager.logout()
            context.startActivity(Intent(context, LoginActivity::class.java))
        }

        private fun navigateToAboutDQScreen(context: Context) {
            val intent = Intent(context, AboutDietQueenActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        }

        @JvmStatic
        fun shareApp(context: Context) {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "DietQueen")
                var shareMessage = """
	
DIETQUEEN - Weight Loss App for Women

"""
                shareMessage = """
            ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
            
            
            """.trimIndent()
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                context.startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) {
                Log.v("TAG", "shareApp() exception: ${e.message}")
            }
        }

        @JvmStatic
        fun navigateToWebsite(context: Context) {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://"+context.getString(R.string.cu_website))))
        }

        @JvmStatic
        fun logout(context: Context, sessionManager: SessionManager) {
            val builder = androidx.appcompat.app.AlertDialog.Builder(context, R.style.MyDatePicker)
            builder.setTitle(context.getString(R.string.logout))
            builder.setMessage(context.getString(R.string.are_you_sure_you_want))
            builder.setPositiveButton(context.getString(R.string.yes)) { dialog: DialogInterface, which: Int ->
                // Do nothing but close the dialog
                deregisterFCMToken(context, sessionManager)
                dialog.dismiss()
            }
            builder.setNegativeButton(
                    context.getString(R.string.no)
            ) { dialog: DialogInterface, which: Int ->
                // Do nothing
                dialog.dismiss()
            }
            val alert = builder.create()
            alert.show()
        }

        private fun deregisterFCMToken(context: Context, sessionManager: SessionManager) {
            val apiInterface= APIClient.getClient(context).create(APIInterface::class.java)
            apiInterface.deregisterPushToken().enqueue(object : Callback<MyResponse?> {
                override fun onResponse(call: Call<MyResponse?>, response: Response<MyResponse?>) {
                    navigateToLoginScreen(context, sessionManager)
                }

                override fun onFailure(call: Call<MyResponse?>, t: Throwable) {
                    navigateToLoginScreen(context, sessionManager)
                }
            })
        }
    }
}