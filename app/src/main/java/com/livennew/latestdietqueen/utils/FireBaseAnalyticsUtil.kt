package com.livennew.latestdietqueen.utils

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

class FireBaseAnalyticsUtil {

    companion object {

        const val LOGIN = "login"
        const val DASHBOARD = "dashboard"
        const val AGE = "age"
        const val PRODUCTS = "products"
        const val LANGUAGE = "language"
        const val VERIFYOTP = "verifyotp"
        const val SCHEDULE = "schedule"
        const val ACHIEVEMENT = "achievement"
        const val FAQ = "faq"
        const val ABOUTUS = "aboutus"
        const val VIDEOS = "aboutus"
        const val BLOGS = "aboutus"
        const val MYCALENDER = "mycalender"

        @JvmStatic
        fun loginEvent(methodname:String,firebaseAnalytics: FirebaseAnalytics) {
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.METHOD, methodname)
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle)
        }


        @JvmStatic
        fun screenViewEvent(methodname:String,firebaseAnalytics: FirebaseAnalytics) {
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.METHOD, methodname)
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle)
        }
    }
}