package com.livennew.latestdietqueen.payu;


/**
 * Created by Rahul Hooda on 14/7/17.
 */
public enum AppEnvironment {

    DEV {
        @Override
        public String merchant_Key() {
            return "suAZO7";
        }

        @Override
        public String merchant_ID() {
            return "5003432";
        }

        @Override
        public String furl() {
            return "https://www.payumoney.com/mobileapp/payumoney/failure.php";
        }

        @Override
        public String surl() {
            return "https://www.payumoney.com/mobileapp/payumoney/success.php";
        }

        @Override
        public String salt() {
            return "hWbVu2jC";
        }

        @Override
        public boolean debug() {
            return true;
        }
    },
    PRODUCTION {
        @Override
        public String merchant_Key() {
            return "suAZO7";
        }

        @Override
        public String merchant_ID() {
            return "5003432";
        }

        @Override
//        public String furl() {
//            return "https://www.payumoney.com/mobileapp/payumoney/failure.php";
//        }
        public String furl() {
            return "https://payuresponse.firebaseapp.com/failure";
        }

        @Override
//        public String surl() {
//            return "https://www.payumoney.com/mobileapp/payumoney/success.php";
//        }
        public String surl() {
            return "https://payuresponse.firebaseapp.com/success";
        }

        @Override
        public String salt() {
            return "hWbVu2jC";
        }

        @Override
        public boolean debug() {
            return false;
        }
    };

    public abstract String merchant_Key();

    public abstract String merchant_ID();

    public abstract String furl();

    public abstract String surl();

    public abstract String salt();

    public abstract boolean debug();


}
