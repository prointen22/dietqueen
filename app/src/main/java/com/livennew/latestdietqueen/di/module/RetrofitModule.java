package com.livennew.latestdietqueen.di.module;


import android.content.Context;
import android.util.Log;

import com.chuckerteam.chucker.api.ChuckerInterceptor;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.retrofit.NetworkConnectionInterceptor;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import dagger.hilt.android.qualifiers.ApplicationContext;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(ApplicationComponent.class)
public class RetrofitModule {

    String lang = "";

    @Provides
    APIInterface getApiInterface(Retrofit retroFit) {
        return retroFit.create(APIInterface.class);
    }

    @Provides
    Retrofit getRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(Api.LIVE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient getOkHttpClient(@ApplicationContext Context context, HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(getHeaderInterceptor(context))
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(new NetworkConnectionInterceptor(context))
                .addInterceptor(new ChuckerInterceptor(context))
                .build();
    }

    @Provides
    Interceptor getHeaderInterceptor(Context context) {
        SessionManager sessionManager = new SessionManager(context);
        String token = " ";

        if (sessionManager.getUser() != null) {
            if (sessionManager.getUser().getToken() != null)
                token = sessionManager.getUser().getToken();
        }

       /* if (sessionManager.getUser() != null) {
            if (sessionManager.getUser().getUserDetail() != null){
                   lang=String.valueOf(sessionManager.getUser().getUserDetail().getLanguage().getId());
            }else{

                lang="1";
            }

            Log.d("jjjjjjj","kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk   lang"+lang);

        }*/

        String finalToken = token;
        return new Interceptor() {
            @Override
            public Response intercept(@NotNull Interceptor.Chain chain) throws IOException {

                if (sessionManager.getUser() != null) {
                    if (sessionManager.getUser().getUserDetail() != null) {
                        lang = String.valueOf(sessionManager.getUser().getUserDetail().getLanguage().getId());
                    } else {

                        lang = "1";
                    }

                    Log.d("jjjjjjj", "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk   lang" + lang);

                }
                Request original = chain.request();
                Request request = original.newBuilder()
                        .header("Content-Type", "application/json; charset=utf-8")
                        .header("Accept", "application/json")
                        .header("Authorization", finalToken)
                        .header("userId", sessionManager.getUser() == null ? " " : String.valueOf(sessionManager.getUser().getId()))
                        .header("languageId", sessionManager.getUser() == null ? " " : lang)
                        .method(original.method(), original.body())
                        .build();
                // Log.d("jjjjjjj","kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk"+sessionManager.getUser().getUserDetail().getLanguage().getId());
                return chain.proceed(request);
            }
        };
    }

    @Provides
    HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }
}
