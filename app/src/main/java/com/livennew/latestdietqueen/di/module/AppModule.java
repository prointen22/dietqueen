package com.livennew.latestdietqueen.di.module;

import android.app.Application;
import android.content.Context;

import com.livennew.latestdietqueen.SessionManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import dagger.hilt.android.qualifiers.ApplicationContext;

@Module
@InstallIn(ApplicationComponent.class)
public class AppModule {

/*

    @Provides
    @Singleton
    Application getApplication(@ApplicationContext Context context) {
        return (Application) context;
    }
*/

   /* @Provides
    @Singleton
    User providesUser(SessionManager session) {
        return session.getUser();
    }*/

    @Provides
    @Singleton
    SessionManager providesSession(@ApplicationContext Context context) {
        return new SessionManager(context);
    }


}
