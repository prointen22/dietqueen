package com.livennew.latestdietqueen.di.module;

import android.content.Context;


import com.livennew.latestdietqueen.di.qualifier.ApplicationContext;
import com.livennew.latestdietqueen.di.scopes.ApplicationScope;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;

@Module
@InstallIn(ApplicationComponent.class)
public class ContextModule {
    private Context context;

   /* public ContextModule(@ApplicationContext Context context) {
        this.context = context;
    }
*/
    @Provides
    @Singleton
    public Context provideContext(@ApplicationContext Context context) {
        return context;
    }
}
