package com.livennew.latestdietqueen.splash_screen;


import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.dashboard.DashboardActivity1;
import com.livennew.latestdietqueen.fragments.products.ProductListActivity;
import com.livennew.latestdietqueen.info_screen.age_screen.AgeActivity;
import com.livennew.latestdietqueen.intro_screen.MainActivity;
import com.livennew.latestdietqueen.language.LanguageActivity;
import com.livennew.latestdietqueen.login_screen.FcmApi;
import com.livennew.latestdietqueen.login_screen.SocialMediaLoginOTP;
import com.livennew.latestdietqueen.model.DietEPlanResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.notification.MyFirebaseMessagingService;
import com.livennew.latestdietqueen.notification.model.Notification;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

@AndroidEntryPoint
public class SplashActivity extends BaseActivity {
    private static final String TAG = "SplashActivity";
    private final CompositeDisposable disposable = new CompositeDisposable();
    @Inject
    SessionManager sessionManager;
    private User user;
    @Inject
    APIInterface apiInterface;
    ObjectAnimator objectanimator;
    ObjectAnimator objectanimator1;
    ObjectAnimator objectanimator2;
    private static FirebaseAnalytics firebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splashscreen);
        user = sessionManager.getUser();
        int SPLASH_SCREEN_TIME_OUT = 2000;
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        PackageManager manager = getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = info.versionName;
        TextView versionTextview = findViewById(R.id.versionTextview);
        versionTextview.setText("Version "+version);

        if (sessionManager.isLoggedIn()) {
            FcmApi.registerFCMToken(apiInterface, sessionManager, this::getUserDetail);
        } else {
            new Handler().postDelayed(this::validateAndNavigate, SPLASH_SCREEN_TIME_OUT);
        }
        Intent i=new Intent(this, MyFirebaseMessagingService.class);
        startService(i);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForegroundService(i);
//        } else {
//            startService(i);
//        }

        FirebaseMessaging.getInstance().subscribeToTopic("breakfast");  // Topic Push Notification
        FirebaseMessaging.getInstance().subscribeToTopic("general");  // Topic Push Notification
    }

    private void getUserDetail() {
        Single<UserResp> responseOneObservable = apiInterface.getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Single<DietEPlanResp> responseTwoObservable = apiInterface.getDietEPlan()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Single.zip(responseOneObservable, responseTwoObservable, (userResp, dietEPlanResp) -> {
            if (!userResp.getError())
                if (dietEPlanResp.getError()) {
                    userResp.getData().setDietPlan(null);
                    userResp.setError(true);
                    userResp.setMessage(dietEPlanResp.getMessage());
                } else {
                    userResp.getData().setDietEPlanId(dietEPlanResp.getData().getId());

                    Log.d("dietqueen","terms pending = "+ dietEPlanResp.getData().getIs_terms_accepted());
                    sessionManager.setIsTermsAccepted(dietEPlanResp.getData().getIs_terms_accepted());

                }
            return userResp;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<UserResp>() {
                    @Override
                    public void onSuccess(@NonNull UserResp userResp) {
                        user = userResp.getData();
                        if (user!=null) {
                            user.setProfileImage(userResp.getOthers().getUserImageBase64Path() + user.getProfileImage());
                            sessionManager.setUser(user);

                            if (userResp.getError()) {
                                Toast.makeText(SplashActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                            }
                            validateAndNavigate();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(SplashActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void validateAndNavigate() {
        Intent i;

        if (user == null || user.getUserDetail() == null) {
            i = new Intent(SplashActivity.this, MainActivity.class);
        } else if (user.getMobile() == null) {
            i = new Intent(SplashActivity.this, SocialMediaLoginOTP.class);
        } else if (user.getUserDetail().getLanguage() == null) {
            FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.LANGUAGE,firebaseAnalytics);
            i = new Intent(SplashActivity.this, LanguageActivity.class);
        } else if (user.getAge() == null || Integer.parseInt(user.getAge()) < 5
                || Integer.parseInt(user.getAge()) > 100 || user.getUserDetail().getHeight() == null
                || user.getUserDetail().getWeight() == null || user.getUserDetail().getHoursOfSleep() == null
                || user.getUserDetail().getRegion() == null || user.getUserDetail().getFood() == null
                || TextUtils.isEmpty(user.getUserDetail().getFood()) || user.getUserDetail().getProfession() == null) {
            FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.AGE,firebaseAnalytics);
            i = new Intent(SplashActivity.this, AgeActivity.class);
        } else if (user.getDietPlan() == null) {
            FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.PRODUCTS,firebaseAnalytics);
            i = new Intent(SplashActivity.this, ProductListActivity.class);
            i.putExtra("plan","expired");
            sessionManager.setNoPlan(0);
        } else {
            FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.DASHBOARD,firebaseAnalytics);

            i = new Intent(SplashActivity.this, DashboardActivity1.class);
            if (getIntent().hasExtra("notification"))
                i.putExtra("notification", (Notification) getIntent().getParcelableExtra("notification"));
        }
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposable != null)
            disposable.dispose();
    }
}



