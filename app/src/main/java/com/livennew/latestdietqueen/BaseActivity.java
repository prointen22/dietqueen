package com.livennew.latestdietqueen;

import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.livennew.latestdietqueen.language.LocaleManager;

import java.util.List;

public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    boolean appendSeparator = false;
    StringBuilder sb;


    @Override
    protected void attachBaseContext(Context newBase) {
        SessionManager sessionManager = new SessionManager(newBase);
        super.attachBaseContext(new LocaleManager(sessionManager).setLocale(newBase));
        Log.d(TAG, "attachBaseContext");
    }

    public StringBuilder getMessageInSingleLine(List<String> message) {
        sb = new StringBuilder();
        for (int i = 0; i < message.size(); i++) {
            if (appendSeparator)
                sb.append('\n'); // a comma
            appendSeparator = true;
            sb.append(message.get(i));
        }
        return sb;
    }
}
