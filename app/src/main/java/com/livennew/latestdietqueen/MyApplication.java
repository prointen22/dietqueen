package com.livennew.latestdietqueen;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.livennew.latestdietqueen.language.LocaleManager;
import com.livennew.latestdietqueen.payu.AppEnvironment;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class MyApplication extends Application {
    private static final String TAG = "MyApplication";

    private static MyApplication mInstance;
    private AppEnvironment appEnvironment;
    private static LocaleManager localeManager;

    SessionManager sessionManager;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        appEnvironment = AppEnvironment.PRODUCTION;
    }

    @Override
    protected void attachBaseContext(Context base) {
        sessionManager=new SessionManager(base);
        localeManager = new LocaleManager(sessionManager);
        super.attachBaseContext(localeManager.setLocale(base));
        Log.d(TAG, "attachBaseContext");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        localeManager.setLocale(this);
        Log.d(TAG, "onConfigurationChanged: " + newConfig.locale.getLanguage());
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public static synchronized LocaleManager getLocaleManager() {
        return localeManager;
    }

    public AppEnvironment getAppEnvironment() {
        return appEnvironment;
    }


}
