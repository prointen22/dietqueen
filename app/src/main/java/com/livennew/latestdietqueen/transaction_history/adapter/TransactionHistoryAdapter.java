package com.livennew.latestdietqueen.transaction_history.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.transaction_history.model.TransactionHistoryPojo;

import java.util.ArrayList;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.MyViewHolder> {

    private ArrayList<TransactionHistoryPojo> transactionHistoryPojoArrayList;
    private Context context;


    public TransactionHistoryAdapter(Context context, ArrayList<TransactionHistoryPojo> transactionHistoryPojoArrayList) {
        this.context = context;
        this.transactionHistoryPojoArrayList = transactionHistoryPojoArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_history_content, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final TransactionHistoryPojo transactionHistoryPojo = transactionHistoryPojoArrayList.get(position);
        RequestOptions requestOptions = new RequestOptions();
        Glide.with(context)
                .load(transactionHistoryPojo.getImgTransactionHistory())
                .apply(requestOptions)
                .into(holder.ivTransactionHistory);
        holder.tvOrderNo.setText(transactionHistoryPojo.getOrderNumber());
        holder.tvTotalPayment.setText(transactionHistoryPojo.getTotalPayment());
        holder.tvOrderPaidBy.setText(transactionHistoryPojo.getPaidBy());
        holder.tvPaymentStatus.setText(transactionHistoryPojo.getPaymentStatus());
    }

    @Override
    public int getItemCount() {
        return transactionHistoryPojoArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivTransactionHistory;
        TextView tvOrderNo, tvTotalPayment, tvOrderPaidBy, tvPaymentStatus;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivTransactionHistory = itemView.findViewById(R.id.iv_transaction_history);
            tvOrderNo = itemView.findViewById(R.id.tv_order_no);
            tvTotalPayment = itemView.findViewById(R.id.tv_total_payment);
            tvOrderPaidBy = itemView.findViewById(R.id.tv_order_paid_by);
            tvPaymentStatus = itemView.findViewById(R.id.tv_payment_status);
        }
    }
}
