package com.livennew.latestdietqueen.transaction_history;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.transaction_history.adapter.TransactionHistoryAdapter;
import com.livennew.latestdietqueen.transaction_history.model.TransactionHistoryPojo;
import com.livennew.latestdietqueen.utils.SupportClass;

import java.util.ArrayList;

public class TransactionHistory extends FabActivity {
    private Toolbar toolbarTransactionHistory;
    private ArrayList<TransactionHistoryPojo> transactionHistoryPojoArrayList;
    private RecyclerView rvTransactionHistoryList;
    private ProgressDialog pDialog;
    private static final String TAG = "Erros";
    private TransactionHistoryAdapter transactionHistoryAdapter;
    private Button btnTransactionReload;
    private boolean error;
    private StringBuilder sb;
    private boolean appendSeparator = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        init();
        setSupportActionBar(toolbarTransactionHistory);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.transaction_history);
        }
        toolbarTransactionHistory.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);

        if (SupportClass.checkConnection(this)) {
            //  videoList();
            btnTransactionReload.setVisibility(View.GONE);
            rvTransactionHistoryList.setVisibility(View.VISIBLE);
        } else {
            SupportClass.noInternetConnectionToast(this);
            btnTransactionReload.setVisibility(View.VISIBLE);
            rvTransactionHistoryList.setVisibility(View.GONE);
        }

        btnTransactionReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SupportClass.checkConnection(TransactionHistory.this)) {
                    // videoList();
                    btnTransactionReload.setVisibility(View.GONE);
                    rvTransactionHistoryList.setVisibility(View.VISIBLE);
                } else {
                    SupportClass.noInternetConnectionToast(TransactionHistory.this);
                    btnTransactionReload.setVisibility(View.VISIBLE);
                    rvTransactionHistoryList.setVisibility(View.GONE);
                }
            }
        });

        rvTransactionHistoryData();
    }

    public void rvTransactionHistoryData() {
        transactionHistoryPojoArrayList = new ArrayList<TransactionHistoryPojo>();
        transactionHistoryAdapter = new TransactionHistoryAdapter(this, transactionHistoryPojoArrayList);
        LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(this);
        rvTransactionHistoryList.setLayoutManager(recyclerLayoutManager);
        rvTransactionHistoryList.setAdapter(transactionHistoryAdapter);
        transactionHistoryList();
    }

    public void init() {
        toolbarTransactionHistory = findViewById(R.id.toolbar_transaction_history);
        pDialog = new ProgressDialog(this);
        rvTransactionHistoryList = findViewById(R.id.rv_transaction_history_list);
        btnTransactionReload = findViewById(R.id.btn_transaction_history_reload);
        sb = new StringBuilder();
    }

    private void transactionHistoryList() {
        TransactionHistoryPojo transactionHistoryPojo = new TransactionHistoryPojo(1, R.drawable.sleep_hour, "#403-1442535-3229125", "20 Sep 2019", "2999", "Credit Card", "Successful");
        transactionHistoryPojoArrayList.add(transactionHistoryPojo);
        transactionHistoryPojo = new TransactionHistoryPojo(2, R.drawable.sleep_hour, "#403-1442535-3229125", "20 Sep 2019", "2999", "Credit Card", "Successful");
        transactionHistoryPojoArrayList.add(transactionHistoryPojo);
        transactionHistoryPojo = new TransactionHistoryPojo(3, R.drawable.sleep_hour, "#403-1442535-3229125", "20 Sep 2019", "2999", "Credit Card", "Successful");
        transactionHistoryPojoArrayList.add(transactionHistoryPojo);
        transactionHistoryPojo = new TransactionHistoryPojo(4, R.drawable.sleep_hour, "#403-1442535-3229125", "20 Sep 2019", "2999", "Credit Card", "Successful");
        transactionHistoryPojoArrayList.add(transactionHistoryPojo);
        transactionHistoryPojo = new TransactionHistoryPojo(5, R.drawable.sleep_hour, "#403-1442535-3229125", "20 Sep 2019", "2999", "Credit Card", "Successful");
        transactionHistoryPojoArrayList.add(transactionHistoryPojo);
        transactionHistoryPojo = new TransactionHistoryPojo(6, R.drawable.sleep_hour, "#403-1442535-3229125", "20 Sep 2019", "2999", "Credit Card", "Successful");
        transactionHistoryPojoArrayList.add(transactionHistoryPojo);
        transactionHistoryPojo = new TransactionHistoryPojo(7, R.drawable.sleep_hour, "#403-1442535-3229125", "20 Sep 2019", "2999", "Credit Card", "Successful");
        transactionHistoryPojoArrayList.add(transactionHistoryPojo);
        transactionHistoryAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
