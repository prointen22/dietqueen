package com.livennew.latestdietqueen.transaction_history.model;

public class TransactionHistoryPojo {
    int id, imgTransactionHistory;
    String orderNumber, orderPlaced, totalPayment, paidBy, paymentStatus;

    public TransactionHistoryPojo(int id, int imgTransactionHistory, String orderNumber, String orderPlaced, String totalPayment, String paidBy, String paymentStatus) {
        this.id = id;
        this.imgTransactionHistory = imgTransactionHistory;
        this.orderNumber = orderNumber;
        this.orderPlaced = orderPlaced;
        this.totalPayment = totalPayment;
        this.paidBy = paidBy;
        this.paymentStatus = paymentStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImgTransactionHistory() {
        return imgTransactionHistory;
    }

    public void setImgTransactionHistory(int imgTransactionHistory) {
        this.imgTransactionHistory = imgTransactionHistory;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderPlaced() {
        return orderPlaced;
    }

    public void setOrderPlaced(String orderPlaced) {
        this.orderPlaced = orderPlaced;
    }

    public String getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(String totalPayment) {
        this.totalPayment = totalPayment;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
