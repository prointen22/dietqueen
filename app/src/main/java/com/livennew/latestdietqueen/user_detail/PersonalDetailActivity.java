package com.livennew.latestdietqueen.user_detail;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;

import com.android.volley.RequestQueue;
import com.bumptech.glide.Glide;
import com.iceteck.silicompressorr.SiliCompressor;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.model.City;
import com.livennew.latestdietqueen.model.JobType;
import com.livennew.latestdietqueen.model.State;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.ExifUtil;
import com.livennew.latestdietqueen.utils.SupportClass;
import com.yalantis.ucrop.UCrop;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@AndroidEntryPoint
public class PersonalDetailActivity extends BaseActivity {
    public static final int UPDATE_USER_REQ = 87;
    CompositeDisposable disposable = new CompositeDisposable();
    private EditText etName, etState, etCity, etEmailId;
    private Button btnNext;
    private ImageView ivEditPhoto;
    private CircleImageView ivImage;
    private Toolbar toolbarPersonalDetail;
    private TextView toolbarTitle;
    private Uri file;
    private Bitmap profileImage;
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;

    private static final String TAG = "Erros";
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private RequestQueue rQueue;
    private NestedScrollView nsv;
    private RelativeLayout rlProgressBar;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private LinearLayoutCompat llCamera;
    User user;
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    private State selectedState;
    private City selectedCity;
    private LinearLayout llBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        user = sessionManager.getUser();
        init();
        setSupportActionBar(toolbarPersonalDetail);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.personal_detail_header);
        }
        toolbarPersonalDetail.setNavigationIcon(R.drawable.ic_navigation_before_pink_24dp);

        btnNext.setOnClickListener(v -> {
            if (isValidate()) {
                user.setName(etName.getText().toString());
                user.setState(selectedState);
                user.setCity(selectedCity);
                user.setEmail(etEmailId.getText().toString());

                user.getUserDetail().setJobType(JobType.WORKING);
                user.getUserDetail().setWakeUp("06:00 AM");
                user.getUserDetail().setSleep("11:00PM");
                user.getUserDetail().setOfficeHrsFrom("10:00 AM");
                user.getUserDetail().setOfficeHrsTo("07:00 PM");
                user.getUserDetail().setBreakfast("08:00 AM");
                user.getUserDetail().setLunch("01:00 PM");
                user.getUserDetail().setDinner("08:30 PM");
                if (SupportClass.checkConnection(this)) {
                    insertUserDetail(user);
                }else {
                    SupportClass.noInternetConnectionToast(this);
                }
//                Intent intent = new Intent(PersonalDetailActivity.this, WorkActivity.class);
//                intent.putExtra("user", user);
//                startActivityForResult(intent, UPDATE_USER_REQ);
            }
        });
        etState.setOnClickListener(v -> startActivityForResult(new Intent(PersonalDetailActivity.this, StateSelectionActivity.class), StateSelectionActivity.STATE_SELECTION_REQ));
        etCity.setOnClickListener(v -> {
            if (selectedState != null) {
                Intent intent = new Intent(PersonalDetailActivity.this, CitySelectionActivity.class);
                intent.putExtra("state", selectedState);
                startActivityForResult(intent, CitySelectionActivity.CITY_SELECTION_REQ);
            } else {
                Toast.makeText(PersonalDetailActivity.this, getString(R.string.please_select_state), Toast.LENGTH_SHORT).show();
            }
        });
        ivEditPhoto.setOnClickListener(view -> llCamera.callOnClick());
        llCamera.setOnClickListener(v -> {
            try {
//                displayImageSelectionDialog();
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        if (SupportClass.checkConnection(this)) {
            getUserDetail();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }

    }

    private void displayImageSelectionDialog() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(PersonalDetailActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                try {
                    if (ActivityCompat.checkSelfPermission(PersonalDetailActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(PersonalDetailActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        file = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".fileprovider", Objects.requireNonNull(SupportClass.getOutputMediaFile()));
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
                        startActivityForResult(intent, CAMERA_REQUEST);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (items[item].equals("Choose from Library")) {
                try {
                    if (ActivityCompat.checkSelfPermission(PersonalDetailActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(PersonalDetailActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, GALLERY_PICTURE);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        // Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_PICTURE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }

        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    file = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".fileprovider", Objects.requireNonNull(SupportClass.getOutputMediaFile()));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
                    startActivityForResult(intent, CAMERA_REQUEST);
                } else {
                    Toast.makeText(PersonalDetailActivity.this, "Please allow permission!!!", Toast.LENGTH_LONG).show();
                }
                break;

            case GALLERY_PICTURE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, GALLERY_PICTURE);
                } else {
                    Toast.makeText(PersonalDetailActivity.this, "Please allow permission!!!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST: {
                if (resultCode == RESULT_OK) {
                    try {
                        startCrop(file);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case GALLERY_PICTURE: {
                if (resultCode == RESULT_OK) {
                    try {
                        Uri selectedImage = data.getData();
                        String[] filePath = {MediaStore.MediaColumns.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        String picturePath = c.getString(columnIndex);
                        c.close();
                        Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                        thumbnail = getResizedBitmap(thumbnail, 400);
                        Bitmap orientedBitmap = ExifUtil.rotateBitmap(picturePath, thumbnail);
                        ivImage.setImageBitmap(orientedBitmap);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        orientedBitmap.compress(Bitmap.CompressFormat.PNG, 60, baos);
                        byte[] b = baos.toByteArray();
                        String imageOne = Base64.encodeToString(b, Base64.DEFAULT);
                        Log.d(TAG, "Image: " + imageOne);
                        //imageOne = Base64.encodeToString(ba, Base64.DEFAULT);
                        sendProfileImage(imageOne);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }

            case UCrop.REQUEST_CROP: {
                if (resultCode == RESULT_OK) {
                    final Uri resultUri = UCrop.getOutput(data);
                    handleCropResult(resultUri);
                } else if (resultCode == UCrop.RESULT_ERROR) {
                    final Throwable cropError = UCrop.getError(data);
                    handleCropError(cropError);
                }
                break;
            }
            case StateSelectionActivity.STATE_SELECTION_REQ: {
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        selectedState = data.getExtras().getParcelable(StateSelectionActivity.SELECTED_STATE);
                        etState.setText(selectedState.getName());
                        etCity.setText("");
                        selectedCity = null;
                    }
                }
                break;
            }
            case CitySelectionActivity.CITY_SELECTION_REQ: {
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        selectedCity = data.getExtras().getParcelable(CitySelectionActivity.SELECTED_CITY);
                        etCity.setText(selectedCity.getName());
                    }
                }
                break;
            }
            case UPDATE_USER_REQ: {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                finish();
                break;
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void startCrop(@NonNull Uri uri) {
        try {
            UCrop.Options option = new UCrop.Options();
            option.setCircleDimmedLayer(true);
            option.setShowCropFrame(false);
            option.setShowCropGrid(false);
            String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
            destinationFileName += ".png";
            UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
            uCrop.withOptions(option);
            // else start uCrop Activity
            uCrop.start(PersonalDetailActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void handleCropResult(Uri resultUri) {
        if (resultUri != null) {
            try {

                profileImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                if (profileImage.getRowBytes() > 5000)
                    profileImage = SiliCompressor.with(PersonalDetailActivity.this).getCompressBitmap(resultUri.getPath());
            } catch (Exception e) {
                e.printStackTrace();
            }
            Bitmap bmp = BitmapFactory.decodeFile(resultUri.getPath());
            // Getting width & height of the given image.
            int w = bmp.getWidth();
            int h = bmp.getHeight();
            // Setting post rotate to 90
            Matrix mtx = new Matrix();
            mtx.postRotate(90);
            Bitmap rotatedBMP = Bitmap.createBitmap(bmp, 0, 0, w, h, mtx, true);
            ivImage.setImageBitmap(rotatedBMP);
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            rotatedBMP.compress(Bitmap.CompressFormat.JPEG, 20, bao);
            byte[] ba = bao.toByteArray();
            String imageOne = Base64.encodeToString(ba, Base64.DEFAULT);
            Log.d(TAG, "Image: " + imageOne);
            sendProfileImage(imageOne);
        } else {
            Toast.makeText(PersonalDetailActivity.this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(Throwable cropError) {
//        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e(TAG, "handleCropError: ", cropError);
            Toast.makeText(PersonalDetailActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(PersonalDetailActivity.this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isValidate() {
        if (etName.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etState.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter State", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etCity.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter City", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etEmailId.getText().toString().isEmpty()) {
            return true;
        }
        if (!etEmailId.getText().toString().trim().matches(emailPattern)) {
            Toast.makeText(this, "Enter valid email id", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void init() {
        etName = findViewById(R.id.et_name);
        etState = findViewById(R.id.et_state);
        etCity = findViewById(R.id.et_city);
        etEmailId = findViewById(R.id.et_email_id);
        btnNext = findViewById(R.id.btn_next_personal_detail);
        toolbarPersonalDetail = findViewById(R.id.toolbar_personal_detail);
        toolbarTitle = toolbarPersonalDetail.findViewById(R.id.toolbarTitle);
        ivEditPhoto = findViewById(R.id.ivEditImage);
        ivImage = findViewById(R.id.iv_photo);
        nsv = findViewById(R.id.nsv);

        llBack = findViewById(R.id.ll_back);
        llBack.setOnClickListener(v -> {
            finish();
        });
        rlProgressBar = findViewById(R.id.rl_progress_bar);
        llCamera = findViewById(R.id.llCamera);
        if (!(user.getName()==null)){
            etName.setText(user.getName());
        }
        if (!(user.getEmail()==null)){
            etName.setText(user.getEmail());
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendProfileImage(String image) {
        rlProgressBar.setVisibility(View.VISIBLE);
        nsv.setVisibility(View.GONE);
        Map<String, String> params = new HashMap<String, String>();
        params.put("userId", String.valueOf(user.getId()));
        params.put("base64", "data:image/jpeg;base64,".concat(image));
        apiInterface.updateProfileImage(params).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NotNull Call<UserResp> call, @NonNull retrofit2.Response<UserResp> response) {
                if (!response.body().getError()) {
                    response.body().getData().setProfileImage(response.body().getOthers().getUserImageBase64Path() + response.body().getData().getProfileImage());
                    sessionManager.setUser(response.body().getData());
                    setImage(response.body().getData().getProfileImage());
                    rlProgressBar.setVisibility(View.GONE);
                    nsv.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(PersonalDetailActivity.this, getMessageInSingleLine(response.body().getMessage()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserResp> call, @NotNull Throwable t) {
                rlProgressBar.setVisibility(View.GONE);
                nsv.setVisibility(View.VISIBLE);
            }
        });

    }

    private void setImage(String imageUrl) {
        Glide
                .with(PersonalDetailActivity.this)
                .load(imageUrl)
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(ivImage);
    }


    private void getUserDetail() {
        nsv.setVisibility(View.GONE);
        rlProgressBar.setVisibility(View.VISIBLE);
        disposable.add(apiInterface.getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserResp>() {
                    @Override
                    public void onSuccess(@io.reactivex.annotations.NonNull UserResp userResp) {
                        user = userResp.getData();
                        user.setProfileImage(userResp.getOthers().getUserImageBase64Path() + user.getProfileImage());
                        if (!userResp.getError()) {
                            etName.setText(user.getName());
                            etEmailId.setText(user.getEmail());
                            setImage(user.getProfileImage());
                            selectedState = user.getState();
                            etState.setText(user.getState() != null ? user.getState().getName() : "");
                            selectedCity = user.getCity();
                            etCity.setText(selectedCity != null ? selectedCity.getName() : "");
                            sessionManager.setUser(user);
                        } else {
                            Toast.makeText(PersonalDetailActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_LONG).show();
                        }
                        nsv.setVisibility(View.VISIBLE);
                        rlProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        Toast.makeText(PersonalDetailActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }


    private void insertUserDetail(User user) {
        btnNext.setEnabled(false);
        rlProgressBar.setVisibility(View.VISIBLE);
        apiInterface.updateUserWorkDetails(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull Response<UserResp> response) {
                btnNext.setEnabled(true);
                UserResp userResp = response.body();
                if (!userResp.getError()) {
                    Toast.makeText(PersonalDetailActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                    try {
                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    sessionManager.setUser(userResp.getData());
                    Intent returnIntent = new Intent();
                    setResult(RESULT_OK, returnIntent);
                    finish();
                    rlProgressBar.setVisibility(View.GONE);
                } else {
                    for (int i = 0; i < userResp.getMessage().size(); i++) {
                        Toast.makeText(PersonalDetailActivity.this, userResp.getMessage().get(i), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserResp> call, @NotNull Throwable t) {
                btnNext.setEnabled(true);

            }
        });
    }
}
