package com.livennew.latestdietqueen.user_detail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.info_screen.weight_screen.WeightActivity;
import com.livennew.latestdietqueen.model.State;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.network_support.APIClient;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.user_detail.adapter.StateSelectionAdapter;
import com.livennew.latestdietqueen.user_detail.model.StateList;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;

@AndroidEntryPoint
public class StateSelectionActivity extends BaseActivity implements StateSelectionAdapter.ContactsAdapterListener {
    private static final String TAG = StateSelectionActivity.class.getSimpleName();
    public static final String SELECTED_STATE = "selectedState";
    public static final int STATE_SELECTION_REQ = 85;
    private RecyclerView mRecyclerView;
    private EditText edSearch;
    private Toolbar toolbar;
    private StateSelectionAdapter mAdapter;
    private ArrayList<State> stateList;
    private ProgressBar progressBar;
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    User user;
    private LinearLayout llBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_selection_new);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        apiInterface = APIClient.getClient(this).create(APIInterface.class);
        user = sessionManager.getUser();
        findViewById();
        initToolbar();
        //initView();
        if (SupportClass.checkConnection(this)) {
            getStats();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        /*mAdapter.setOnClickListener(state -> {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(SELECTED_STATE, (Parcelable) state);
            setResult(RESULT_OK, returnIntent);
            finish();
        });*/

    }



    private void getStats() {
        progressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        edSearch.setVisibility(View.GONE);
        apiInterface.getState().enqueue(new Callback<StateList>() {
            @Override
            public void onResponse(@NonNull Call<StateList> call, @NonNull retrofit2.Response<StateList> response) {
                try {
                    StateList stateListData = response.body();
                    progressBar.setVisibility(View.GONE);
                    assert stateList != null;
                    if (!stateListData.getError()) {
//                        Toast.makeText(WeightActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
//                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
//                        user.getUserDetail().setWeight(weightKg.toString());
//                        sessionManager.setUser(userResp.getData());
//                        sessionManager.setUser(user);
//                        navigateToHealthScreen();
//                        finish();

                        stateList.clear();
                        for (int i = 0; i < stateListData.getData().size(); i++) {
                            // adding contacts to contacts list
                            State state = stateListData.getData().get(i);
//                            State state = new State();
//                            state.setId(jsonObject.getInt("id"));
//                            state.setName(jsonObject.getString("state_name"));
                            stateList.add(state);
                        }
                        // refreshing recycler view
                        //mAdapter.setData(stateList);
//                                mAdapter.notifyDataSetChanged();
                        mAdapter = new StateSelectionAdapter(StateSelectionActivity.this, stateList, StateSelectionActivity.this);
                        LinearLayoutManager manager = new LinearLayoutManager(StateSelectionActivity.this);
                        mRecyclerView.setHasFixedSize(true);
                        mRecyclerView.setLayoutManager(manager);
                        mRecyclerView.setAdapter(mAdapter);
                        progressBar.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        edSearch.setVisibility(View.VISIBLE);
                    } else {
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(StateSelectionActivity.this);
//                        dialog.setMessage(getMessageInSingleLine(stateListData.getMessage()));
//                        dialog.setCancelable(false);
//                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                    }
                } catch (Exception e) {
                    Log.v("TAG", "Exception: "+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StateList> call, Throwable t) {
                Toast.makeText(StateSelectionActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Api.MAIN_URL + Api.GETSTATE, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            /*if (response == null) {
//                                Toast.makeText(getApplicationContext(), "Couldn't fetch the contacts! Pleas try again.", Toast.LENGTH_LONG).show();
//                                return;
//                            }*/
//                            JSONObject jObj = new JSONObject(response.toString());
//                            boolean error = jObj.getBoolean("error");
//                            if (!error) {
//                                JSONArray jsonArray = jObj.getJSONArray("data");
//                                //List<State> items = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<State>>() {}.getType());
//                                stateList.clear();
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    // adding contacts to contacts list
//                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                    State state = new State();
//                                    state.setId(jsonObject.getInt("id"));
//                                    state.setName(jsonObject.getString("state_name"));
//                                    stateList.add(state);
//                                }
//                                // refreshing recycler view
//                                //mAdapter.setData(stateList);
////                                mAdapter.notifyDataSetChanged();
//                                mAdapter = new StateSelectionAdapter(StateSelectionActivity.this, stateList, StateSelectionActivity.this);
//                                LinearLayoutManager manager = new LinearLayoutManager(StateSelectionActivity.this);
//                                mRecyclerView.setHasFixedSize(true);
//                                mRecyclerView.setLayoutManager(manager);
//                                mRecyclerView.setAdapter(mAdapter);
//                                progressBar.setVisibility(View.GONE);
//                                mRecyclerView.setVisibility(View.VISIBLE);
//                                edSearch.setVisibility(View.VISIBLE);
//                            } else {
//                                Toast.makeText(StateSelectionActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, error ->
//                Log.e(TAG, "response Error: " + error.getMessage())) {
//            /**
//             * Passing some request headers
//             */
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("Accept", "application/json");
//                headers.put("Authorization", user.getToken());
//                //           headers.put("userId", String.valueOf(pref.getUserId()));
//                return headers;
//            }
//        };
//
////        MyApplication.getInstance().addToRequestQueue(request);
//        request.setShouldCache(false);
//        // Adding request to request queue
//        RequestQueue requestQueue = Volley.newRequestQueue(StateSelectionActivity.this);
//        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        requestQueue.add(request);

    }

    private void findViewById() {
        // llProgress = (LinearLayout) findViewById(R.id.llProgress);
        edSearch = (EditText) findViewById(R.id.edSearch);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        stateList = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progress_bar);
        llBack =findViewById(R.id.ll_back);
        llBack.setOnClickListener(v->{
            finish();
        });
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(" ");
        }
        toolbar.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideSoftKeyboard(StateSelectionActivity.this);
    }


    /**
     * Hide the Soft Keyboard.
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);

        if (inputMethodManager == null)
            return;

        if (activity.getCurrentFocus() != null && activity.getCurrentFocus().getWindowToken() != null)
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onContactSelected(State state) {
        //Toast.makeText(getApplicationContext(), "Selected: " + state.getId() + ", " + state.getState_name(), Toast.LENGTH_LONG).show();
        Intent returnIntent = new Intent();
        returnIntent.putExtra(SELECTED_STATE, state);
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}
//setContentView(R.layout.activity_state_selection);