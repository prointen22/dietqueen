package com.livennew.latestdietqueen.user_detail.model;

import com.livennew.latestdietqueen.model.State;

import java.util.ArrayList;

public class StateList {
  ArrayList<State> data = new ArrayList<State>();
  private String message;
  private boolean error;
  private float status_code;


 // Getter Methods 

  public String getMessage() {
    return message;
  }

  public boolean getError() {
    return error;
  }

  public float getStatus_code() {
    return status_code;
  }

  public ArrayList<State> getData() {
    return data;
  }

  public void setData(ArrayList<State> data){
    this.data = data;
  }

  // Setter Methods

  public void setMessage( String message ) {
    this.message = message;
  }

  public void setError( boolean error ) {
    this.error = error;
  }

  public void setStatus_code( float status_code ) {
    this.status_code = status_code;
  }
}