package com.livennew.latestdietqueen.user_detail;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.ActivityWorkBinding;
import com.livennew.latestdietqueen.model.JobType;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserDetail;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class WorkActivity extends FabActivity {
    private TimePickerDialog timePickerDialog;
    private Calendar calendar;
    private int currentHour;
    private int currentMinute;
    // String amPm;
    private String msg = "working";
    private static final String TAG = "Erros";
    User user;
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    private ActivityWorkBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityWorkBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this,R.color.white));
        user = sessionManager.getUser();
        if (getIntent() != null)
            user = getIntent().getParcelableExtra("user");

        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.work);
        }
        binding.toolbar.setNavigationIcon(R.drawable.ic_navigation_before_pink_24dp);
        try {
            Glide
                    .with(this)
                    .load(sessionManager.getUser().getProfileImage())
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(binding.ivPhoto);
        } catch (Exception e) {
            e.printStackTrace();
        }


        binding.rbWorking.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                clearRadioChecked();
                binding.rbWorking.setChecked(true);
                binding.llOfficeHrs.setVisibility(View.VISIBLE);
            } else {
                binding.rbWorking.setChecked(false);
            }
        });
        binding.rbHouseWife.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                clearRadioChecked();
                binding.rbHouseWife.setChecked(true);
                binding.llOfficeHrs.setVisibility(View.GONE);
            } else {
                binding.rbHouseWife.setChecked(false);
            }
        });
        if (user.getUserDetail() != null) {
            UserDetail userDetail = user.getUserDetail();
            if (userDetail.getJobType() != null) {
                if (userDetail.getJobType().equals(JobType.WORKING)) {
                    binding.rbWorking.setChecked(true);
                } else {
                    binding.rbHouseWife.setChecked(true);
                }
            }
            if (userDetail.getWakeUp() != null) {
                binding.etWakeUp.setText(userDetail.getWakeUp());
            }
            if (userDetail.getSleep() != null) {
                binding.etSleep.setText(userDetail.getSleep());
            }
            if (userDetail.getOfficeHrsFrom() != null) {
                binding.etOfficeHrFrom.setText(userDetail.getOfficeHrsFrom());
            }
            if (userDetail.getOfficeHrsTo() != null) {
                binding.etOfficeHrTo.setText(userDetail.getOfficeHrsTo());
            }
            if (userDetail.getBreakfast() != null) {
                binding.etBreakfast.setText(userDetail.getBreakfast());
            }
            if (userDetail.getLunch() != null) {
                binding.etLunch.setText(userDetail.getLunch());
            }
            if (userDetail.getDinner() != null) {
                binding.etDinner.setText(userDetail.getDinner());
            }
        }
        binding.etWakeUp.setOnClickListener(v -> {
            calendar = Calendar.getInstance();
            currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            currentMinute = calendar.get(Calendar.MINUTE);

            timePickerDialog = new TimePickerDialog(WorkActivity.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                    boolean isPM = (hourOfDay >= 12);
                    binding.etWakeUp.setText(String.format("%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minutes, isPM ? "PM" : "AM"));
                }
            }, currentHour, currentMinute, false);

            timePickerDialog.show();
        });

        binding.etSleep.setOnClickListener(v -> {
            calendar = Calendar.getInstance();
            currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            currentMinute = calendar.get(Calendar.MINUTE);

            timePickerDialog = new TimePickerDialog(WorkActivity.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                    boolean isPM = (hourOfDay >= 12);
                    binding.etSleep.setText(String.format("%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minutes, isPM ? "PM" : "AM"));
                }
            }, currentHour, currentMinute, false);

            timePickerDialog.show();
        });

        binding.etOfficeHrFrom.setOnClickListener(v -> {
            calendar = Calendar.getInstance();
            currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            currentMinute = calendar.get(Calendar.MINUTE);

            timePickerDialog = new TimePickerDialog(WorkActivity.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                    boolean isPM = (hourOfDay >= 12);
                    binding.etOfficeHrFrom.setText(String.format("%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minutes, isPM ? "PM" : "AM"));

                }
            }, currentHour, currentMinute, false);

            timePickerDialog.show();
        });

        binding.etOfficeHrTo.setOnClickListener(v -> {
            calendar = Calendar.getInstance();
            currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            currentMinute = calendar.get(Calendar.MINUTE);

            timePickerDialog = new TimePickerDialog(WorkActivity.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                    boolean isPM = (hourOfDay >= 12);
                    binding.etOfficeHrTo.setText(String.format("%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minutes, isPM ? "PM" : "AM"));

                }
            }, currentHour, currentMinute, false);

            timePickerDialog.show();
        });

        binding.etBreakfast.setOnClickListener(v -> {
            calendar = Calendar.getInstance();
            currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            currentMinute = calendar.get(Calendar.MINUTE);

            timePickerDialog = new TimePickerDialog(WorkActivity.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                    boolean isPM = (hourOfDay >= 12);
                    binding.etBreakfast.setText(String.format("%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minutes, isPM ? "PM" : "AM"));
                }
            }, currentHour, currentMinute, false);

            timePickerDialog.show();
        });

        binding.etLunch.setOnClickListener(v -> {
            calendar = Calendar.getInstance();
            currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            currentMinute = calendar.get(Calendar.MINUTE);

            timePickerDialog = new TimePickerDialog(WorkActivity.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                    boolean isPM = (hourOfDay >= 12);
                    binding.etLunch.setText(String.format("%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minutes, isPM ? "PM" : "AM"));
                }
            }, currentHour, currentMinute, false);

            timePickerDialog.show();
        });

        binding.etDinner.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            // TODO Auto-generated method stub
            calendar = Calendar.getInstance();
            currentHour = calendar.get(Calendar.HOUR);
            currentMinute = calendar.get(Calendar.MINUTE);

            timePickerDialog = new TimePickerDialog(WorkActivity.this, R.style.datepicker, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                    boolean isPM = (hourOfDay >= 12);
                    binding.etDinner.setText(String.format("%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minutes, isPM ? "PM" : "AM"));
                }
            }, currentHour, currentMinute, false);
            timePickerDialog.show();
        });


        binding.btnSubmitWork.setOnClickListener(View -> {
            if (isValidate()) {
                if (SupportClass.checkConnection(this)) {
                    user.getUserDetail().setJobType(binding.rbWorking.isChecked() ? JobType.WORKING : JobType.HOUSE_WIFE);
                    user.getUserDetail().setWakeUp(binding.etWakeUp.getText().toString());
                    user.getUserDetail().setSleep(binding.etSleep.getText().toString());
                    user.getUserDetail().setOfficeHrsFrom(binding.etOfficeHrFrom.getText().toString());
                    user.getUserDetail().setOfficeHrsTo(binding.etOfficeHrTo.getText().toString());
                    user.getUserDetail().setBreakfast(binding.etBreakfast.getText().toString());
                    user.getUserDetail().setLunch(binding.etLunch.getText().toString());
                    user.getUserDetail().setDinner(binding.etDinner.getText().toString());
                    insertUserDetail(user);
                } else {
                    SupportClass.noInternetConnectionToast(this);
                }
            }
        });
    }

    public void clearRadioChecked() {
        binding.rbWorking.setChecked(false);
        binding.rbHouseWife.setChecked(false);
    }

    private boolean isValidate() {
        if ((!binding.rbWorking.isChecked()) && (!binding.rbHouseWife.isChecked())) {
            Toast.makeText(this, "Select Job type", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (binding.etWakeUp.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Wake Up Time", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (binding.etSleep.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Sleep Time", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (binding.rbWorking.isChecked()) {
            if (binding.etOfficeHrFrom.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter Office from", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (binding.etOfficeHrTo.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter Office to", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if (binding.etBreakfast.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Breakfast time", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (binding.etLunch.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Lunch time", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (binding.etDinner.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Dinner time", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void insertUserDetail(User user) {
        binding.btnSubmitWork.setEnabled(false);
        apiInterface.updateUserWorkDetails(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull Response<UserResp> response) {
                binding.btnSubmitWork.setEnabled(true);
                UserResp userResp = response.body();
                if (!userResp.getError()) {
                    Toast.makeText(WorkActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                    try {
                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    sessionManager.setUser(userResp.getData());
                    Intent returnIntent = new Intent();
                    setResult(RESULT_OK, returnIntent);
                    finish();
                } else {
                    for (int i = 0; i < userResp.getMessage().size(); i++) {
                        Toast.makeText(WorkActivity.this, userResp.getMessage().get(i), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserResp> call, @NotNull Throwable t) {
                binding.btnSubmitWork.setEnabled(true);

            }
        });
    }

}
