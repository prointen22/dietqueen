package com.livennew.latestdietqueen.user_detail;

import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceResp;
import com.livennew.latestdietqueen.info_screen.WaistlineActivity;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.SupportClass;
import com.livennew.latestdietqueen.utils.TopBarUtil;
import com.livennew.latestdietqueen.dashboard.DashboardSubActivity;
import com.livennew.latestdietqueen.databinding.ActivityProfileBinding;
import com.livennew.latestdietqueen.diet_info.InstructionActivity;

import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceActivityNew;
import com.livennew.latestdietqueen.info_screen.age_screen.AgeActivity;
import com.livennew.latestdietqueen.info_screen.food_preference_screen.FoodPreferencesActivity;
import com.livennew.latestdietqueen.info_screen.height_screen.HeightActivity;
import com.livennew.latestdietqueen.info_screen.height_screen.HeightActivityEdit;
import com.livennew.latestdietqueen.info_screen.sleep_screen.HoursOfSleepActivity;
import com.livennew.latestdietqueen.info_screen.weight_screen.WeightActivity;

import com.livennew.latestdietqueen.language.LanguageActivity;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.notification.NotificationListActivity;
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class AchievementsActivity extends AppCompatActivity {
    private Toolbar toolbarAchievement;
    private CircleImageView ivProfile;
    private TextView tvUserNm, tvLocation, tvWeight, tvHeight, tvBMI, tvAchiv1, tvAchive2, tvAchive3, tvAchive4;
    private ImageView ivAchive1, ivAchive2, ivAchive3, ivAchive4;
    private LinearLayoutCompat llSubscription, llLanguage, llTransactionHistory, llUserDetail,ll_myprofile;
    private static final String TAG = "Errors";
    private ProgressBar progressBar;
    TextView  tvt_weight_edit,tvt_height_edit;
    ArrayList<Integer> id;
    User user;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;
    ActivityProfileBinding mBinding;
    private static FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        mBinding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        setContentView(R.layout.activity_achievements);
        user = sessionManager.getUser();
        ImageView mIvNotification = findViewById(R.id.iv_notification);
        ImageView mIvDoctor = findViewById(R.id.iv_doctor);
        ImageView mIvSideMenu = findViewById(R.id.iv_menu);
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager);
        init();
//        initToolbar();
        mBinding.tvLanguageUpdate.setOnClickListener(View -> {
            Intent intent = new Intent(AchievementsActivity.this, LanguageActivity.class);
            intent.putExtra("achievment", "achievment");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });
        progressBar = findViewById(R.id.progress_bar);
        if (SupportClass.checkConnection(this)) {
//            getAchivement();
            getFitnessAdvice();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }
        /*init();
        setSupportActionBar(toolbarAchievement);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if (SupportClass.checkConnection(this)) {
            getAchivement();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }
        toolbarAchievement.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
        llTransactionHistory.setOnClickListener(View -> {
            Intent intent = new Intent(AchievementsActivity.this, TransactionHistory.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });
        llLanguage.setOnClickListener(View -> {
            Intent intent = new Intent(AchievementsActivity.this, LanguageActivity.class);
            intent.putExtra("achievment", "achievment");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });

        ll_myprofile.setOnClickListener(View -> {
            Intent intent = new Intent(AchievementsActivity.this, FitnessAdviceActivityNew.class);
            intent.putExtra("achievment", "achievment");
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        });

        llSubscription.setOnClickListener(View -> {
            Intent intent = new Intent(AchievementsActivity.this, DashboardActivity1.class);
            intent.putExtra("navigate", 2);
            startActivity(intent);
            finish();
        });
        llUserDetail.setOnClickListener(View -> {
            Intent intent = new Intent(AchievementsActivity.this, PersonalDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });
        tvt_weight_edit.setOnClickListener(View -> {
            Intent intent = new Intent(AchievementsActivity.this, AddMyWeightActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });

        tvt_height_edit.setOnClickListener(View -> {
            Intent intent = new Intent(AchievementsActivity.this, HeightActivityEdit.class);
            intent.putExtra("user", user);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });*/
    }

    private void initToolbar() {
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(null);
//
//        setSupportActionBar(toolbar);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setTitle("");
//        }

        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        user = sessionManager.getUser();
//        setUserDetails();
    }

    @Override
    protected void onResume() {
        super.onResume();
        user = sessionManager.getUser();
        setUserDetails();
    }

    private void init() {
        id = new ArrayList<>();
        /*toolbarAchievement = findViewById(R.id.toolbar_achievment);
        llTransactionHistory = findViewById(R.id.ll_transaction_history);
        ll_myprofile = findViewById(R.id.ll_myprofile);
        llLanguage = findViewById(R.id.ll_language);
        llSubscription = findViewById(R.id.ll_subscription);
        llUserDetail = findViewById(R.id.ll_user_detail);
        ivProfile = findViewById(R.id.imageViewProfile);
        tvUserNm = findViewById(R.id.tv_user_nm);
        tvLocation = findViewById(R.id.tv_user_locaion);

        tvWeight = findViewById(R.id.tv_weight);
        tvHeight = findViewById(R.id.tv_height);
        tvt_weight_edit= findViewById(R.id.tvt_weight_edit);
        tvt_height_edit= findViewById(R.id.tvt_height_edit);
        tvBMI = findViewById(R.id.tv_bmi);
        tvAchiv1 = findViewById(R.id.tv_achi1);
        ivAchive1 = findViewById(R.id.iv_achi1);
        tvAchive2 = findViewById(R.id.tv_achi2);
        ivAchive2 = findViewById(R.id.iv_achi2);
        tvAchive3 = findViewById(R.id.tv_achi3);
        ivAchive3 = findViewById(R.id.iv_achi3);
        tvAchive4 = findViewById(R.id.tv_achi4);
        ivAchive4 = findViewById(R.id.iv_achi4);*/

        findViewById(R.id.iv_notification).setOnClickListener(v -> navigateToNotificationScreen());
        findViewById(R.id.iv_doctor).setOnClickListener(v -> startActivity(new Intent(this, InstructionActivity.class)));

        mBinding.ivEdit.setOnClickListener(v-> navigateToPersonalDetailScreen());
        mBinding.imageViewProfile.setOnClickListener(v-> navigateToPersonalDetailScreen());
        mBinding.cvWeight.setOnClickListener(v->{
            navigateToWeightActivity();
        });
        mBinding.cvSleep.setOnClickListener(v->{
            navigateToHoursOfSleepActivity();
        });
        mBinding.cvFood.setOnClickListener(v->{
            navigateToFoodPrefernceActivity();
        });

        mBinding.cvWaist.setOnClickListener(v->{
           navigateToWaistActivity();
        });

        mBinding.cvHeight.setOnClickListener(v->{
            navigateToHeightActivity();
        });
        mBinding.tvPlanDetail.setOnClickListener(v-> navigatePricingScreen());
        mBinding.ivCompBar.setOnClickListener(v-> Toast.makeText(this,"Coming Soon",Toast.LENGTH_SHORT).show());
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void getAchivement() {
        progressBar.setVisibility(View.VISIBLE);
        // rvFAQ.setVisibility(View.GONE);
        //JSONObject params = new JSONObject();
        // params.put("type", "exclusive");
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.GET, Api.MAIN_URL + Api.GETACHIVEMENT, null, response -> {
            Log.d(TAG, "onResponse Response: " + response);
            progressBar.setVisibility(View.GONE);
            //   rvFAQ.setVisibility(View.VISIBLE);
            try {
                JSONObject jObj = new JSONObject(response.toString());
                boolean error = jObj.getBoolean("error");
                if (!error) {
                    JSONObject jsonObject = jObj.getJSONObject("data");
                    String bmi = String.valueOf(jsonObject.getDouble("bmi"));
//                    tvBMI.setText(bmi);
                    mBinding.tvBmiValue.setText(bmi);
                    mBinding.ivWeightArrow.setVisibility(View.GONE);
                    mBinding.tvIdealWeight.setVisibility(View.VISIBLE);
                    if (jsonObject.getString("weightType").equals("Loss")){
                        mBinding.tvIdealWeight.
                                setText("Ideal body weight: "+(Integer.parseInt(user.getUserDetail().getWeight())
                                        - Integer.parseInt(jsonObject.getString("lossAndGain"))+"kg"));
                    }else{
                        mBinding.ivWeightArrow.setRotation(180.0f);
                        mBinding.tvIdealWeight.setText(
                                "Ideal body weight: "+(Integer.parseInt(user.getUserDetail().getWeight()) + Integer.parseInt(jsonObject.getString("lossAndGain"))+"kg"));
                    }
//                    tvHeight.setText(jsonObject.getString("latest_height").concat(" ").concat(getString(R.string.cm)));
//                    tvWeight.setText(jsonObject.getString("weight").concat(" ").concat(getString(R.string.kg)));

                    JSONArray jsonArray = jsonObject.getJSONArray("latest_achivements");
                    if (jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            id.add(i);
                        }
                        if (id != null) {
                            for (int j = 0; j < id.size(); j++) {
                                if (id.get(j) == 0) {
                                    ivAchive1.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_award_orange));
                                    tvAchiv1.setText(getResources().getString(R.string.fst_achive));
                                }
                                if (id.get(j) == 1) {
                                    ivAchive2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_award_green));
                                    tvAchive2.setText(getResources().getString(R.string.scnd_achive));
                                }
                                if (id.get(j) == 2) {
                                    ivAchive3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_award_orange));
                                    tvAchive3.setText(getResources().getString(R.string.trd_achive));
                                }
                                if (id.get(j) == 3) {
                                    ivAchive4.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_award_green));
                                    tvAchive4.setText(getResources().getString(R.string.frth_achive));
                                }
                            }
                        }
                    }
                } else {
                    //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                    Toast.makeText(AchievementsActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                headers.put("Authorization", user.getToken());
                headers.put("userId", String.valueOf(user.getId()));
                return headers;
            }
        };
        strReq.setShouldCache(false);
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(AchievementsActivity.this);
        strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(strReq);
    }

    public void getFitnessAdvice() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            apiInterface.getFitnessAdvice().enqueue(new Callback<FitnessAdviceResp>() {
                @Override
                public void onResponse(@NotNull Call<FitnessAdviceResp> call, @NotNull Response<FitnessAdviceResp> response) {
                    if (!response.body().getError()) {
//
                        mBinding.tvBmiValue.setText(response.body().getData().getBmi());
                        mBinding.tvIdealBmi.setText("Ideal BMI: 18 to 23");
                        mBinding.ivBmiArrow.setVisibility(View.VISIBLE);
                        if(Double.parseDouble(response.body().getData().getBmi())<18.00){
                            mBinding.ivBmiArrow.setRotation(180.0f);
                        }else if(Double.parseDouble(response.body().getData().getBmi())>23.00){
//                            mBinding.ivBmiArrow.setRotation(180.0f);
                        }else{
                            mBinding.ivBmiArrow.setVisibility(View.GONE);
                        }


                        mBinding.ivWeightArrow.setVisibility(View.GONE);
                        mBinding.tvIdealWeight.setVisibility(View.VISIBLE);
                        if (response.body().getData().getWeightType().equals("Loss")){
                            mBinding.tvIdealWeight.
                                    setText("Ideal body weight: "+(Integer.parseInt(user.getUserDetail().getWeight())
                                            - Integer.parseInt(response.body().getData().getLossAndGain())+"kg"));
                        }else{
                            mBinding.ivWeightArrow.setRotation(180.0f);
                            mBinding.tvIdealWeight.setText(
                                    "Ideal body weight: "+(Integer.parseInt(user.getUserDetail().getWeight()) + Integer.parseInt(response.body().getData().getLossAndGain())+"kg"));
                        }

//                        wcText.setText(response.body().getData().getWeightType().equals("Loss")?"I am overweight":"I am underweight");



                    } else {
                        for (int i = 0; i < response.body().getMessage().size(); i++) {
                            Toast.makeText(AchievementsActivity.this, response.body().getMessage().get(i).toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NotNull Call<FitnessAdviceResp> call, @NotNull Throwable t) {
                    Toast.makeText(AchievementsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setUserDetails() {

        if (user.getDietPlan().getDietPlaDetails().getId() == 60){
        String planBasic = user.getDietPlan().getDietPlaDetails().getPlan_name() + ", ";
        String planDetail = planBasic +"\n"+ getString(R.string.move_to_advance);
        Spannable spannable = new SpannableString(planDetail);
        spannable.setSpan(new ForegroundColorSpan(getColor(R.color.orange_theme)), planBasic.length(), planDetail.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        mBinding.tvPlanDetail.setText(spannable, TextView.BufferType.SPANNABLE);
        }else{
            mBinding.tvPlanDetail.setText(user.getDietPlan().getDietPlaDetails().getPlan_name());
        }

        try {
            Glide
                    .with(this)
                    .load(sessionManager.getUser().getProfileImage())
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(mBinding.imageViewProfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user.getName() != null) {
            mBinding.tvName.setText(user.getName());
        } else {
            mBinding.tvName.setVisibility(View.GONE);
        }

        if (user.getUserDetail().getProfession()!=null){
            mBinding.tvProfession.setText(user.getUserDetail().getProfession().getName());
        }else{
            mBinding.tvProfession.setVisibility(View.GONE);
        }

        String location = "";
        if (user.getCity() != null) {
            location = user.getCity().getName();
        }
        if (user.getState()!=null) {
            location += " - "+user.getState().getName();
        }

        if (!TextUtils.isEmpty(location)) {
            mBinding.tvLocation.setText(location);
        } else {
            mBinding.tvLocation.setVisibility(View.GONE);
        }

        String id = "";
        if (user.getId()!=0) {
            id = "Id. " + user.getId();
        }

        if(!TextUtils.isEmpty(id)) {
            mBinding.tvId.setText(id);
        } else {
            mBinding.tvId.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(user.getMobile())) {
            mBinding.tvMobileNumber.setText(user.getMobile());
        } else {
            mBinding.tvMobileNumber.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(user.getEmail())) {
            mBinding.tvEmail.setText(user.getEmail());
        } else {
            mBinding.tvEmail.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(user.getAge())) {
            mBinding.tvAgeValue.setText(user.getAge()+" yrs");
        } else {
            mBinding.tvAgeValue.setText("");
        }

        if (!TextUtils.isEmpty(user.getAge())) {
            mBinding.tvAge.setText(user.getAge()+" yrs");
        } else {
            mBinding.tvAge.setText("");
        }

        if (!TextUtils.isEmpty(user.getUserDetail().getHeight())) {
            mBinding.tvHeight.setText(user.getUserDetail().getHeight()+" cms");
        } else {
            mBinding.tvHeight.setText("");
        }

        int height = 0;
        if (!TextUtils.isEmpty(user.getUserDetail().getHeight())) {
            mBinding.tvHeightValue.setText(user.getUserDetail().getHeight()+" cms");
            height = Integer.parseInt(user.getUserDetail().getHeight());
        } else {
            mBinding.tvHeightValue.setText("");
        }

        if (!TextUtils.isEmpty(user.getUserDetail().getWeight())) {
            mBinding.tvWeightValue.setText(user.getUserDetail().getWeight()+" kgs");
        } else {
            mBinding.tvWeightValue.setText("");
        }


        if (user.getUserDetail().getWaistline()!=null) {
            mBinding.tvWaistValue.setText(Float.parseFloat(user.getUserDetail().getWaistline()) + " cms");
//            mBinding.ivWaistArrow.setVisibility(View.VISIBLE);

            if (height > 0) {
                float a = height / 2;
                float b = a + 5;
                mBinding.tvIdealWaist.setText("Ideal waist line: " + a + "-" + b + " cms");
//                mBinding.ivWaistArrow.setVisibility(View.VISIBLE);
                if (a > Float.parseFloat(user.getUserDetail().getWaistline())) {
//                    mBinding.ivWaistArrow.setRotation(180.0f);
                } else if (b < Float.parseFloat(user.getUserDetail().getWaistline())) {
                } else {
//                    mBinding.ivWaistArrow.setVisibility(View.GONE);
                }

            }
        }else{
            mBinding.tvWaistValue.setText("0.00 cms");
//            mBinding.ivWaistArrow.setVisibility(View.VISIBLE);
//            mBinding.ivWaistArrow.setRotation(180.0f);
            if (height > 0) {
                float a = height / 2;
                float b = a + 5;
                mBinding.tvIdealWaist.setText("Ideal waist line: " + a + "-" + b + " cms");
            }
        }


        if (!TextUtils.isEmpty(user.getUserDetail().getHoursOfSleep())) {
            mBinding.tvSleepValue.setText(user.getUserDetail().getHoursOfSleep()+" Hrs");
        }else {
            mBinding.tvSleepValue.setText("");
        }
        mBinding.tvIdealSleep.setText("Min Sleep: 7 hrs daily");

        mBinding.ivSleepArrow.setVisibility(View.VISIBLE);
        if (Integer.parseInt(user.getUserDetail().getHoursOfSleep())>7){

        }else if (Integer.parseInt(user.getUserDetail().getHoursOfSleep())<7){
            mBinding.ivSleepArrow.setRotation(180.0f);
        }else {
            mBinding.ivSleepArrow.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(user.getUserDetail().getFood())) {
            String foodValue = "";
            if (user.getUserDetail().getFood().equals("v")){
                foodValue = "Veg";
            }else if (user.getUserDetail().getFood().equals("ve")){
                foodValue = "Veg + Egg";
            }else if (user.getUserDetail().getFood().equals("nv")){
                foodValue = "Non Veg";
            }else{
                foodValue = "Veg";
            }
            mBinding.tvFoodValue.setText(foodValue+" : "+user.getUserDetail().getRegion().getName());
        }else {
            mBinding.tvFoodValue.setText("");
        }

        if (user.getUserDetail().getLanguage().getName()!=null) {
            mBinding.tvLanguage.setText(user.getUserDetail().getLanguage().getName());
        } else {
            mBinding.tvLanguage.setText("");
        }


        if (user.getUserDetail().getProfession().getName()!=null) {
            mBinding.tvProfession.setText(user.getUserDetail().getProfession().getName());
        } else {
            mBinding.tvProfession.setText("");
        }
        mBinding.cvAge.setOnClickListener(v->{
            navigateAge();
        });
    }

    private void navigateAge() {
        Intent intent = new Intent(AchievementsActivity.this, AgeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("operation", "update");
        intent.putExtra("user", user);
        startActivity(intent);
    }

    private void navigateToWeightActivity() {

        Intent intent = new Intent(AchievementsActivity.this, WeightActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("operation", "update");
        intent.putExtra("user", user);
        startActivityForResult(intent, 0000);

    }

    private void navigateToHeightActivity() {

        Intent intent = new Intent(AchievementsActivity.this, HeightActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("operation", "update");
        intent.putExtra("user", user);
        startActivity(intent);

    }


    private void navigateToHoursOfSleepActivity() {
        Intent intent = new Intent(AchievementsActivity.this, HoursOfSleepActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("operation", "update");
        intent.putExtra("user", user);
        startActivity(intent);
    }

    private void navigateToFoodPrefernceActivity() {
        Intent intent = new Intent(AchievementsActivity.this, FoodPreferencesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("operation", "update");
        intent.putExtra("user", user);
        startActivity(intent);
    }

    private void navigateToWaistActivity() {
        Intent intent = new Intent(AchievementsActivity.this, WaistlineActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("operation", "update");
        intent.putExtra("user", user);
        startActivity(intent);
    }

    private void navigateToNotificationScreen() {
        FireBaseAnalyticsUtil.screenViewEvent("", firebaseAnalytics);
        Intent intent = new Intent(AchievementsActivity.this, NotificationListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToPersonalDetailScreen() {
        Intent intent = new Intent(AchievementsActivity.this, PersonalDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigatePricingScreen() {
        Intent intent = new Intent(AchievementsActivity.this, DashboardSubActivity.class);
        intent.putExtra("type", getString(R.string.pricing));
        startActivity(intent);
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0000: {
                if (SupportClass.checkConnection(this)) {
//            getAchivement();
                    getFitnessAdvice();
                } else {
                    SupportClass.noInternetConnectionToast(this);
                }
            }
        }
    }
}
