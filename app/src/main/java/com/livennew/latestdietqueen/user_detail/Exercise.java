package com.livennew.latestdietqueen.user_detail;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.user_detail.adapter.ExerciseAdapter;
import com.livennew.latestdietqueen.user_detail.model.AddExercisePojo;
import com.livennew.latestdietqueen.utils.SupportClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercise extends FabActivity {
    private Toolbar toolbarExercise;
    private RecyclerView rvExercise;
    private ExerciseAdapter exerciseAdapter;
    private TextView tvAddExercise;
    private Spinner spnrExerciseType, spnrExerciseTime, spnExerciseReport;
    private Button btnAddExercise, btnNextExercise;
    private TextView tvBackArrow;
    private ArrayList<AddExercisePojo> alAddExercise = new ArrayList<>();
    String[] exerciseType = new String[]{
            "Select Exercise Type",
            "ABS Exercise",
            "Bycept",
            "Tricept",
            "Wings"
    };
    String[] exerciseTime = new String[]{
            "Select Exercise Time",
            "10 mins",
            "20 mins",
            "30 mins",
            "40 mins"
    };

    String[] exerciseReport = new String[]{
            "Select Exercise Report",
            "Daily",
            "Weekly",
            "Hourly"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);
        init();
        setSupportActionBar(toolbarExercise);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbarExercise.setNavigationIcon(R.drawable.ic_navigation_before_pink_24dp);

        exerciseAdapter = new ExerciseAdapter(this, alAddExercise);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvExercise.setLayoutManager(mLayoutManager);
        rvExercise.setItemAnimator(new DefaultItemAnimator());
        rvExercise.setAdapter(exerciseAdapter);
        btnNextExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Exercise.this, WorkActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        tvAddExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Exercise.this);
                dialog.setContentView(R.layout.add_exercise_popup_box);
                dialog.setCanceledOnTouchOutside(false);
                tvBackArrow = dialog.findViewById(R.id.toolbarTitle);
                spnrExerciseType = dialog.findViewById(R.id.spnr_type);
                spnrExerciseTime = dialog.findViewById(R.id.spnr_time);
                spnExerciseReport = dialog.findViewById(R.id.spnr_report);
                btnAddExercise = dialog.findViewById(R.id.btn_add_exercise);
                reloadFirstSpinner();
                reloadSecondSpinner();
                reloadThirdSpinner();
                tvBackArrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(android.view.View v) {
                        dialog.dismiss();
                    }
                });
                btnAddExercise.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(android.view.View v) {
                        if (validation()) {
                            if (SupportClass.checkConnection(Exercise.this)) {
                                //This code is temporary commented
                                passData();
                                dialog.dismiss();
                            } else {
                                Toast.makeText(Exercise.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
                dialog.show();
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT);
            }
        });
    }

    private void reloadFirstSpinner() {
        final List<String> plantsList = new ArrayList<>(Arrays.asList(exerciseType));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spnrExerciseType.setAdapter(spinnerArrayAdapter);

        spnrExerciseType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void reloadSecondSpinner() {
        final List<String> plantsList = new ArrayList<>(Arrays.asList(exerciseTime));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spnrExerciseTime.setAdapter(spinnerArrayAdapter);

        spnrExerciseTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void reloadThirdSpinner() {
        final List<String> plantsList = new ArrayList<>(Arrays.asList(exerciseReport));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spnExerciseReport.setAdapter(spinnerArrayAdapter);

        spnExerciseReport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void passData() {
        String exerciseType = "", exerciseTime = "", exerciseReport = "";
        exerciseType = String.valueOf(spnrExerciseType.getSelectedItem());
        exerciseTime = String.valueOf(spnrExerciseTime.getSelectedItem());
        exerciseReport = String.valueOf(spnExerciseReport.getSelectedItem());

        AddExercisePojo addExercisePojo = new AddExercisePojo(exerciseType, exerciseTime, exerciseReport);
        alAddExercise.add(addExercisePojo);
        exerciseAdapter.notifyDataSetChanged();

    }

    private boolean validation() {
        if (spnrExerciseType.getSelectedItemPosition() == 0) {
            Toast.makeText(this, getString(R.string.pleaseSelectExercise), Toast.LENGTH_SHORT).show();
        } else if (spnrExerciseTime.getSelectedItemPosition() == 0) {
            Toast.makeText(this, getString(R.string.pleaseSelectExerciseTime), Toast.LENGTH_SHORT).show();
        } else if (spnExerciseReport.getSelectedItemPosition() == 0) {
            Toast.makeText(this, getString(R.string.pleaseSelectExerciseReport), Toast.LENGTH_SHORT).show();
        } else {
            return true;
        }
        return false;
    }

    private void init() {
        toolbarExercise = findViewById(R.id.toolbar_exercise);
        rvExercise = findViewById(R.id.rv_add_exercise);
        tvAddExercise = findViewById(R.id.tv_add_exercise);
        btnNextExercise = findViewById(R.id.btn_next_exercise);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
