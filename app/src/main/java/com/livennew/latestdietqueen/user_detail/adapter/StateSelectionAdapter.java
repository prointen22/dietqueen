package com.livennew.latestdietqueen.user_detail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.model.State;

import java.util.ArrayList;
import java.util.List;


public class StateSelectionAdapter extends RecyclerView.Adapter<StateSelectionAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<State> stateList;
    private List<State> stateListFiltered;
    private ContactsAdapterListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView view/*, phone*/;
        //public ImageView thumbnail;

        public MyViewHolder(View itemView) {
            super(itemView);
            // name = view.findViewById(R.id.et_state);
            view = (TextView) itemView;
            // phone = view.findViewById(R.id.phone);
            //thumbnail = view.findViewById(R.id.thumbnail);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(stateListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public StateSelectionAdapter(Context context, List<State> stateList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.stateList = stateList;
        this.stateListFiltered = stateList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.state_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final State state = stateListFiltered.get(position);
        holder.view.setText(state.getName());
    }

    @Override
    public int getItemCount() {
        return stateListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    stateListFiltered = stateList;
                } else {
                    List<State> filteredList = new ArrayList<>();
                    for (State row : stateList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    stateListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = stateListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                stateListFiltered = (ArrayList<State>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(State contact);
    }

}
