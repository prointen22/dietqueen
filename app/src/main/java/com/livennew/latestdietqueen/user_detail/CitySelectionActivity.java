package com.livennew.latestdietqueen.user_detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.model.City;
import com.livennew.latestdietqueen.model.State;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.user_detail.adapter.CitySelectionAdapter;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class CitySelectionActivity extends BaseActivity implements CitySelectionAdapter.ContactsAdapterListener {
    private static final String TAG = StateSelectionActivity.class.getSimpleName();
    public static final int CITY_SELECTION_REQ = 58;
    public static final String SELECTED_CITY = "selectedCity";

    private RecyclerView mRecyclerView;
    private EditText edSearch;
    // private LinearLayout llProgress;
    private Toolbar toolbar;
    private CitySelectionAdapter mAdapter;
    private ArrayList<City> stateList;
    private ProgressBar progressBar;
    private State state;
    private LinearLayout llBack;
    @Inject
    SessionManager sessionManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_selection);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));

        user = sessionManager.getUser();
        findViewById();
        initToolbar();
        //initView();
        state = getIntent().getParcelableExtra("state");
        if (SupportClass.checkConnection(this)) {
            getStats();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }


    private void getStats() {
        progressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        edSearch.setVisibility(View.GONE);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Api.MAIN_URL + Api.GETCITY + "/" + state.getId(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jObj = new JSONObject(response.toString());
                            boolean error = jObj.getBoolean("error");
                            if (!error) {
                                JSONArray jsonArray = jObj.getJSONArray("data");
                                //List<State> items = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<State>>() {}.getType());
                                stateList.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    // adding contacts to contacts list
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    City city = new City();
                                    city.setId(jsonObject.getInt("id"));
                                    city.setName(jsonObject.getString("city_name"));
                                    stateList.add(city);
                                }
                                mAdapter = new CitySelectionAdapter(CitySelectionActivity.this, stateList, CitySelectionActivity.this);
                                LinearLayoutManager manager = new LinearLayoutManager(CitySelectionActivity.this);
                                mRecyclerView.setHasFixedSize(true);
                                mRecyclerView.setLayoutManager(manager);
                                mRecyclerView.setAdapter(mAdapter);
                                progressBar.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                edSearch.setVisibility(View.VISIBLE);
                            } else {
                                Toast.makeText(CitySelectionActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, error ->
                Log.e(TAG, "response Error: " + error.getMessage())) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                headers.put("Authorization", user.getToken());
                //           headers.put("userId", String.valueOf(pref.getUserId()));
                return headers;
            }
        };

//        MyApplication.getInstance().addToRequestQueue(request);
        request.setShouldCache(false);
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(CitySelectionActivity.this);
        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);

    }

    private void findViewById() {
        // llProgress = (LinearLayout) findViewById(R.id.llProgress);
        edSearch = (EditText) findViewById(R.id.edSearch);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        stateList = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progress_bar);
        llBack = findViewById(R.id.ll_back);
        llBack.setOnClickListener(v->{
            finish();
        });
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(" ");
        }
        toolbar.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
    }


    @Override
    public void onPause() {
        super.onPause();
        hideSoftKeyboard(CitySelectionActivity.this);
    }


    /**
     * Hide the Soft Keyboard.
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);

        if (inputMethodManager == null)
            return;

        if (activity.getCurrentFocus() != null && activity.getCurrentFocus().getWindowToken() != null)
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onContactSelected(City city) {
        //  Toast.makeText(getApplicationContext(), "Selected: " + city.getCity_id() + ", " + city.getCity_name(), Toast.LENGTH_LONG).show();
        Intent returnIntent = new Intent();
        returnIntent.putExtra(SELECTED_CITY, city);
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}
//setContentView(R.layout.activity_state_selection);