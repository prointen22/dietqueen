package com.livennew.latestdietqueen.user_detail.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.user_detail.model.AddExercisePojo;

import java.util.ArrayList;

public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ExerciseHolder> {
    private Context mContext;
    private ArrayList<AddExercisePojo> alAddExercise;

    public ExerciseAdapter(Context mContext, ArrayList<AddExercisePojo> alAddExercise) {
        this.mContext = mContext;
        this.alAddExercise = alAddExercise;
    }

    @NonNull
    @Override
    public ExerciseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exercise_content, parent, false);
        ExerciseHolder exerciseHolder = new ExerciseHolder(view);
        return exerciseHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExerciseHolder holder, int position) {
        final AddExercisePojo addExercisePojo = alAddExercise.get(position);
        holder.tvExerciseType.setText(addExercisePojo.getExerciseType());
        holder.tvExerciseTime.setText(addExercisePojo.getExerciseTime());
        holder.tvExerciseReport.setText(addExercisePojo.getExerciseReport());
        holder.ibTrashImpl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alAddExercise.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, alAddExercise.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return alAddExercise.size();
    }

    public class ExerciseHolder extends RecyclerView.ViewHolder {
        ImageButton ibTrashImpl;
        TextView tvExerciseType, tvExerciseTime, tvExerciseReport;

        private ExerciseHolder(View itemView) {
            super(itemView);
            tvExerciseType = itemView.findViewById(R.id.tv_exercise_type);
            tvExerciseTime = itemView.findViewById(R.id.tv_exercise_time);
            tvExerciseReport = itemView.findViewById(R.id.tv_exercise_report);
            ibTrashImpl = itemView.findViewById(R.id.ib_impl_remove);
        }

    }
}

