package com.livennew.latestdietqueen.user_detail.model;

public class AddExercisePojo {
    private String exerciseType;
    private String exerciseTime;
    private String exerciseReport;

    public AddExercisePojo() {
    }

    public AddExercisePojo(String exerciseType, String exerciseTime, String exerciseReport) {
        this.exerciseType = exerciseType;
        this.exerciseTime = exerciseTime;
        this.exerciseReport = exerciseReport;
    }


    public String getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(String exerciseType) {
        this.exerciseType = exerciseType;
    }

    public String getExerciseTime() {
        return exerciseTime;
    }

    public void setExerciseTime(String exerciseTime) {
        this.exerciseTime = exerciseTime;
    }

    public String getExerciseReport() {
        return exerciseReport;
    }

    public void setExerciseReport(String exerciseReport) {
        this.exerciseReport = exerciseReport;
    }
}
