package com.livennew.latestdietqueen;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.livennew.latestdietqueen.model.FCMParam;
import com.livennew.latestdietqueen.model.Language;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserDetail;
import com.livennew.latestdietqueen.utils.SharedPreferenceUtils;


public class SessionManager {
    private static final String TAG = "SessionManager";
    private static final String KEY_USER = "user";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String FCM_TOKEN = "fcmToken";
    private static final String LANGUAGE = "language";
    private static final String IS_TERMS_ACCEPTED = "isTermsAccepted";
    private static final String NO_PLAN = "noPlan";
    private static final String KEY_DIET_E_PLAN_ID = "diet_e_plan_id";
    private final SharedPreferenceUtils sharedPreference;
    private String defaultLanguage = "{\n" +
            "      \"id\": 1,\n" +
            "      \"name\": \"English\",\n" +
            "      \"code\": \"en\",\n" +
            "      \"priority\": 1\n" +
            "    }";

    public SessionManager(Context context) {
        sharedPreference = SharedPreferenceUtils.getInstance(context);
    }

    private void setLogin(boolean isLoggedIn) {
        sharedPreference.setValue(KEY_IS_LOGGEDIN, isLoggedIn);
    }

    public void setIsTermsAccepted(int isTermsAccepted) {
        sharedPreference.setValue(IS_TERMS_ACCEPTED, isTermsAccepted);
    }

    public void setNoPlan(int noPlan) {
        sharedPreference.setValue(NO_PLAN, noPlan);
    }

    public int getNoPlan(){
        return sharedPreference.getIntValue(NO_PLAN,1);
    }

    public int isTermsAccepted(){
        return  sharedPreference.getIntValue(IS_TERMS_ACCEPTED,0);
    }

    public boolean isLoggedIn() {
        return sharedPreference.getBooleanValue(KEY_IS_LOGGEDIN, false);
    }

    public void setUser(User user) {
        setLogin(true);
        sharedPreference.setValue(KEY_USER, new Gson().toJson(user));
        if (user.getDietEPlanId() != 0) {
            setDietEPlanId(user.getDietEPlanId());
        }
    }

    private void setDietEPlanId(int id) {
        sharedPreference.setValue(KEY_DIET_E_PLAN_ID, id);
    }

    public int getDietEPlanId() {
        return sharedPreference.getIntValue(KEY_DIET_E_PLAN_ID, 0);
    }

    public User getUser() {
        if (isLoggedIn()) {
            return new Gson().fromJson(sharedPreference.getStringValue(KEY_USER, ""), User.class);
        }
        return null;
    }

    private void clear() {
        sharedPreference.clear();
    }

    public void setLanguage(Language language) {
        sharedPreference.setValue(LANGUAGE, new Gson().toJson(language));
        User user = getUser();
        if (getUser().getUserDetail() == null) {
            UserDetail userDetails = new UserDetail();
            userDetails.setLanguage(language);
            user.setUserDetail(userDetails);
        } else {
            user.getUserDetail().setLanguage(language);
        }
        setUser(user);

    }

    public Language getLanguage() {
        return new Gson().fromJson(sharedPreference.getStringValue(LANGUAGE, defaultLanguage), Language.class);
    }


    public void logout() {
        clear();
    }

    public void setPushToken(String token) {
        sharedPreference.setValue(FCM_TOKEN, token);
    }

    public String getFCMToken() {
        String token = sharedPreference.getStringValue(FCM_TOKEN, "");
        Log.e(TAG, "getFCMToken: " + token);
        return token;
    }
}