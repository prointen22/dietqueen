package com.livennew.latestdietqueen.party_planner;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.PartyPlannerTermsConditionDialogBinding;
import com.livennew.latestdietqueen.party_planner.model.PartyPlannerTermsResp;
import com.livennew.latestdietqueen.retrofit.APIInterface;


import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class PartyPlannerTermsDialogFragment extends AppCompatDialogFragment {
    public static final String TAG = "PartyPlannerTermsDialog";
    private PartyPlannerTermsConditionDialogBinding binding;
    @Inject
    APIInterface apiInterface;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = PartyPlannerTermsConditionDialogBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
        binding.progress.setVisibility(View.VISIBLE);
        apiInterface.getPartyPlannerTerms().enqueue(new Callback<PartyPlannerTermsResp>() {
            @Override
            public void onResponse(@NotNull Call<PartyPlannerTermsResp> call, @NotNull Response<PartyPlannerTermsResp> response) {
                if (!response.body().getError()) {
                    binding.text.setText(Util.fromHtml(response.body().getData().getContent()));
                }
                binding.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NotNull Call<PartyPlannerTermsResp> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                binding.progress.setVisibility(View.GONE);
            }
        });
        binding.btnOk.setOnClickListener(view1 -> dismiss());
        return view;
    }
}
