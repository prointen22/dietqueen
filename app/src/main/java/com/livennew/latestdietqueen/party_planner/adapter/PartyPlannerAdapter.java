package com.livennew.latestdietqueen.party_planner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.party_planner.model.PartyPlannerModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PartyPlannerAdapter extends RecyclerView.Adapter<PartyPlannerAdapter.PartyPlannerHolder> {
    private Context mContext;
    private ArrayList<PartyPlannerModel> alPartyPlanner;

    public PartyPlannerAdapter(Context mContext, ArrayList<PartyPlannerModel> alPartyPlanner) {
        this.mContext = mContext;
        this.alPartyPlanner = alPartyPlanner;
    }

    @NonNull
    @Override
    public PartyPlannerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.party_planner_content, parent, false);
        PartyPlannerHolder partyPlannerHolder = new PartyPlannerHolder(view);
        return partyPlannerHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PartyPlannerHolder holder, int position) {
        final PartyPlannerModel partyPlannerModel = alPartyPlanner.get(position);
        Glide.with(mContext).
                load(partyPlannerModel.getPartyPlannerImg())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.ivPartyPlanner);
        holder.tvPartyPlannerNm.setText(partyPlannerModel.getPartyPlannerNm());
        /*holder.cbPartyPlanner.setOnCheckedChangeListener(new OnC {
                alAddPhysAct.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, alAddPhysAct.size());
        });*/
        holder.cbPartyPlanner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    partyPlannerModel.setSelected(true);
                } else {
                    partyPlannerModel.setSelected(false);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return alPartyPlanner.size();
    }

    public class PartyPlannerHolder extends RecyclerView.ViewHolder {
        TextView tvPartyPlannerNm;
        CheckBox cbPartyPlanner;
        CircleImageView ivPartyPlanner;

        private PartyPlannerHolder(View itemView) {
            super(itemView);
            tvPartyPlannerNm = itemView.findViewById(R.id.party_planner_nm);
            cbPartyPlanner = itemView.findViewById(R.id.cb_party_planner);
            ivPartyPlanner = itemView.findViewById(R.id.iv_party_planner);
        }

    }
}
