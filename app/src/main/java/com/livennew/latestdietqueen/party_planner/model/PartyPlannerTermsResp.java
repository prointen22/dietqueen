package com.livennew.latestdietqueen.party_planner.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.about_us.AboutUs;
import com.livennew.latestdietqueen.model.MyResponse;

public class PartyPlannerTermsResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private AboutUs data;

    public AboutUs getData() {
        return data;
    }

    public void setData(AboutUs data) {
        this.data = data;
    }
}
