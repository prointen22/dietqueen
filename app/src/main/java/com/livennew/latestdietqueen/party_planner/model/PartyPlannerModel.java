package com.livennew.latestdietqueen.party_planner.model;

public class PartyPlannerModel {
    private int partyPlannerId;
    private String partyPlannerNm, partyPlannerImg;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getPartyPlannerId() {
        return partyPlannerId;
    }

    public void setPartyPlannerId(int partyPlannerId) {
        this.partyPlannerId = partyPlannerId;
    }

    public String getPartyPlannerNm() {
        return partyPlannerNm;
    }

    public void setPartyPlannerNm(String partyPlannerNm) {
        this.partyPlannerNm = partyPlannerNm;
    }

    public String getPartyPlannerImg() {
        return partyPlannerImg;
    }

    public void setPartyPlannerImg(String partyPlannerImg) {
        this.partyPlannerImg = partyPlannerImg;
    }
}
