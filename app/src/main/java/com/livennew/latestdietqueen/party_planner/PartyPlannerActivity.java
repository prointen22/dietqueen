package com.livennew.latestdietqueen.party_planner;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.dashboard.DashboardActivity1;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.party_planner.adapter.PartyPlannerAdapter;
import com.livennew.latestdietqueen.party_planner.model.PartyPlannerModel;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import de.hdodenhof.circleimageview.CircleImageView;

@AndroidEntryPoint
public class PartyPlannerActivity extends FabActivity {
    private Toolbar toolbarPartyPlanner;
    private Spinner spnrPartyPlan;
    private static final String TAG = "Erros";
    private TextView tvFromDate, tvToDate, tvTAndC;
    private EditText etAddComment;
    private CheckBox cbCheckTAndC;
    private Button btnPrtyPlanSbmt, btnOk;
    private ArrayList<PartyPlannerModel> alPartyPlanner;
    private RecyclerView rvPartPlanner;
    private PartyPlannerAdapter partyPlannerAdapter;
    private ProgressBar progressBar;
    private Calendar myCalendar;
    String[] partyPlannerType = new String[]{
            "Select Diet Party Plan",
            "Kitty Party",
            "Weekend",
            "Short Vacation",
            "Long Vacation"
    };
    private LinearLayoutManager recyclerLayoutManager;
    private JSONArray partyPlannerArray;
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    User user;
    private CircleImageView ivProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party_planner);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        user = sessionManager.getUser();
        init();
        setSupportActionBar(toolbarPartyPlanner);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.party_planner));
        }
        toolbarPartyPlanner.setNavigationIcon(R.drawable.ic_navigation_before_pink_24dp);
        SupportClass.hideKeyboardFrom(this);

        final List<String> plantsList = new ArrayList<>(Arrays.asList(partyPlannerType));
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        @NotNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnrPartyPlan.setAdapter(spinnerArrayAdapter);

        spnrPartyPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                if (position > 0) {
                    ((TextView) spnrPartyPlan.getSelectedView()).setTextColor(getResources().getColor(R.color.black));
                    ((TextView) spnrPartyPlan.getSelectedView()).setTextSize(getResources().getDimension(R.dimen.iner_radius));
                    Typeface typeface = ResourcesCompat.getFont(PartyPlannerActivity.this, R.font.segoeui);
                    ((TextView) spnrPartyPlan.getSelectedView()).setTypeface(typeface, Typeface.BOLD);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        DatePickerDialog.OnDateSetListener fromDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tvFromDate.setText(updateLabel());
            }
        };
        DatePickerDialog.OnDateSetListener toDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tvToDate.setText(updateLabel());
            }
        };

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd MMM, yyyy", Locale.US);
        String formattedDate = df.format(c);
        tvFromDate.setText(formattedDate);

        String formattedDate1 = df.format(c);
        tvToDate.setText(formattedDate1);

        tvFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(PartyPlannerActivity.this, R.style.MyDatePicker, fromDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                //tvFromDate.setText(updateLabel());
            }
        });
        tvToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(PartyPlannerActivity.this, R.style.MyDatePicker, toDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                //tvToDate.setText(updateLabel());
            }
        });
        cbCheckTAndC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    cbCheckTAndC.setChecked(true);
                } else {
                    cbCheckTAndC.setChecked(false);
                }
            }
        });
        if (SupportClass.checkConnection(this)) {
            getPartyPlannerList();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }
        btnPrtyPlanSbmt.setOnClickListener(View -> {
            try {
                partyPlannerArray = new JSONArray();
                for (PartyPlannerModel i : alPartyPlanner) {
                    if (i.isSelected()) {
                        JSONObject jsonObjectBrand = new JSONObject();
                        jsonObjectBrand.put("id", i.getPartyPlannerId());
                        partyPlannerArray.put(jsonObjectBrand);
                    }
                }
                if (validate(partyPlannerArray.length())) {
                    insertPartyPlanner();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        tvTAndC.setOnClickListener(View -> {
            showTermsAndConditionDialog();

        });
        Glide
                .with(this)
                .load(user.getProfileImage())
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(ivProfile);
    }

    private boolean validate(int count) {
        if (count == 0) {
            Toast.makeText(this, getString(R.string.please_select_cat), Toast.LENGTH_SHORT).show();
            return false;
        } else if (spnrPartyPlan.getSelectedItemPosition() == 0) {
            Toast.makeText(this, getString(R.string.pleaseSelectPartyPlan), Toast.LENGTH_SHORT).show();
            return false;
        } else if (tvToDate.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_select_to_date), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!cbCheckTAndC.isChecked()) {
            Toast.makeText(this, getString(R.string.please_select_tc), Toast.LENGTH_SHORT).show();
            return false;
        } else if (etAddComment.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.please_enter_comment), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private String updateLabel() {
        String myFormat = "dd MMM, yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        //tvFromDate.setText(sdf.format(myCalendar.getTime()));
        return sdf.format(myCalendar.getTime());
    }

    private void init() {
        myCalendar = Calendar.getInstance();
        toolbarPartyPlanner = findViewById(R.id.toolbar_party_planner);
        ivProfile = (CircleImageView) findViewById(R.id.ivProfile);
        spnrPartyPlan = findViewById(R.id.spnr_party_plan);
        tvFromDate = findViewById(R.id.tv_from_date);
        tvToDate = findViewById(R.id.tv_to_date);
        tvTAndC = findViewById(R.id.tv_term_cond);
        etAddComment = findViewById(R.id.et_party_planner_comment);
        btnPrtyPlanSbmt = findViewById(R.id.btn_pp_sbmt);
        cbCheckTAndC = findViewById(R.id.cbAgreeTandC);
        rvPartPlanner = findViewById(R.id.rv_party_palnner_cat);
        progressBar = findViewById(R.id.progress_bar);
        alPartyPlanner = new ArrayList<>();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void getPartyPlannerList() {
        progressBar.setVisibility(View.VISIBLE);
        rvPartPlanner.setVisibility(View.GONE);
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.PARTYPLANNERLIST, null, response -> {
            Log.d(TAG, "onResponse Response: " + response);
            progressBar.setVisibility(View.GONE);
            rvPartPlanner.setVisibility(View.VISIBLE);
            try {
                JSONObject jObj = new JSONObject(response.toString());
                boolean error = jObj.getBoolean("error");
                if (!error) {
                    JSONArray jsonArray = jObj.getJSONArray("data");
                    JSONObject jsonObject = jObj.getJSONObject("others");
                    String partyPlannerPath = jsonObject.getString("party_planner_category_images_path");
                    alPartyPlanner.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObjectData1 = jsonArray.getJSONObject(i);
                        PartyPlannerModel partyPlannerModel = new PartyPlannerModel();
                        partyPlannerModel.setPartyPlannerId(jsonObjectData1.getInt("id"));
                        partyPlannerModel.setPartyPlannerNm(jsonObjectData1.getString("name"));
                        partyPlannerModel.setPartyPlannerImg(partyPlannerPath.concat(jsonObjectData1.getString("image")));

                        alPartyPlanner.add(partyPlannerModel);
                    }
                    partyPlannerAdapter = new PartyPlannerAdapter(PartyPlannerActivity.this, alPartyPlanner);
                    recyclerLayoutManager = new LinearLayoutManager(PartyPlannerActivity.this);
                    rvPartPlanner.setLayoutManager(recyclerLayoutManager);
                    rvPartPlanner.setAdapter(partyPlannerAdapter);
                } else {
                    Toast.makeText(PartyPlannerActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                headers.put("Authorization", user.getToken());
                headers.put("userId", String.valueOf(user.getId()));
                return headers;
            }
        };
        strReq.setShouldCache(false);
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(PartyPlannerActivity.this);
        strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(strReq);
    }


    private void insertPartyPlanner() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy", Locale.US);
            Date date = sdf.parse(tvFromDate.getText().toString());
            sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            String start_date = sdf.format(date);

            SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM, yyyy", Locale.US);
            Date date1 = sdf1.parse(tvToDate.getText().toString());
            sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            String end_date = sdf.format(date1);

            JSONObject params = new JSONObject();
            params.put("weekend_type", spnrPartyPlan.getSelectedItem());
            params.put("start_from", start_date);
            params.put("end_to", end_date);
            params.put("party_planner_category", partyPlannerArray);
            params.put("comment", etAddComment.getText().toString());
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.INSERTPARTYPLANNER, params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    // NewTractors newTractors = new Gson().fromJson(response.toString(), NewTractors.class);
                    //if (!newTractors.isError()) {
                    if (!error) {
                        JSONArray jsonArray = jObj.getJSONArray("message");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                        }
                        Intent intent = new Intent(PartyPlannerActivity.this, DashboardActivity1.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                        //Toast.makeText(PartyPlanner.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                        JSONArray jsonArray = jObj.getJSONArray("message");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(PartyPlannerActivity.this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showTermsAndConditionDialog() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment frag = manager.findFragmentByTag(PartyPlannerTermsDialogFragment.TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        PartyPlannerTermsDialogFragment alertDialogFragment = new PartyPlannerTermsDialogFragment();
        alertDialogFragment.show(manager, PartyPlannerTermsDialogFragment.TAG);
    }
}
