package com.livennew.latestdietqueen.newsblog;

import android.view.View;

import androidx.hilt.Assisted;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.livennew.latestdietqueen.fragments.home.model.Blog;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.StateMutableLiveData;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlogDetailViewModel extends ViewModel {
    private final StateMutableLiveData<BlogDetailResp> blogLiveData;
    private APIInterface apiInterface;

    @ViewModelInject
    BlogDetailViewModel(APIInterface apiInterface, @Assisted SavedStateHandle savedStateHandle) {
        this.apiInterface = apiInterface;
        blogLiveData = new StateMutableLiveData<>();
        init(savedStateHandle.get("blog_id"));
    }

    private void init(int id) {
        blogLiveData.postLoading();
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("type", "blog");
        apiInterface.getBlogDetail(params).enqueue(new Callback<BlogDetailResp>() {
            @Override
            public void onResponse(@NotNull Call<BlogDetailResp> call, @NotNull Response<BlogDetailResp> response) {
                try {
                    if (response.isSuccessful())
                        if (!response.body().getError()) {
                            blogLiveData.postSuccess(response.body());
                        } else {
                            blogLiveData.postError(response.body().getMessage().get(0));

                        }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<BlogDetailResp> call, @NotNull Throwable t) {
                blogLiveData.postError(t.getMessage());
            }
        });
    }

    public StateMutableLiveData<BlogDetailResp> getBlogLiveData() {
        return blogLiveData;
    }
}
