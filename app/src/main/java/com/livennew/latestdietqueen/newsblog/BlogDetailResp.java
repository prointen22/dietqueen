package com.livennew.latestdietqueen.newsblog;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.fragments.home.model.Blog;
import com.livennew.latestdietqueen.fragments.home.model.BlogListResp;
import com.livennew.latestdietqueen.model.MyResponse;

public class BlogDetailResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private Blog blog;

    @SerializedName("others")
    @Expose
    private BlogListResp.Others others;

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public BlogListResp.Others getOthers() {
        return others;
    }

    public void setOthers(BlogListResp.Others others) {
        this.others = others;
    }
}
