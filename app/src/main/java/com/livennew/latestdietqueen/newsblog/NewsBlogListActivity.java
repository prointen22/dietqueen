package com.livennew.latestdietqueen.newsblog;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.utils.TopBarUtil;
import com.livennew.latestdietqueen.diet_info.InstructionActivity;
import com.livennew.latestdietqueen.fragments.home.model.BlogModel;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.newsblog.adapter.NewsBlogListAdapter;
import com.livennew.latestdietqueen.notification.NotificationListActivity;
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil;
import com.livennew.latestdietqueen.utils.RecyclerItemClickListener;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class NewsBlogListActivity extends AppCompatActivity implements View.OnClickListener {
    //    Toolbar toolbarBlogList;
    ArrayList<BlogModel> alNewsBlogList;
    private RecyclerView rvNewsBlogList;
    private static final String TAG = "Erros";
    NewsBlogListAdapter newsBlogListAdapter;
    private Button btnNewsBlogReload;
    private boolean error;
    StringBuilder sb;
    boolean appendSeparator = false;
    private LinearLayoutCompat llProgress;
    private int lastPage = 0;
    private String curapgeNo = "1";
    private Handler handler;
    private int currentItem, totalItems, scrollOutItems;
    private Boolean isScrolling = false;
    private LinearLayoutManager recyclerLayoutManager;
    private ProgressBar progressBar;
//    ImageView mIvNotification, mIvDoctor;
    private static FirebaseAnalytics firebaseAnalytics;
    @Inject
    SessionManager sessionManager;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_blog_list);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        user = sessionManager.getUser();

        ImageView mIvNotification = findViewById(R.id.iv_notification);
        ImageView mIvDoctor = findViewById(R.id.iv_doctor);
        ImageView mIvSideMenu = findViewById(R.id.iv_menu);
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager);

//        initToolbar();
        init();
//        setSupportActionBar(toolbarBlogList);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayShowHomeEnabled(true);
//        }
//        toolbarBlogList.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);

        if (SupportClass.checkConnection(this)) {
            curapgeNo = "1";
            blogList();
            btnNewsBlogReload.setVisibility(View.GONE);
            rvNewsBlogList.setVisibility(View.VISIBLE);
        } else {
            SupportClass.noInternetConnectionToast(this);
            btnNewsBlogReload.setVisibility(View.VISIBLE);
            rvNewsBlogList.setVisibility(View.GONE);
        }

        btnNewsBlogReload.setOnClickListener(v -> {
            if (SupportClass.checkConnection(NewsBlogListActivity.this)) {
                curapgeNo = "1";
                blogList();
                btnNewsBlogReload.setVisibility(View.GONE);
                rvNewsBlogList.setVisibility(View.VISIBLE);
            } else {
                SupportClass.noInternetConnectionToast(NewsBlogListActivity.this);
                btnNewsBlogReload.setVisibility(View.VISIBLE);
                rvNewsBlogList.setVisibility(View.GONE);
            }
        });

        rvNewsBlogList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = recyclerLayoutManager.getChildCount();
                totalItems = recyclerLayoutManager.getItemCount();
                scrollOutItems = recyclerLayoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItem + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int page = Integer.parseInt(curapgeNo);
                            page++;
                            curapgeNo = page + "";
                            if (page <= lastPage) {
                                if (SupportClass.checkConnection(NewsBlogListActivity.this)) {
                                    BlogListPagination();
                                } else {
                                    SupportClass.noInternetConnectionToast(NewsBlogListActivity.this);
                                }
                            }
                        }
                    }, 2000);
                }
            }
        });
    }

    public void init() {
//        toolbarBlogList = findViewById(R.id.toolbar);
        rvNewsBlogList = findViewById(R.id.rv_blog_list);
        btnNewsBlogReload = findViewById(R.id.btn_news_blog_reload);
        sb = new StringBuilder();
        llProgress = findViewById(R.id.ll_progress);
        handler = new Handler();
        progressBar = findViewById(R.id.progress_bar);
        alNewsBlogList = new ArrayList<>();

//        mIvNotification = findViewById(R.id.iv_notification);
//        mIvNotification.setOnClickListener(this);
//
//        mIvDoctor = findViewById(R.id.iv_doctor);
//        mIvDoctor.setOnClickListener(this);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }

        init();
    }

    /**
     * Blog data list declared
     * passing
     */
    public void blogList() {
        llProgress.setVisibility(View.VISIBLE);
        rvNewsBlogList.setVisibility(View.GONE);
        try {
            JSONObject params = new JSONObject();
            params.put("type", "blog");
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.BLOGLIST + "page=" + Integer.parseInt(curapgeNo), params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                llProgress.setVisibility(View.GONE);
                rvNewsBlogList.setVisibility(View.VISIBLE);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject jsonObject1 = jObj.getJSONObject("data");
                        lastPage = jsonObject1.getInt("last_page");
                        JSONArray jsonArrayData = jsonObject1.getJSONArray("blog_exclusive");
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
                        String blogImagePath = jsonObjectOther.getString("blogImagePath");
                        alNewsBlogList.clear();
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);
                            BlogModel blogModel = new BlogModel();

                            blogModel.setId(jsonObjectData.getInt("id"));
                            blogModel.setBlogTitle(jsonObjectData.getString("title"));
                            blogModel.setBlogAuthor(jsonObjectData.getString("author"));
                            // blogModel.setBlogDesc(jsonObjectData.getString("description"));
                            blogModel.setBlogImage(blogImagePath.concat(jsonObjectData.getString("upload_file")));
                            //Set layout manager to position the items
                            alNewsBlogList.add(blogModel);
                        }
                        newsBlogListAdapter = new NewsBlogListAdapter(NewsBlogListActivity.this, alNewsBlogList);
                        recyclerLayoutManager = new LinearLayoutManager(this);
                        rvNewsBlogList.setLayoutManager(recyclerLayoutManager);
                        rvNewsBlogList.setAdapter(newsBlogListAdapter);
                        rvNewsBlogList.addOnItemTouchListener(new RecyclerItemClickListener(NewsBlogListActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Intent intent = new Intent(NewsBlogListActivity.this, BlogDetailActivity.class);
                                intent.putExtra("blog_id", alNewsBlogList.get(position).getId());
                                intent.putExtra("type", "blog");
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }));
                    } else {
                        Toast.makeText(NewsBlogListActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(NewsBlogListActivity.this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void BlogListPagination() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            JSONObject params = new JSONObject();
            params.put("type", "blog");
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.BLOGLIST + "page=" + Integer.parseInt(curapgeNo), params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                llProgress.setVisibility(View.GONE);
                rvNewsBlogList.setVisibility(View.VISIBLE);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject jsonObject1 = jObj.getJSONObject("data");
                        lastPage = jsonObject1.getInt("last_page");
                        JSONArray jsonArrayData = jsonObject1.getJSONArray("blog_exclusive");
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
                        String blogImagePath = jsonObjectOther.getString("blogImagePath");
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject jsonObjectData = jsonArrayData.getJSONObject(i);
                            BlogModel blogModel = new BlogModel();

                            blogModel.setId(jsonObjectData.getInt("id"));
                            blogModel.setBlogTitle(jsonObjectData.getString("title"));
                            blogModel.setBlogImage(blogImagePath.concat(jsonObjectData.getString("upload_file")));
                            //Set layout manager to position the items
                            alNewsBlogList.add(blogModel);
                        }
                        newsBlogListAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(NewsBlogListActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(NewsBlogListActivity.this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_notification:
                navigateToNotificationScreen();
                break;
            case R.id.iv_doctor:
                startActivity(new Intent(this, InstructionActivity.class));
                break;
        }
    }

    private void navigateToNotificationScreen() {
        FireBaseAnalyticsUtil.screenViewEvent("", firebaseAnalytics);
        Intent intent = new Intent(NewsBlogListActivity.this, NotificationListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
