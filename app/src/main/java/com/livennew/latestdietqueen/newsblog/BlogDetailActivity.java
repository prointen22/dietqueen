package com.livennew.latestdietqueen.newsblog;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.ActivityNewsBlogDetailsBinding;
import com.livennew.latestdietqueen.fragments.home.model.Blog;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.StateData;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class BlogDetailActivity extends FabActivity {
    private ActivityNewsBlogDetailsBinding binding;
    private BlogDetailViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        binding = ActivityNewsBlogDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        viewModel = new ViewModelProvider(this).get(BlogDetailViewModel.class);
        setSupportActionBar(binding.incHeader.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        binding.incHeader.toolbar.setNavigationIcon(R.drawable.ic_navigation_before_pink_24dp);
        binding.incHeader.toolbar.setTitle("");
        viewModel.getBlogLiveData().observe(this, blogStateData -> {
            switch (blogStateData.getStatus()) {
                case LOADING:
//                    binding.incProgress.setVisibility(View.VISIBLE);
                    break;
                case SUCCESS:
//                    binding.incProgress.setVisibility(View.GONE);
                    Blog blog = blogStateData.getData().getBlog();
                    binding.incBody.tvBlogTitle.setText(blog.getTitle());
                    binding.incBody.tvBlogDetailDesc.setText(Util.fromHtml(blog.getDescription()));
                    Glide.with(BlogDetailActivity.this)
                            .load(blogStateData.getData().getOthers().getBlogImagePath() + blog.getUploadFile())
                            .error(R.mipmap.ic_launcher)
                            .into(binding.incHeader.ivBlog);
                    break;
                case ERROR:
                    Toast.makeText(BlogDetailActivity.this, blogStateData.getError(), Toast.LENGTH_LONG).show();
            }
        });
        binding.incHeader.appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                } else if (isShow) {
                    isShow = false;
                }
            }
        });
        binding.incHeader.appBarLayout.setBackgroundColor(getResources().getColor(R.color.white));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
