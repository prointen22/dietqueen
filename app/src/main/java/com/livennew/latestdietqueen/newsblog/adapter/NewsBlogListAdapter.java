package com.livennew.latestdietqueen.newsblog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.fragments.home.model.BlogModel;

import java.util.ArrayList;

public class NewsBlogListAdapter extends RecyclerView.Adapter<NewsBlogListAdapter.MyViewHolder> {

    private ArrayList<BlogModel> alNewsBlogList;
    private Context context;


    public NewsBlogListAdapter(Context context, ArrayList<BlogModel> alNewsBlogList) {
        this.context = context;
        this.alNewsBlogList = alNewsBlogList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_blog_content_new, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final BlogModel blogModel = alNewsBlogList.get(position);
        Glide.with(context)
                .load(blogModel.getBlogImage())
                .into(holder.ivListBlog);
        holder.tvListBlogTitle.setText(alNewsBlogList.get(position).getBlogTitle());
    }

    @Override
    public int getItemCount() {
        return alNewsBlogList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivListBlog;
        TextView tvListBlogTitle;
        CardView cvBlogList;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivListBlog = itemView.findViewById(R.id.iv_blog_list_content);
            tvListBlogTitle = itemView.findViewById(R.id.tv_blog_title);
            cvBlogList = itemView.findViewById(R.id.cv_blog_list);
        }
    }
}