package com.livennew.latestdietqueen.fitness_advice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

public class FitnessAdviceResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private Fitness data;

    public Fitness getData() {
        return data;
    }

    public void setData(Fitness data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "FitnessAdviceResp{" +
                "data=" + data +
                '}';
    }
}
