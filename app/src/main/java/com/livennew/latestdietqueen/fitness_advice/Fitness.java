package com.livennew.latestdietqueen.fitness_advice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fitness {
    @SerializedName("bmi")
    @Expose
    private String bmi="0";
    @SerializedName("ideal_height")
    @Expose
    private String idealHeight="0";
    @SerializedName("ibw")
    @Expose
    private String ibw="0";
    @SerializedName("weight_type")
    @Expose
    private String weightType="0";
    @SerializedName("loss_and_gain")
    @Expose
    private String lossAndGain="0";
    @SerializedName("total_months")
    @Expose
    private String totalMonths="0";
    @SerializedName("required_energy")
    @Expose
    private String requiredEnergy="0";

    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }

    public String getIdealHeight() {
        return idealHeight;
    }

    public void setIdealHeight(String idealHeight) {
        this.idealHeight = idealHeight;
    }

    public String getIbw() {
        return ibw;
    }

    public void setIbw(String ibw) {
        this.ibw = ibw;
    }

    public String getWeightType() {
        return weightType;
    }

    public void setWeightType(String weightType) {
        this.weightType = weightType;
    }

    public String getLossAndGain() {
        return lossAndGain;
    }

    public void setLossAndGain(String lossAndGain) {
        this.lossAndGain = lossAndGain;
    }

    public String getTotalMonths() {
        return totalMonths;
    }

    public void setTotalMonths(String totalMonths) {
        this.totalMonths = totalMonths;
    }

    public String getRequiredEnergy() {
        return requiredEnergy;
    }

    public void setRequiredEnergy(String requiredEnergy) {
        this.requiredEnergy = requiredEnergy;
    }

    @Override
    public String toString() {
        return "Fitness{" +
                "bmi=" + bmi +
                ", idealHeight=" + idealHeight +
                ", ibw=" + ibw +
                ", weightType='" + weightType + '\'' +
                ", lossAndGain=" + lossAndGain +
                ", totalMonths=" + totalMonths +
                ", requiredEnergy=" + requiredEnergy +
                '}';
    }
}
