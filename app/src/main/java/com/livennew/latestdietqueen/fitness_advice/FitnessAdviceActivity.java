package com.livennew.latestdietqueen.fitness_advice;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.MyApplication;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.dashboard.DashboardActivity1;
import com.livennew.latestdietqueen.databinding.ActivityFitnessAdviceBinding;
import com.livennew.latestdietqueen.fragments.products.ProductListActivity;
import com.livennew.latestdietqueen.model.SubScribe;
import com.livennew.latestdietqueen.model.SubScribeResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class FitnessAdviceActivity extends BaseActivity {
    @Inject
    SessionManager sessionManager;
    User user;
    @Inject
    APIInterface apiInterface;
    private ActivityFitnessAdviceBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFitnessAdviceBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(FitnessAdviceActivity.this, R.color.white));
        user = sessionManager.getUser();
        if (SupportClass.checkConnection(this)) {
            getFitnessAdvice();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }

        binding.btnNext.setOnClickListener(view -> {
            binding.btnNext.setEnabled(false);
            Intent intent;
            if (user.getDietPlan() == null)
                intent = new Intent(FitnessAdviceActivity.this, ProductListActivity.class);
            else
                intent = new Intent(FitnessAdviceActivity.this, DashboardActivity1.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        });

        binding.textView9.setOnClickListener(view ->{
            binding.textView9.setEnabled(false);
            subScribe();
//            Intent intent;
//            if (user.getDietPlan() == null)
//                intent = new Intent(FitnessAdviceActivity.this, ProductListActivity.class);
//            else
//                intent = new Intent(FitnessAdviceActivity.this, DashboardActivity1.class);
//            startActivity(intent);
//            finish();
//            overridePendingTransition(R.anim.anim_slide_in_left,
//                    R.anim.anim_slide_out_left);
        });

    }


    private void subScribe() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("diet_plan_id",60);
//        params.put("razorpay_txn_id", payuResponse);
        apiInterface.subScribe(params).enqueue(new retrofit2.Callback<SubScribeResp>() {
            @Override
            public void onResponse(@NotNull Call<SubScribeResp> call, @NotNull Response<SubScribeResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (!response.body().getError()) {
                            SubScribe data = response.body().getData();
                            Intent intent = new Intent(FitnessAdviceActivity.this, DashboardActivity1.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.anim_slide_in_left,
                                    R.anim.anim_slide_out_left);
//                            txtProductTitle = data.getDietPlan().getPlanName();
//                            durationMonth = data.getDietPlan().getDurationInMonth();
//                            Dialog dialog = new Dialog(FitnessAdviceActivity.this);
//                            dialog.setContentView(R.layout.congratulation_screen);
//                            dialog.setCanceledOnTouchOutside(false);
//                            TextView tvMsg = dialog.findViewById(R.id.tv_detail);
//                            Button btnOk = dialog.findViewById(R.id.btn_ok);
////                            tvMsg.setText(Util.fromHtml("Your ".concat(data.getDietPlan().getDurationInMonth() + " Months " + "&ldquo; " + data.getDietPlan().getPlanName() + " &rdquo;" + " Diet Plan Active Now!")));
//                            tvMsg.setText(Util.fromHtml("Your " + "&ldquo;" + data.getDietPlan().getPlanName() + "&rdquo;" + " is Active."));
//                            btnOk.setOnClickListener(view -> {
//                                dialog.dismiss();
//                                MyApplication.getLocaleManager().setNewLocale(FitnessAdviceActivity.this, user.getUserDetail().getLanguage());
//                                Intent intent = new Intent(FitnessAdviceActivity.this, DashboardActivity1.class);
//                                startActivity(intent);
//                                finish();
//
////                                Intent returnIntent = new Intent();
////                                activity.setResult(RESULT_OK, returnIntent);
////                                activity.finish();
//                            });
//                            dialog.show();
//                            ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
//                            InsetDrawable inset = new InsetDrawable(back, 40);
//                            dialog.getWindow().setBackgroundDrawable(inset);
//                            DisplayMetrics metrics = getResources().getDisplayMetrics();
//                            int width = metrics.widthPixels;
//                            dialog.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
                        } else {
                            Toast.makeText(FitnessAdviceActivity.this, response.body().getMessage().get(0), Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(FitnessAdviceActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(@NotNull Call<SubScribeResp> call, @NotNull Throwable t) {
                Log.e("", "onFailure: ", t);
                Toast.makeText(FitnessAdviceActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        finish();

    }

    public void getFitnessAdvice() {
        try {
            apiInterface.getFitnessAdvice().enqueue(new Callback<FitnessAdviceResp>() {
                @Override
                public void onResponse(@NotNull Call<FitnessAdviceResp> call, @NotNull Response<FitnessAdviceResp> response) {
                    if (!response.body().getError()) {
//                        binding.tvBMI.setText(getString(R.string.your_body_mass_index_is) + response.body().getData().getBmi());
//                        binding.tvIBW.setText(getString(R.string.your_ideal_body_weight_is) + response.body().getData().getIbw());
                        binding.ibwText.setText(response.body().getData().getIbw());
                        binding.bmiText.setText(response.body().getData().getBmi());
                        binding.weightTextview.setText(response.body().getData().getLossAndGain());
//                        binding.wcText.setText(response.body().getData().getWeightType().equals("Loss")?"I am overweight":"I am underweight");
                        binding.weigntTypeTextview.setText(response.body().getData().getWeightType().equals("Loss")?"Overweight":"Underweight");
                        if (response.body().getData().getLossAndGain().equals("0")) {
                            binding.tvYouAreOverWeight.setText(getString(R.string.please_tap_on_next_button_to_select_your_suitable_plan));
                            binding.tvYouWillLoss.setVisibility(View.GONE);
                        } else {
//                            binding.tvYouWillLoss.setVisibility(View.VISIBLE);
//                            binding.cvText.setText("Overweight");
                            String str = "DietQueen Promise "+(response.body().getData().getWeightType().equals("Loss")?"loss":"gain")+ " " + response.body().getData().getLossAndGain() + "KGS in "+response.body().getData().getTotalMonths() + " Months.";
                            binding.tvYouAreOverWeight.setText(str);
                            if (response.body().getData().getWeightType().equals("Loss")){
                              binding.image.setImageDrawable(getResources().getDrawable(R.drawable.ic_img_report));
                              binding.weigntTypeTextview.setText("Overweight");
//                              binding.wcText.setText("I am overweight");
                              binding.overweightImageview.setImageDrawable(getResources().getDrawable(R.drawable.ic_overweight));
                            }else{
                                binding.image.setImageDrawable(getResources().getDrawable(R.drawable.ic_img1_report));
                                binding.weigntTypeTextview.setText("Underweight");
//                                binding.wcText.setText("I am underweight");
                                binding.overweightImageview.setImageDrawable(getResources().getDrawable(R.drawable.ic_underweight));


                            }
//                            binding.tvYouWillLoss.setText("You will " + response.body().getData().getWeightType() + " " + response.body().getData().getLossAndGain() + " Kgs in " + response.body().getData().getTotalMonths() + " Months.");
                        }


                    } else {
                        for (int i = 0; i < response.body().getMessage().size(); i++) {
                            Toast.makeText(FitnessAdviceActivity.this, response.body().getMessage().get(i).toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call<FitnessAdviceResp> call, @NotNull Throwable t) {
                    Toast.makeText(FitnessAdviceActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
