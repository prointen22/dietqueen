package com.livennew.latestdietqueen.info_screen.food_preference_screen.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.info_screen.food_preference_screen.IRegionListner;
import com.livennew.latestdietqueen.model.Region;

import java.util.ArrayList;

public class RegionAdapter extends RecyclerView.Adapter<RegionAdapter.RegionHolder> {
    private Context mContext;
    private ArrayList<Region> alRegionalModel;
    private IRegionListner iRegionListner;
    private int lastCheckedPosition = -1;
    private int selectedRegion = 0;
//    private IRegionListner iRegionListner;
    public RegionAdapter(Context mContext, ArrayList<Region> alRegionalModel, int selectedRegion, IRegionListner iRegionListner) {
        this.mContext = mContext;
        this.alRegionalModel = alRegionalModel;
        this.iRegionListner = iRegionListner;
        this.selectedRegion =selectedRegion;

    }

    @NonNull
    @Override
    public RegionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.regional_content, parent, false);
        RegionHolder regionHolder = new RegionHolder(view);
        return regionHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RegionHolder holder, @SuppressLint("RecyclerView") int position) {
        final Region regionModel = alRegionalModel.get(position);
        holder.rbRegional.setText(regionModel.getName());
        if (selectedRegion!=0){
            if (position == selectedRegion){
                  lastCheckedPosition =position;
//                lastCheckedPosition = getAdapterPosition();
                holder.rbRegional.setChecked(position ==lastCheckedPosition);
                selectedRegion =0;
            }
        }
        holder.rbRegional.setChecked(position == lastCheckedPosition);

    }

    @Override
    public int getItemCount() {
        return alRegionalModel.size();
    }

    public class RegionHolder extends RecyclerView.ViewHolder {
        RadioButton rbRegional;
        TextView tvRegionNm;
        private RegionHolder(View itemView) {
            super(itemView);
            rbRegional = itemView.findViewById(R.id.rb_regional_content);
            //tvRegionNm = itemView.findViewById(R.id.tv_region_nm);
            rbRegional.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int copyOfLastCheckedPosition = lastCheckedPosition;
                    lastCheckedPosition = getAdapterPosition();
                    notifyItemChanged(copyOfLastCheckedPosition);
                    notifyItemChanged(lastCheckedPosition);
                    iRegionListner.setData(lastCheckedPosition);
                   // Toast.makeText(mContext, alRegionalModel.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
