package com.livennew.latestdietqueen.info_screen.food_preference_screen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.model.Profession;

import java.util.ArrayList;
import java.util.List;

public class ProfessionAdapter extends RecyclerView.Adapter<ProfessionAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<Profession> stateList;
    private List<Profession> stateListFiltered;
    private ContactsAdapterListener listener;
    private int lastSelectedPosition = -1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
//        public TextView view/*, phone*/;
        //public ImageView thumbnail;
        public TextView tvProfession;
        public RadioButton rbProfession;
        private View itemView;
        private LinearLayoutCompat mLLC;

        public MyViewHolder(View itemView) {
            super(itemView);
//            itemView = (TextView) itemView;
//            itemView.setOnClickListener(view -> {
//                // send selected contact in callback
//                listener.onContactSelected(stateListFiltered.get(getAdapterPosition()));
//            });
            tvProfession = itemView.findViewById(R.id.tv_language);
            rbProfession = itemView.findViewById(R.id.rb_check_lang);
            mLLC = itemView.findViewById(R.id.llc_row);
        }
    }


    public ProfessionAdapter(Context context, List<Profession> stateList, ContactsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.stateList = stateList;
        this.stateListFiltered = stateList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_language_content, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Profession professionModel = stateListFiltered.get(position);
//        holder.view.setText(professionModel.getName());
        /*holder.phone.setText(contact.getPhone());
        Glide.with(context)
                .load(contact.getImage())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.thumbnail);*/

        holder.rbProfession.setChecked(lastSelectedPosition == position);
        holder.tvProfession.setText(stateListFiltered.get(position).getName());
        holder.rbProfession.setChecked(stateListFiltered.get(position).isSelected());
        holder.mLLC.setOnClickListener(view -> listener.onContactSelected(stateListFiltered.get(position)));
        holder.rbProfession.setOnClickListener(view -> holder.mLLC.callOnClick());

        if (stateListFiltered.get(position).isSelected()) {
            holder.mLLC.setBackgroundColor(ContextCompat.getColor(context, R.color.selector_color));
            holder.tvProfession.setTextColor(ContextCompat.getColor(context, R.color.orange_theme));
        } else {
            holder.mLLC.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            holder.tvProfession.setTextColor(ContextCompat.getColor(context, R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return stateListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    stateListFiltered = stateList;
                } else {
                    List<Profession> filteredList = new ArrayList<>();
                    for (Profession row : stateList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    stateListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = stateListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                stateListFiltered = (ArrayList<Profession>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(Profession contact);
    }

}

