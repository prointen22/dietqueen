package com.livennew.latestdietqueen.info_screen.height_screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.Toast;

import com.kevalpatel2106.rulerpicker.RulerValuePicker;
import com.kevalpatel2106.rulerpicker.RulerValuePickerListener;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.info_screen.age_screen.AgeActivity;
import com.livennew.latestdietqueen.info_screen.age_screen.AgeActivityfromAchivement;
import com.livennew.latestdietqueen.info_screen.sleep_screen.HoursOfSleepActivity;
import com.livennew.latestdietqueen.info_screen.sleep_screen.HoursOfSleepActivityAchivement;
import com.livennew.latestdietqueen.info_screen.weight_screen.WeightActivity;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserDetail;
import com.shawnlin.numberpicker.NumberPicker;

import java.text.DecimalFormat;

public class HeightActivityFromAchivement extends AppCompatActivity {

    private String heightCM = "", height = "5.0";
    private Button btnAge, btnHeightNext;
    private RulerValuePicker rulerPickerHeightCm;
    private Switch switchHeight;
    Double heightCm = 0.0;
    private User user;
    private NumberPicker npFeet;
    private NumberPicker npInch;
    private Group gFeet;
    private String screen;
    LinearLayoutCompat  ll_view;
    private RadioButton mRbFeet, mRbCM;
    LinearLayout mLLBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_height_actvity);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        init();

        Intent intent = getIntent();
        if (intent != null) {
            user = intent.getParcelableExtra("user");
            if (user.getUserDetail() == null)
                user.setUserDetail(new UserDetail());
        } else {
            Log.i("check age", "Please check your age");
        }
        btnAge.setText(user.getAge());
        switchButton();
        rulerPickerHeightCm.setValuePickerListener(new RulerValuePickerListener() {
            @Override
            public void onValueChange(final int selectedValue) {
                Log.i("msg", String.valueOf(selectedValue));
                height = String.valueOf(selectedValue);
            }

            @Override
            public void onIntermediateValueChange(int selectedValue) {
                Log.i("msg", String.valueOf(selectedValue));
                height = String.valueOf(selectedValue);
            }
        });


        btnHeightNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchHeight.isChecked()) {
                    height = rulerPickerHeightCm.getCurrentValue() + "";
                    heightCm = Double.valueOf(height);
                } else {
                    heightCm = npFeet.getValue() * 30.48 + npInch.getValue() * 2.54;
                }
                if (heightCm < 30.48 || heightCm > 338.64) {
                    Toast.makeText(HeightActivityFromAchivement.this, getString(R.string.please_select_correct_height), Toast.LENGTH_SHORT).show();
                    return;
                }
                heightCM = String.valueOf(new DecimalFormat("##").format(heightCm)).concat("CM");
                Intent intent = new Intent(HeightActivityFromAchivement.this, HoursOfSleepActivityAchivement.class);
                user.getUserDetail().setHeight(heightCM);
                intent.putExtra("user", user);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        });
        rulerPickerHeightCm.selectValue(155);
    }

    public void init() {
        btnAge = findViewById(R.id.btn_age);
        rulerPickerHeightCm = findViewById(R.id.ruler_picker_cm);
        gFeet = (Group) findViewById(R.id.gFeet);
        npFeet = (NumberPicker) findViewById(R.id.npFeet);
        npInch = (NumberPicker) findViewById(R.id.npInch);
        npFeet.setValue(5);
        npInch.setValue(0);
        switchHeight = findViewById(R.id.swtch_height);
        btnHeightNext = findViewById(R.id.btn_height_next);
        btnAge.setVisibility(View.GONE);
        ll_view=findViewById(R.id.ll_view);
        ll_view.setVisibility(View.GONE);

        mRbFeet = findViewById(R.id.rb_feet);
        mRbCM = findViewById(R.id.rb_cm);
        setFeetSelected();

        mRbFeet.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setFeetSelected();
            }
        });
        mRbCM.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setCMSelected();
            }
        });

        mLLBack = findViewById(R.id.ll_back);
        mLLBack.setOnClickListener(v -> onBackPressed());
    }

    /**
     * This function is used for switch view
     */
    public void switchButton() {
        switchHeight.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                gFeet.setVisibility(View.GONE);
                rulerPickerHeightCm.setVisibility(View.VISIBLE);
                rulerPickerHeightCm.selectValue(150);
            } else {
                gFeet.setVisibility(View.VISIBLE);
                rulerPickerHeightCm.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(HeightActivityFromAchivement.this, AgeActivityfromAchivement.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("achievment", "achievment");
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }



    private void setFeetSelected() {
        mRbFeet.setChecked(true);
        mRbFeet.setTextColor(ContextCompat.getColor(this, R.color.pink));
        mRbCM.setChecked(false);
        mRbCM.setTextColor(ContextCompat.getColor(this, R.color.black));
        gFeet.setVisibility(View.VISIBLE);
        rulerPickerHeightCm.setVisibility(View.GONE);
    }

    private void setCMSelected() {
        mRbCM.setChecked(true);
        mRbCM.setTextColor(ContextCompat.getColor(this, R.color.pink));
        mRbFeet.setChecked(false);
        mRbFeet.setTextColor(ContextCompat.getColor(this, R.color.black));
        gFeet.setVisibility(View.GONE);
        rulerPickerHeightCm.setVisibility(View.VISIBLE);
        rulerPickerHeightCm.selectValue(150);
    }


}
