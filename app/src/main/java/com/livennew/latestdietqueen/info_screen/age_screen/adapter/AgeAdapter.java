package com.livennew.latestdietqueen.info_screen.age_screen.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.info_screen.age_screen.model.LabelerDate;

import java.util.ArrayList;

public class AgeAdapter extends RecyclerView.Adapter<AgeAdapter.AgeViewHolder> {
    private static final int VIEW_TYPE_PADDING = 1;
    private static final int VIEW_TYPE_ITEM = 2;
    private int paddingWidthDate = 0;
    private Context context;
    private int selectedItem = -1;
    private ArrayList<LabelerDate> dateDataList;
    private static final String TAG = "Errors";

    public AgeAdapter(Context context, ArrayList<LabelerDate> dateData, int paddingWidthDate) {
        this.context = context;
        this.dateDataList = dateData;
        this.paddingWidthDate = paddingWidthDate;
    }

    @NonNull
    @Override
    public AgeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        if (viewType == VIEW_TYPE_PADDING) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            layoutParams.width = paddingWidthDate;
            view.setLayoutParams(layoutParams);
        }
            return new AgeViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull AgeViewHolder holder, int position) {
        LabelerDate labelerDate = dateDataList.get(position);
        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            holder.tvDate.setText(labelerDate.getNumber());
            holder.tvDate.setVisibility(View.VISIBLE);

            Log.d(TAG, "default " + position + ", selected " + selectedItem);
            if (position == selectedItem) {
                Log.d(TAG, "center" + position);
                holder.tvDate.setTextColor(Color.parseColor("#FD139A"));
                holder.tvDate.setTypeface(null, Typeface.BOLD);
                holder.tvDate.setTextSize(28);
                //Broadcast receiver using to send data in parent activity
                String ItemName = holder.tvDate.getText().toString();
                Intent intent = new Intent("custom-message");
                intent.putExtra("item", ItemName);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            } else {
                holder.tvDate.setTextColor(Color.GRAY);
                holder.tvDate.setTypeface(null, Typeface.BOLD);
                holder.tvDate.setTextSize(18);
            }
        } else {
            holder.tvDate.setVisibility(View.INVISIBLE);
        }
    }

    public void setSelecteditem(int selecteditem) {
        this.selectedItem = selecteditem;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dateDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        LabelerDate labelerDate = dateDataList.get(position);
        if (labelerDate.getType() == VIEW_TYPE_PADDING) {
            return VIEW_TYPE_PADDING;
        }
        return VIEW_TYPE_ITEM;
    }

    public class AgeViewHolder extends RecyclerView.ViewHolder {
        public TextView tvDate;

        public AgeViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.txt_age);
        }
    }
}



