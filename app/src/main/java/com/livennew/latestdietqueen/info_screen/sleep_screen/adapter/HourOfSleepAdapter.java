package com.livennew.latestdietqueen.info_screen.sleep_screen.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.info_screen.age_screen.model.LabelerDate;
import com.livennew.latestdietqueen.info_screen.sleep_screen.HoursOfSleepActivity;

import java.util.ArrayList;

public class HourOfSleepAdapter extends RecyclerView.Adapter<HourOfSleepAdapter.HoursViewHolder> {
    private static final int VIEW_TYPE_PADDING = 1;
    private static final int VIEW_TYPE_ITEM = 2;
    private int paddingWidthDate = 0;
    private Context context;
    private int selectedItem = -1;
    private ArrayList<LabelerDate> dateDataList;
    private static final String TAG = "Errors";

    public HourOfSleepAdapter(Context context, ArrayList<LabelerDate> dateData, int paddingWidthDate,int selectedItem) {
        this.context = context;
        this.dateDataList = dateData;
        this.paddingWidthDate = paddingWidthDate;
        this.selectedItem = selectedItem;
    }

    @NonNull
    @Override
    public HoursViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sleep_item, parent, false);
        if (viewType == VIEW_TYPE_PADDING) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            layoutParams.width = paddingWidthDate;
            view.setLayoutParams(layoutParams);
        }
        return new HoursViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull HoursViewHolder holder, int position) {
        LabelerDate labelerDate = dateDataList.get(position);
        if (getItemViewType(position) == VIEW_TYPE_ITEM) {
            holder.tvHOSleep.setText(labelerDate.getNumber());
            holder.tvHOSleep.setVisibility(View.VISIBLE);

            Log.d(TAG, "default " + position + ", selected " + selectedItem+" number = "+labelerDate.getNumber());
            if (position == selectedItem) {
                Log.d(TAG, "center" + position);
                holder.tvHOSleep.setTextColor(ContextCompat.getColor(context, R.color.orange_theme));
                holder.tvHOSleep.setTypeface(null, Typeface.BOLD);
                holder.tvHOSleep.setTextSize(28);
                String text = "<u>" + labelerDate.getNumber()+ "</u>";
//                String text = labelerDate.getNumber();
                holder.tvHOSleep.setText(Html.fromHtml(text));
//                holder.tvHOSleep.setText(Html.fromHtml("<font color=#FD139A>  <u>" + text + "</u>  </font>"));

//                if (!text.isEmpty()) {
//                    holder.view1.setVisibility(View.VISIBLE);
//                }else{
//                    holder.view1.setVisibility(View.GONE);
//                }
                //Broadcast receiver using to send data in parent activity
                String ItemName = holder.tvHOSleep.getText().toString();
                Intent intent = new Intent("custom-message");
                intent.putExtra("item", ItemName);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            } else {
                holder.tvHOSleep.setTextColor(Color.GRAY);
                holder.tvHOSleep.setTypeface(null, Typeface.BOLD);
                holder.tvHOSleep.setTextSize(18);
//                holder.view1.setVisibility(View.GONE);
            }
        } else {
            holder.tvHOSleep.setVisibility(View.INVISIBLE);
        }
    }

    public void setSelecteditem(int selecteditem) {
        this.selectedItem = selecteditem;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dateDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        LabelerDate labelerDate = dateDataList.get(position);
        if (labelerDate.getType() == VIEW_TYPE_PADDING) {
            return VIEW_TYPE_PADDING;
        }
        return VIEW_TYPE_ITEM;
    }

    public class HoursViewHolder extends RecyclerView.ViewHolder {
        public TextView tvHOSleep;
//        public View view1;

        public HoursViewHolder(@NonNull View itemView) {
            super(itemView);
            tvHOSleep = itemView.findViewById(R.id.txt_sleep);
//            view1 = itemView.findViewById(R.id.view1);
        }
    }
}
