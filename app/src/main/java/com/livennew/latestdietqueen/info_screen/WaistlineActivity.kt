package com.livennew.latestdietqueen.info_screen

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.kevalpatel2106.rulerpicker.RulerValuePickerListener
import com.livennew.latestdietqueen.BaseActivity
import com.livennew.latestdietqueen.R
import com.livennew.latestdietqueen.SessionManager
import com.livennew.latestdietqueen.databinding.ActivityWaistlineBinding
import com.livennew.latestdietqueen.model.User
import com.livennew.latestdietqueen.model.UserResp
import com.livennew.latestdietqueen.network_support.APIClient
import com.livennew.latestdietqueen.retrofit.APIInterface
import com.livennew.latestdietqueen.utils.TopBarUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class WaistlineActivity : BaseActivity() {
    private lateinit var mBinding:ActivityWaistlineBinding
    var sessionManager: SessionManager? = null
    private var mWaistCM = ""
    private var mWaistInch = ""
    var user: User? = null


    @Inject
    var apiInterface: APIInterface? = null
    private var pDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        mBinding = ActivityWaistlineBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        sessionManager = SessionManager(applicationContext)
        user = sessionManager!!.user
        apiInterface = APIClient.getClient(this).create(APIInterface::class.java)
        pDialog = ProgressDialog(this)
        init()
    }

    private fun init() {
        val mIvNotification: ImageView = findViewById(R.id.iv_notification)
        val mIvDoctor: ImageView = findViewById(R.id.iv_doctor)
        val mIvSideMenu: ImageView = findViewById(R.id.iv_menu)
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager!!)

        mBinding.rbInch.setOnCheckedChangeListener { buttonView: CompoundButton?, isChecked: Boolean ->
            if (isChecked) {
                setInchSelected()
            }
        }
        mBinding.rbCm.setOnCheckedChangeListener { buttonView: CompoundButton?, isChecked: Boolean ->
            if (isChecked) {
                setCMSelected()
            }
        }

//        setInchSelected()
       setCMSelected()
        var waistValue:String
        if(user!!.userDetail.waistline!=null) {
            if (user!!.userDetail.waistline.contains(".")) {
                waistValue = user!!.userDetail.waistline.split(".")[0]
            } else {
                waistValue = user!!.userDetail.waistline
            }

            mBinding.rpCm.selectValue(waistValue.toInt())
        }else{
            mBinding.rpCm.selectValue(85)
        }
        mBinding.rpInch.setValuePickerListener(object : RulerValuePickerListener {
            override fun onValueChange(selectedValue: Int) {
                Log.v("TAG", "Waist in INCH: $selectedValue")
                mWaistInch = selectedValue.toString()
            }

            override fun onIntermediateValueChange(selectedValue: Int) {
                Log.v("TAG", "Waist in INCH: $selectedValue")
                mWaistInch = selectedValue.toString()
            }
        })

        mBinding.rpCm.setValuePickerListener(object : RulerValuePickerListener {
            override fun onValueChange(selectedValue: Int) {
                Log.v("TAG", "Waist in CM: $selectedValue")
                mWaistCM = selectedValue.toString()
            }

            override fun onIntermediateValueChange(selectedValue: Int) {
                Log.v("TAG", "Waist in CM: $selectedValue")
                mWaistCM = selectedValue.toString()
            }
        })
        mBinding.btnSave.setOnClickListener{
            var waistValue:String
            if(mBinding.rbCm.isChecked){
                waistValue = mWaistCM
            }else{
                waistValue = (mWaistInch.toInt() * 2.54).toString()
            }
            UpdateWaist(waistValue)
        }
        mBinding.tvBack.setOnClickListener {
            finish()
        }
    }

    private fun UpdateWaist(waistValue: String) {
        pDialog!!.setMessage("Loading...")
        pDialog!!.setCancelable(false)
        pDialog!!.show()

        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        val todayDate = formatter.format(Date())
        val params = HashMap<String, String>()
        params["date"] = todayDate
        params["waistline"] = waistValue
        apiInterface!!.setBodyWeight(params).enqueue(object : Callback<UserResp?> {
            override fun onResponse(call: Call<UserResp?>, response: Response<UserResp?>) {
                try {

                    val userResp = response.body()
                    pDialog!!.dismiss()
                    assert(userResp != null)
                    if (!userResp!!.error) {
//                        Toast.makeText(
//                            this@WaistlineActivity,
//                            getMessageInSingleLine(userResp!!.message),
//                            Toast.LENGTH_SHORT
//                        ).show()
//                        userResp!!.data.profileImage =
//                            userResp!!.others.userImageBase64Path + userResp!!.data.profileImage
                        sessionManager!!.user = userResp!!.data
                        //                        navigateToHealthScreen();
                        finish()
                    } else {
                        val dialog = AlertDialog.Builder(this@WaistlineActivity)
                        dialog.setMessage(getMessageInSingleLine(userResp!!.message))
                        dialog.setCancelable(false)
                        dialog.setPositiveButton(
                            android.R.string.ok
                        ) { dialog1: DialogInterface, which: Int -> dialog1.dismiss() }.show()
                    }
                } catch (e: Exception) {
                    Log.v("TAG", "Exception: " + e.message)
                }
            }

            override fun onFailure(call: Call<UserResp?>, t: Throwable) {
                Toast.makeText(this@WaistlineActivity, t.localizedMessage, Toast.LENGTH_SHORT).show()
                pDialog!!.dismiss()
            }
        })
    }

    private fun setInchSelected() {
        mBinding.rbInch.isChecked = true
        mBinding.rbInch.setTextColor(ContextCompat.getColor(this, R.color.orange_theme))
        mBinding.rbCm.isChecked = false
        mBinding.rbCm.setTextColor(ContextCompat.getColor(this, R.color.black))
        mBinding.rpInch.visibility = View.VISIBLE
        mBinding.rpInch.selectValue(30)
        mBinding.rpCm.visibility = View.GONE
    }

    private fun setCMSelected() {
        mBinding.rbCm.isChecked = true
        mBinding.rbCm.setTextColor(ContextCompat.getColor(this, R.color.orange_theme))
        mBinding.rbInch.isChecked = false
        mBinding.rbInch.setTextColor(ContextCompat.getColor(this, R.color.black))
        mBinding.rpCm.visibility = View.VISIBLE
        mBinding.rpCm.selectValue(85)
        mBinding.rpInch.visibility = View.GONE
    }
}