package com.livennew.latestdietqueen.info_screen.height_screen;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;

import com.kevalpatel2106.rulerpicker.RulerValuePicker;
import com.kevalpatel2106.rulerpicker.RulerValuePickerListener;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.info_screen.age_screen.AgeActivity;
import com.livennew.latestdietqueen.info_screen.weight_screen.WeightActivity;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserDetail;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.network_support.APIClient;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.shawnlin.numberpicker.NumberPicker;

import java.text.DecimalFormat;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;

public class HeightActivity extends BaseActivity {

    private String heightCM = "", height = "5.0";
    private Button btnAge, btnHeightNext;
    private RulerValuePicker rulerPickerHeightCm;
    private Switch switchHeight;
    Double heightCm = 0.0;
    private User user;
    private NumberPicker npFeet;
    private NumberPicker npInch;
    private Group gFeet;
    private String screen;
    private RadioButton mRbFeet, mRbCM;
    LinearLayout mLLBack;
    private boolean mUpdate = false;
    private ProgressDialog pDialog;
    @Inject
    APIInterface apiInterface;
    @Inject
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        setContentView(R.layout.activity_height_actvity);
        pDialog = new ProgressDialog(this);
        sessionManager = new SessionManager(this);
        apiInterface = APIClient.getClient(this).create(APIInterface.class);
        init();

//        Intent intent = getIntent();
//        if (intent != null) {
//            user = intent.getParcelableExtra("user");
//            if (user.getUserDetail() == null)
//                user.setUserDetail(new UserDetail());
//        } else {
//            Log.i("check age", "Please check your age");
//        }

        Intent intent1 = getIntent();
        if (intent1 != null) {
            user = intent1.getParcelableExtra("user");
            if (intent1.hasExtra("operation") && intent1.getStringExtra("operation").equals("update")) {
                mUpdate = true;
                btnHeightNext.setText(getString(R.string.save));
//                if (!TextUtils.isEmpty(user.getUserDetail().getWeight())) {
//                    binding.rpWeightKG.selectValue(Integer.parseInt(user.getUserDetail().getWeight()));
//                }
            } else {
//                binding.rpWeightKG.selectValue(70);
                mUpdate = false;
                btnHeightNext.setText(getString(R.string.next));
            }
        } else {
            Log.i("check age", "Please check your age");
        }

        btnAge.setText(user.getAge());
        switchButton();
        rulerPickerHeightCm.setValuePickerListener(new RulerValuePickerListener() {
            @Override
            public void onValueChange(final int selectedValue) {
                Log.i("msg", String.valueOf(selectedValue));
                height = String.valueOf(selectedValue);
            }

            @Override
            public void onIntermediateValueChange(int selectedValue) {
                Log.i("msg", String.valueOf(selectedValue));
                height = String.valueOf(selectedValue);
            }
        });


        btnHeightNext.setOnClickListener(v -> {
            if (mRbCM.isChecked()) {
                height = rulerPickerHeightCm.getCurrentValue() + "";
                heightCm = Double.valueOf(height);
            } else {
                heightCm = npFeet.getValue() * 30.48 + npInch.getValue() * 2.54;
            }
            if (heightCm < 30.48 || heightCm > 338.64) {
                Toast.makeText(HeightActivity.this, getString(R.string.please_select_correct_height), Toast.LENGTH_SHORT).show();
                return;
            }
            heightCM = String.valueOf(new DecimalFormat("##").format(heightCm)).concat("CM");
            if (mUpdate) {
                user.getUserDetail().setHeight(heightCM.toString());
                removeLastTwoCharForHeight();
                updateWeight();
//                    intent1 = new Intent(WeightActivity.this, HealthActivity.class);
//                    sessionManager.setUser(user);
            } else {
                if (user!=null) {
                    if (user.getUserDetail()!=null) {
                        user.getUserDetail().setHeight(heightCM);
                    }else{
//                        UserDetail userDetail = new UserDetail();
//                        user.setUserDetail(userDetail);
                        user.getUserDetail().setHeight(heightCM);
                    }
                }
                Intent intent2 = new Intent(HeightActivity.this, WeightActivity.class);
                intent2.putExtra("user", user);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                finish();
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        });
        rulerPickerHeightCm.selectValue(155);
    }

    public void init() {
        btnAge = findViewById(R.id.btn_age);
        rulerPickerHeightCm = findViewById(R.id.ruler_picker_cm);
        gFeet = findViewById(R.id.gFeet);
        npFeet = findViewById(R.id.npFeet);
        npInch = findViewById(R.id.npInch);
        npFeet.setValue(5);
        npInch.setValue(0);
        switchHeight = findViewById(R.id.swtch_height);
        btnHeightNext = findViewById(R.id.btn_height_next);
        mRbFeet = findViewById(R.id.rb_feet);
        mRbCM = findViewById(R.id.rb_cm);
        setFeetSelected();

        mRbFeet.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setFeetSelected();
            }
        });
        mRbCM.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setCMSelected();
            }
        });

        mLLBack = findViewById(R.id.ll_back);
        mLLBack.setOnClickListener(v -> onBackPressed());
    }

    /**
     * This function is used for switch view
     */
    public void switchButton() {
        switchHeight.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                gFeet.setVisibility(View.GONE);
                rulerPickerHeightCm.setVisibility(View.VISIBLE);
                rulerPickerHeightCm.selectValue(150);

            } else {
                gFeet.setVisibility(View.VISIBLE);
                rulerPickerHeightCm.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mUpdate) {
//            navigateToHealthScreen();
            finish();
        } else {
            Intent intent = new Intent(HeightActivity.this, AgeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("user", user);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    private void setFeetSelected() {
        mRbFeet.setChecked(true);
        mRbFeet.setTextColor(ContextCompat.getColor(this, R.color.orange_theme));
        mRbCM.setChecked(false);
        mRbCM.setTextColor(ContextCompat.getColor(this, R.color.black));
        gFeet.setVisibility(View.VISIBLE);
        rulerPickerHeightCm.setVisibility(View.GONE);
    }

    private void setCMSelected() {
        mRbCM.setChecked(true);
        mRbCM.setTextColor(ContextCompat.getColor(this, R.color.orange_theme));
        mRbFeet.setChecked(false);
        mRbFeet.setTextColor(ContextCompat.getColor(this, R.color.black));
        gFeet.setVisibility(View.GONE);
        rulerPickerHeightCm.setVisibility(View.VISIBLE);
        rulerPickerHeightCm.selectValue(150);
    }

    public void updateWeight() {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        apiInterface.updateUser(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull retrofit2.Response<UserResp> response) {
                try {
                    UserResp userResp = response.body();
                    pDialog.dismiss();
                    assert userResp != null;
                    if (!userResp.getError()) {
//                        Toast.makeText(HeightActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
//                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
                        sessionManager.setUser(userResp.getData());
//                        navigateToHealthScreen();
                        finish();
                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(HeightActivity.this);
                        dialog.setMessage(getMessageInSingleLine(userResp.getMessage()));
                        dialog.setCancelable(false);
                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                    }
                } catch (Exception e) {
                    Log.v("TAG", "Exception: "+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UserResp> call, Throwable t) {
                Toast.makeText(HeightActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        });
    }

    public void removeLastTwoCharForHeight() {
        user.getUserDetail().setHeight(Double.parseDouble(user.getUserDetail().getHeight().substring(0, user.getUserDetail().getHeight().length() - 2)) + "");
    }

}
