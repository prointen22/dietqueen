package com.livennew.latestdietqueen.info_screen.age_screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.info_screen.health_screen.HealthActivity;
import com.livennew.latestdietqueen.info_screen.height_screen.HeightActivity;
import com.livennew.latestdietqueen.info_screen.age_screen.adapter.AgeAdapter;
import com.livennew.latestdietqueen.info_screen.age_screen.model.LabelerDate;
import com.livennew.latestdietqueen.info_screen.height_screen.HeightActivityFromAchivement;
import com.livennew.latestdietqueen.info_screen.sleep_screen.HoursOfSleepActivity;
import com.livennew.latestdietqueen.language.LanguageActivity;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.user_detail.AchievementsActivity;
import com.livennew.latestdietqueen.utils.SupportClass;
import com.shawnlin.numberpicker.NumberPicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;

@AndroidEntryPoint
public class AgeActivity extends BaseActivity {
    RecyclerView rvAge;
    DatePicker datePicker;
    Switch switchAge;
    public float firstItemWidthDate;
    public float itemWidthDate;
    public int allPixelsDate;
    public int finalWidthDate;
    private AgeAdapter ageAdapter;
    private ArrayList<LabelerDate> labelerDates;
    private static final int VIEW_TYPE_PADDING = 1;
    private static final int VIEW_TYPE_ITEM = 2;
    private Button btnAgeNext;
    private String ageList = "", ageDatePicker = "";
    private ImageView ivBorder;
    private String screen;
    private NumberPicker npAge;
    private ProgressDialog pDialog;
    @Inject
    APIInterface apiInterface;

    @Inject
    SessionManager sessionManager;
    private User user;

    private RadioButton mRbYears, mRbDob;
    LinearLayout mLLBack;
    private TextView mTvYears;
    private boolean mUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_age);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        user = sessionManager.getUser();
        pDialog = new ProgressDialog(this);
        init();
        if (getIntent().getExtras() != null) {
            screen = getIntent().getExtras().getString("achievment");

            Log.d("Screen",">>>>>>>>>>>>>>>>  AgeActivity >>>>>>>>>>>>>>>>>>>>>>"+screen);
        }

        Intent intent1 = getIntent();
        if (getIntent().getExtras() != null) {
            user = intent1.getParcelableExtra("user");
            if (intent1.hasExtra("operation") && intent1.getStringExtra("operation").equals("update")) {
                mUpdate = true;
                btnAgeNext.setText(getString(R.string.save));
                npAge.setValue(Integer.parseInt(user.getAge()));
            } else {
                mUpdate = false;
                btnAgeNext.setText(getString(R.string.next));
            }
        } else {
            Log.i("check age", "Please check your age");
        }
        //Given code is use for year
        getRecyclerviewDate();

        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-message".
        LocalBroadcastManager.getInstance(AgeActivity.this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-message"));

        //Given code is use for date
        setCalender();

        //Switch code
        switchButton();

        btnAgeNext.setOnClickListener(v -> {


            if (mRbYears.isChecked()){
                if (user!=null){
                    user.setAge(String.valueOf(npAge.getValue()));
                }else{
                    user = new User();
                    user.setAge(String.valueOf(npAge.getValue()));
//                    sessionManager.setUser(user);
                }


            }else{
                dateToYearConverter();
                if (user!=null) {
                    user.setAge(ageDatePicker);
                }else{
                    user = new User();
                    user.setAge(ageDatePicker);
//                    sessionManager.setUser(user);
                }
            }

//            if (ageList.equalsIgnoreCase("")) {
//                //DatePicker through calculate age
//                dateToYearConverter();
//            }

                    if (mUpdate) {
                        updateHoursOfSleep();
                    } else {
                        Intent intent = new Intent(AgeActivity.this, HeightActivity.class);
                        intent.putExtra("user", user);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    }


//                    if (screen != null) {  //when coming from achivement page
//
//                        Intent intent = new Intent(AgeActivity.this, HeightActivityFromAchivement.class);
//                        if (!ageList.equalsIgnoreCase("")) {
//                            user.setAge(ageList);
//                        } else {
//                            user.setAge(ageDatePicker);
//                        }
//
//                        intent.putExtra("user", user);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//                        finish();
//                        overridePendingTransition(R.anim.anim_slide_in_left,
//                                R.anim.anim_slide_out_left);
//
//                    }else {
//                        Intent intent = new Intent(AgeActivity.this, HeightActivity.class);
//                        if (!ageList.equalsIgnoreCase("")) {
//                            user.setAge(ageList);
//                        } else {
//                            user.setAge(ageDatePicker);
//                        }
//
//                        intent.putExtra("user", user);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(intent);
//                        finish();
//                        overridePendingTransition(R.anim.anim_slide_in_left,
//                                R.anim.anim_slide_out_left);
//                    }



        });
    }


    /**
     * This function is used for set max date and min date in spinner date picker
     */
    public void setCalender() {
        Calendar calendar = Calendar.getInstance();//get the current day
        Date maximumDate = new Date();
        calendar.setTime(maximumDate);
        calendar.add(Calendar.YEAR, -14);
        long maxDate = calendar.getTime().getTime(); // maximum date is set 5 years

        datePicker.setMaxDate(maxDate);
        Date minimumDate = new Date();
        calendar.setTime(minimumDate);
        calendar.add(Calendar.YEAR, -100);
        long minDate = calendar.getTime().getTime();
        datePicker.setMinDate(minDate);  // minimum date is set 70 years
    }

    /**
     * This function is used for declared widgets
     */
    public void init() {
        rvAge = findViewById(R.id.rv_age);
        btnAgeNext = findViewById(R.id.btn_age_next);
        switchAge = findViewById(R.id.swtch_age);
        datePicker = findViewById(R.id.date_picker_age);
        ivBorder = findViewById(R.id.iv_border);
        labelerDates = new ArrayList<>();
        mRbYears = findViewById(R.id.rb_years);
        mRbDob = findViewById(R.id.rb_dob);
        mLLBack = findViewById(R.id.ll_back);
        mTvYears = findViewById(R.id.tv_years);
        npAge = findViewById(R.id.npAge);

        setYearsSelected();

        mRbYears.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setYearsSelected();
            }
        });
        mRbDob.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setDOBSelected();
            }
        });
        mLLBack.setOnClickListener(v -> onBackPressed());
    }

    /**
     * This function is used for switch view
     */
    public void switchButton() {
        switchAge.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ageList = "";
                rvAge.setVisibility(View.GONE);
                ivBorder.setVisibility(View.GONE);
                datePicker.setVisibility(View.VISIBLE);
            } else {
                ageDatePicker = "";
                rvAge.setVisibility(View.VISIBLE);
                ivBorder.setVisibility(View.VISIBLE);
                datePicker.setVisibility(View.GONE);
            }
        });
    }

    /**
     * This Function is use for showing age numbers, selected item showing big and color is changed
     */
    public void getRecyclerviewDate() {
        rvAge.postDelayed(new Runnable() {
            @Override
            public void run() {
                setDateValue();
            }
        }, 300);
        ViewTreeObserver vtoDate = rvAge.getViewTreeObserver();
        vtoDate.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

            @Override
            public boolean onPreDraw() {
                rvAge.getViewTreeObserver().removeOnPreDrawListener(this);
                finalWidthDate = rvAge.getMeasuredWidth();
                itemWidthDate = getResources().getDimension(R.dimen.item_dob_width);
                firstItemWidthDate = (finalWidthDate - itemWidthDate) / 2;
                allPixelsDate = 0;

                final LinearLayoutManager dateLayoutManager = new LinearLayoutManager(getApplicationContext());
                dateLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                rvAge.setLayoutManager(dateLayoutManager);

                /* Create a LinearSnapHelper and attach the recyclerView to it. */
                final LinearSnapHelper snapHelper = new LinearSnapHelper();
                snapHelper.attachToRecyclerView(rvAge);

                rvAge.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        allPixelsDate += dx;
                        recyclerView.post(() -> setDateValue());
                    }
                });
                genLabelerDate();
                ageAdapter = new AgeAdapter(AgeActivity.this, labelerDates, (int) firstItemWidthDate);
                rvAge.setAdapter(ageAdapter);
                ageAdapter.setSelecteditem(ageAdapter.getItemCount() - 1);
                rvAge.post(new Runnable() {
                    @Override
                    public void run() {
                        rvAge.smoothScrollToPosition(ageAdapter.getItemCount() - 64);
                    }
                });
                return true;
            }
        });
    }

    private void genLabelerDate() {
        for (int i = 4; i <= 101; i++) {
            LabelerDate labelerDate = new LabelerDate();
            labelerDate.setNumber(Integer.toString(i));
            labelerDates.add(labelerDate);

            if (i == 4 || i == 101) {
                labelerDate.setType(VIEW_TYPE_PADDING);
            } else {
                labelerDate.setType(VIEW_TYPE_ITEM);
            }
        }
    }

    private void setDateValue() {
        int expectedPositionDateColor = Math.round(allPixelsDate / itemWidthDate);
        int setColorDate = expectedPositionDateColor + 1;
        //  set color here
        ageAdapter.setSelecteditem(setColorDate);
    }

    /**
     * Date to year converter
     */
    public void dateToYearConverter() {
        String birthDate = datePicker.getDayOfMonth() + "-" + (datePicker.getMonth() + 1) + "-" + datePicker.getYear();
        SimpleDateFormat df = new SimpleDateFormat("dd-mm-yyyy", Locale.US);
        Date birthdate = null;
        try {
            birthdate = df.parse(birthDate);
            System.out.println(SupportClass.calculateAge(birthdate));
            ageDatePicker = String.valueOf(SupportClass.calculateAge(birthdate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //Received Data from AgeAdapter
    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            ageList = intent.getStringExtra("item");
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();


        if (mUpdate) {
        finish();

        }else {
            Intent intent = new Intent(AgeActivity.this, LanguageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    private void setYearsSelected() {
        mRbYears.setChecked(true);
        mRbYears.setTextColor(ContextCompat.getColor(this, R.color.orange_theme));
        mRbDob.setChecked(false);
        mRbDob.setTextColor(ContextCompat.getColor(this, R.color.black));
        mTvYears.setVisibility(View.VISIBLE);
//        rvAge.setVisibility(View.VISIBLE);
//        ivBorder.setVisibility(View.VISIBLE);
        npAge.setVisibility(View.VISIBLE);
        datePicker.setVisibility(View.GONE);
    }

    private void setDOBSelected() {
        mRbDob.setChecked(true);
        mRbDob.setTextColor(ContextCompat.getColor(this, R.color.orange_theme));
        mRbYears.setChecked(false);
        mRbYears.setTextColor(ContextCompat.getColor(this, R.color.black));
        mTvYears.setVisibility(View.GONE);
//        rvAge.setVisibility(View.GONE);
        npAge.setVisibility(View.GONE);
//        ivBorder.setVisibility(View.GONE);
        datePicker.setVisibility(View.VISIBLE);
    }

    public void updateHoursOfSleep() {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        apiInterface.updateUser(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull retrofit2.Response<UserResp> response) {
                try {
                    UserResp userResp = response.body();
                    pDialog.dismiss();
                    assert userResp != null;
                    if (!userResp.getError()) {
//                        Toast.makeText(AgeActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
                        sessionManager.setUser(userResp.getData());
//                        navigateToHealthScreen();
                        finish();
                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(AgeActivity.this);
                        dialog.setMessage(getMessageInSingleLine(userResp.getMessage()));
                        dialog.setCancelable(false);
                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                    }
                } catch (Exception e) {
                    Log.v("TAG", "Exception: "+e.getMessage());
                }
            }


            private void navigateToHealthScreen() {
                Intent intent = new Intent(AgeActivity.this, AchievementsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }

            @Override
            public void onFailure(Call<UserResp> call, Throwable t) {
                Toast.makeText(AgeActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        });
    }
}