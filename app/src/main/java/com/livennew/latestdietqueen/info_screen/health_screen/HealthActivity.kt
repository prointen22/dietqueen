package com.livennew.latestdietqueen.info_screen.health_screen

import android.R.attr.*
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.google.firebase.analytics.FirebaseAnalytics
import com.livennew.latestdietqueen.R
import com.livennew.latestdietqueen.SessionManager
import com.livennew.latestdietqueen.utils.TopBarUtil
import com.livennew.latestdietqueen.Util
import com.livennew.latestdietqueen.dashboard.DashboardSubActivity
import com.livennew.latestdietqueen.databinding.ActivityHealthBinding
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceResp
import com.livennew.latestdietqueen.info_screen.sleep_screen.HoursOfSleepActivity
import com.livennew.latestdietqueen.info_screen.weight_screen.WeightActivity
import com.livennew.latestdietqueen.model.User
import com.livennew.latestdietqueen.model.UserResp
import com.livennew.latestdietqueen.network_support.APIClient
import com.livennew.latestdietqueen.notification.NotificationListActivity
import com.livennew.latestdietqueen.retrofit.APIInterface
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil.Companion.screenViewEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HealthActivity : AppCompatActivity() {
    private var toolbar: Toolbar? = null
    var sessionManager: SessionManager? = null
    var apiInterface: APIInterface? = null
    private var user: User? = null
//    private lateinit var mIvNotification: ImageView
//    private lateinit var mIvDoctor: ImageView
    private lateinit var mBinding: ActivityHealthBinding
    private var pDialog: ProgressDialog? = null

    companion object {
        private var firebaseAnalytics: FirebaseAnalytics? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        mBinding = ActivityHealthBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        sessionManager = SessionManager(applicationContext)
        apiInterface = APIClient.getClient(applicationContext).create(APIInterface::class.java)
//        user = sessionManager?.user
//        initToolbar()
    }

    fun init() {
        val planBasic = user!!.dietPlan.dietPlaDetails.plan_name + ", "
        val planDetail = planBasic + getString(R.string.move_to_advance)
        val spannable: Spannable = SpannableString(planDetail)
        spannable.setSpan(ForegroundColorSpan(getColor(R.color.orange_theme)), planBasic.length, planDetail.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        mBinding.tvPlanDetail.setText(spannable, TextView.BufferType.SPANNABLE)

        mBinding.tvPlanDetail.setOnClickListener { navigatePricingScreen() }

        val mIvNotification:ImageView = findViewById(R.id.iv_notification)
        val mIvDoctor:ImageView = findViewById(R.id.iv_doctor)
        val mIvSideMenu:ImageView = findViewById(R.id.iv_menu)
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager!!)

//        mIvNotification = findViewById(R.id.iv_notification)
//        mIvNotification.setOnClickListener { navigateToNotificationScreen() }
//
//        mIvDoctor = findViewById(R.id.iv_doctor)
//        mIvDoctor.setOnClickListener {
//            startActivity(Intent(this, InstructionActivity::class.java))
//        }

        mBinding.tvWeightUpdate.setOnClickListener{ navigateToWeightActivity() }
        mBinding.tvSleepUpdate.setOnClickListener{ navigateToHoursOfSleepActivity() }

        pDialog = ProgressDialog(this)

        setUserDetails()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        user = sessionManager?.user
//        initToolbar()
        init()
    }

    private fun initToolbar() {
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar?.navigationIcon = null
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.title = ""
        }

//        getUserDetail()
        init()
    }

    private fun setUserDetails() {
        mBinding.tvWeight.text = ""
        mBinding.tvWaist.text = "36 inch"
        mBinding.tvBmi.text = ""
        mBinding.tvSleep.text = ""
        mBinding.tvFoodPreferences.text = ""
        mBinding.tvIdealWeight.text = ""

        if (!TextUtils.isEmpty(user!!.userDetail.weight)) {
            mBinding.tvWeight.text = "${user!!.userDetail.weight} kg"
        }

        if (!TextUtils.isEmpty(user!!.userDetail.hoursOfSleep)) {
            mBinding.tvSleep.text = "${user!!.userDetail.hoursOfSleep} hrs"
        }

        val string = SpannableString(getString(R.string.show_report))
        string.setSpan(UnderlineSpan(), 0, string.length, 0)
        mBinding.tvPhysicalActivity.text = string

        if (!TextUtils.isEmpty(user!!.userDetail.food)) {
            if (user!!.userDetail.food.equals("v")){
                mBinding.tvFoodPreferences.text = "Veg"
            }else if (user!!.userDetail.food.equals("nv")){
                mBinding.tvFoodPreferences.text = "Non Veg"
            }else if (user!!.userDetail.food.equals("ve")){
                mBinding.tvFoodPreferences.text = "Veg + Eggs"
            }

        }

        getFitnessAdvice()
    }

    private fun navigateToNotificationScreen() {
        firebaseAnalytics?.let { screenViewEvent("", it) }
        val intent1 = Intent(this, NotificationListActivity::class.java)
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent1)
    }

    private fun navigatePricingScreen() {
        val intent = Intent(this, DashboardSubActivity::class.java)
        intent.putExtra("type", getString(R.string.pricing))
        startActivity(intent)
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left)
    }

    private fun navigateToWeightActivity() {
        val intent = Intent(this, WeightActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("operation", "update")
        intent.putExtra("user", user)
        startActivity(intent)
    }

    private fun navigateToHoursOfSleepActivity() {
        val intent = Intent(this, HoursOfSleepActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("operation", "update")
        intent.putExtra("user", user)
        startActivity(intent)
    }

    private fun getUserDetail() {
        CompositeDisposable().add(apiInterface!!.profile
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<UserResp?>() {
                    override fun onSuccess(userResp: UserResp) {
                        if (!userResp.error) {
                            user = userResp.data
                            user?.profileImage = (userResp.others.userImageBase64Path + user?.profileImage)
                            sessionManager!!.user = user

                            Log.v("TAG", "Plan name: ${user?.dietPlan?.dietPlaDetails?.plan_name}")
                        } else {
                            Toast.makeText(applicationContext, Util.getMessageInSingleLine(userResp.message), Toast.LENGTH_LONG).show()
                        }
                        init()
                    }

                    override fun onError(e: Throwable) {
                        init()
                    }
                }))
    }

    private fun getFitnessAdvice() {
        try {
            pDialog!!.setMessage("Loading...")
            pDialog!!.setCancelable(false)
            pDialog!!.show()

            apiInterface!!.fitnessAdvice.enqueue(object : Callback<FitnessAdviceResp> {
                override fun onResponse(call: Call<FitnessAdviceResp>, response: Response<FitnessAdviceResp>) {
                    pDialog!!.dismiss()
                    if (!response.body()!!.error) {
                        mBinding.tvBmi.text = response.body()!!.data.bmi
                        if (response.body()!!.data.weightType.equals("Loss")){
                            mBinding.tvIdealWeight.text =
                                    "Ideal body weight: "+(user!!.userDetail.weight.toInt() - response.body()!!.data.lossAndGain.toInt()).toString()+"kg"
                        }else{
                            mBinding.ivWeight.rotation = 180.0f
                            mBinding.tvIdealWeight.text =
                                    "Ideal body weight: "+(user!!.userDetail.weight.toInt() + response.body()!!.data.lossAndGain.toInt()).toString()+"kg"
                        }
                    } else {
                        for (i in response.body()!!.message.indices) {
                            Toast.makeText(this@HealthActivity, response.body()!!.message[i].toString(), Toast.LENGTH_SHORT).show()
                        }
                    }
                }

                override fun onFailure(call: Call<FitnessAdviceResp>, t: Throwable) {
                    Toast.makeText(this@HealthActivity, t.message, Toast.LENGTH_SHORT).show()
                    pDialog!!.dismiss()
                }
            })
        } catch (e: Exception) {
            Log.v("TAG", "Exception is: ${e.message}")
        }
    }

}