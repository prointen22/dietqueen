package com.livennew.latestdietqueen.info_screen.sleep_screen;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;

import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.info_screen.age_screen.model.LabelerDate;
import com.livennew.latestdietqueen.info_screen.food_preference_screen.FoodPreferencesActivity;
import com.livennew.latestdietqueen.info_screen.health_screen.HealthActivity;
import com.livennew.latestdietqueen.info_screen.sleep_screen.adapter.HourOfSleepAdapter;
import com.livennew.latestdietqueen.info_screen.weight_screen.WeightActivity;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.retrofit.APIInterface;

import java.util.ArrayList;

import javax.inject.Inject;

@AndroidEntryPoint
public class HoursOfSleepActivity extends BaseActivity {
    private String sleepHour = "";
    private Button btnHosAge, btnHosWeight, btnHosHeight, btnHosNext, mBtnMinus, mBtnPlus;
    private RecyclerView rvHoursOfSleep;
    private HourOfSleepAdapter hourOfSleepAdapter;
    public float firstItemWidthDate;
    public float itemWidthDate;
    public int allPixelsDate;
    public int finalWidthDate;
    private ArrayList<LabelerDate> labelerHours;
    private static final int VIEW_TYPE_PADDING = 1;
    private static final int VIEW_TYPE_ITEM = 2;
    private User user;
    LinearLayout mLLBack;
    private int selectedItem;
    private LinearLayoutManager dateLayoutManager;
    private boolean mUpdate = false;

    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        setContentView(R.layout.activity_hours_of_sleep);
        init();
        pDialog = new ProgressDialog(this);
        Intent intent = getIntent();
        if (intent != null) {
            user = intent.getParcelableExtra("user");
            if (intent.hasExtra("operation") && intent.getStringExtra("operation").equals("update")) {
                mUpdate = true;
                btnHosNext.setText(getString(R.string.save));
            } else {
                mUpdate = false;
                btnHosNext.setText(getString(R.string.next));
            }
        } else {
            Log.i("check age", "Please check your age");
        }
        btnHosAge.setText(user.getAge());
        btnHosHeight.setText(user.getUserDetail().getHeight());
        btnHosWeight.setText(user.getUserDetail().getWeight());

        btnHosNext.setOnClickListener(v -> {
            if (mUpdate) {
                user.getUserDetail().setHoursOfSleep(sleepHour);
                updateHoursOfSleep();
            } else {
                Intent intent1 = new Intent(HoursOfSleepActivity.this, FoodPreferencesActivity.class);
                user.getUserDetail().setHoursOfSleep(sleepHour.concat(" Hr"));
                intent1.putExtra("user", user);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                finish();
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        });
        //Given code is use for year
        getRvSleepTime();
        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-message".
        LocalBroadcastManager.getInstance(HoursOfSleepActivity.this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-message"));


    }

    private void getRvSleepTime() {
        rvHoursOfSleep.postDelayed(() -> setHoursValue(), 300);
        ViewTreeObserver vtoDate = rvHoursOfSleep.getViewTreeObserver();
        vtoDate.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

            @Override
            public boolean onPreDraw() {
                rvHoursOfSleep.getViewTreeObserver().removeOnPreDrawListener(this);
                finalWidthDate = rvHoursOfSleep.getMeasuredWidth();
                itemWidthDate = getResources().getDimension(R.dimen.item_dob_width);
                firstItemWidthDate = (finalWidthDate - itemWidthDate) / 2;
                allPixelsDate = 0;

                dateLayoutManager = new LinearLayoutManager(getApplicationContext());
                dateLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                rvHoursOfSleep.setLayoutManager(dateLayoutManager);

                /* Create a LinearSnapHelper and attach the recyclerView to it. */
                final LinearSnapHelper snapHelper = new LinearSnapHelper();
                snapHelper.attachToRecyclerView(rvHoursOfSleep);

                rvHoursOfSleep.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        allPixelsDate += dx;
                        recyclerView.post(new Runnable() {
                            public void run() {
                                setHoursValue();
                            }
                        });
                    }
                });
                genLabelerSleepHour();
                Log.i("TAG","labelerHours = "+labelerHours);
                Log.i("TAG","firstItemWidthDate = "+firstItemWidthDate);
                hourOfSleepAdapter = new HourOfSleepAdapter(HoursOfSleepActivity.this, labelerHours, (int) firstItemWidthDate,selectedItem);
                rvHoursOfSleep.setAdapter(hourOfSleepAdapter);
                hourOfSleepAdapter.setSelecteditem(hourOfSleepAdapter.getItemCount() - 1);
                return true;
            }
        });
    }

    private void genLabelerSleepHour() {
        for (int i = 3; i <= 25; i++) {
            LabelerDate labelerDate = new LabelerDate();
            labelerDate.setNumber(Integer.toString(i));
            labelerHours.add(labelerDate);

            if (i == 3|| i == 25) {
                labelerDate.setType(VIEW_TYPE_PADDING);
            } else {
                labelerDate.setType(VIEW_TYPE_ITEM);
            }
        }
    }

    private void setHoursValue() {
        int expectedPositionDateColor = Math.round(allPixelsDate / itemWidthDate);
        int setColorDate = expectedPositionDateColor + 1;
        //  set color here
        selectedItem = setColorDate;
        hourOfSleepAdapter.setSelecteditem(setColorDate);
    }

    private void init() {
        btnHosAge = findViewById(R.id.btn_hos_age);
        btnHosWeight = findViewById(R.id.btn_hos_weight);
        btnHosHeight = findViewById(R.id.btn_hos_height);
        btnHosNext = findViewById(R.id.btn_sleep_next);
        rvHoursOfSleep = findViewById(R.id.rv_sleep);
        labelerHours = new ArrayList<>();
        mBtnMinus = findViewById(R.id.btn_minus);
        mBtnPlus = findViewById(R.id.btn_plus);
        mLLBack = findViewById(R.id.ll_back);

        mBtnMinus.setOnClickListener(v -> {
            selectedItem--;
            hourOfSleepAdapter.setSelecteditem(selectedItem);
            dateLayoutManager.scrollToPosition(selectedItem);
        });

        mBtnPlus.setOnClickListener(v -> {
            selectedItem++;
            hourOfSleepAdapter.setSelecteditem(selectedItem);
            dateLayoutManager.scrollToPosition(selectedItem);
        });

        mLLBack.setOnClickListener(v -> {
            onBackPressed();
        });
    }

    //Received Data from AgeAdapter
    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            sleepHour = intent.getStringExtra("item");
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mUpdate) {
//            navigateToHealthScreen();
            finish();
        } else {
            Intent intent = new Intent(HoursOfSleepActivity.this, WeightActivity.class);
            intent.putExtra("user", user);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    public void updateHoursOfSleep() {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        apiInterface.updateUser(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull retrofit2.Response<UserResp> response) {
                try {
                    UserResp userResp = response.body();
                    pDialog.dismiss();
                    assert userResp != null;
                    if (!userResp.getError()) {
//                        Toast.makeText(HoursOfSleepActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
                        sessionManager.setUser(userResp.getData());
//                        navigateToHealthScreen();
                        finish();
                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(HoursOfSleepActivity.this);
                        dialog.setMessage(getMessageInSingleLine(userResp.getMessage()));
                        dialog.setCancelable(false);
                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                    }
                } catch (Exception e) {
                    Log.v("TAG", "Exception: "+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UserResp> call, Throwable t) {
                Toast.makeText(HoursOfSleepActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        });
    }

    private void navigateToHealthScreen() {
        Intent intent = new Intent(HoursOfSleepActivity.this, HealthActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }
}
