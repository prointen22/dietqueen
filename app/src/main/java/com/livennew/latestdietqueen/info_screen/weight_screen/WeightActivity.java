package com.livennew.latestdietqueen.info_screen.weight_screen;

import static com.livennew.latestdietqueen.fragments.products.ProductListFragment.TAG;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.PointValue;
import retrofit2.Call;
import retrofit2.Callback;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kevalpatel2106.rulerpicker.RulerValuePicker;
import com.kevalpatel2106.rulerpicker.RulerValuePickerListener;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.ActivityWeightBinding;
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceActivityNew;
import com.livennew.latestdietqueen.info_screen.food_preference_screen.FoodPreferencesActivity;
import com.livennew.latestdietqueen.info_screen.health_screen.HealthActivity;
import com.livennew.latestdietqueen.info_screen.height_screen.HeightActivity;
import com.livennew.latestdietqueen.info_screen.sleep_screen.HoursOfSleepActivity;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.my_plans.AddMyWeightActivity;
import com.livennew.latestdietqueen.my_plans.model.WeightAndWaistLine;
import com.livennew.latestdietqueen.my_plans.model.WeightHistoryResp;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.retrofit.APIInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

@AndroidEntryPoint
public class WeightActivity extends AppCompatActivity {

    private String weight = "0";
    Double weightKg = 0.0;
    private User user;
    private ActivityWeightBinding binding;
    private boolean mUpdate = false;
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        binding = ActivityWeightBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        pDialog = new ProgressDialog(this);
        Intent intent = getIntent();
        if (intent != null) {
            user = intent.getParcelableExtra("user");
            if (intent.hasExtra("operation") && intent.getStringExtra("operation").equals("update")) {
                mUpdate = true;
                binding.btnWeightNext.setText(getString(R.string.save));
                if (!TextUtils.isEmpty(user.getUserDetail().getWeight())) {
                    binding.rpWeightKG.selectValue(Integer.parseInt(user.getUserDetail().getWeight()));
                }
            } else {
                binding.rpWeightKG.selectValue(70);
                mUpdate = false;
                binding.btnWeightNext.setText(getString(R.string.next));
            }
        } else {
            Log.i("check age", "Please check your age");
        }
//        binding.btnShowAge.setText(user.getAge());
//        binding.btnHeight.setText(user.getUserDetail().getHeight());
        //Switch code
        switchButton();

        //RulerPicker code of Weight
        binding.rpWeightKG.setValuePickerListener(new RulerValuePickerListener() {
            @Override
            public void onValueChange(final int selectedValue) {
                weight = String.valueOf(selectedValue);
            }

            @Override
            public void onIntermediateValueChange(int selectedValue) {
                //Value changed but the `user` is still scrolling the ruler.
                //This value is not final value. Application can utilize this value to display the current selected value.
                Log.i("msg", String.valueOf(selectedValue));
                weight = String.valueOf(selectedValue);
            }
        });
        binding.rpWeightLB.setValuePickerListener(new RulerValuePickerListener() {
            @Override
            public void onValueChange(int selectedValue) {
                weight = String.valueOf(selectedValue);
            }

            @Override
            public void onIntermediateValueChange(int selectedValue) {
                weight = String.valueOf(selectedValue);
            }
        });
        binding.btnWeightNext.setOnClickListener(v -> {
            if (Integer.parseInt(weight) > 10) {
                if (binding.rbKg.isChecked()) {
                    weight = binding.rpWeightKG.getCurrentValue() + "";
                    weightKg = Double.parseDouble(weight);
//                    user.getUserDetail().setWeight(weight + "KG");
                } else {
                    if (binding.rbLb.isChecked()) {
                        weight = binding.rpWeightLB.getCurrentValue() + "";
                        weightKg = Double.parseDouble(weight) / 2.2046;
                        String weightLB = String.valueOf(new DecimalFormat("##").format(weightKg)).concat("KG");
//                        user.getUserDetail().setWeight(weightLB);
                    }
                }
                if (mUpdate) {
                    user.getUserDetail().setWeight(weightKg.toString());
                    updateWeight();
//                    intent1 = new Intent(WeightActivity.this, HealthActivity.class);
//                    sessionManager.setUser(user);
                } else {
                    user.getUserDetail().setWeight(weightKg + "KG");
                    Intent intent1 = new Intent(WeightActivity.this, HoursOfSleepActivity.class);
                    intent1.putExtra("user", user);
                    intent1.putExtra("weight", user.getUserDetail().getWeight());
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    overridePendingTransition(R.anim.anim_slide_in_left,
                            R.anim.anim_slide_out_left);
                }
            } else {
                Toast.makeText(WeightActivity.this, "Minimum 10 KG or LB weight is required", Toast.LENGTH_SHORT).show();
            }
        });
        setKGSelected();
        binding.rbKg.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setKGSelected();
            }
        });
        binding.rbLb.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setLBSelected();
            }
        });
        binding.llBack.setOnClickListener(e-> onBackPressed());
    }

    /**
     * This function is used for switch view
     */
    public void switchButton() {
        binding.include2.swtchWeight.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                binding.rpWeightKG.setVisibility(View.GONE);
                binding.rpWeightLB.setVisibility(View.VISIBLE);
                binding.rpWeightLB.selectValue(155);
            } else {
                binding.rpWeightKG.setVisibility(View.VISIBLE);
                binding.rpWeightLB.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mUpdate) {
//            navigateToHealthScreen();
            finish();
        } else {
            Intent intent = new Intent(WeightActivity.this, HeightActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("user", user);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    private void setKGSelected() {
        binding.rbKg.setChecked(true);
        binding.rbKg.setTextColor(ContextCompat.getColor(this, R.color.orange_theme));
        binding.rbLb.setChecked(false);
        binding.rbLb.setTextColor(ContextCompat.getColor(this, R.color.black));
        binding.rpWeightKG.setVisibility(View.VISIBLE);
        binding.rpWeightLB.setVisibility(View.GONE);
    }

    private void setLBSelected() {
        binding.rbLb.setChecked(true);
        binding.rbLb.setTextColor(ContextCompat.getColor(this, R.color.orange_theme));
        binding.rbKg.setChecked(false);
        binding.rbKg.setTextColor(ContextCompat.getColor(this, R.color.black));
        binding.rpWeightKG.setVisibility(View.GONE);
        binding.rpWeightLB.setVisibility(View.VISIBLE);
        binding.rpWeightLB.selectValue(155);
    }

    public void updateWeight() {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        String todayDate = formatter.format(new Date());
        HashMap<String,String> params = new HashMap<>();
        params.put("date", todayDate);
        params.put("weight", weightKg.toString());
        apiInterface.setBodyWeight(params).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull retrofit2.Response<UserResp> response) {
                try {
                    UserResp userResp = response.body();
                    pDialog.dismiss();
                    assert userResp != null;
                    if (!userResp.getError()) {
//                        Toast.makeText(WeightActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
//                       user.getUserDetail().setWeight(weightKg.toString());
                        sessionManager.setUser(userResp.getData());
//                        sessionManager.setUser(user);
//                        navigateToHealthScreen();
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);

                        finish();
                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(WeightActivity.this);
                        dialog.setMessage(getMessageInSingleLine(userResp.getMessage()));
                        dialog.setCancelable(false);
                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                    }
                } catch (Exception e) {
                    Log.v("TAG", "Exception: "+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UserResp> call, Throwable t) {
                Toast.makeText(WeightActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        });

//        try {
////            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
////
////            String todayDate = formatter.format(new Date());
////            JSONObject params = new JSONObject();
////            params.put("date", todayDate);
////            params.put("weight", weightKg.toString());
//
//            Log.i(TAG,"params = "+params);
////            params.put("waistline", totalHeight);
//            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.INSERTBODYWEIGHT, params, response -> {
//                Log.d(TAG, "onResponse Response: " + response);
//                try {
//                    JSONObject jObj = new JSONObject(response.toString());
//                    boolean error = jObj.getBoolean("error");
//                    if (!error) {
//                        JSONArray jsonArray = jObj.getJSONArray("message");
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
//                        }
//                        user.getUserDetail().setWeight(weightKg.toString());
//                        sessionManager.setUser(user);
//                        finish();
////                        getBodyWeight();
////                        etWeight.setText("");
////                        etWaistline.setText("");
////                        binding.etDate.setText("");
////                        if (Integer.parseInt(tvLastWeight.getText().toString()) < Integer.parseInt(totalWeight)) {
////                            AlertDialog.Builder dialog = new AlertDialog.Builder(AddMyWeightActivity.this);
////                            dialog.setMessage(String.format(getString(R.string.your_weight_has_increased_let_us), user.getName()));
////                            dialog.setCancelable(false);
////                            dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
////                        }
//                    } else {
//                        //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
//                        Toast.makeText(WeightActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
//                    }
//                    pDialog.dismiss();
////                    updateLabel();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    pDialog.dismiss();
//                }
//            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> headers = new HashMap<String, String>();
//                    headers.put("Content-Type", "application/json; charset=utf-8");
//                    headers.put("Accept", "application/json");
//                    headers.put("Authorization", user.getToken());
//                    headers.put("userId", String.valueOf(user.getId()));
//                    return headers;
//                }
//            };
//            strReq.setShouldCache(false);
//            RequestQueue requestQueue = Volley.newRequestQueue(WeightActivity.this);
//            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            requestQueue.add(strReq);
//        } catch (Exception e) {
//            e.printStackTrace();
//            pDialog.dismiss();
//        }
    }

    public StringBuilder getMessageInSingleLine(List<String> message) {
        StringBuilder sb = new StringBuilder();
        boolean appendSeparator = false;
        for (int i = 0; i < message.size(); i++) {
            if (appendSeparator)
                sb.append('\n'); // a comma
            appendSeparator = true;
            sb.append(message.get(i));
        }
        return sb;
    }

    private void navigateToHealthScreen() {
        Intent intent = new Intent(WeightActivity.this, HealthActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }

}
