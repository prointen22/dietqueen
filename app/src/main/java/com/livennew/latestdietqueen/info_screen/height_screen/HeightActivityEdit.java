package com.livennew.latestdietqueen.info_screen.height_screen;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;

import com.kevalpatel2106.rulerpicker.RulerValuePicker;
import com.kevalpatel2106.rulerpicker.RulerValuePickerListener;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceActivity;
import com.livennew.latestdietqueen.fragments.products.ProductDetailActivity;
import com.livennew.latestdietqueen.info_screen.age_screen.AgeActivity;
import com.livennew.latestdietqueen.info_screen.food_preference_screen.FoodPreferencesActivity;
import com.livennew.latestdietqueen.info_screen.sleep_screen.HoursOfSleepActivityAchivement;
import com.livennew.latestdietqueen.model.SubScribe;
import com.livennew.latestdietqueen.model.SubScribeResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserDetail;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.user_detail.AchievementsActivity;
import com.livennew.latestdietqueen.user_detail.PersonalDetailActivity;
import com.shawnlin.numberpicker.NumberPicker;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.QueryMap;
@AndroidEntryPoint
public class HeightActivityEdit extends AppCompatActivity {

    private String heightCM = "", height = "5.0";
    private Button btnAge, btnHeightNext;
    private RulerValuePicker rulerPickerHeightCm;
    private Switch switchHeight;
    Double heightCm = 0.0;
    private User user;
    private NumberPicker npFeet;
    private NumberPicker npInch;
    private Group gFeet;
    LinearLayoutCompat ll_view;
    private String screen;
    @Inject
    SessionManager sessionManager;
    private RadioButton mRbFeet, mRbCM;
    @Inject
    APIInterface apiInterface;
    LinearLayout mLLBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        setContentView(R.layout.activity_height_actvity);
        init();
      //user = sessionManager.getUser();

       Intent intent = getIntent();
        if (intent != null) {
            user = intent.getParcelableExtra("user");
            if (user.getUserDetail() == null)
                user.setUserDetail(new UserDetail());
        } else {
            Log.i("check age", "Please check your age");
        }
       btnAge.setText(user.getAge());
        switchButton();
        rulerPickerHeightCm.setValuePickerListener(new RulerValuePickerListener() {
            @Override
            public void onValueChange(final int selectedValue) {
                Log.i("msg", String.valueOf(selectedValue));
                height = String.valueOf(selectedValue);
            }

            @Override
            public void onIntermediateValueChange(int selectedValue) {
                Log.i("msg", String.valueOf(selectedValue));
                height = String.valueOf(selectedValue);
            }
        });

        Log.d("dddd","dddddddddddddddddddddddddddddddddd user.getheight() dddddddddddddddddddddddddddddddddddd"+user.getUserDetail().getHeight());

        btnHeightNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchHeight.isChecked()) {
                    height = rulerPickerHeightCm.getCurrentValue() + "";
                    heightCm = Double.valueOf(height);
                } else {
                    heightCm = npFeet.getValue() * 30.48 + npInch.getValue() * 2.54;
                }
                if (heightCm < 30.48 || heightCm > 338.64) {
                    Toast.makeText(HeightActivityEdit.this, getString(R.string.please_select_correct_height), Toast.LENGTH_SHORT).show();
                    return;
                }
                heightCM = String.valueOf(new DecimalFormat("##").format(heightCm)).concat("CM");
                editHeight(heightCM);
               /* Intent intent = new Intent(HeightActivityEdit.this, HoursOfSleepActivityAchivement.class);
                user.getUserDetail().setHeight(heightCM);
                intent.putExtra("user", user);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);*/



            }
        });

//        rulerPickerHeightCm.selectValue(155);
        rulerPickerHeightCm.selectValue(Integer.parseInt(user.getUserDetail().getHeight()));
//        npFeet.setValue(5);
//        npInch.setValue(0);

        double a = (Integer.parseInt(user.getUserDetail().getHeight()))/2.54;
        int b = Integer.parseInt(String.valueOf(a/12).substring(0,6).split("\\.")[0]); //ft
        double c = (a - 12) * b;

        double feetInch = (Integer.parseInt(user.getUserDetail().getHeight())/ 30.48);
        String[] arr = String.valueOf(feetInch).split("\\.");
        int feet = Integer.parseInt(arr[0]);
        int inch = Integer.parseInt(arr[1].substring(0,5));
        npFeet.setValue(b);
        npInch.setValue(Integer.parseInt(String.valueOf(c).substring(0,2)));
    }

    public void init() {
        btnAge = findViewById(R.id.btn_age);
        btnAge.setVisibility(View.GONE);
        rulerPickerHeightCm = findViewById(R.id.ruler_picker_cm);
        gFeet = (Group) findViewById(R.id.gFeet);
        npFeet = (NumberPicker) findViewById(R.id.npFeet);
        npInch = (NumberPicker) findViewById(R.id.npInch);
        switchHeight = findViewById(R.id.swtch_height);
        btnHeightNext = findViewById(R.id.btn_height_next);
        btnHeightNext.setText("Submit");
        ll_view=findViewById(R.id.ll_view);
        ll_view.setVisibility(View.GONE);
        mRbFeet = findViewById(R.id.rb_feet);
        mRbCM = findViewById(R.id.rb_cm);
        setFeetSelected();

        mRbFeet.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setFeetSelected();
            }
        });
        mRbCM.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                setCMSelected();
            }
        });
        mLLBack = findViewById(R.id.ll_back);
        mLLBack.setOnClickListener(v -> onBackPressed());
    }

    /**
     * This function is used for switch view
     */
    public void switchButton() {
        switchHeight.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                gFeet.setVisibility(View.GONE);
                rulerPickerHeightCm.setVisibility(View.VISIBLE);
                rulerPickerHeightCm.selectValue(Integer.parseInt(user.getUserDetail().getHeight()));

            } else {
                gFeet.setVisibility(View.VISIBLE);
                rulerPickerHeightCm.setVisibility(View.GONE);
                rulerPickerHeightCm.selectValue(Integer.parseInt(user.getUserDetail().getHeight()));


            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(HeightActivityEdit.this, AchievementsActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.putExtra("achievment", "achievment");
//        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }


    public void editHeight(String heightCM){
       // rlProgressBar.setVisibility(View.VISIBLE);
        //nsv.setVisibility(View.GONE);
        Log.d("dddd","dddddddddddddddddddddddddddddddddd user.getId() dddddddddddddddddddddddddddddddddddd"+sessionManager.getUser().getId());
       ProgressDialog pDialog=new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        user.getUserDetail().setHeight(Double.parseDouble(heightCM.substring(0, heightCM.length() - 2)) + "");

       // user.getUserDetail().setHeight(heightCM);

        apiInterface.updateHeight(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull Response<UserResp> response) {
                try {
                    UserResp userResp = response.body();
                    pDialog.dismiss();
                    if (!userResp.getError()) {
                        Log.d("ccccc","ddddddddddddddddddddddddddddddddddd Saved"+userResp.getData());
                      //  sessionManager.getUser().setHeight(heightCM);

                        sessionManager.setUser(userResp.getData());

                        Intent intent = new Intent(HeightActivityEdit.this, AchievementsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                      //  sessionManager.getUser().setHeight(heightCM);
                       // sessionManager.setUser(user.setHeight("fdgfd"));
                //      user.getUserDetail().setHeight(heightCM);
                    //    userResp.getData().setHeight(heightCM);

                        //     Toast.makeText(this, "Height Updated Succesfully"), Toast.LENGTH_SHORT).show();
/*
                        Intent intent = new Intent(HeightActivityEdit.this, AchievementsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                       Toast.makeText(FoodPreferencesActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                        //userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
                        sessionManager.setUser(userResp.getData());
                        Intent intent = new Intent(FoodPreferencesActivity.this, FitnessAdviceActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);*/
                    } else {
                        Log.d("ccccc","ddddddddddddddddddddddddddddddddddd error");

                     /*   AlertDialog.Builder dialog = new AlertDialog.Builder(FoodPreferencesActivity.this);
                        dialog.setMessage(getMessageInSingleLine(userResp.getMessage()));
                        dialog.setCancelable(false);
                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserResp> call, Throwable t) {
              //  Toast.makeText(FoodPreferencesActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();

            }
        });


    }


    private void setFeetSelected() {
        mRbFeet.setChecked(true);
        mRbFeet.setTextColor(ContextCompat.getColor(this, R.color.pink));
        mRbCM.setChecked(false);
        mRbCM.setTextColor(ContextCompat.getColor(this, R.color.black));
        gFeet.setVisibility(View.VISIBLE);
        rulerPickerHeightCm.setVisibility(View.GONE);
    }

    private void setCMSelected() {
        mRbCM.setChecked(true);
        mRbCM.setTextColor(ContextCompat.getColor(this, R.color.pink));
        mRbFeet.setChecked(false);
        mRbFeet.setTextColor(ContextCompat.getColor(this, R.color.black));
        gFeet.setVisibility(View.GONE);
        rulerPickerHeightCm.setVisibility(View.VISIBLE);
        rulerPickerHeightCm.selectValue(Integer.parseInt(user.getUserDetail().getHeight()));
    }

}
