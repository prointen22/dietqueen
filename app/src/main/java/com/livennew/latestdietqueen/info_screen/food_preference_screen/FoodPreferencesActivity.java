package com.livennew.latestdietqueen.info_screen.food_preference_screen;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.about_us.AboutUsActivity;
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceActivityNew;
import com.livennew.latestdietqueen.info_screen.food_preference_screen.adapter.RegionAdapter;
import com.livennew.latestdietqueen.info_screen.sleep_screen.HoursOfSleepActivity;
import com.livennew.latestdietqueen.info_screen.weight_screen.WeightActivity;
import com.livennew.latestdietqueen.model.FoodType;
import com.livennew.latestdietqueen.model.Region;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.RecyclerItemClickListener;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;


@AndroidEntryPoint
public class FoodPreferencesActivity extends BaseActivity implements IRegionListner {
    private static final String TAG = "FoodPreferences";
    private TextView tvVegButton, tvNonVegButton, tvVegEggBtn, tvTermAndCondition;
    private Button btnAge, btnWeight, btnHeight, btnFood, btnSubmitData, btnSleep;
    private ProgressDialog pDialog;
    private BottomSheetDialog mBottomSheetDialog;
    View bottomSheet;
    private RecyclerView rvRegion;
    private ArrayList<Region> alRegional;
    private RegionAdapter regionAdapter;
    private GridLayoutManager recyclerLayoutManager;
    private ProgressBar progressBar;
    private EditText etProfession;
    User user;
    private boolean mUpdate = false;
    @Inject
    SessionManager sessionManager;
    @Inject
    APIInterface apiInterface;
    LinearLayout mLLBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_preferences);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        user = sessionManager.getUser();
        init();
        Intent intent = getIntent();
        if (intent != null) {
            user = intent.getParcelableExtra("user");
        } else {
            Log.i("check age", "Please check your age");
        }


        Intent intent1 = getIntent();
        if (intent1 != null) {
            user = intent1.getParcelableExtra("user");
            if (intent1.hasExtra("operation") && intent1.getStringExtra("operation").equals("update")) {
                mUpdate = true;
                btnSubmitData.setText(getString(R.string.save));
                String food = null,region="";
                if (!TextUtils.isEmpty(user.getUserDetail().getFood())) {
                    food = user.getUserDetail().getFood();
                    if (food.equalsIgnoreCase("v")){
                        clickOnVegButton();
                    }else if (food.equalsIgnoreCase("ve")){
                        clickOnVegEggButton();
                    }else if (food.equalsIgnoreCase("nv")){
                        clickOnNonVegButton();
                    }else{
                        clickOnVegButton();
                    }
                }
               if (user.getUserDetail().getRegion()!=null){
                   region = user.getUserDetail().getRegion().getName();
               }


            } else {
//                binding.rpWeightKG.selectValue(70);
                mUpdate = false;
                btnSubmitData.setText(getString(R.string.next));
            }
        } else {
            Log.i("check age", "Please check your age");
        }
        btnAge.setText(user.getAge());
        btnWeight.setText(user.getUserDetail().getWeight());
        btnHeight.setText(user.getUserDetail().getHeight());
        btnSleep.setText(user.getUserDetail().getHoursOfSleep());
        tvTermAndCondition.setOnClickListener(v -> {
            Intent i = new Intent(FoodPreferencesActivity.this, AboutUsActivity.class);
            i.putExtra("pageNo", 1);
            startActivity(i);
//                showBottomSheetDialog();
        });
        tvVegButton.setOnClickListener(v -> clickOnVegButton());

        tvNonVegButton.setOnClickListener(v -> clickOnNonVegButton());

        if (SupportClass.checkConnection(this)) {
            getRegionalList();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }

        tvVegEggBtn.setOnClickListener(View -> {
            clickOnVegEggButton();
        });

        etProfession.setOnClickListener(View -> {
            startActivityForResult(new Intent(FoodPreferencesActivity.this, ProfessionActivity.class), ProfessionActivity.PROFESSION_SELECTION_REQ);
        });
        btnSubmitData.setOnClickListener(v -> {

//            removeLastTwoCharForWeight();

//            removeLastTwoCharForHeight();
//            removeLastTwoCharForHour();
            if (user.getUserDetail().getRegion() == null) {
                Toast.makeText(FoodPreferencesActivity.this, "Please select region!!!", Toast.LENGTH_SHORT).show();
            } else {
                if (user.getUserDetail().getFood() == null) {
                    Toast.makeText(FoodPreferencesActivity.this, "Please select food preference", Toast.LENGTH_SHORT).show();
                } else {
                    if (mUpdate){
                        updateWeight();
                    }else {
                        Intent intent2 = new Intent(FoodPreferencesActivity.this, ProfessionActivity.class);
                        intent2.putExtra("user", user);
                        intent2.putExtra("screen", "signup");
                        startActivity(intent2);
                        finish();
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    }

//                    if (user.getUserDetail().getProfession() == null) {
////                        Toast.makeText(FoodPreferencesActivity.this, "Please select profession", Toast.LENGTH_SHORT).show();
////                        startActivityForResult(new Intent(FoodPreferencesActivity.this, ProfessionActivity.class), ProfessionActivity.PROFESSION_SELECTION_REQ);
//                        Intent intent1 = new Intent(FoodPreferencesActivity.this, ProfessionActivity.class);
//                        intent1.putExtra("user", user);
//                        startActivity(intent1);
//                        finish();
//                        overridePendingTransition(R.anim.anim_slide_in_left,
//                                R.anim.anim_slide_out_left);
//                    } else {
//                        if (SupportClass.checkConnection(FoodPreferencesActivity.this)) {
//                            infoSubmit();
//                        } else {
//                            SupportClass.noInternetConnectionToast(FoodPreferencesActivity.this);
//                        }
//                    }
                }
            }
        });
//        tvVegButton.callOnClick();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ProfessionActivity.PROFESSION_SELECTION_REQ) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    user.getUserDetail().setProfession(data.getExtras().getParcelable(ProfessionActivity.SELECTED_PROFESSION));
                    etProfession.setText(user.getUserDetail().getProfession().getName());
                }
            }
        }
    }

    public void init() {
        tvVegButton = findViewById(R.id.tv_veg_button);
        tvNonVegButton = findViewById(R.id.tv_non_veg_button);
        tvVegEggBtn = findViewById(R.id.tv_veg_egg_btn);
        rvRegion = findViewById(R.id.rv_region);
        progressBar = findViewById(R.id.progress_bar);
        btnAge = findViewById(R.id.age_button);
        btnWeight = findViewById(R.id.weight_button);
        btnHeight = findViewById(R.id.height_button);
        btnFood = findViewById(R.id.food_button);
        btnSubmitData = findViewById(R.id.info_submit_button);
        pDialog = new ProgressDialog(this);
        tvTermAndCondition = findViewById(R.id.tv_term_condition);
        bottomSheet = findViewById(R.id.framelayout_bottom_sheet);
        btnSleep = findViewById(R.id.sleep_button);
        alRegional = new ArrayList<>();
        etProfession = findViewById(R.id.et_profession);
        mLLBack = findViewById(R.id.ll_back);
        mLLBack.setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mUpdate) {
//            navigateToHealthScreen();
            finish();
        } else {
            Intent intent = new Intent(FoodPreferencesActivity.this, HoursOfSleepActivity.class);
            intent.putExtra("user", user);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);
        }
    }

    /**
     * User click on veg button then we store value of veg button
     * and reset the value of non veg button
     */
    public void clickOnVegButton() {
        tvVegButton.setTextColor(getResources().getColor(R.color.orange_theme));
//        tvVegButton.setBackground(getResources().getDrawable(R.drawable.button_background));
        tvNonVegButton.setTextColor(getResources().getColor(R.color.buttontext));
//        tvNonVegButton.setBackground(getResources().getDrawable(R.drawable.button_default_background));
        tvVegEggBtn.setTextColor(getResources().getColor(R.color.buttontext));
//        tvVegEggBtn.setBackground(getResources().getDrawable(R.drawable.button_default_background));
        user.getUserDetail().setFood(FoodType.VEG);
        btnFood.setText(this.getResources().getString(R.string.veg));
        // Toast.makeText(FoodPreferences.this, foodPrefrence, Toast.LENGTH_SHORT).show();
    }

    /**
     * User click on non veg button then we store value of non veg button
     * and reset the value of veg button
     */
    public void clickOnNonVegButton() {
        tvNonVegButton.setTextColor(getResources().getColor(R.color.orange_theme));
//        tvNonVegButton.setBackground(getResources().getDrawable(R.drawable.button_background));
        tvVegButton.setTextColor(getResources().getColor(R.color.buttontext));
//        tvVegButton.setBackground(getResources().getDrawable(R.drawable.button_default_background));
        tvVegEggBtn.setTextColor(getResources().getColor(R.color.buttontext));
//        tvVegEggBtn.setBackground(getResources().getDrawable(R.drawable.button_default_background));
        user.getUserDetail().setFood(FoodType.NON_VEG);
        btnFood.setText(this.getResources().getString(R.string.non_veg));
        //  Toast.makeText(FoodPreferences.this, foodPrefrence, Toast.LENGTH_SHORT).show();
    }

    /**
     * User click on non veg button then we store value of non veg button
     * and reset the value of veg button
     */
    public void clickOnVegEggButton() {
        tvVegEggBtn.setTextColor(getResources().getColor(R.color.orange_theme));
//        tvVegEggBtn.setBackground(getResources().getDrawable(R.drawable.button_background));
        tvNonVegButton.setTextColor(getResources().getColor(R.color.buttontext));
//        tvNonVegButton.setBackground(getResources().getDrawable(R.drawable.button_default_background));
        tvVegButton.setTextColor(getResources().getColor(R.color.buttontext));
//        tvVegButton.setBackground(getResources().getDrawable(R.drawable.button_default_background));
        user.getUserDetail().setFood(FoodType.VEG_EGG);
        btnFood.setText(this.getResources().getString(R.string.veg_egg));
    }

    public void removeLastTwoCharForWeight() {

            user.getUserDetail().setWeight(Double.parseDouble(user.getUserDetail().getWeight().substring(0, user.getUserDetail().getWeight().length() - 2)) + "");


    }

    public void removeLastTwoCharForHour() {
        user.getUserDetail().setHoursOfSleep(Double.parseDouble(user.getUserDetail().getHoursOfSleep().substring(0, user.getUserDetail().getHoursOfSleep().length() - 2)) + "");
    }

    public void removeLastTwoCharForHeight() {
        user.getUserDetail().setHeight(Double.parseDouble(user.getUserDetail().getHeight().substring(0, user.getUserDetail().getHeight().length() - 2)) + "");
    }

    public void infoSubmit() {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        apiInterface.updateUser(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull retrofit2.Response<UserResp> response) {
                try {
                    UserResp userResp = response.body();
                    pDialog.dismiss();
                    if (!userResp.getError()) {
                        Toast.makeText(FoodPreferencesActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
                        sessionManager.setUser(userResp.getData());
                        Intent intent = new Intent(FoodPreferencesActivity.this, FitnessAdviceActivityNew.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(FoodPreferencesActivity.this);
                        dialog.setMessage(getMessageInSingleLine(userResp.getMessage()));
                        dialog.setCancelable(false);
                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserResp> call, Throwable t) {
                Toast.makeText(FoodPreferencesActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();

            }
        });

    }


    /**
     * Showing terms and condition dialog
     */
    public void showBottomSheetDialog() {
        final View bottomSheetLayout = getLayoutInflater().inflate(R.layout.terms_condition_alertbox, null);
        (bottomSheetLayout.findViewById(R.id.iv_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog = new BottomSheetDialog(this);
        mBottomSheetDialog.setContentView(bottomSheetLayout);
        mBottomSheetDialog.show();
    }

    private void getRegionalList() {
        progressBar.setVisibility(View.VISIBLE);
        rvRegion.setVisibility(View.GONE);
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.SHOWREGIONALLIST, null, response -> {
            Log.d(TAG, "onResponse Response: " + response);
            progressBar.setVisibility(View.GONE);
            rvRegion.setVisibility(View.VISIBLE);
            try {
                JSONObject jObj = new JSONObject(response.toString());
                boolean error = jObj.getBoolean("error");
                if (!error) {
                    JSONArray jsonArray = jObj.getJSONArray("data");
                    alRegional.clear();
                    int selectedId=0;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObjectData1 = jsonArray.getJSONObject(i);
                        Region regionModel = new Region();
                        regionModel.setId(jsonObjectData1.getInt("id"));
                        regionModel.setName(jsonObjectData1.getString("name"));
                        alRegional.add(regionModel);
                        if (user.getUserDetail().getRegion()!=null){
                            if (user.getUserDetail().getRegion().getName().equals(jsonObjectData1.getString("name"))){
                                selectedId = i;
                            }
                        }
                    }
                    Region selectedRegion = null;
                    if (user.getUserDetail().getRegion()!=null){
                        selectedRegion = user.getUserDetail().getRegion();
                    }
                    regionAdapter = new RegionAdapter(FoodPreferencesActivity.this, alRegional,selectedId,this);
//                    recyclerLayoutManager = new GridLayoutManager(FoodPreferencesActivity.this, 2);
//                    rvRegion.setLayoutManager(recyclerLayoutManager);
                    rvRegion.setAdapter(regionAdapter);
                    rvRegion.addOnItemTouchListener(new RecyclerItemClickListener(this, (view, position) -> user.getUserDetail().setRegion(alRegional.get(position))));
                } else {
                    Toast.makeText(FoodPreferencesActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
//                headers.put("Authorization", user.getToken());
                headers.put("userId", String.valueOf(user.getId()));
                return headers;
            }
        };
        strReq.setShouldCache(false);
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(FoodPreferencesActivity.this);
        strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(strReq);
    }

    @Override
    public void setData(int lastCheckedPosition) {
        user.getUserDetail().setRegion(alRegional.get(lastCheckedPosition));
    }

    public void updateWeight() {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        apiInterface.updateUser(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull retrofit2.Response<UserResp> response) {
                try {
                    UserResp userResp = response.body();
                    pDialog.dismiss();
                    assert userResp != null;
                    if (!userResp.getError()) {
//                        Toast.makeText(FoodPreferencesActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
                        sessionManager.setUser(userResp.getData());
//                        navigateToHealthScreen();
                        finish();
                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(FoodPreferencesActivity.this);
                        dialog.setMessage(getMessageInSingleLine(userResp.getMessage()));
                        dialog.setCancelable(false);
                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                    }
                } catch (Exception e) {
                    Log.v("TAG", "Exception: "+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UserResp> call, Throwable t) {
                Toast.makeText(FoodPreferencesActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        });
    }
}
