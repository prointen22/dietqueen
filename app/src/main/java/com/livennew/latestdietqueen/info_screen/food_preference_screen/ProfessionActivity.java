package com.livennew.latestdietqueen.info_screen.food_preference_screen;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceActivity;
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceActivityNew;
import com.livennew.latestdietqueen.info_screen.food_preference_screen.adapter.ProfessionAdapter;
import com.livennew.latestdietqueen.login_screen.RegistrationActivity;
import com.livennew.latestdietqueen.model.Profession;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;

@AndroidEntryPoint
public class ProfessionActivity extends BaseActivity implements ProfessionAdapter.ContactsAdapterListener {

    private static final String TAG = ProfessionActivity.class.getSimpleName();
    public static final String SELECTED_PROFESSION = "selectedState";
    public static final int PROFESSION_SELECTION_REQ = 83;
    private RecyclerView mRecyclerView;
    private EditText edSearch;
    // private LinearLayout llProgress;
    private Toolbar toolbar;
    private ProfessionAdapter mAdapter;
    private ArrayList<Profession> professionList;
    private ProgressBar progressBar;
    @Inject
    SessionManager sessionManager;
    User user;
    private ProgressDialog pDialog;
    LinearLayout mLLBack;
    Profession mSelectedProfession;
    Button mBtnNext;
    @Inject
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_selection);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        user = sessionManager.getUser();
        pDialog = new ProgressDialog(this);
        findViewById();
//        initToolbar();
        if (SupportClass.checkConnection(this)) {
            getProfession();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }


        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("screen")){
                if (intent.getStringExtra("screen").equals("signup")){
                    user = intent.getParcelableExtra("user");
                }
            }

        } else {
            Log.i("check age", "Please check your age");
        }

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }


    private void getProfession() {
        progressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        edSearch.setVisibility(View.GONE);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.GETPROFESSION, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jObj = new JSONObject(response.toString());
                            boolean error = jObj.getBoolean("error");
                            if (!error) {
                                JSONArray jsonArray = jObj.getJSONArray("data");
                                professionList.clear();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    // adding contacts to contacts list
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Profession professionModel = new Profession();
                                    professionModel.setId(jsonObject.getInt("id"));
                                    professionModel.setName(jsonObject.getString("name"));
                                    professionList.add(professionModel);
                                }
                                // refreshing recycler view
                                //mAdapter.setData(stateList);
//                                mAdapter.notifyDataSetChanged();
                                mAdapter = new ProfessionAdapter(ProfessionActivity.this, professionList, ProfessionActivity.this);
                                LinearLayoutManager manager = new LinearLayoutManager(ProfessionActivity.this);
                                mRecyclerView.setHasFixedSize(true);
                                mRecyclerView.setLayoutManager(manager);
                                mRecyclerView.setAdapter(mAdapter);
                                progressBar.setVisibility(View.GONE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                edSearch.setVisibility(View.VISIBLE);
                            } else {
                                Toast.makeText(ProfessionActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, error ->
                Log.e(TAG, "response Error: " + error.getMessage())) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                headers.put("Authorization", user.getToken());
                headers.put("userId", String.valueOf(user.getId()));
                return headers;
            }
        };

//        MyApplication.getInstance().addToRequestQueue(request);
        request.setShouldCache(false);
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(ProfessionActivity.this);
        request.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);

    }

    private void findViewById() {
        // llProgress = (LinearLayout) findViewById(R.id.llProgress);
        edSearch = (EditText) findViewById(R.id.edSearch);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        professionList = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progress_bar);
        mLLBack = findViewById(R.id.ll_back);
        mLLBack.setOnClickListener(v -> onBackPressed());

        mBtnNext = findViewById(R.id.btn_next);
        mBtnNext.setOnClickListener(v -> {
            if(mSelectedProfession == null) {
                Toast.makeText(ProfessionActivity.this, "Please select profession", Toast.LENGTH_SHORT).show();
                return;
            }
//            Intent returnIntent = new Intent();
//            returnIntent.putExtra(SELECTED_PROFESSION, mSelectedProfession);
//            setResult(RESULT_OK, returnIntent);
//            finish();

//            removeLastTwoCharForHeight();
//            removeLastTwoCharForHour();
            if (user.getUserDetail().getRegion() == null) {
                Toast.makeText(ProfessionActivity.this, "Please select region!!!", Toast.LENGTH_SHORT).show();
            } else {
//                if  {
//                    startActivityForResult(new Intent(ProfessionActivity.this, ProfessionActivity.class), ProfessionActivity.PROFESSION_SELECTION_REQ);
                    if (user.getUserDetail().getProfession() == null) {
                        Toast.makeText(ProfessionActivity.this, "Please select profession", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(ProfessionActivity.this, RegistrationActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("user", user);
                        intent.putExtra("screen", "signup");
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
//                        if (SupportClass.checkConnection(ProfessionActivity.this)) {
//                            infoSubmit();
//                        } else {
//                            SupportClass.noInternetConnectionToast(ProfessionActivity.this);
//                        }
                    }
//                }
            }
        });
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.select_profession));
        }
        toolbar.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
    }



    @Override
    protected void onPause() {
        super.onPause();
        hideSoftKeyboard(ProfessionActivity.this);
    }



    /**
     * Hide the Soft Keyboard.
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);

        if (inputMethodManager == null)
            return;

        if (activity.getCurrentFocus() != null && activity.getCurrentFocus().getWindowToken() != null)
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    public void removeLastTwoCharForHour() {
        user.getUserDetail().setHoursOfSleep(Double.parseDouble(user.getUserDetail().getHoursOfSleep().substring(0, user.getUserDetail().getHoursOfSleep().length() - 2)) + "");
    }

    public void removeLastTwoCharForHeight() {
        user.getUserDetail().setHeight(Double.parseDouble(user.getUserDetail().getHeight().substring(0, user.getUserDetail().getHeight().length() - 2)) + "");
    }

    @Override
    public void onContactSelected(Profession professionModel) {
        mSelectedProfession = professionModel;
        for (Profession item: professionList) {
            item.setSelected(item.getId() == professionModel.getId());
        }
        user.getUserDetail().setProfession(mSelectedProfession);
        mAdapter.notifyDataSetChanged();
    }

    public void infoSubmit() {
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        apiInterface.updateUser(user).enqueue(new Callback<UserResp>() {
            @Override
            public void onResponse(@NonNull Call<UserResp> call, @NonNull retrofit2.Response<UserResp> response) {
                try {
                    UserResp userResp = response.body();
                    pDialog.dismiss();
                    if (!userResp.getError()) {
                        Toast.makeText(ProfessionActivity.this, getMessageInSingleLine(userResp.getMessage()), Toast.LENGTH_SHORT).show();
                        userResp.getData().setProfileImage(userResp.getOthers().getUserImageBase64Path() + userResp.getData().getProfileImage());
                        sessionManager.setUser(userResp.getData());
                        Intent intent = new Intent(ProfessionActivity.this, FitnessAdviceActivityNew.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.anim_slide_in_left,
                                R.anim.anim_slide_out_left);
                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(ProfessionActivity.this);
                        dialog.setMessage(getMessageInSingleLine(userResp.getMessage()));
                        dialog.setCancelable(false);
                        dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserResp> call, Throwable t) {
                Toast.makeText(ProfessionActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();

            }
        });

    }
}
