package com.livennew.latestdietqueen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DietEPlanFood;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Food;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeReplaceDietAdapter;
import com.livennew.latestdietqueen.fragments.home.adapters.HomeTodayDietAdapter;
import com.livennew.latestdietqueen.model.FoodReplaceResp;
import com.livennew.latestdietqueen.model.ReplaceFoodParam;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.DateUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReplaceItemsActivity extends AppCompatActivity {
    List<Food> listingvID = new ArrayList<>();
    List<String> listingReplaceID = new ArrayList<>();
    List<DietEPlanFood> listingimgpath = new ArrayList<>();
    RecyclerView rv_home_today_alter_diet;
    List<Food> a = new ArrayList<>();
    SharedPreferences mPrefsobj;
    String[]  rData=null;
    String lastId="";
    String dataEdatatId="";
    String replaceData;
    @Inject
    APIInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.replace_layout);

        Intent intent = getIntent();

        mPrefsobj = this.getPreferences(MODE_PRIVATE);

        // check intent is null or not
        if (intent != null) {

            String sdata = intent.getStringExtra("s");
            replaceData=intent.getStringExtra("replaceData");

            Log.d("ccccc nnnnnnnnnnnnnnnn","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"+">>>>>>>>>"+replaceData);

            String[] cdata = sdata.replaceAll("\\[", "").replaceAll("\\]", "").split(",");
            //  Log.d("foodGroupId : ","llllllllllllllllllllllllll sdata lllllllllllllllllllllllll"+cdata);

            for (int i = 0; i < cdata.length; i++) {

                // Log.d("ccccc nnnnnnnnnnnnnnnn",i+">>>>>>>>>"+cdata[i]);
                String[] tt = cdata[i].split("##");
                // Log.d("foodGroupId : ","llllllllllllllllllllllllll tt  lllllllllllllllllllllllll"+tt);
                Log.d("ccccc", i + ">>>>>>>>>" + tt[0]);
                Log.d("ccccc", i + ">>>>>>>>>" + tt[1]);
                Log.d("ccccc", i + ">>>>>>>>>" + tt[2]);
                Log.d("ccccc", i + ">>>>>>>>>" + tt[3]);
                String fgid = tt[0];
                String fname = tt[1];
                String fimage = tt[2];
                Integer fid = Integer.parseInt(tt[3].toString());

                Food f = new Food();
                f.setFoodGroupId(fgid);
                f.setFoodMasterName(fname);
                f.setImage(fimage);
                f.setId(fid);
                listingvID.add(f);
            }

            rData=replaceData.split("##");
            Log.d("ccccc", ">>>>>>>>> rdata 0" + rData[0]);
            Log.d("ccccc  444 ",  ",,,,,,,,,,,  rdata 1" + rData[1]);

            lastId=rData[0];
            dataEdatatId=rData[1];

        }

        rv_home_today_alter_diet = findViewById(R.id.rv_home_today_alter_diet);





    }

    public void replace(Integer id,Integer lastselectID,Integer dataEData) {
        ProgressDialog pd=new ProgressDialog(this);

        pd.show();

        apiInterface.replaceFood(new ReplaceFoodParam(id, lastselectID,dataEData, DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
            @Override
            public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
                pd.dismiss();
                if (response.body() != null) {
                    if (response.body().getError()) {
                        //  for (int i = 0; i < response.body().getMessage().size(); i++)
                        //Toast.makeText(this, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                    } else {
                        //todayDietMenu.setFood(response.body().getData().getFood());
                        //  myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull Throwable t) {
                pd.dismiss();
            }
        });






//current_iten_id,last_select_item_id,diet_eplan_id,time
  /*    apiInterface.replaceFood(new ReplaceFoodParam(todayDietMenu.getFood().getId(), replaceItemAdapter.getLastSelectedFood().getId(), todayDietMenu.getDietEDataId(), DateUtil.getTodayDate())).enqueue(new Callback<FoodReplaceResp>() {
          @Override
          public void onResponse(@NonNull Call<FoodReplaceResp> call, @NonNull Response<FoodReplaceResp> response) {
              if (response.body() != null) {
                  if (response.body().getError()) {
                      for (int i = 0; i < response.body().getMessage().size(); i++)
                          Toast.makeText(activity, response.body().getMessage().get(i), Toast.LENGTH_SHORT).show();
                  } else {
                      todayDietMenu.setFood(response.body().getData().getFood());
                      myTodayScheduleRecyclerViewAdapter.updateSubListItem(mainItemPosition, subItemPosition, todayDietMenu);
                  }
              }
              dialog.dismiss();
          }

          @Override
          public void onFailure(@NonNull Call call, @NonNull Throwable t) {
              dialog.dismiss();
          }
      });

  }*/
    }

}