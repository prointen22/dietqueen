package com.livennew.latestdietqueen;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.fragment.app.FragmentActivity;

import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionButton;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionHelper;
import com.wangjie.rapidfloatingactionbutton.RapidFloatingActionLayout;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RFACLabelItem;
import com.wangjie.rapidfloatingactionbutton.contentimpl.labellist.RapidFloatingActionContentLabelList;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public abstract class FabActivity extends BaseActivity {

    private RapidFloatingActionLayout rapidFloatingActionLayout;
    private RapidFloatingActionContentLabelList rfaContent;
    private RapidFloatingActionHelper rfabHelper;
    private RapidFloatingActionButton btnFab;


    @Override
    public void onResume() {
        super.onResume();
        addFabButtonItems();
    }

    protected void setFabMargin(int bottom) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, 0, Util.convertDipToPixels(this, 20), Util.convertDipToPixels(this, bottom));
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.ALIGN_PARENT_END);
        btnFab.setLayoutParams(params);
    }

    public void addFabLayout(List<RFACLabelItem> items) {
        rapidFloatingActionLayout = (RapidFloatingActionLayout) this.getLayoutInflater().inflate(R.layout.rapid_floating_action_layout, null, false);
        btnFab = (RapidFloatingActionButton) rapidFloatingActionLayout.findViewById(R.id.activity_main_rfab);
        addContentView(rapidFloatingActionLayout, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        initFabButtons(items);
    }

    private void initFabButtons(List<RFACLabelItem> items) {
        rfaContent = new RapidFloatingActionContentLabelList(this);
        setOnRapidFloatingActionContentLabelListListener(rfaContent);
        rfaContent
                .setItems(items)
                .setIconShadowColor(0xff888888);
        rfabHelper = new RapidFloatingActionHelper(
                this,
                rapidFloatingActionLayout,
                rapidFloatingActionLayout.findViewById(R.id.activity_main_rfab),
                rfaContent
        ).build();

    }

    protected void setOnRapidFloatingActionContentLabelListListener(RapidFloatingActionContentLabelList rfaContent) {
        rfaContent.setOnRapidFloatingActionContentLabelListListener(new RapidFloatingActionContentLabelList.OnRapidFloatingActionContentLabelListListener() {
            @Override
            public void onRFACItemLabelClick(int position, RFACLabelItem item) {
                fabAction(position);
            }

            @Override
            public void onRFACItemIconClick(int position, RFACLabelItem item) {
                fabAction(position);
            }
        });
    }

    protected void fabAction(int position) {
        switch (position) {
            case 1: {
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.whats_app_no))));
                rfabHelper.toggleContent();
                break;
            }
            case 0: {
                onWhatsAppClick(this);
                rfabHelper.toggleContent();
                break;
            }
            case 2: {
              //  onWhatsAppClick(this);
                shareApp();
                rfabHelper.toggleContent();
                break;
            }
            default: {
                rfabHelper.toggleContent();
            }
        }
    }

    protected void addFabButtonItems() {
        addFabLayout(initFabItems());
    }

    private ArrayList<RFACLabelItem> initFabItems() {
        ArrayList<RFACLabelItem> items = new ArrayList<RFACLabelItem>();
        items.add(new RFACLabelItem<Integer>()
                .setLabel(getString(R.string.whats_app))
                .setResId(R.drawable.whats_app)
                .setIconNormalColor(getResources().getColor(R.color.colorAccent))
                .setLabelColor(getResources().getColor(R.color.colorAccent))
                .setWrapper(items.size())
        );
        items.add(new RFACLabelItem<Integer>()
                .setLabel(getString(R.string.help))
                .setResId(R.drawable.ic_call_white_24dp)
                .setLabelColor(getResources().getColor(R.color.colorAccent))
                .setIconNormalColor(getResources().getColor(R.color.colorAccent))
                .setWrapper(items.size())
        );

        items.add(new RFACLabelItem<Integer>()
                .setLabel(getString(R.string.share))
                .setResId(R.drawable.ic_share_white_24dp)
                .setLabelColor(getResources().getColor(R.color.colorAccent))
                .setIconNormalColor(getResources().getColor(R.color.colorAccent))
                .setWrapper(items.size())
        );

        return items;
    }

    protected void addFabButtonItems(List<RFACLabelItem<Integer>> newItems) {
        List<RFACLabelItem> items = initFabItems();
        if (newItems != null)
            items.addAll(newItems);
        addFabLayout(items);
    }

    protected void onWhatsAppClick(FragmentActivity activity) {
        try {
            PackageManager packageManager = activity.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + getString(R.string.whats_app_no) + "&text=" + URLEncoder.encode("", "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                this.startActivity(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        removeFabButton();
        super.onPause();
    }

    public void removeFabButton() {
        if (rapidFloatingActionLayout != null)
            rapidFloatingActionLayout.setVisibility(View.GONE);
    }

    public void shareApp(){
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "DietQueen");
            String shareMessage= "\n\t\n" +
                    "DIETQUEEN - Weight Loss App for Women\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch(Exception e) {
            //e.toString();
        }
    }
}
