package com.livennew.latestdietqueen.retrofit;

import android.content.Context;

import com.livennew.latestdietqueen.R;

import java.io.IOException;

public class NoConnectivityException extends IOException {

    private Context context;

    public NoConnectivityException(Context context) {

        this.context = context;
    }

    @Override
    public String getMessage() {
        return context.getString(R.string.no_internet_connection);
        // You can send any message whatever you want from here.
    }
}
