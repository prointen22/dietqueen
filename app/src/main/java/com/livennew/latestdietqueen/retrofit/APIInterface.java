package com.livennew.latestdietqueen.retrofit;
import com.livennew.latestdietqueen.about_us.AboutUsResp;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.DoneParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.FoodGroupParamSearch;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Foods;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Plan;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.PlanParam;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.Plans;
import com.livennew.latestdietqueen.diet_info.fragment.schedule_vertical_fragment.model.UpdateMealPrefParam;
import com.livennew.latestdietqueen.diet_info.model.IngredientResp;
import com.livennew.latestdietqueen.diet_info.model.InstructionResp;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.VideoDetailResp;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.VideoResp;
import com.livennew.latestdietqueen.fitness_advice.FitnessAdviceResp;
import com.livennew.latestdietqueen.fragments.home.model.BlogListResp;
import com.livennew.latestdietqueen.fragments.home.model.RecepieList;
import com.livennew.latestdietqueen.fragments.home.model.TestimonialListResp;
import com.livennew.latestdietqueen.fragments.products.model.ProductResp;
import com.livennew.latestdietqueen.model.DietEPlanResp;
import com.livennew.latestdietqueen.model.FCMParam;
import com.livennew.latestdietqueen.model.FoodReplaceResp;
import com.livennew.latestdietqueen.model.MyResponse;
import com.livennew.latestdietqueen.model.ReplaceFoodParam;
import com.livennew.latestdietqueen.model.State;
import com.livennew.latestdietqueen.model.SubScribeResp;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.model.UserResp;
import com.livennew.latestdietqueen.my_plans.model.WeightHistoryResp;
import com.livennew.latestdietqueen.newsblog.BlogDetailResp;
import com.livennew.latestdietqueen.notification.model.NotificationResp;
import com.livennew.latestdietqueen.party_planner.model.PartyPlannerTermsResp;
import com.livennew.latestdietqueen.user_detail.model.StateList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface APIInterface {
    @POST("mobileApi/mobile/diet/current-time")
    Call<Plan> getDietPlan(@Body PlanParam planParam);

    @POST("mobileApi/mobile/diet-e-plan/time/custom")
    Call<Plans> getDietPlans(@Body PlanParam planParam);

    @POST("mobileApi/mobile/diet-e-plan/time-and-meal-tag/custom")
    Call<Plans> getDietPlansNew(@Body PlanParam planParam);

    @POST("mobileApi/mobile/meal-tag/update-preference")
    Call<MyResponse> updateMealPreference(@Body UpdateMealPrefParam updateMealPrefParam);

    @POST("mobileApi/mobile/diet-done")
    Call<MyResponse> setDietDone(@Body DoneParam doneParam);

    @POST("mobileApi/mobile/food-group")
    Call<Foods> getFoodsByGroup(@Body FoodGroupParam foodGroup);


    @POST("mobileApi/mobile/food-group")  // used for Search
    Call<Foods> getFoodsByGroupSearch(@Body FoodGroupParam foodGroup,@Query("search") String Search);


  //  Call<Foods> getFoodsByGroupSearch(FoodGroupParamSearch foodGroupParamSearch);

    @POST("mobileApi/mobile/diet/food-replace")
    Call<FoodReplaceResp> replaceFood(@Body ReplaceFoodParam param);

    @POST("mobileApi/login/social")
    Call<UserResp> loginWithSocial(@Body User user);

    @POST("mobileApi/user/otp")
    Call<UserResp> loginWithMobile(@Body User user);

    @GET("mobileApi/mobile/fitness-advice")
    Call<FitnessAdviceResp> getFitnessAdvice();

    @FormUrlEncoded
    @POST("mobileApi/user/profileImage/base64")
    Call<UserResp> updateProfileImage(@FieldMap Map<String, String> param);

    @POST("mobileApi/user/profile/detail")
    Single<UserResp> getProfile();
/*
    @GET("mobileApi/user/height")
    Call<UserResp> updateHeight(@Body HashMap<String, String> params);
*/
    @POST("mobileApi/user/height")
    Call<UserResp> updateHeight(@Body User user);   //only updates  height

    @POST("mobileApi/user/detail")
    Call<UserResp> updateUser(@Body User user);   //see to update height

    @POST("mobileApi/user/profile")
    Call<UserResp> updateUserWorkDetails(@Body User user);

    @POST("mobileApi/user/update_user_accept_terms")
    Call<MyResponse> updateAgreeTerms(@Body Map<String, Integer> user);

    @GET("mobileApi/mobile/about-us")
    Call<AboutUsResp> getAboutUs();

    @GET("mobileApi/mobile/ingredient")
    Call<IngredientResp> getIngredient();

    @GET("mobileApi/mobile/diet-plan/feature-benefit/{id}")
    Call<ProductResp> getProductDetails(@Path("id") int id);

    @GET("https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&maxResults=5")
    Call<VideoResp> getVideos(@Query("channelId") String channelId, @Query("key") String youtubeKey);

    @POST("mobileApi/mobile/blog-exclusive?")
    Call<BlogListResp> getBlogs(@Body Map<String, Object> type);

    @POST("mobileApi/mobile/testimonials")
    Call<TestimonialListResp> getTestimonials(@Body Map<String, Integer> type);


    @POST("mobileApi/mobile/recipes")
    Call<RecepieList> getRecepie(@Body Map<String, Integer> type);


    @POST("mobileApi/mobile/receipe_like_dislike/")
    Call<ProductResp> is_like(@Body Map<String, String > type);

    @GET("https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics,status")
    Call<VideoDetailResp> getVideoDetails(@Query("id") String id, @Query("key") String key);

    @POST("mobileApi/fcm-token")
    Call<MyResponse> registerPushToken(@Body FCMParam fcmParam);

    @DELETE("mobileApi/fcm-token")
    Call<MyResponse> deregisterPushToken();

    @GET("mobileApi/mobile/notification")
    Call<NotificationResp> getNotifications();

    @GET("mobileApi/mobile/party_planner/term_condition")
    Call<PartyPlannerTermsResp> getPartyPlannerTerms();

    @GET("mobileApi/mobile/instruction")
    Call<InstructionResp> getInstructions(@Query("page") int page,@Query("languageId") int languageId);

    @GET("mobileApi/mobile/user/body-weight")
    Single<WeightHistoryResp> getBodyWeight();

    @POST("mobileApi/mobile/user/body-weight")
    Call<UserResp> setBodyWeight(@Body Map<String, String> type);

    @GET("commonApi/state")
    Call<StateList> getState();

    @POST("mobileApi/mobile/blog-exclusive")
    Call<BlogDetailResp> getBlogDetail(@Body Map<String, Object> type);

    @POST("mobileApi/user/subscribe")
    Call<SubScribeResp> subScribe(@Body HashMap<String, Object> params);

    @GET("mobileApi/mobile/diet-e-plan")
    Single<DietEPlanResp> getDietEPlan();

}
