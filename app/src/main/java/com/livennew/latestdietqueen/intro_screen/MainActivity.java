package com.livennew.latestdietqueen.intro_screen;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.databinding.NewMainActivityBinding;
import com.livennew.latestdietqueen.intro_screen.adapter.MyAdapter;
import com.livennew.latestdietqueen.login_screen.LoginActivity;
import com.livennew.latestdietqueen.R;


public class MainActivity extends AppCompatActivity {

    MyAdapter adapter;
    // private PrefManager prefManager;
    private Button btnNext;
    private int[] layouts;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private NewMainActivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            this.getSupportActionBar().hide();
        } catch (NullPointerException e) {

        }
        binding = NewMainActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        layouts = new int[]{
                R.layout.intro_one,
                R.layout.intro_two
        };
        init();
        // adding bottom dots
        addBottomDots(0);


        binding.viewPager.setAdapter(adapter);
        binding.viewPager.addOnPageChangeListener(viewPagerPageChangeListener);


        btnNext.setOnClickListener(e -> {
            // checking for last page
            // if last page home screen will be launched
            int current = getItem(+1);
            if (current < layouts.length) {
                // move to next screen
                binding.viewPager.setCurrentItem(current);
            } else {
                launchHomeScreen();
            }

        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Util.fromHtml("&#x02015;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }


    private int getItem(int i) {
        return binding.viewPager.getCurrentItem() + i;
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.get_started));

            } else {
                // still pages are left
                btnNext.setText(getString(R.string.next));
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

    private void init() {
        btnNext = findViewById(R.id.btn_next);
        dotsLayout = findViewById(R.id.layoutDots);
        adapter = new MyAdapter(this, layouts);
    }

    public void launchHomeScreen() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }
}