package com.livennew.latestdietqueen.about_us;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AboutUs {
    @SerializedName("page_type")
    @Expose
    private String pageType;
    @SerializedName("content")
    @Expose
    private String content;

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
