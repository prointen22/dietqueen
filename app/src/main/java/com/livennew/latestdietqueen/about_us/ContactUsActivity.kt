package com.livennew.latestdietqueen.about_us

import com.livennew.latestdietqueen.retrofit.APIInterface
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.livennew.latestdietqueen.R
import com.google.firebase.analytics.FirebaseAnalytics
import android.content.Intent
import android.net.Uri
import android.util.Log
import com.livennew.latestdietqueen.notification.NotificationListActivity
import com.livennew.latestdietqueen.network_support.APIClient
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.livennew.latestdietqueen.BuildConfig
import com.livennew.latestdietqueen.SessionManager
import com.livennew.latestdietqueen.utils.TopBarUtil
import com.livennew.latestdietqueen.model.User
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil.Companion.screenViewEvent
import com.livennew.latestdietqueen.databinding.ActivityContactUsBinding
import java.lang.Exception
import java.net.URLEncoder

open class ContactUsActivity : AppCompatActivity() {
    private var toolbar: Toolbar? = null
    var sessionManager: SessionManager? = null
    var apiInterface: APIInterface? = null
    private var user: User? = null
//    private lateinit var mIvNotification: ImageView
//    private lateinit var mIvDoctor: ImageView
    private lateinit var mBinding: ActivityContactUsBinding

    companion object {
        private var firebaseAnalytics: FirebaseAnalytics? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        mBinding = ActivityContactUsBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        sessionManager = SessionManager(applicationContext)
        apiInterface = APIClient.getClient(applicationContext).create(APIInterface::class.java)
    }

    fun init() {
        val mIvNotification:ImageView = findViewById(R.id.iv_notification)
        val mIvDoctor:ImageView = findViewById(R.id.iv_doctor)
        val mIvSideMenu:ImageView = findViewById(R.id.iv_menu)
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager!!)

//        mIvNotification = findViewById(R.id.iv_notification)
//        mIvNotification.setOnClickListener { navigateToNotificationScreen() }
//
//        mIvDoctor = findViewById(R.id.iv_doctor)
//        mIvDoctor.setOnClickListener {
//            startActivity(Intent(this, InstructionActivity::class.java))
//        }

        mBinding.tvCall.setOnClickListener { startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.whats_app_no)))) }
        mBinding.tvWhatsapp.setOnClickListener {onWhatsAppClick()}
        mBinding.tvShare.setOnClickListener { TopBarUtil.shareApp(this)}
        mBinding.tvEmail.setOnClickListener {sendEmail()}
        mBinding.tvWebsite.setOnClickListener { TopBarUtil.navigateToWebsite(this) }
        mBinding.tvBack.setOnClickListener { onBackPressed() }

    }

    private fun sendEmail() {
        val emailAddressArray = arrayOf<String>("help@dietqueen.com")
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
        }
        intent.putExtra(Intent.EXTRA_EMAIL, emailAddressArray)
        intent.putExtra(Intent.EXTRA_SUBJECT,"")
        intent.putExtra(Intent.EXTRA_TEXT,"")
        if (intent.resolveActivity(packageManager)!=null){
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        user = sessionManager?.user
//        initToolbar()
        init()
    }

    private fun initToolbar() {
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar?.navigationIcon = null
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.title = ""
        }


    }

    private fun navigateToNotificationScreen() {
        firebaseAnalytics?.let { screenViewEvent("", it) }
        val intent1 = Intent(this, NotificationListActivity::class.java)
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent1)
    }

    private fun onWhatsAppClick() {
        try {
            val packageManager = packageManager
            val i = Intent(Intent.ACTION_VIEW)
            val url = "https://api.whatsapp.com/send?phone=" + getString(R.string.whats_app_no) + "&text=" + URLEncoder.encode("", "UTF-8")
            i.setPackage("com.whatsapp")
            i.data = Uri.parse(url)
            if (i.resolveActivity(packageManager) != null) {
                this.startActivity(i)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    open fun shareApp() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "DietQueen")
            var shareMessage = """
	
DIETQUEEN - Weight Loss App for Women

"""
            shareMessage = """
            ${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
            
            
            """.trimIndent()
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "choose one"))
        } catch (e: Exception) {
            Log.v("TAG", "shareApp() exception: ${e.message}")
        }
    }
}