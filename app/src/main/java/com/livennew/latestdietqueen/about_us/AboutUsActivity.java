package com.livennew.latestdietqueen.about_us;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.google.android.material.tabs.TabLayout;
import com.livennew.latestdietqueen.BaseActivity;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.about_us.fragment.AboutAppFragment;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.databinding.ActivityAboutUsBinding;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class AboutUsActivity extends BaseActivity {
    public static int int_items = 4;
    @Inject
    APIInterface apiInterface;
    private int type = 0;
    private ActivityAboutUsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAboutUsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().setStatusBarColor(ContextCompat.getColor(AboutUsActivity.this, R.color.orange_theme));
        setSupportActionBar(binding.toolbarAboutUs);
        Intent intent = getIntent();
        if (intent!=null) {
            if (intent.hasExtra("type")) {
                type = intent.getIntExtra("type", 0);
                binding.viewpager.setCurrentItem(type);
            }
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("");
        }
        binding.toolbarAboutUs.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);


        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        binding.viewpager.setCurrentItem(0);
                        getSupportActionBar().setTitle(R.string.information_source);
                        break;
                    case 1:
                        binding.viewpager.setCurrentItem(1);
                        getSupportActionBar().setTitle(R.string.terms_and_conditions);
                        break;
                    case 2:
                        binding.viewpager.setCurrentItem(2);
                        getSupportActionBar().setTitle(R.string.privacy_policy);
                        break;
                    case 3:
                        binding.viewpager.setCurrentItem(3);
                        getSupportActionBar().setTitle("Reference");
                        break;
                    default:
                        binding.viewpager.setCurrentItem(tab.getPosition());
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //getMyFavorites();


        getAboutUs();
    }


    private void getAboutUs() {
        binding.llProgress.setVisibility(View.VISIBLE);
        apiInterface.getAboutUs().enqueue(new Callback<AboutUsResp>() {
            @Override
            public void onResponse(@NonNull Call<AboutUsResp> call, @NonNull Response<AboutUsResp> response) {
                binding.llProgress.setVisibility(View.GONE);
                binding.viewpager.setAdapter(new MyAdapter(getSupportFragmentManager(), response.body().getData()));
                binding.tabs.post(() -> binding.tabs.setupWithViewPager(binding.viewpager));
                if (getIntent() != null) {
                    binding.viewpager.setCurrentItem(type);
                }
            }

            @Override
            public void onFailure(@NonNull Call<AboutUsResp> call, @NonNull Throwable t) {
                binding.llProgress.setVisibility(View.GONE);
            }
        });
    }

    class MyAdapter extends FragmentStatePagerAdapter {

        private final List<AboutUs> aboutUs;

        MyAdapter(FragmentManager fm, List<AboutUs> aboutUs) {
            super(fm);
            this.aboutUs = aboutUs;
        }

        /**
         * Return fragment with respect to Position .
         */

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return AboutAppFragment.getInstance(aboutUs.get(2).getContent());
                case 1:
                    return AboutAppFragment.getInstance(aboutUs.get(0).getContent());
                case 2:
                    return AboutAppFragment.getInstance(aboutUs.get(1).getContent());
                case 3:
                    return AboutAppFragment.getInstance(aboutUs.get(4).getContent());
            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;

        }

        /**
         * This method returns the title of the tab according to the position.
         */

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return AboutUsActivity.this.getString(R.string.about_app);
                case 1:
                    return AboutUsActivity.this.getString(R.string.terms_of_services);
                case 2:
                    return AboutUsActivity.this.getString(R.string.privacy_policy);
                case 3:
                    return AboutUsActivity.this.getString(R.string.reference);
            }
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
