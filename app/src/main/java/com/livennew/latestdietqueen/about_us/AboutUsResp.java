package com.livennew.latestdietqueen.about_us;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

import java.util.List;

public class AboutUsResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private List<AboutUs> data = null;

    public List<AboutUs> getData() {
        return data;
    }

    public void setData(List<AboutUs> data) {
        this.data = data;
    }
}
