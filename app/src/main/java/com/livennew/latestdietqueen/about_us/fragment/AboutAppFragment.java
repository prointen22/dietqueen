package com.livennew.latestdietqueen.about_us.fragment;

import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livennew.latestdietqueen.Util;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.databinding.FragmentAboutAppBinding;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class AboutAppFragment extends Fragment {

    private FragmentAboutAppBinding binding;
    @Inject
    APIInterface apiInterface;

    public static AboutAppFragment getInstance(String data) {
        AboutAppFragment fr = new AboutAppFragment();
        Bundle bundle = new Bundle();
        bundle.putString("data", data);
        fr.setArguments(bundle);
        return fr;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAboutAppBinding.inflate(inflater, container, false);
        if (getArguments() != null) {
            binding.tvAboutApp.setText(Util.fromHtml(getArguments().getString("data", "")));
        }
        return binding.getRoot();
    }


}
