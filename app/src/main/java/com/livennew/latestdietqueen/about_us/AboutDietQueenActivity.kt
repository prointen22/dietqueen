package com.livennew.latestdietqueen.about_us

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.livennew.latestdietqueen.BaseActivity

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics

import com.livennew.latestdietqueen.R
import com.livennew.latestdietqueen.SessionManager
import com.livennew.latestdietqueen.utils.TopBarUtil
import com.livennew.latestdietqueen.about_us.adapter.AboutDQAdapter
import com.livennew.latestdietqueen.dashboard.model.DashboardModel
import com.livennew.latestdietqueen.databinding.ActivityAboutDietQueenBinding
import com.livennew.latestdietqueen.diet_info.InstructionActivity
import com.livennew.latestdietqueen.exclusive_youtube.ExclusiveYoutubeMainClass
import com.livennew.latestdietqueen.exclusive_youtube.testAllActivity
import com.livennew.latestdietqueen.faq.FAQActivity
import com.livennew.latestdietqueen.newsblog.NewsBlogListActivity
import com.livennew.latestdietqueen.utils.FireBaseAnalyticsUtil
import com.livennew.latestdietqueen.utils.GridSpacingItemDecoration


class AboutDietQueenActivity : BaseActivity() {


    private lateinit var mBinding: ActivityAboutDietQueenBinding
    var sessionManager: SessionManager? = null
    private var mDataList = ArrayList<DashboardModel>()
    private var firebaseAnalytics: FirebaseAnalytics? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        mBinding = ActivityAboutDietQueenBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        sessionManager = SessionManager(applicationContext)
        init()
    }

    private fun init() {
        val mIvNotification: ImageView = findViewById(R.id.iv_notification)
        val mIvDoctor: ImageView = findViewById(R.id.iv_doctor)
        val mIvSideMenu: ImageView = findViewById(R.id.iv_menu)
        TopBarUtil.handleClick(this, mIvNotification, mIvDoctor, mIvSideMenu, sessionManager!!)

        mBinding.tvPrivacyPolicy.setOnClickListener {
//            startActivity(Intent(this@AboutDietQueenActivity, AboutUsActivity::class.java))
            val intent = Intent(this@AboutDietQueenActivity, AboutUsActivity::class.java)
            intent.putExtra("type",2)
            startActivity(intent)
        }
        mBinding.tvTermsConditions.setOnClickListener {
//            startActivity(Intent(this@AboutDietQueenActivity, AboutUsActivity::class.java))
            val intent = Intent(this@AboutDietQueenActivity, AboutUsActivity::class.java)
            intent.putExtra("type",1)
            startActivity(intent)
        }
        mBinding.tvInformationSource.setOnClickListener {
//            startActivity(Intent(this@AboutDietQueenActivity, AboutUsActivity::class.java))
            val intent = Intent(this@AboutDietQueenActivity, AboutUsActivity::class.java)
            intent.putExtra("type",0)
            startActivity(intent)
        }

        mBinding.rvAbout.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        val spanCount = 2 // 2 columns
        val spacing = 50 // 50px
        val includeEdge = true
        mBinding.rvAbout.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))

        prepareData()
    }

    private fun prepareData() {
        mDataList.clear()

        val mVideos = DashboardModel()
        mVideos.id = 1
        mVideos.title = getString(R.string.videos)
        mVideos.icon = resources.getIdentifier("ic_dash_videos", "drawable", packageName)
        mDataList.add(mVideos)

        val mInstructions = DashboardModel()
        mInstructions.id = 2
        mInstructions.title = getString(R.string.instruction)
        mInstructions.icon = resources.getIdentifier("ic_dr_instruction", "drawable", packageName)
        mDataList.add(mInstructions)

        val mDQBlogs = DashboardModel()
        mDQBlogs.id = 3
        mDQBlogs.title = getString(R.string.dq_blogs)
        mDQBlogs.icon = resources.getIdentifier("ic_dash_blog", "drawable", packageName)
        mDataList.add(mDQBlogs)

        val mFaqs = DashboardModel()
        mFaqs.id = 4
        mFaqs.title = getString(R.string.faqs)
        mFaqs.icon = resources.getIdentifier("ic_dash_faqs", "drawable", packageName)
        mDataList.add(mFaqs)

        val mContact = DashboardModel()
        mContact.id = 5
        mContact.title = getString(R.string.contact)
        mContact.icon = resources.getIdentifier("ic_dash_contact", "drawable", packageName)
        mDataList.add(mContact)

        val mTestimonials = DashboardModel()
        mTestimonials.id = 6
        mTestimonials.title = getString(R.string.testimonials)
        mTestimonials.icon = resources.getIdentifier("ic_dash_testimonials", "drawable", packageName)
        mDataList.add(mTestimonials)

        setAdapter()
    }

    private fun setAdapter() {
        val mAdapter = AboutDQAdapter(this, mDataList) { clickedModel: DashboardModel ->
            adapterItemClicked(clickedModel)
        }
        mBinding.rvAbout.adapter = mAdapter
    }

    private fun adapterItemClicked(dashboardObj: DashboardModel) {
        Log.v("TAG", "Selected tile: ${dashboardObj.title}")

        when (dashboardObj.title) {
            getString(R.string.videos) -> {
                Toast.makeText(this,"Coming Soon",Toast.LENGTH_SHORT).show()
//                firebaseAnalytics?.let { FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.VIDEOS, it) }
//                startActivity(Intent(this@AboutDietQueenActivity, ExclusiveYoutubeMainClass::class.java))
            }
            getString(R.string.instruction) -> {
                startActivity(Intent(this@AboutDietQueenActivity, InstructionActivity::class.java))
            }
            getString(R.string.dq_blogs) -> {
                firebaseAnalytics?.let { FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.BLOGS, it) }
                val intent = Intent(this@AboutDietQueenActivity, NewsBlogListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            getString(R.string.faqs) -> {
                firebaseAnalytics?.let { FireBaseAnalyticsUtil.screenViewEvent(FireBaseAnalyticsUtil.FAQ, it) }
                startActivity(Intent(this@AboutDietQueenActivity, FAQActivity::class.java))
            }
            getString(R.string.contact) -> {
                startActivity(Intent(this@AboutDietQueenActivity, ContactUsActivity::class.java))
            }
            getString(R.string.testimonials)-> {
                val intent = Intent(this@AboutDietQueenActivity, testAllActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
        }
    }
}