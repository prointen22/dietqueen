package com.livennew.latestdietqueen.my_plans;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.ActivityAddMyWeightBinding;
import com.livennew.latestdietqueen.exclusive_youtube.YouTubePlayerActivity;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Id;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.Video;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.VideoDetail;
import com.livennew.latestdietqueen.exclusive_youtube.fragment.model.VideoDetailResp;
import com.livennew.latestdietqueen.info_screen.food_preference_screen.FoodPreferencesActivity;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.my_plans.model.WeightAndWaistLine;
import com.livennew.latestdietqueen.my_plans.model.WeightHistoryResp;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.retrofit.APIInterface;
import com.livennew.latestdietqueen.utils.InputFilterMinMax;
import com.livennew.latestdietqueen.utils.SupportClass;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@AndroidEntryPoint
public class AddMyWeightActivity extends FabActivity {
    private Toolbar toolbarWeightList;
    CompositeDisposable disposable = new CompositeDisposable();
    private ProgressDialog pDialog;
    private StringBuilder sb;
    private EditText etWeight, etWaistline;
    private LineChartView lineChartViewWeight;
    private List<PointValue> waitYAxis;
    private List<PointValue> waistLineYAxis;
    private TextView tvLastWeight, tvEightDayWeight, tvFifteenDayWeight;
    private ImageView ivWeightVideo;
    private String videoThumbnail = "";
    private Calendar myCalendar;
    private static final String TAG = "Erros";
    String todayDate = "";
    private ProgressBar progressBar, progressBar1;
    private Switch switchWeight, switchHeight;
    private String weightKG = "", weightLB = "", totalWeight = "";
    private String heightCM = "", heightInch = "", totalHeight = "";
    String weightUnit="";
    Double weightKg = 0.0, heightCm = 0.0;
    @Inject
    SessionManager sessionManager;
    User user;
    private LineChartView lcWaistLine;
    @Inject
    APIInterface apiInterface;
    private ActivityAddMyWeightBinding binding;
    private List<AxisValue> axisValues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorAccent));
        binding = ActivityAddMyWeightBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        user = sessionManager.getUser();
        init();
        etWeight.setFilters(new InputFilter[]{new InputFilterMinMax("1", "200")});
        etWaistline.setFilters(new InputFilter[]{new InputFilterMinMax("1", "50")});
        setSupportActionBar(toolbarWeightList);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.add_my_weight);
        }
        toolbarWeightList.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);
        //Switch code
        switchButtonWeight();
        switchButtonHeight();
        /**Line chart view*/
        binding.weightSubmitButton.setOnClickListener(v -> {



            if (isValidate()) {
                String weight=binding.etWeight.getText().toString();
                gotoDialog(weight);
            }




        });
        if (SupportClass.checkConnection(AddMyWeightActivity.this)) {
            getBodyWeight();
            GetBanner();
        } else {
            SupportClass.noInternetConnectionToast(AddMyWeightActivity.this);
        }


        DatePickerDialog.OnDateSetListener date = (view1, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };
        binding.etDate.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            /*new DatePickerDialog(AddMyWeightActivity.this, R.style.MyDatePicker, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();*/
        });

        ivWeightVideo.setOnClickListener(view12 -> apiInterface.getVideoDetails(ivWeightVideo.getTag() + "", getString(R.string.youtube_key)).enqueue(new Callback<VideoDetailResp>() {
            @Override
            public void onResponse(@NotNull Call<VideoDetailResp> call, @NotNull Response<VideoDetailResp> response) {
                try {
                    VideoDetail item1 = response.body().getItems().get(0);
                    Video video = new Video();
                    video.setId(new Id());
                    video.getId().setVideoId(item1.getId());
                    video.setSnippet(item1.getSnippet());
                    Intent intent = new Intent(AddMyWeightActivity.this, YouTubePlayerActivity.class);
                    intent.putExtra("video", video);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<VideoDetailResp> call, @NotNull Throwable t) {
                Toast.makeText(AddMyWeightActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }));

    }

    public void switchButtonWeight() {
        switchWeight.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                weightKG = "";
                weightLB = "LB";
                weightUnit= "LB";
                etWeight.setFilters(new InputFilter[]{new InputFilterMinMax("1", "400")});
            } else {
                weightLB = "";
                weightKG = "KG";
                weightUnit= "KG";
                etWeight.setFilters(new InputFilter[]{new InputFilterMinMax("1", "200")});
            }
        });
    }

    public void switchButtonHeight() {
        switchHeight.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                heightInch = "";
                heightCM = "CM";
                etWaistline.setFilters(new InputFilter[]{new InputFilterMinMax("1", "127")});
            } else {
                heightCM = "";
                heightInch = "IN";
                etWaistline.setFilters(new InputFilter[]{new InputFilterMinMax("1", "50")});
            }
        });
    }

    private boolean isValidate() {
        if (etWeight.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Weight", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etWaistline.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Waistline", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (binding.etDate.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Date", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        binding.etDate.setText(sdf.format(myCalendar.getTime()));
    }


    private void init() {
        progressBar = findViewById(R.id.progress_bar);
        myCalendar = Calendar.getInstance();
        updateLabel();
        toolbarWeightList = findViewById(R.id.toolbar_weight);
        pDialog = new ProgressDialog(this);
        sb = new StringBuilder();
        progressBar1 = findViewById(R.id.progress_bar1);
        lineChartViewWeight = findViewById(R.id.cv_weight_graph);
        lcWaistLine = (LineChartView) findViewById(R.id.lcWaistLine);
        etWeight = findViewById(R.id.et_weight);
        etWaistline = findViewById(R.id.et_waistline);
        tvLastWeight = findViewById(R.id.tv_last_weight);
        tvEightDayWeight = findViewById(R.id.tv_eight_day_weight);
        tvFifteenDayWeight = findViewById(R.id.tv_fifteen_day_weight);
        ivWeightVideo = findViewById(R.id.ivVideo);
        View tvVideoTitle = findViewById(R.id.tvName);
        tvVideoTitle.setVisibility(View.GONE);
        axisValues = new ArrayList<>();
        waitYAxis = new ArrayList<>();
        waistLineYAxis = new ArrayList<>();
        switchWeight = findViewById(R.id.swtch_weight);
        switchHeight = findViewById(R.id.swtch_waist);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void GetBanner() {
        try {
            JSONObject params = new JSONObject();
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.GETBANNER, params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONObject jsonObjectOther = jObj.getJSONObject("others");
                        videoThumbnail = jsonObjectOther.getString("youtube_exclusive_home_banner");
                        String youtubeVideo = videoThumbnail.substring(videoThumbnail.indexOf("v=") + 2, videoThumbnail.length());
                        String youtubeImage = "https://img.youtube.com/vi/" + youtubeVideo + "/0.jpg";
                        Picasso.get().load(youtubeImage).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).error(R.drawable.ic_award).into(ivWeightVideo);
                        ivWeightVideo.setTag(youtubeVideo);
                    } else {
                        JSONArray jsonArray = jObj.getJSONArray("message");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(AddMyWeightActivity.this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//
    private void addBodyWeight() {
        if (weightLB.equalsIgnoreCase("")) {
            totalWeight = etWeight.getText().toString();
        } else {
            weightKg = Double.parseDouble(etWeight.getText().toString()) / 2.2046;
            totalWeight = String.valueOf(new DecimalFormat("##").format(weightKg));
        }
        if (heightCM.equalsIgnoreCase("")) {
            heightCm = Double.parseDouble(etWaistline.getText().toString()) / 0.39370;
            totalHeight = String.valueOf(new DecimalFormat("##").format(heightCm));
        } else {
            totalHeight = etWaistline.getText().toString();
        }

        try {
            progressBar1.setVisibility(View.VISIBLE);
            JSONObject params = new JSONObject();
            params.put("date", todayDate);
            params.put("weight", totalWeight);
            params.put("waistline", totalHeight);
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.INSERTBODYWEIGHT, params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        JSONArray jsonArray = jObj.getJSONArray("message");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                        }
                        getBodyWeight();
                        etWeight.setText("");
                        etWaistline.setText("");
                        binding.etDate.setText("");
                        if (Integer.parseInt(tvLastWeight.getText().toString()) < Integer.parseInt(totalWeight)) {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(AddMyWeightActivity.this);
                            dialog.setMessage(String.format(getString(R.string.your_weight_has_increased_let_us), user.getName()));
                            dialog.setCancelable(false);
                            dialog.setPositiveButton(android.R.string.ok, (dialog1, which) -> dialog1.dismiss()).show();
                        }
                    } else {
                        //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                        Toast.makeText(AddMyWeightActivity.this, "Oops something went wrong!!!", Toast.LENGTH_LONG).show();
                    }
                    progressBar1.setVisibility(View.GONE);
                    updateLabel();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(AddMyWeightActivity.this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getBodyWeight() {
        progressBar.setVisibility(View.VISIBLE);
        apiInterface.getBodyWeight()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<WeightHistoryResp>() {
                    @Override
                    public void onSuccess(@NonNull WeightHistoryResp weightHistoryResp) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            float viewportTop = 0, viewportTopWaistline = 0;
                            if (!weightHistoryResp.getError()) {
                                waitYAxis.clear();
                                waistLineYAxis.clear();
                                axisValues.clear();
                                if (weightHistoryResp.getData().getWeightAndWaistLine().size() <= 1) {
                                    axisValues.add(0, new AxisValue(0).setLabel("0"));
                                    waitYAxis.add(new PointValue(0, 0f));
                                    waistLineYAxis.add(new PointValue(0, 0f));
                                    binding.tvWaistLineGraph.setVisibility(View.GONE);
                                    binding.cvWaistLineGraph.setVisibility(View.GONE);
                                }
                                for (int i = 0; i < weightHistoryResp.getData().getWeightAndWaistLine().size(); i++) {
                                    WeightAndWaistLine item = weightHistoryResp.getData().getWeightAndWaistLine().get(i);
                                    String xAxisDate = new SimpleDateFormat("dd MMM", Locale.US).format(new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(item.getDate()));
                                    axisValues.add(axisValues.size(), new AxisValue(axisValues.size()).setLabel(xAxisDate));
                                    waitYAxis.add(new PointValue(waitYAxis.size(), Float.valueOf(item.getWeight())));
                                    waistLineYAxis.add(new PointValue(waistLineYAxis.size(), Float.valueOf(item.getWaistline())));
                                    if (Integer.parseInt(item.getWeight()) > viewportTop)
                                        viewportTop = Integer.parseInt(item.getWeight());
                                    if (Integer.parseInt(item.getWaistline()) > viewportTopWaistline)
                                        viewportTopWaistline = Integer.parseInt(item.getWaistline());
                                }
                                dietGraphData(viewportTop, viewportTopWaistline);
                                tvLastWeight.setText(weightHistoryResp.getData().getWeightHistory().getLastBodyWeight() + "");
                                tvEightDayWeight.setText(weightHistoryResp.getData().getWeightHistory().getDays8BodyWeight() + "");
                                tvFifteenDayWeight.setText(weightHistoryResp.getData().getWeightHistory().getDays15BodyWeight() + "");
                                if (weightHistoryResp.getData().getWeightHistory().getNextWeightDays() > 0) {
                                    binding.nextWeightDaysMsg.setVisibility(View.VISIBLE);
                                    binding.weightSubmitButton.setEnabled(false);
                                    binding.weightSubmitButton.setClickable(false);
                                    binding.nextWeightDaysMsg.setText(String.format(getString(R.string.please_add_your_weight_after_days), weightHistoryResp.getData().getWeightHistory().getNextWeightDays()));
                                } else {
                                    binding.nextWeightDaysMsg.setVisibility(View.GONE);
                                    binding.weightSubmitButton.setEnabled(true);
                                    binding.weightSubmitButton.setClickable(true);
                                }
                            } else {
                                //  Toast.makeText(activity, newTractors.getMsg().get(0), Toast.LENGTH_LONG).show();
                                Toast.makeText(AddMyWeightActivity.this, weightHistoryResp.getMessage().get(0), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e(TAG, "onError: ", e);
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }

    private void dietGraphData(float viewportTop, float viewportTopWaistline) {
        //If we need to change values just create new list and pass to Line(yAxisValues)
        Line line = new Line(waitYAxis).setColor(Color.parseColor("#008577"));
        Line waistLineLine = new Line(waistLineYAxis).setColor(Color.parseColor("#008577"));
        List<Line> lines = new ArrayList<>();
        lines.add(line);
        LineChartData data = new LineChartData();
        data.setLines(lines);
        Axis axis = new Axis();
        axis.setValues(axisValues);
        axis.setTextSize(12);
        axis.setHasLines(true);
        axis.setMaxLabelChars(3);
        axis.setTextColor(getResources().getColor(R.color.graph_color));
        data.setAxisXBottom(axis);
        Axis yAxis = new Axis();
        yAxis.setTextColor(getResources().getColor(R.color.graph_color));
        yAxis.setTextSize(12);
        data.setAxisYLeft(yAxis);
        lineChartViewWeight.setLineChartData(data);
        Viewport viewport = new Viewport(lineChartViewWeight.getMaximumViewport());
        viewport.top = viewportTop;
        lineChartViewWeight.setMaximumViewport(viewport);
        lineChartViewWeight.setCurrentViewport(viewport);

        List<Line> linesWaist = new ArrayList<>();
        linesWaist.add(waistLineLine);
        LineChartData waistLineData = new LineChartData();
        waistLineData.setLines(linesWaist);
        waistLineData.setAxisXBottom(axis);
        Axis yAxisWaist = new Axis();
        yAxisWaist.setTextColor(getResources().getColor(R.color.graph_color));
        yAxisWaist.setTextSize(12);
        waistLineData.setAxisYLeft(yAxisWaist);
        lcWaistLine.setLineChartData(waistLineData);
        Viewport viewportWaist = new Viewport(lcWaistLine.getMaximumViewport());
        viewportWaist.top = viewportTopWaistline;
        lcWaistLine.setMaximumViewport(viewportWaist);
        lcWaistLine.setCurrentViewport(viewportWaist);
        binding.tvWaistLineGraph.setVisibility(View.VISIBLE);
        binding.cvWaistLineGraph.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    public void gotoDialog(String weight){

        final Dialog dialog = new Dialog(this);
        //  dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialogbox_status);
        //   dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));



        Button btn_submit = (Button) dialog.findViewById(R.id.btn_submit);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        TextView tvt_heading = (TextView) dialog.findViewById(R.id.tvt_heading);
        tvt_heading.setText("Update Weight");

        TextView tvt_message = (TextView) dialog.findViewById(R.id.tvt_message);

        String unit="";

        Log.d("bbbbb","bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"+weightUnit);
        if(weightUnit.equals("")){
            unit="KG";
        }else {
            unit=weightUnit;
        }

        tvt_message.setText("Your new weight is "+  weight+  " "+unit);
        btn_submit.setText("OK");

        btn_cancel.setText("Cancel");




        // btn_submit.setTag(et_comment.getText().toString());
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();


                //String status_comment_tag=v.getTag().toString();
                // Log.d("gggg",":vvvnhnnnnnnnnnnnnnnnnnnnnnn 1 nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"+status_comment_tag.length());

                if (isValidate()) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                    Date date = null;
                    try {
                        date = formatter.parse(binding.etDate.getText().toString());
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                    todayDate = formatter.format(date);
                    SupportClass.hideSoftKeyboard(AddMyWeightActivity.this);
                    if (SupportClass.checkConnection(AddMyWeightActivity.this)) {
                        addBodyWeight();
                    } else {
                        SupportClass.noInternetConnectionToast(AddMyWeightActivity.this);
                    }
                }



                dialog.dismiss();


            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();


                dialog.dismiss();



            }
        });

        dialog.show();
    }


}
