package com.livennew.latestdietqueen.my_plans.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.livennew.latestdietqueen.model.MyResponse;

public class WeightHistoryResp extends MyResponse {
    @SerializedName("data")
    @Expose
    private WeightHistoryData data;
//    @SerializedName("others")
//    @Expose
//    private Others others;

    public WeightHistoryData getData() {
        return data;
    }

    public void setData(WeightHistoryData data) {
        this.data = data;
    }
}
