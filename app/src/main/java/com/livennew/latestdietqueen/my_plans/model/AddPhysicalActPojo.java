package com.livennew.latestdietqueen.my_plans.model;

public class AddPhysicalActPojo {
    private String physicalActType;
    private String physicalActTime;

    public AddPhysicalActPojo() {
    }

    public AddPhysicalActPojo(String physicalActType, String physicalActTime) {
        this.physicalActType = physicalActType;
        this.physicalActTime = physicalActTime;
    }

    public String getPhysicalActType() {
        return physicalActType;
    }

    public void setPhysicalActType(String physicalActType) {
        this.physicalActType = physicalActType;
    }

    public String getPhysicalActTime() {
        return physicalActTime;
    }

    public void setPhysicalActTime(String physicalActTime) {
        this.physicalActTime = physicalActTime;
    }
}
