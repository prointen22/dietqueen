package com.livennew.latestdietqueen.my_plans;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.livennew.latestdietqueen.FabActivity;
import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.SessionManager;
import com.livennew.latestdietqueen.databinding.ActivityMyPhysicalBinding;
import com.livennew.latestdietqueen.model.User;
import com.livennew.latestdietqueen.network_support.Api;
import com.livennew.latestdietqueen.utils.SupportClass;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MyPhysicalActivity extends FabActivity {
    private Button btnStartTimer, btnStopTimer;
    private Toolbar toolbarPhysicalActivity;
    private TextView tvBackArrow;

    private static final String TAG = "Erros";
    private TextView timeViewTimer;
    private Dialog dialog;
    private int seconds = 0;
    private int counter = 59;
    private boolean isCounterStart = true;
    // Is the stopwatch running?
    private String startTime, stopTime, physicalType = "";
    private boolean running;
    private boolean wasRunning;
    private TextView tvWalkingTime, tvJoggingTime;
    private CardView cvWalking, cvJogging;
    @Inject
    SessionManager sessionManager;
    User user;
    private ActivityMyPhysicalBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMyPhysicalBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorAccent));
        user = sessionManager.getUser();
        init();
        setSupportActionBar(toolbarPhysicalActivity);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(R.string.my_physical_activity);
        }
        toolbarPhysicalActivity.setNavigationIcon(R.drawable.ic_navigate_before_white_24dp);

        if (SupportClass.checkConnection(this)) {
            getPhysicalActivity();
        } else {
            SupportClass.noInternetConnectionToast(this);
        }

        cvWalking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                physicalType = "walking";
                startTimer(savedInstanceState);
            }
        });
        cvJogging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                physicalType = "jogging";
                startTimer(savedInstanceState);
            }
        });

    }


    private void startTimer(Bundle savedInstanceState) {
        dialog = new Dialog(MyPhysicalActivity.this);
        dialog.setContentView(R.layout.add_physical_activity_popupbox);
        dialog.setCanceledOnTouchOutside(false);
        tvBackArrow = dialog.findViewById(R.id.toolbarTitle);
        btnStartTimer = dialog.findViewById(R.id.start_button);
        btnStopTimer = dialog.findViewById(R.id.stop_button);
        timeViewTimer = dialog.findViewById(R.id.time_view);
        if (physicalType.equals("walking")) {
            tvBackArrow.setText(getResources().getString(R.string.walking));
        } else {
            tvBackArrow.setText(getResources().getString(R.string.jogging));
        }
        if (savedInstanceState != null) {

            seconds
                    = savedInstanceState
                    .getInt("seconds");
            running
                    = savedInstanceState
                    .getBoolean("running");
            wasRunning
                    = savedInstanceState
                    .getBoolean("wasRunning");
        }
        runTimer();
        tvBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                dialog.dismiss();
                running = false;
                seconds = 0;
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
            }
        });
        dialog.show();
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    //   finish();
                    dialog.dismiss();
                    running = false;
                    seconds = 0;
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);
                }
                return true;
            }
        });

    }

    private void init() {
        toolbarPhysicalActivity = findViewById(R.id.toolbar_physical);
        // Get the text view.
        cvWalking = findViewById(R.id.cv_walking);
        cvJogging = findViewById(R.id.cv_jogging);
        tvWalkingTime = findViewById(R.id.tv_walk_time);
        tvJoggingTime = findViewById(R.id.tv_jogging_time);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSaveInstanceState(@NotNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState
                .putInt("seconds", seconds);
        savedInstanceState
                .putBoolean("running", running);
        savedInstanceState
                .putBoolean("wasRunning", wasRunning);
    }

    @Override
    public void onPause() {
        super.onPause();
        wasRunning = running;
        running = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (wasRunning) {
            running = true;
        }
    }

    public void onClickStart(View view) {
        running = true;
        btnStartTimer.setVisibility(View.GONE);
        startTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        if (isCounterStart) {
            new CountDownTimer(61000, 1000) {
                public void onTick(long millisUntilFinished) {
                    counter--;
                    String timer = String.valueOf(counter);
                }

                public void onFinish() {
                    btnStopTimer.setVisibility(View.VISIBLE);
                }
            }.start();
            //   isCounterStart = false;
        }
    }

    public void onClickStop(View view) {
        stopTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        if (startTime != null) {
            if (startTime.equalsIgnoreCase(stopTime)) {
                Toast.makeText(MyPhysicalActivity.this, "Minimum 1 minute workout required", Toast.LENGTH_SHORT).show();
            } else {
                if (SupportClass.checkConnection(MyPhysicalActivity.this)) {
                    running = false;
                    seconds = 0;
                    insertPhysicalActivity();
                    btnStartTimer.setVisibility(View.VISIBLE);
                    btnStopTimer.setVisibility(View.GONE);
                } else {
                    Toast.makeText(MyPhysicalActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                }
            }

        }

    }

    private void runTimer() {
        // Creates a new Handler
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                int hours = seconds / 3600;
                int minutes = (seconds % 3600) / 60;
                int secs = seconds % 60;

                String time = String.format(Locale.getDefault(), "%d:%02d:%02d", hours, minutes, secs);
                // Set the text view text.
                timeViewTimer.setText(time);

                if (running) {
                    seconds++;
                }

                handler.postDelayed(this, 1000);
            }
        });
    }

    private void insertPhysicalActivity() {
        try {
            //progressBar.setVisibility(View.VISIBLE);
            //rvFAQ.setVisibility(View.GONE);
            JSONObject params = new JSONObject();
            params.put("physical_type", physicalType);
            params.put("start_time", startTime);
            params.put("end_time", stopTime);
            JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST, Api.MAIN_URL + Api.INSERTPHYSICALDATA, params, response -> {
                Log.d(TAG, "onResponse Response: " + response);
                //  progressBar.setVisibility(View.GONE);
                // rvFAQ.setVisibility(View.VISIBLE);
                try {
                    JSONObject jObj = new JSONObject(response.toString());
                    boolean error = jObj.getBoolean("error");
                    if (!error) {

                    } else {
                        JSONArray jsonArray = jObj.getJSONArray("message");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Toast.makeText(MyPhysicalActivity.this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, error -> Log.e(TAG, "response Error: " + error.getMessage())) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", user.getToken());
                    headers.put("userId", String.valueOf(user.getId()));
                    return headers;
                }
            };
            strReq.setShouldCache(false);
            // Adding request to request queue
            RequestQueue requestQueue = Volley.newRequestQueue(MyPhysicalActivity.this);
            strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(strReq);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPhysicalActivity() {
        binding.progressJogging.setVisibility(View.VISIBLE);
        binding.progressWalking.setVisibility(View.VISIBLE);
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.GET, Api.MAIN_URL + Api.GETPHYSICALACTIVITY, null, response -> {
            Log.d(TAG, "onResponse Response: " + response);
            try {
                binding.progressJogging.setVisibility(View.GONE);
                binding.progressWalking.setVisibility(View.GONE);
                JSONObject jObj = new JSONObject(response.toString());
                boolean error = jObj.getBoolean("error");
                if (!error) {
                    JSONObject jsonObject = jObj.getJSONObject("data");
                    tvWalkingTime.setText(jsonObject.getString("walking"));
                    tvJoggingTime.setText(jsonObject.getString("jogging"));
                } else {
                    JSONArray jsonArray = jObj.getJSONArray("message");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Toast.makeText(this, jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, error -> {
            Log.e(TAG, "response Error: " + error.getMessage());
            binding.progressJogging.setVisibility(View.VISIBLE);
            binding.progressWalking.setVisibility(View.VISIBLE);
        }) {
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                headers.put("Authorization", user.getToken());
                headers.put("userId", String.valueOf(user.getId()));
                return headers;
            }
        };
        strReq.setShouldCache(false);
        // Adding request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(MyPhysicalActivity.this);
        strReq.setRetryPolicy(new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(strReq);
    }

}
