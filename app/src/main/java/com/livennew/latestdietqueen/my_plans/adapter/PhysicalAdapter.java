package com.livennew.latestdietqueen.my_plans.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.livennew.latestdietqueen.R;
import com.livennew.latestdietqueen.my_plans.model.AddPhysicalActPojo;

import java.util.ArrayList;

public class PhysicalAdapter extends RecyclerView.Adapter<PhysicalAdapter.ExerciseHolder> {
    private Context mContext;
    private ArrayList<AddPhysicalActPojo> alAddPhysAct;

    public PhysicalAdapter(Context mContext, ArrayList<AddPhysicalActPojo> alAddPhysAct) {
        this.mContext = mContext;
        this.alAddPhysAct = alAddPhysAct;
    }

    @NonNull
    @Override
    public ExerciseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.physical_exercise_content, parent, false);
        ExerciseHolder exerciseHolder = new ExerciseHolder(view);
        return exerciseHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExerciseHolder holder, int position) {
        final AddPhysicalActPojo addPhysicalActPojo = alAddPhysAct.get(position);
        holder.tvPhysType.setText(addPhysicalActPojo.getPhysicalActType());
        holder.tvPhysTime.setText(addPhysicalActPojo.getPhysicalActTime());
        holder.ibPhysActRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alAddPhysAct.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, alAddPhysAct.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return alAddPhysAct.size();
    }

    public class ExerciseHolder extends RecyclerView.ViewHolder {
        TextView tvPhysType, tvPhysTime;
        ImageButton ibPhysActRemove;

        private ExerciseHolder(View itemView) {
            super(itemView);
            tvPhysType = itemView.findViewById(R.id.tv_phy_act_nm);
            tvPhysTime = itemView.findViewById(R.id.tv_phy_act_time);
            ibPhysActRemove = itemView.findViewById(R.id.ib_phys_act_remove);
        }

    }
}

