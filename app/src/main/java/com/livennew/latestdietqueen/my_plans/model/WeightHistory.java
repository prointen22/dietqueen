package com.livennew.latestdietqueen.my_plans.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeightHistory {
    @SerializedName("last_body_weight")
    @Expose
    private String lastBodyWeight;
    @SerializedName("days_8_body_weight")
    @Expose
    private int days8BodyWeight;
    @SerializedName("days_15_body_weight")
    @Expose
    private int days15BodyWeight;

    @SerializedName("next_weight_days")
    @Expose
    private int nextWeightDays;

    public String getLastBodyWeight() {
        return lastBodyWeight;
    }

    public void setLastBodyWeight(String lastBodyWeight) {
        this.lastBodyWeight = lastBodyWeight;
    }

    public int getDays8BodyWeight() {
        return days8BodyWeight;
    }

    public void setDays8BodyWeight(int days8BodyWeight) {
        this.days8BodyWeight = days8BodyWeight;
    }

    public int getDays15BodyWeight() {
        return days15BodyWeight;
    }

    public void setDays15BodyWeight(int days15BodyWeight) {
        this.days15BodyWeight = days15BodyWeight;
    }

    public int getNextWeightDays() {
        return nextWeightDays;
    }

    public void setNextWeightDays(int nextWeightDays) {
        this.nextWeightDays = nextWeightDays;
    }

}
